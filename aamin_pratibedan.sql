-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2018 at 10:53 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ratnanagar_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `aamin_pratibedan`
--

CREATE TABLE `aamin_pratibedan` (
  `id` int(20) NOT NULL,
  `form_id` int(11) NOT NULL,
  `bato_category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_direction` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_front` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_back` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_left` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_right` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_purba` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_paschim` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_utar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_dakshin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_purba` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_paschim` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_utar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_dakshin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_width` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_field_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_width` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_field_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at_nepali` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `other` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hitension_wire_distance` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `electricity_volt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `river_side_bata_chodnu_parne_duri` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aamin_pratibedan`
--

INSERT INTO `aamin_pratibedan` (`id`, `form_id`, `bato_category`, `bato_length`, `bato_direction`, `bato_area`, `mohoda_front`, `mohoda_back`, `mohoda_left`, `mohoda_right`, `chadnu_parne_duri_purba`, `chadnu_parne_duri_paschim`, `chadnu_parne_duri_utar`, `chadnu_parne_duri_dakshin`, `chadnu_parne_setback_purba`, `chadnu_parne_setback_paschim`, `chadnu_parne_setback_utar`, `chadnu_parne_setback_dakshin`, `napi_length`, `napi_width`, `napi_field_area`, `siteko_napi_length`, `siteko_napi_width`, `siteko_napi_field_area`, `created_at_nepali`, `other`, `hitension_wire_distance`, `electricity_volt`, `river_side_bata_chodnu_parne_duri`, `created_by`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, 1, 'ख', '१२', '३२', '२३', '२३', '२३४', '२३४', '१२', '२१', '१२', '२३४', '१२', '१२', '१२', '१२१०', '१२', '१२१', '१२', '१', '३४', '३४', '२३४', 'बिही, असोज २५, २०७५', NULL, '४३', '१२', NULL, 4, '2018-10-11 08:53:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aamin_pratibedan`
--
ALTER TABLE `aamin_pratibedan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aamin_pratibedan`
--
ALTER TABLE `aamin_pratibedan`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
