<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectLog extends Model
{
    protected $table = 'reject_log';
    public $timestamps = false;
}
