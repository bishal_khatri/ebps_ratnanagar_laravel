<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SandirSuchana extends Model
{
    protected $table = 'sandhirsuchana';

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
