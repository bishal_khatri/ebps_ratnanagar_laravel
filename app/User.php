<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','contact','address','email', 'password','is_super','consultancy_name','consultancy_address','consultancy_license','consultancy_license_number','staff_id','extension_number','avatar',
    ];
    protected $dates = [
        'last_login_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany('App\Post','created_by');
    }

    public function hasAccess($permissions)
    {
        $userPermissions = $this->getUserPermission();
        if (in_array($permissions,$userPermissions)){
            return true;
        }
        else{
            return false;
        }
    }

    public function getUserPermission()
    {
        $perm = array();
        $permissions = \DB::table('auth_permission_user')->select('permission')->where('user_id',\Auth::user()->id)->get()->toArray();
        foreach ($permissions as $val){
            $perm[] = $val->permission;
        }
//        dd($perm);
        return $perm;
    }
}
