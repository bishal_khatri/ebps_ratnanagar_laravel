<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DpcScan extends Model
{
    protected $table = 'dpc_scan';
    public $timestamps = false;
    protected  $primaryKey = 'dpc_scan_id';

    public function form(){
        return $this->belongsTo('App\Form');
    }

}
