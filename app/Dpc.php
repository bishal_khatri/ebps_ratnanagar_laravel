<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dpc extends Model
{
    protected $table = 'dpc';
    protected $primaryKey = 'dpc_id';
    public $fillable = ['form_id'];

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
