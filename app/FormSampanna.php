<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormSampanna extends Model
{
    protected $table = 'form_sampanna';
    public $timestamps = false;
    protected $primaryKey = 'sampanna_id';

    public function form(){
        return $this->belongsTo('App\Form');
    }

}
