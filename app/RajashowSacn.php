<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RajashowSacn extends Model
{
    protected $table = 'rajashow_scan';
    protected $primaryKey = 'rajashow_scan_id';
    public $fillable = ['form_id'];

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
