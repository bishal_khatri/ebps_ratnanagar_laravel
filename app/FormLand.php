<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormLand extends Model
{
    protected $table = 'form_landinfo';
    public $timestamps = false;
    protected $primaryKey = 'landinfo_id';
    public $fillable = ['form_id'];

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
