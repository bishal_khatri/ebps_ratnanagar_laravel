<?php
//app/Helpers/Envato/User.php
namespace App\Helper;

use App\Form;
use App\RejectLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Tools
{

    public static function setResponse($type,$message,$data,$meta)
    {
        if ($type=='success') {
            $error = false;
        }
        elseif($type=='fail'){
            $error = true;
        }
        $responseData = [
            'error' => $error,
            'message'=>$message,
            'data' => $data,
            'meta' => $meta
        ];
        return $responseData;
    }

    /**
     * @param $form_level
     * @return string
     */
    public static function form_levelconverter($form_level){
        switch ($form_level){
            case 1:
                $level = 'Form has been submitted to Engineer for checking.';
                break;

            case 2:
                $level = 'Form has been checked by Engineer and 15 days notice is printed.';
                break;

            case 3:
                $level = 'Aamin Pratibedan has been filled.';
                break;

            case 4:
                $level = 'Aamin Pratibedan is cheked and Rajashow is uploaded by Sub-Engineer';
                break;

            case 5:
                $level = 'Tippani & first Ejajjat Patra is printed & uploaded';
                break;

            case 6:
                $level = 'Dpc Form is uploaded';
                break;

            case 7:
                $level = 'Dpc Form is checked.';
                break;

            case 8:
                $level = 'Tippani & Dpc is printed and uploaded';
                break;

            case 9:
                $level = 'Both dpc form and uploaded dpc are checked.';
                break;

            case 10:
                $level = 'Superstructure Certificate is being uploaded.';
                break;

            case 11:
                $level = 'Sampanna form is uploaded';
                break;

            case 12:
                $level = 'Sampanna form is being checked.';
                 break;

            case 13:
                $level = 'Sampanna form is again being verified by Engineer';
                break;

            case 14:
                $level = 'Sampanna form is again being verified by Main Engineer';
                break;

            case 15:
                $level = 'Upload Sampanna Certificate.';
                break;

            case 16:
                $level = 'EBPS process completed';
                break;
        }
        return $level;
    }

    public static function generate_dartanumber(){
        $darta_number = date('yjmh');

        return $darta_number;
    }

    public static function count_file(){
        $data['rejected'] = Form::where('is_rejected',1)->where('created_by',Auth::user()->id)->where('proceeded_to','consultancy')->count();
        $data['rejected_aamin'] = Form::where('is_rejected',1)->where('form_level',2)->where('proceeded_to','aamin')->count();
        $data['draft']= Form::where('is_draft',1)->where('created_by',Auth::user()->id)->count();
        return $data;
    }

    public static function rejectLog($form_id, $reason, $from_form_level, $to_form_level)
    {
        $log = new RejectLog();
        $log->form_id = $form_id;
        $log->rejected_by = Auth::user()->id;
        $log->reason = $reason;
        $log->from_form_level = $from_form_level;
        $log->to_form_level = $to_form_level;
        $log->created_at = date('Y-m-d H:i:s');
        $log->save();

        return 'success';
    }

}
