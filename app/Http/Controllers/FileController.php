<?php

namespace App\Http\Controllers;

use DB;
use App\Form;
use App\FormLand;
use App\SandirSuchana;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class FileController extends Controller
{
    public function get_file_status(Request $request)
    {
        if (isset($request->darta_number)){
            $data = Form::where('darta_number', $request->darta_number)->with('last_reject_log')->first();
            if (!is_null($data)){
                // send days left if 15 days notice has been published
                if ($data->form_level==2){
                    $sandhir = SandirSuchana::where('form_id',$data->id)->first();
                    if (!empty($sandhir)){
                        $created_at = new \DateTime($sandhir->created_at);
                        $current_date = new \DateTime();
                        $interval = $created_at->diff($current_date);
                        switch ($interval->days){
                            case 0:$data['days_left'] = 15;break;
                            case 1:$data['days_left'] = 14;break;
                            case 2:$data['days_left'] = 13;break;
                            case 3:$data['days_left'] = 12;break;
                            case 4:$data['days_left'] = 11;break;
                            case 5:$data['days_left'] = 10;break;
                            case 6:$data['days_left'] = 9;break;
                            case 7:$data['days_left'] = 8;break;
                            case 8:$data['days_left'] = 7;break;
                            case 9:$data['days_left'] = 6;break;
                            case 10:$data['days_left'] = 5;break;
                            case 11:$data['days_left'] = 4;break;
                            case 12:$data['days_left'] = 3;break;
                            case 13:$data['days_left'] = 2;break;
                            case 14:$data['days_left'] = 1;break;
                            case 15:$data['days_left'] = 0;break;
                            default:$data['days_left'] = 0;
                        }
                    }
                }
            }
        }else{
            $data = false;
        }
        return view('file.file_status')->with('data',$data);
    }

    public function getCount(){
        $files = FormLand::groupBy('current_ward_number')
                        ->select('current_ward_number', DB::raw('count(*) as total'))
                        ->orderBy('current_ward_number', 'ASC')->get();


        $newFiles = new Collection();
        foreach ($files as $value) {
            $newFiles->push([
                'total'=> $value->total,
                'current_ward_number1' => $value->current_ward_number,
                'current_ward_number' => self::change_ward($value->current_ward_number)
            ]);
        }
        $data['total_files'] = 0;
        foreach($files as $total)
            $data['total_files'] += $total->total;
//        dd($data['total_files']);

        $data['files'] = $newFiles->sortBy('current_ward_number');

        return view('file.files_count', $data);
    }

    public function showFilesByWard($id){

        $ward_files = FormLand::where('current_ward_number', $id)->with('form')->get();
        return view('file.files_by_ward')->with('ward_files', $ward_files);
    }

    private function change_ward($ward)
    {
        switch ($ward){
            case '१': $w = '०१';break;
            case '२': $w = '०२';break;
            case '३': $w = '०३';break;
            case '४': $w = '०४';break;
            case '५': $w = '०५';break;
            case '६': $w = '०६';break;
            case '७': $w = '०७';break;
            case '८': $w = '०८';break;
            case '९': $w = '०९';break;
            case '१०': $w = '१०';break;
            case '११': $w = '११';break;
            case '१२': $w = '१२';break;
            case '१३': $w = '१३';break;
            case '१४': $w = '१४';break;
            case '१५': $w = '१५';break;
            case '१६': $w = '१६';break;

            default: $w= 'null';
        }
        return $w;
    }

}
