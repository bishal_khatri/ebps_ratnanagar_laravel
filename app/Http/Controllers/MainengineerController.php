<?php

namespace App\Http\Controllers;

use App\DpcScan;
use App\Form;
use App\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainengineerController extends Controller
{
    public function index()
    {
        $data['ward'] = Ward::all();
        $level = [1,2,9,14];
        if (isset($_GET['ward_number'])){
            $data['ward_number'] = $_GET['ward_number'];
            // $data['files'] = Form::get_files(['fl.current_ward_number'=>$data['ward_number'],'proceeded_to'=>'main-engineer','form_level'=>9,'is_rejected'=>0,'is_draft'=>0]);

            $data['files'] = Form::whereIn('form_level',$level)
                ->where('proceeded_to','main-engineer')
                ->whereHas('landInfo', function($query){ $query->where('current_ward_number',$_GET['ward_number']);})
                ->with('landInfo')
                ->orderBy('id','desc')
                ->get();
        }
        return view('main_engineer.index',$data);
    }

    public function get_dpc($form_id)
    {
        $data['formData'] = Form::where('id', $form_id)->where('is_draft',0)->where('is_rejected',0)->where('proceeded_to', 'main-engineer')->where('form_level' , 9)->with('dpc')->first();
        if(!empty($data['formData'])){
            return view('main_engineer.view_dpc',$data);
        }
        else {
            abort(404);
        }
    }

    public function get_dpc_scan($form_id)
    {
        $data['dpcScan'] = DpcScan::where('form_id', $form_id)->first();
        if(!empty($data['dpcScan'])){
            $data['form_id'] = $form_id;
            return view('main_engineer.view_dpc_scan',$data);
        }
        else{
            abort(404);
        }
    }

    public function proceed($form_id)
    {
        $form = Form::find($form_id);

        if(!empty($form)){
            $form->proceeded_to = 'sub-engineer';
            $form->is_draft = 0;
            $form->is_rejected = 0;
            $form->form_level = 10;
            $form->updated_at = date('Y-m-d H:i:s' , time());
            $form->updated_by = Auth::user()->id;
            $form->rejected_at = null;
            $form->rejected_message = null;
            $form->save();

            \Session::flash('success','File Proceeded Successfully ');
            return redirect()->route('mainengineer.proceeddpc',$form_id);
        }
        else{
            abort(404);
        }
    }

    public function get_proceeddpc($form_id){

        $data['formData'] = Form::where('id', $form_id)->where('is_draft',0)->where('is_rejected',0)->where('proceeded_to', 'sub-engineer')->where('form_level' , 10)->with('dpc')->first();
        if(!empty($data['formData'])){
            return view('main_engineer.view_dpc',$data);
        }
        else {
            abort(404);
        }
    }

    public function get_sampanna($form_id){
        $data['formData'] = Form::where('id',$form_id)->with('landInfo', 'sampanna')->first();

        if(!empty($data['formData'])){
            return view('main_engineer.view_sampanna',$data);
        }
        else{
            abort(404);
        }
    }

    public function proceed_sampanna($form_id)
    {
        $form = Form::find($form_id);
        if(!empty($form)){
            $form->proceeded_to = 'sub-engineer';
            $form->form_level = 15;
            $form->updated_at = date('Y-m-d H:i:s', time());
            $form->updated_by = Auth::user()->id;
            $form->save();

            $data['action'] = 'sub-engineer';
            return view('main_engineer.show_message',$data);
        }
        else{
            abort(404);
        }
    }
}
