<?php

namespace App\Http\Controllers;

use App\AuthPermission;
use App\AuthPermissionUser;
use App\User;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index(){
        $data['permissions'] = AuthPermission::orderBy('code_name')->get();
        return view('permission.index',$data);
    }

    public function create(Request $request)
    {
        $prem = new AuthPermission();
        $prem->display_name = $request->display_name;
        $prem->code_name = $request->code_name;
        $prem->save();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Permission successfully saved.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $prem = AuthPermission::find($request->id);
        $prem->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Permission deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
    }

    public function permission_view(Request $request)
    {
        $permissions = \DB::table('auth_permission_user as pu')->select('p.display_name')
            ->join('users as u','u.id','pu.user_id')->join('auth_permissions as p','p.id','pu.permission_id')->where('u.id',$request->id)->get();
        $data = array();
            foreach ($permissions as $val){
                $data[] = "<li class='list-group-item'>".$val->display_name."</li>";
            }
        if (!empty($data)){
            $return = $data;
        }else{
            $return = "<li class='list-group-item'>No permission has been assigned.</li>";
        }


        return $return;
    }

    public function assign_permission($id)
    {
        $data['user'] = User::find($id);
        if(!empty($data['user'])){
            $active_user_permission = AuthPermissionUser::where('user_id',$data['user']->id)->get();
            $u_prem = array();
            foreach ($active_user_permission as $val){
                $u_prem[] = $val->permission;
            }
//        dd($active_user_permission);
            $data['active_user_permission'] = $u_prem;
            $data['permissions'] = AuthPermission::all();
            return view('auth.assign_permission',$data);
        }
        else{
            abort(404);
        }

    }

    public function assign_permission_store(Request $request)
    {

        if (!empty($request->selected_permission)) {
                $permission = AuthPermission::find($request->selected_permission);
//                dd($permission);

                $pu = AuthPermissionUser::where('user_id',$request->user_id)->first();
                $pu->permission_id = $permission->id;
                $pu->permission = $permission->code_name;
                $pu->save();
        }
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Permission assigned successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

            return redirect()->route('user.index');

    }
}
