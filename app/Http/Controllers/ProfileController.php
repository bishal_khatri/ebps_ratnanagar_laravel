<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use \Validator;
use Session;
class ProfileController extends Controller
{
    public function get_profile($status = null){


        if(isset($status)){
            $data['profile'] = '';
            $data['settings'] = 'active';
        }
        else
        {
            $data['profile'] = 'active';
            $data['settings'] = '';
        }

        $id = Auth::user()->id;
        $data['formData'] = User::find($id);
        return view('profile.profile',$data);
    }

    public function post_profile(Request $request){

        $form = User::find(Auth::user()->id);
        $status= 'settings';

        if($request->action == 1){

            $this->validate($request,[
                'first_name' => 'required',
                'last_name' => 'required',
                'address' => 'required',
                'contact' => 'required'
            ]);
//            dd($form);

            //if there is already image.
            if($request->hasFile('change_avatar')){

                //deleting old image from storage.
                Storage::delete('public/'.Auth::user()->avatar);
                $img_path = $request->file('change_avatar')->store('upload_image/'.Auth::user()->id, 'public');
            }
            else {
                $img_path = Auth::user()->avatar;
            }

            $form->first_name = $request->first_name;
            $form->last_name = $request->last_name;
            $form->address = $request->address;
            $form->contact = $request->contact;
            $form->avatar = $img_path;
            $form->updated_at = date('Y-m-d h:i:s' , time());
            $form->save();
            Session::flash('success','Profile Updated  Successfully !');


        }
        else {

            $validator = Validator::make($request->all(), [
                'password' => 'required|min:6|confirmed',
                'old_password' => 'required',
            ]);


            if ($validator->fails()) {
                return redirect()->route('profile',$status)
                    ->withErrors($validator)
                    ->withInput();
            }

                $form = User::find(Auth::user()->id);

                $hashedPassword = $form->password;

                if (Hash::check($request->old_password, $hashedPassword)) {

                    $password = Hash::make($request->password);
                    $form->password = $password;
                    $form->save();
                    Session::flash('success','Password Updated Successfully !');


                } else {
                    return redirect()->route('profile',$status)->withErrors(['old_password'=>'Old Password did not match.']);
                }

        }


        return redirect()->route('profile',$status);
    }
}
