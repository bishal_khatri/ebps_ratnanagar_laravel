<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Http\Request;

class AdministrationController extends Controller
{
    public function get_files()
    {
        $data['files'] = Form::where('is_draft',0)->with('landInfo')->orderBy('id','desc')->get();
//        dd($data);
        return view('administrator.index',$data);
    }

    public function get_edit($id)
    {
        $data['form_id'] = $id;
        $data['files'] = Form::where('id',$id)->with('landInfo')->orderBy('id','desc')->first();
        $data['token'] = 'administration';
        return view('administrator.editmenu',$data);
    }
}
