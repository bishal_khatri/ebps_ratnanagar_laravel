<?php

namespace App\Http\Controllers;

use App\Dpc;
use App\DpcScan;
use App\FormBuildingFile;
use App\Ward;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Form;
use App\FormLand;
use App\FormBuildingInfo;
use App\FormFloorInfo;
use App\FormSampanna;
use App\SampannaScan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Helper\Tools;
use Session;


class ConsultancyController extends Controller
{
    public function index()
    {
        $data['formData'] = Form::where('created_by', Auth::user()->id)->where('is_draft',0)->where('is_rejected',0)->with('landInfo','floorInfo')->orderBy('id','Desc')->get();
        return view('consultancy.index',$data);

    }

    public function get_draft()
    {
        $data['formData'] = Form::where('created_by', Auth::user()->id)->where('is_draft',1)->where('is_rejected',0)->with('landInfo','floorInfo')->orderBy('id','desc')->get();
        return view('consultancy.draft',$data);

    }

    public function get_edit_form($form_id)
    {

        $data['formData'] = Form::where('id', $form_id)->with('landInfo','floorInfo','buildingInfo','buildingFile')->first();
        if(empty($data['formData'])) {
            abort(404);
        }
        else{
            $data['ward_number'] = Ward::all();
            if(isset($_GET['token'])){
                $data['action'] = 'administration';
            } else{
                $data['action'] = 'consultancy';
            }
            return view('consultancy.edit_form',$data);
        }

    }

    public function get_full_form($form_id)
    {

//        $data['formData'] = Form::select_file('id',$form_id);
        $data['formData'] = Form::where('id', $form_id)->where('created_by',Auth::user()->id)->with('landInfo','floorInfo','buildingInfo','buildingFile')->first();

        if(empty($data['formData'])){
            abort(404);

        } else {
            return view('consultancy.view_form',$data);
        }

    }

    public function get_rejected_file()
    {
        $data['formData'] = Form::where('created_by', Auth::user()->id)->where('is_draft',0)->where('is_rejected',1)->where('proceeded_to','consultancy')->with('landInfo','floorInfo')->orderBy('id','desc')->get();
        return view('consultancy.list_rejected',$data);

    }

    //to edit the entire form at once.
    public function post_edit_form(Request $request){
        //validating personal_info data
        $this->validate($request, [
            'sambandha_ma' => 'required',
            'building_type' => 'required',
            'option_1'=> 'required' ,
            'option_2'=> 'required' ,
            'sambodhan'=> 'required' ,
            'field_owner_name' => 'required',
            'field_owner_address'=> 'required' ,
            'field_owner_age'=> 'required' ,
            'field_owner_job' => 'required',
            'father_husband_option'=> 'required' ,
            'father_husband_name' => 'required' ,
            'grandfather_option' => 'required' ,
            'grandfather_name' => 'required' ,

        ]);

        //validation for landinfo file
        $this->validate($request, [
            'kitta_number' => 'required',
            'sabik_kitta_number'=> 'required' ,
            'sabik_ward_number'=> 'required' ,
            'field_area'=> 'required' ,
            'village_name'=> 'required' ,
            'jagga_mohada'=> 'required' ,
            'ward_number' => 'required',
            'current_ward_number'=> 'required' ,
            'sabik_kittama_ghar' => 'required' ,
            'haak_pugeko' => 'required' ,
            'electricity_line' => 'required' ,
            'river' => 'required'
        ]);

        //validating building info files
        $this->validate($request , [
            'building_category' => 'required',
            'building_structure'=> 'required' ,
            'plith_height'=> 'required' ,
            'foundation_size'=> 'required' ,
            'pillar_size'=> 'required' ,
            'pillar_rod_size'=> 'required' ,
            'pillar_rod_number' => 'required',
            'pillar_churi_size'=> 'required' ,
            'left_right_chadne_duri' => 'required' ,
            'setback' => 'required' ,
            'east_border' => 'required',
            'west_border'=> 'required',
            'north_border'=> 'required',
            'south_border'=> 'required',
            'building_length'=> 'required',
            'building_breadth'=> 'required',
            'building_height'=> 'required',
            'number_of_room' => 'required',
            'number_of_door'=> 'required',
            'number_of_window'=> 'required',
            'number_of_bathroom'=> 'required',
            'septic_tank'=> 'required',
            'number_of_channel_gate'=> 'required',
            'number_of_satar'=> 'required',
            'roof'=> 'required',
            'jodai'=> 'required',
            'building_purpose' => 'required',
            'house_owner_name' => 'required',
            'citizenship_number' => 'required',
            'district' => 'required',
            'phone' => 'required',
            'ground_area_coverage' => 'required',
            'floor_area_ratio' => 'required'
        ]);

        //validating floor info
        $this->validate($request , [
            'form_id' => 'required',
            'no_of_floor' => 'required' ,
            'underground_floor_area' => 'required' ,
            'ground_floor_area' => 'required' ,
        ]);

        //updating person_info data
        if($request->building_type == 'नयाँ घर निर्माण')
        {
            $building_type_slug = 'new_building';
        }
        else {
            $building_type_slug = 'add_floor';
        }

        $form = Form::find($request->form_id);
        $form->sambandha_ma = $request->sambandha_ma;
        $form->option_1 = $request->option_1;
        $form->option_2 = $request->option_2;

        $form->building_type = $request->building_type;
        $form->building_type_slug = $building_type_slug;

        $form->sambodhan = $request->sambodhan;
        $form->field_owner_name = $request->field_owner_name;
        $form->field_owner_address = $request->field_owner_address;
        $form->field_owner_age = $request->field_owner_age;
        $form->field_owner_job = $request->field_owner_job;

        $form->father_husband_option = $request->father_husband_option;
        $form->father_husband_name = $request->father_husband_name;

        $form->grandfather_option = $request->grandfather_option;
        $form->grandfather_name = $request->grandfather_name;

        $form->save();



        //updating land_info table

        $form = FormLand::where('form_id',$request->form_id)->first();
        $form->form_id = $request->form_id;
        $form->ward_number = $request->ward_number;
        $form->sabik_ward_number = $request->sabik_ward_number;
        $form->current_ward_number = $request->current_ward_number;
        $form->kitta_number = $request->kitta_number;
        $form->sabik_kitta_number = $request->sabik_kitta_number;
        $form->field_area = $request->field_area;
        $form->village_name = $request->village_name;
        $form->sabik_kittama_ghar = $request->sabik_kittama_ghar;
        $form->haak_pugeko = $request->haak_pugeko;
        $form->jagga_mohada = $request->jagga_mohada;
        $form->electricity_line = $request->electricity_line;
        $form->river = $request->river;

        $form->save();

        // updating building

        $form = FormBuildingInfo::where('form_id',$request->form_id)->first();
        $form->form_id = $request->form_id;
        $form->building_category = $request->building_category;
        $form->building_structure = $request->building_structure;
        $form->plith_height = $request->plith_height;
        $form->foundation_size = $request->foundation_size;
        $form->pillar_size= $request->pillar_size;

        $form->pillar_rod_size = $request->pillar_rod_size;
        $form->pillar_rod_number = $request->pillar_rod_number;
        $form->pillar_churi_size = $request->pillar_churi_size;
        $form->left_right_chadne_duri = $request->left_right_chadne_duri;
        $form->setback = $request->setback;

        $form->east_border = $request->east_border;
        $form->west_border = $request->west_border;
        $form->north_border = $request->north_border;
        $form->south_border = $request->south_border;
        $form->building_length = $request->building_length;

        $form->building_breadth = $request->building_breadth;
        $form->building_height = $request->building_height;
        $form->number_of_room = $request->number_of_room;
        $form->number_of_door = $request->number_of_door;
        $form->number_of_window = $request->number_of_window;

        $form->number_of_bathroom = $request->number_of_bathroom;
        $form->septic_tank = $request->septic_tank;
        $form->number_of_channel_gate = $request->number_of_channel_gate;
        $form->number_of_satar = $request->number_of_satar;
        $form->roof = $request->roof;

        $form->jodai = $request->jodai;
        $form->building_purpose = $request->building_purpose;
        $form->house_owner_name = $request->house_owner_name;
        $form->citizenship_number = $request->citizenship_number;
        $form->district = $request->district;

        $form->phone = $request->phone;
        $form->ground_area_coverage = $request->ground_area_coverage;
        $form->floor_area_ratio = $request->floor_area_ratio;
        $form->save();



        //updating floor_info files

        $form = FormFloorInfo::where('form_id',$request->form_id)->first();
        $form->no_of_floor = $request->no_of_floor;
        $form->form_id = $request->form_id;
        $form->underground_floor_area = $request->underground_floor_area;
        $form->ground_floor_area = $request->ground_floor_area;


        if($request->no_of_floor == 1){
            $form->first_floor_area  = null;
            $form->second_floor_area = null;
            $form->third_floor_area  = null;
            $form->fourth_floor_area = null;
        }
        elseif($request->no_of_floor == 2){
            $form->first_floor_area = $request->first_floor_area;
            $form->second_floor_area = null;
            $form->third_floor_area  = null;
            $form->fourth_floor_area = null;
        }
        elseif ($request->no_of_floor == 3){
            $form->first_floor_area = $request->first_floor_area;
            $form->second_floor_area = $request->second_floor_area;
            $form->third_floor_area  = null;
            $form->fourth_floor_area = null;
        }
        elseif ($request->no_of_floor == 4){
            $form->first_floor_area = $request->first_floor_area;
            $form->second_floor_area = $request->second_floor_area;
            $form->third_floor_area  = $request->third_floor_area;
            $form->fourth_floor_area = null;
        }
        elseif ($request->no_of_floor == 5){
            $form->first_floor_area = $request->first_floor_area;
            $form->second_floor_area = $request->second_floor_area;
            $form->third_floor_area  = $request->third_floor_area;
            $form->fourth_floor_area = $request->fourth_floor_area;
        }

        $form->save();

        //updating buliding files

        $form = FormBuildingFile::where('form_id',$request->form_id)->first();
//        dd($form);
        if (isset($request->lalpurja)){
            Storage::delete('public/'.$form->lalpurja);
            $lalpurja_path = $request->file('lalpurja')->store('files/'.$request->form_id.'/building_file/lalpurja', 'public');
        }

        if (isset($request->citizenship)){
            Storage::delete('public/'.$form->citizenship);
            $citizenship_path = $request->file('citizenship')->store('files/'.$request->form_id.'/building_file/citizenship', 'public');
        }

        if (isset($request->blueprint)){
            Storage::delete('public/'.$form->blueprint);
            $blueprint_path = $request->file('blueprint')->store('files/'.$request->form_id.'/building_file/blueprint', 'public');
        }

        if (isset($request->house_map)){
            Storage::delete('public/'.$form->house_map);
            $house_map_path = $request->file('house_map')->store('files/'.$request->form_id.'/building_file/house_map', 'public');
        }

        if (isset($request->tero_rashid)){
            Storage::delete('public/'.$form->tero_rashid);
            $tero_rashid_path = $request->file('tero_rashid')->store('files/'.$request->form_id.'/building_file/tero_rashid', 'public');
        }

        if (isset($request->naamsari)){
            Storage::delete('public/'.$form->naamsari);
            $naamsari_path = $request->file('naamsari')->store('files/'.$request->form_id.'/building_file/naamsari', 'public');
        }

        if (isset($request->ward_sifaris)){
            Storage::delete('public/'.$form->ward_sifaris);
            $ward_sifaris_path = $request->file('ward_sifaris')->store('files/'.$request->form_id.'/building_file/ward_sifaris', 'public');
        }

        if (isset($request->other_document_1)){
            Storage::delete('public/'.$form->other_document_1);
            $other_document_1 = $request->file('other_document_1')->store('files/'.$request->form_id.'/building_file/other_document_1', 'public');
        }

        if (isset($request->other_document_2)){
            Storage::delete('public/'.$form->other_document_2);
            $other_document_2 = $request->file('other_document_2')->store('files/'.$request->form_id.'/building_file/other_document_2', 'public');
        }

        if (isset($request->other_document_3)){
            Storage::delete('public/'.$form->other_document_3);
            $other_document_3 = $request->file('other_document_3')->store('files/'.$request->form_id.'/building_file/other_document_3', 'public');
        }


        $formBuildingFiles = FormBuildingFile::where('form_id',$request->form_id)->first();
        $formBuildingFiles->form_id = $request->form_id;

        if(isset($lalpurja_path)){
            $formBuildingFiles->lalpurja = $lalpurja_path;
        }

        if(isset($citizenship_path)){
            $formBuildingFiles->citizenship = $citizenship_path;
        }

        if(isset($blueprint_path)){
            $formBuildingFiles->blueprint = $blueprint_path;
        }

        if(isset($house_map_path)){
            $formBuildingFiles->house_map = $house_map_path;
        }

        if(isset($tero_rashid_path)){
            $formBuildingFiles->tero_rashid = $tero_rashid_path;
        }

        if(isset($naamsari_path)){
            $formBuildingFiles->naamsari = $naamsari_path;
        }

        if(isset($ward_sifaris_path)){
            $formBuildingFiles->ward_sifaris = $ward_sifaris_path;
        }

        if(isset($other_document_1)){
            $formBuildingFiles->other_document_1 = $other_document_1;
        }

        if(isset($other_document_2)){
            $formBuildingFiles->other_document_2 = $other_document_2;
        }

        if(isset($other_document_3)){
            $formBuildingFiles->other_document_3 = $other_document_3;
        }

        if(isset($lalpurja_path)){
            $formBuildingFiles->lalpurja = $lalpurja_path;
        }

        $formBuildingFiles->other_document_1_description = $request->other_document_1_description;
        $formBuildingFiles->other_document_2_description = $request->other_document_2_description;
        $formBuildingFiles->other_document_3_description = $request->other_document_3_description;
        $formBuildingFiles->save();


        if($request->action == 'consultancy'){

            //updating form level and rejected message.
            $form = Form::find($request->form_id);
            $form->proceeded_to = 'engineer';
            $form->is_rejected = 0;
            $form->rejected_message = null;
            $form->rejected_at = null;
            $form->updated_by = Auth::user()->id;
            $form->form_level = 1;
            $form->save();
            \Session::flash('success','File Updated Successfully');
            return redirect()->route('consultancy.view_full_form',$form->id);
        }

        else  {
            \Session::flash('success','File Updated Successfully');
            return redirect()->route('administration.get_edit',$request->form_id);

        }



    }

    /**
     * FORM PERSONAL INFO
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_form_personalinfo($id = null)
    {
        if(isset($id)){
            $data['formData'] = Form::find($id);
            return view('consultancy.form_personalinfo',$data);
        } else{
            return view('consultancy.form_personalinfo');
        }
    }


    public function post_form_personalinfo(Request $request)
    {
        $page_num = $request->page;
        if($page_num == 1){
            $this->validate($request, [
                'page' => 'required',
                'sambandha_ma' => 'required',
                'building_type' => 'required',
                'option_1'=> 'required' ,
                'option_2'=> 'required' ,
                'sambodhan'=> 'required' ,
                'field_owner_name' => 'required',
                'field_owner_address'=> 'required' ,
                'field_owner_age'=> 'required' ,
                'field_owner_job' => 'required',
                'father_husband_option'=> 'required' ,
                'father_husband_name' => 'required' ,
                'grandfather_option' => 'required' ,
                'grandfather_name' => 'required' ,
                'created_at_nepali' => 'required'
            ]);

            if($request->building_type == 'नयाँ घर निर्माण')
            {
                $building_type_slug = 'new_building';
            }
            else {
                $building_type_slug = 'add_floor';
            }

            if(!empty($request->form_id)){
                $form = Form::find($request->form_id);
            }
            else{
                $form = new Form;
            }
            $form->sambandha_ma = $request->sambandha_ma;
            $form->option_1 = $request->option_1;
            $form->option_2 = $request->option_2;

            $form->building_type = $request->building_type;
            $form->building_type_slug = $building_type_slug;

            $form->sambodhan = $request->sambodhan;
            $form->field_owner_name = $request->field_owner_name;
            $form->field_owner_address = $request->field_owner_address;
            $form->field_owner_age = $request->field_owner_age;
            $form->field_owner_job = $request->field_owner_job;

            $form->father_husband_option = $request->father_husband_option;
            $form->father_husband_name = $request->father_husband_name;

            $form->grandfather_option = $request->grandfather_option;
            $form->grandfather_name = $request->grandfather_name;
            $form->created_at_nepali = $request->created_at_nepali;

            //change/reset form status
            $form->form_level = 0;
            $form->proceeded_to = 'consultancy';
            $form->is_rejected = 0;
            $form->is_draft = 1;
            $form->is_completed = 0;
            $form->created_by = Auth::user()->id;

            $form->save();
            $form_id = $form->id;

            return redirect()->route('consultancy.add_form_landinfo',$form_id);
        }
        else {
            return redirect()->route('add_form_personalinfo');
        }
    }

    /**
     * FORM LAND INFO
     * @param $form_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_form_landinfo($form_id){
        $data['formData'] = Form::where('id',$form_id)->with('landinfo')->first();
        if(empty($data['formData'])){
            abort(404);
        }
        else{
            $data['ward_number'] = Ward::all();

            if ($data['formData']->form_level !=0){
                return redirect()->route('consultancy.view_full_form',$form_id);
            }
            else{

                return view('consultancy.form_landinfo',$data);
            }
        }

    }

    public function post_form_landinfo(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'form_id' => 'required',
            'page' => 'required',
            'kitta_number' => 'required',
            'sabik_kitta_number'=> 'required' ,
            'sabik_ward_number'=> 'required' ,
            'field_area'=> 'required' ,
            'village_name'=> 'required' ,
            'jagga_mohada'=> 'required' ,
            'ward_number' => 'required',
            'current_ward_number'=> 'required' ,
            'sabik_kittama_ghar' => 'required' ,
            'haak_pugeko' => 'required' ,
            'electricity_line' => 'required' ,
            'river' => 'required'
        ]);

        $form_id =$request->form_id;

        if($request->page == 2 ){
            $form = FormLand::firstOrNew(['form_id' => $request->form_id]);
            $form->form_id = $request->form_id;
            $form->ward_number = $request->ward_number;
            $form->sabik_ward_number = $request->sabik_ward_number;
            $form->current_ward_number = $request->current_ward_number;
            $form->kitta_number = $request->kitta_number;
            $form->sabik_kitta_number = $request->sabik_kitta_number;
            $form->field_area = $request->field_area;
            $form->village_name = $request->village_name;
            $form->sabik_kittama_ghar = $request->sabik_kittama_ghar;
            $form->haak_pugeko = $request->haak_pugeko;
            $form->jagga_mohada = $request->jagga_mohada;
            $form->electricity_line = $request->electricity_line;
            $form->river = $request->river;

            $form->save();

            return redirect()->route('consultancy.add_form_buildinginfo' , $form_id);
        }
        else {
            return redirect()->route('add_form_personalinfo');
        }

    }

    /**
     * FORM BUILDING INFO
     * @param $form_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_form_buildinginfo($form_id){

        $data['formData']= Form::where('id',$form_id)->with('buildinginfo')->first();
        if(empty($data['formData'])){
            abort(404);
        }
        else {

            if ($data['formData']->form_level !=0){
                return redirect()->route('consultancy.view_full_form',$form_id);
            }
            else {

                return view('consultancy.form_buildinginfo', $data);
            }
        }

    }
    public function post_form_buildinginfo(Request $request){
//         dd($request->all());

        $this->validate($request , [
            'form_id' => 'required',
            'page' => 'required',
            'building_category' => 'required',
            'building_structure'=> 'required' ,
            'plith_height'=> 'required' ,
            'foundation_size'=> 'required' ,
            'pillar_size'=> 'required' ,
            'pillar_rod_size'=> 'required' ,
            'pillar_rod_number' => 'required',
            'pillar_churi_size'=> 'required' ,
            'left_right_chadne_duri' => 'required' ,
            'setback' => 'required' ,
            'east_border' => 'required',
            'west_border'=> 'required',
            'north_border'=> 'required',
            'south_border'=> 'required',
            'building_length'=> 'required',
            'building_breadth'=> 'required',
            'building_height'=> 'required',
            'number_of_room' => 'required',
            'number_of_door'=> 'required',
            'number_of_window'=> 'required',
            'number_of_bathroom'=> 'required',
            'septic_tank'=> 'required',
            'number_of_channel_gate'=> 'required',
            'number_of_satar'=> 'required',
            'roof'=> 'required',
            'jodai'=> 'required',
            'building_purpose' => 'required',
            'house_owner_name' => 'required',
            'citizenship_number' => 'required',
            'district' => 'required',
            'phone' => 'required',
            'ground_area_coverage' => 'required',
            'floor_area_ratio' => 'required'
        ]);

        if($request->page == 3){
            $form = FormBuildingInfo::firstOrNew(['form_id' => $request->form_id]);

            $form->form_id = $request->form_id;
            $form->building_category = $request->building_category;
            $form->building_structure = $request->building_structure;
            $form->plith_height = $request->plith_height;
            $form->foundation_size = $request->foundation_size;
            $form->pillar_size= $request->pillar_size;

            $form->pillar_rod_size = $request->pillar_rod_size;
            $form->pillar_rod_number = $request->pillar_rod_number;
            $form->pillar_churi_size = $request->pillar_churi_size;
            $form->left_right_chadne_duri = $request->left_right_chadne_duri;
            $form->setback = $request->setback;

            $form->east_border = $request->east_border;
            $form->west_border = $request->west_border;
            $form->north_border = $request->north_border;
            $form->south_border = $request->south_border;
            $form->building_length = $request->building_length;

            $form->building_breadth = $request->building_breadth;
            $form->building_height = $request->building_height;
            $form->number_of_room = $request->number_of_room;
            $form->number_of_door = $request->number_of_door;
            $form->number_of_window = $request->number_of_window;

            $form->number_of_bathroom = $request->number_of_bathroom;
            $form->septic_tank = $request->septic_tank;
            $form->number_of_channel_gate = $request->number_of_channel_gate;
            $form->number_of_satar = $request->number_of_satar;
            $form->roof = $request->roof;

            $form->jodai = $request->jodai;
            $form->building_purpose = $request->building_purpose;
            $form->house_owner_name = $request->house_owner_name;
            $form->citizenship_number = $request->citizenship_number;
            $form->district = $request->district;

            $form->phone = $request->phone;
            $form->ground_area_coverage = $request->ground_area_coverage;
            $form->floor_area_ratio = $request->floor_area_ratio;
            $form->save();

            $form_id = $request->form_id;

            return redirect()->route('consultancy.add_form_floorinfo' , $form_id);
        }
        else {
            return redirect()->route('add_form_personalinfo');
        }
    }

    /**
     * FORM BUILDING FILES
     * @param $form_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_form_buildingfiles($form_id)
    {
        $data['formData'] = Form::where('id',$form_id)->with('landInfo')->first();
        if(empty($data['formData'])) {
            abort(404);
        }
        else {
            if ($data['formData']->form_level !=0){
                return redirect()->route('consultancy.view_full_form',$form_id);
            }
            else {
                return view('consultancy.form_buildingfiles', $data);
            }
        }

    }

    public function post_form_buildingfiles(Request $request)
    {
        $this->validate($request, [
            'lalpurja' => 'required|mimes:jpeg,bmp,png|max:6144',
            'citizenship' => 'required|mimes:jpeg,bmp,png|max:6144',
            'blueprint' => 'required|mimes:jpeg,bmp,png|max:16384',
            'house_map' => 'required|mimes:pdf|max:16384',
            'tero_rashid' => 'required|mimes:jpeg,bmp,png|max:6144',
            'naamsari' => 'mimes:jpeg,bmp,png|max:6144',

        ]);
        $form = Form::find($request->form_id);
        if (isset($request->lalpurja)){
            $lalpurja_path = $request->file('lalpurja')->store('files/'.$form->id.'/building_file/lalpurja', 'public');
        }else{
            $lalpurja_path = '';
        }

        if (isset($request->citizenship)){
            $citizenship_path = $request->file('citizenship')->store('files/'.$form->id.'/building_file/citizenship', 'public');
        }else{
            $citizenship_path = '';
        }

        if (isset($request->blueprint)){
            $blueprint_path = $request->file('blueprint')->store('files/'.$form->id.'/building_file/blueprint', 'public');
        }else{
            $blueprint_path = '';
        }

        if (isset($request->house_map)){
            $house_map_path = $request->file('house_map')->store('files/'.$form->id.'/building_file/house_map', 'public');
        }else{
            $house_map_path = '';
        }

        if (isset($request->tero_rashid)){
            $tero_rashid_path = $request->file('tero_rashid')->store('files/'.$form->id.'/building_file/tero_rashid', 'public');
        }else{
            $tero_rashid_path = '';
        }

        if (isset($request->naamsari)){
            $naamsari_path = $request->file('naamsari')->store('files/'.$form->id.'/building_file/naamsari', 'public');
        }else{
            $naamsari_path = '';
        }

        if (isset($request->ward_sifaris)){
            $ward_sifaris_path = $request->file('ward_sifaris')->store('files/'.$form->id.'/building_file/ward_sifaris', 'public');
        }else{
            $ward_sifaris_path = '';
        }

        if (isset($request->other_document_1)){
            $other_document_1 = $request->file('other_document_1')->store('files/'.$form->id.'/building_file/other_document_1', 'public');
        }else{
            $other_document_1 = '';
        }

        if (isset($request->other_document_2)){
            $other_document_2 = $request->file('other_document_2')->store('files/'.$form->id.'/building_file/other_document_2', 'public');
        }else{
            $other_document_2 = '';
        }

        if (isset($request->other_document_3)){
            $other_document_3 = $request->file('other_document_3')->store('files/'.$form->id.'/building_file/other_document_3', 'public');
        }else{
            $other_document_3 = '';
        }


        $formBuildingFiles = FormBuildingFile::firstOrNew(['form_id'=>$form->id]);
        $formBuildingFiles->form_id = $form->id;
        $formBuildingFiles->lalpurja = $lalpurja_path;
        $formBuildingFiles->citizenship = $citizenship_path;
        $formBuildingFiles->blueprint = $blueprint_path;
        $formBuildingFiles->house_map = $house_map_path;
        $formBuildingFiles->tero_rashid = $tero_rashid_path;
        $formBuildingFiles->naamsari = $naamsari_path;
        $formBuildingFiles->ward_sifaris = $ward_sifaris_path;
        $formBuildingFiles->other_document_1 = $other_document_1;
        $formBuildingFiles->other_document_1_description = $request->other_document_1_description;
        $formBuildingFiles->other_document_2 = $other_document_2;
        $formBuildingFiles->other_document_2_description = $request->other_document_2_description;
        $formBuildingFiles->other_document_3 = $other_document_3;
        $formBuildingFiles->other_document_3_description = $request->other_document_3_description;
        $formBuildingFiles->save();

        $form->proceeded_to = 'engineer';
        $form->is_rejected = 0;
        $form->rejected_message = null;
        $form->rejected_at = null;
        $form->is_draft = 0;
        $form->form_level = 1;
        $form->darta_number = Tools::generate_dartanumber().$form->id;

        $form->save();

        \Session::flash('success','File Uploaded Successfully');
        return redirect()->route('consultancy.view_full_form',$form->id);
    }

    /**
     * FORM FLOOR INFO
     * @param $form_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_form_floorinfo($form_id){
        $data['formData'] = Form::where('id',$form_id)->with('floorinfo')->first();
        if(empty($data['formData'])) {
            abort(404);
        }
        else {
            if ($data['formData']->form_level !=0){
                return redirect()->route('consultancy.view_full_form',$form_id);
            }
            else {
                return view('consultancy.form_floorinfo', $data);
            }
        }


    }
    public function post_form_floorinfo(Request $request){
        // dd($request->all());
        $this->validate($request , [
            'form_id' => 'required',
            'page' => 'required' ,
            'no_of_floor' => 'required' ,
            'underground_floor_area' => 'required' ,
            'ground_floor_area' => 'required' ,
        ]);

        if($request->page == 4) {
            $form = FormFloorInfo::firstOrNew(['form_id' => $request->form_id]);
            $form->no_of_floor = $request->no_of_floor;
            $form->form_id = $request->form_id;
            $form->underground_floor_area = $request->underground_floor_area;
            $form->ground_floor_area = $request->ground_floor_area;
            $form->first_floor_area = $request->first_floor_area;
            $form->second_floor_area = $request->second_floor_area;
            $form->third_floor_area  = $request->third_floor_area;
            $form->fourth_floor_area = $request->fourth_floor_area;
            $form->save();

            $form_id = $request->form_id;
            return redirect()->route('consultancy.add_form_buildingfiles', $form_id);
        } else {
            return redirect()->route('add_form_personalinfo');

        }
    }

    public function get_dpc_form($form_id)
    {
        $data['formData'] = Form::where('id',$form_id)->with(['floorInfo','buildingInfo','aaminPratibedan'])->first();

        if(empty($data['formData'])){
            abort(404);
        }
        else{
            $data['form_id'] = $form_id;
            return view('consultancy.dpc_form', $data);
        }

    }

    public function post_dpc_form(Request $request)
    {
        $this->validate($request , [
            'building_category' => 'required|max:255',
            'building_structure' => 'required|max:255',
            'building_length' => 'required|max:255',
            'building_width' => 'required|max:255',
            'foundation_size' => 'required|max:255',
            'setback_purba' => 'required|max:255',
            'setback_pachim' => 'required|max:255',
            'setback_utar' => 'required|max:255',
            'setback_dakshin' => 'required|max:255',
            'mohoda_right' => 'required|max:255',
            'mohoda_left' => 'required|max:255',
            'strap_beam_swkrit_anusar' => 'required|max:255',
            'strap_beam' => 'required|max:255',
            'plinth_height' => 'required|max:255',
            'pillar_size' => 'required|max:255',
            'pillar_rod_size' => 'required|max:255',
            'pillar_rod_number' => 'required|max:255',
            'pillar_churi_size' => 'required|max:255',
            'tie_beam_size_swkrit_anusar' => 'required|max:255',
            'tie_beam_size' => 'required|max:255',
            'hodding_board_swkrit_anusar' => 'required|max:255',
            'hodding_board' => 'required|max:255'
        ]);

        if($request->action =='edit'){
            $dpc = Dpc::find($request->dpc_form_id);
        }
        else {
            $dpc = new Dpc();
        }

        $dpc->form_id = $request->form_id;
        $dpc->building_category = $request->building_category;
        $dpc->building_structure = $request->building_structure;
        $dpc->building_length = $request->building_length;
        $dpc->building_width = $request->building_width;
        $dpc->foundation_size = $request->foundation_size;
        $dpc->setback_purba = $request->setback_purba;
        $dpc->setback_pachim = $request->setback_pachim;
        $dpc->setback_utar = $request->setback_utar;
        $dpc->setback_dakshin = $request->setback_dakshin;
        $dpc->mohoda_right = $request->mohoda_right;
        $dpc->mohoda_left = $request->mohoda_left;
        $dpc->strap_beam_swkrit_anusar = $request->strap_beam_swkrit_anusar;
        $dpc->strap_beam = $request->strap_beam;
        $dpc->plinth_height = $request->plinth_height;
        $dpc->pillar_size = $request->pillar_size;
        $dpc->pillar_churi_size = $request->pillar_churi_size;
        $dpc->pillar_rod_size = $request->pillar_rod_size;
        $dpc->pillar_rod_number = $request->pillar_rod_number;
        $dpc->tie_beam_size_swkrit_anusar = $request->tie_beam_size_swkrit_anusar;
        $dpc->tie_beam_size = $request->tie_beam_size;
        $dpc->hodding_board_swkrit_anusar = $request->hodding_board_swkrit_anusar;
        $dpc->hodding_board = $request->hodding_board;
        $dpc->created_by = Auth::user()->id;
        $dpc->save();

        if($request->redirect == 'administration'){
            \Session::flash('success','DPC Updated Successfully.');
            return redirect()->route('administration.get_edit',$request->form_id);
        }
        else {
            if($request->action =='edit'){
                \Session::flash('success','DPC Updated Successfully.');
            }
            else
            {
                \Session::flash('success','DPC Uploaded Successfully.');
            }
            return redirect()->route('consultancy.get_dpc_view',$request->form_id.'?action='.$request->action);
        }
    }

    public function get_dpc_view($form_id)
    {
        $data['formData'] = Form::where('id',$form_id)->with(['floorInfo','buildingInfo','aaminPratibedan','dpc'])->first();

        if(empty($data['formData'])){
            abort(404);
        }
        else {
            $data['action'] = $_GET['action'];
            $data['form_id'] = $form_id;
            return view('consultancy.print_dpc',$data);
        }

    }

    public function post_upload_dpc(Request $request){
        $this->validate($request,
            [
                'upload_dpc' => 'required',
                'action' => 'required'
            ]);

        if($request->action == 'edit'){
            $dpc_scan = DpcScan::where('form_id',$request->form_id)->first();
            Storage::delete('public/'.$dpc_scan->dpc_scan);

        }
        else {
            $dpc_scan = new DpcScan();
        }
        $path = $request->file('upload_dpc')->store('files/'.$request->form_id.'/dpc_scan', 'public');

        $dpc_scan->form_id = $request->form_id;
        $dpc_scan->slug = 'upload_dpc_form';
        $dpc_scan->dpc_scan = $path;
        $dpc_scan->created_by = Auth::user()->id;

        if($request->action == 'edit'){
            $dpc_scan->updated_by = Auth::user()->id;
        }
        $dpc_scan->save();


        $form = Form::find($request->form_id);
        $form->proceeded_to = 'sub-engineer';
        $form->is_rejected = 0;
        $form->rejected_message = null;
        $form->rejected_at = null;
        $form->is_draft = 0;
        $form->form_level = 7;
        $form->save();

        $data['form_id'] = $request->form_id;

        if($request->action == 'edit'){
            \Session::flash('success','DPC File  Updated Successfully');
        }
        else
        {
            \Session::flash('success','DPC File  Uploaded Successfully');
        }
        return redirect()->route('consultancy.view_dpc',$data);

    }

    public function get_upload_dpc($id){
        $data['formData'] = Form::where('id',$id)->with(['floorInfo','buildingInfo','aaminPratibedan','dpc'])->first();
        return view('consultancy.view_dpc',$data);
    }

    public function get_edit_dpcform($form_id){

        $data['formData'] = Form::where('id',$form_id)->with(['floorInfo','buildingInfo','aaminPratibedan','dpc'])->first();
//        dd($data);
        if(empty($data['formData'])){
            abort(404);

        }
        else{
            if(isset($_GET['token'])){
                $data['action'] = $_GET['token'];
            } else{
                $data['action'] = 'consultancy';
            }
            $data['form_id'] = $form_id;

            return view('consultancy.edit_dpc', $data);
        }

    }

    public function get_sampanna_form($form_id)
    {
        $data['formData'] = Form::where('id',$form_id)->with(['floorInfo','buildingInfo','aaminPratibedan','dpc','landInfo'])->first();

        if(empty($data['formData'])){
            abort(404);

        }
        else{
            $data['form_id'] = $form_id;
            return view('consultancy.form_sampanna', $data);
        }

    }


    public function post_form_sampanna(Request $request)
    {
        $this->validate($request , [
            'form_id' => 'required|max:255',
            'field_area' => 'required|max:255',
            'site_plan_area' => 'required|max:255',
            'ground_coverage' => 'required|max:255',

            'ground_coverage_mapdanda' => 'required|max:255',
            'ground_coverage_swikrit_anusar' => 'required|max:255',
            'ground_coverage_nirman_esthithi' => 'required|max:255',
            'talla_mapdanda' => 'required|max:255',


            'talla_swikrit_anusar' => 'required|max:255',
            'talla_nirman_esthithi' => 'required|max:255',
            'height_mapdanda' => 'required|max:255',
            'height_swikrit_anusar' => 'required|max:255',


            'height_nirman_esthithi' => 'required|max:255',
            'floor_area_mapdanda' => 'required|max:255',
            'floor_area_swikrit_anusar' => 'required|max:255',
            'floor_area_nirman_esthithi' => 'required|max:255',


            'swikrit_vanda_badi_mapdanda' => 'required|max:255',
            'swikrit_vanda_badi_swikrit_anusar' => 'required|max:255',
            'swikrit_vanda_badi_nirman_esthithi' => 'required|max:255',
            'r_o_w_mapdanda' => 'required|max:255',


            'r_o_w_swikrit_anusar' => 'required|max:255',
            'r_o_w_nirman_esthithi' => 'required|max:255',
            'setback_mapdanda' => 'required|max:255',
            'setback_swikrit_anusar' => 'required|max:255',

            'setback_nirman_esthithi' => 'required|max:255',
            'electricity_line' => 'required|max:255',
            'electricity_line_mapdanda' => 'required|max:255',
            'electricity_line_swikrit_anusar' => 'required|max:255',
            'electricity_line_nirman_esthithi' => 'required|max:255',

            'river_side_mapdanda' => 'required|max:255',
            'river_side_swikrit_anusar' => 'required|max:255',
            'river_side_nirman_esthithi' => 'required|max:255',

            'entry_name' => 'required|max:255',
            'consultancy_post' => 'required|max:255',
            'consultancy_name' => 'required|max:255'
        ]);

        //if edit it will update the code.
        if($request->action == 'edit')
        {
            $form = FormSampanna::find($request->id);

        }
        else    //insert new data into the form.
        {
            $form = new FormSampanna();
        }
        $form->form_id = $request->form_id;
        $form->field_area = $request->field_area;

        $form->site_plan_area = $request->site_plan_area;
        $form->ground_coverage = $request->ground_coverage;

        $form->ground_coverage_mapdanda = $request->ground_coverage_mapdanda;
        $form->ground_coverage_swikrit_anusar = $request->ground_coverage_swikrit_anusar;
        $form->ground_coverage_nirman_esthithi = $request->ground_coverage_nirman_esthithi;

        $form->talla_mapdanda = $request->talla_mapdanda;
        $form->talla_swikrit_anusar = $request->talla_swikrit_anusar;
        $form->talla_nirman_esthithi = $request->talla_nirman_esthithi;
        $form->height_mapdanda = $request->height_mapdanda;

        $form->height_swikrit_anusar = $request->height_swikrit_anusar ;
        $form->height_nirman_esthithi = $request->height_nirman_esthithi;
        $form->floor_area_mapdanda = $request->floor_area_mapdanda ;
        $form->floor_area_swikrit_anusar = $request->floor_area_swikrit_anusar ;

        $form->floor_area_nirman_esthithi = $request->floor_area_nirman_esthithi;
        $form->swikrit_vanda_badi_mapdanda = $request->swikrit_vanda_badi_mapdanda ;
        $form->swikrit_vanda_badi_swikrit_anusar = $request->swikrit_vanda_badi_swikrit_anusar;
        $form->swikrit_vanda_badi_nirman_esthithi = $request->swikrit_vanda_badi_nirman_esthithi;

        $form->r_o_w_mapdanda = $request->r_o_w_mapdanda;
        $form->r_o_w_swikrit_anusar = $request->r_o_w_swikrit_anusar;
        $form->r_o_w_nirman_esthithi = $request->r_o_w_nirman_esthithi;
        $form->setback_mapdanda = $request->setback_mapdanda;

        $form->setback_swikrit_anusar = $request->setback_swikrit_anusar;
        $form->setback_nirman_esthithi = $request->setback_nirman_esthithi;
        $form->electricity_line = $request->electricity_line;

        $form->electricity_line_mapdanda = $request->electricity_line_mapdanda;
        $form->electricity_line_swikrit_anusar = $request->electricity_line_swikrit_anusar;

        $form->electricity_line_nirman_esthithi = $request->electricity_line_nirman_esthithi;
        $form->river_side_mapdanda = $request->river_side_mapdanda;
        $form->river_side_swikrit_anusar = $request->river_side_swikrit_anusar;
        $form->river_side_nirman_esthithi = $request->river_side_nirman_esthithi;
        $form->entry_name = $request->entry_name;
        $form->consultancy_post = $request->consultancy_post;
        $form->consultancy_name = $request->consultancy_name;
        $form->created_by = Auth::user()->id;

        $form->save();

        $token = Hash::make($request->action);


        // dd($data);
        if($request->action == 'edit')
        {
            \Session::flash('success',' निर्माण सम्पन्न प्रतिवेदन Updated Successfully');
        }
        else{
            \Session::flash('success',' निर्माण सम्पन्न प्रतिवेदन Uploaded Successfully');

        }
        return redirect()->route('consultancy.get_upload_sampanna_form',$request->form_id.'?action='.$token);
    }

    //function to show print/upload view.
    public function get_upload_sampanna_form($id){
        $data['formData'] = Form::where('id', $id)->with('landInfo','sampanna')->first();
        if(empty($data['formData'])){
            abort(404);
        }
        else{
            if (Hash::check('edit', $_GET['action'])) {
                $data['action'] = 'edit';
            } else {
                $data['action'] = 'add';
            }
            return view('consultancy.print_sampanna',$data);
        }

    }

    //function after uploading data from modal after form_sampanna
    public function post_upload_sampanna(Request $request)
    {
        $this->validate($request,
            [
                'upload_sampanna' => 'required',
                'action' => 'required'
            ]);

        if ($request->action == 'edit') {
            $sampanna_scan = SampannaScan::where('form_id', $request->form_id)->first();
            Storage::delete('public/' . $sampanna_scan->form);
        }
        else {
            $sampanna_scan = new SampannaScan();
        }
        $path = $request->file('upload_sampanna')->store('files/' . $request->form_id . '/sampanna_scan/form', 'public');

        $sampanna_scan->form_id = $request->form_id;
        $sampanna_scan->form = $path;
        $sampanna_scan->tippani = '';
        $sampanna_scan->certificate = '';
        $sampanna_scan->created_by = Auth::user()->id;
        $sampanna_scan->save();


        $form = Form::find($request->form_id);
        $form->proceeded_to = 'sub-engineer';
        $form->is_rejected = 0;
        $form->rejected_message = null;
        $form->rejected_at = null;
        $form->rejected_by = null;
        $form->is_draft = 0;
        $form->form_level = 12;
        $form->updated_by = Auth::user()->id;
        $form->save();

        if ($request->action == 'edit') {
            \Session::flash('success', ' Sampanna File Updated Successfully,');
        }
        else {
            \Session::flash('success', ' Sampanna File Uploaded Successfully.');
        }
        return redirect()->route('consultancy.view_sampanna',$request->form_id);

    }

    public function get_view_sampanna($id){
        $data['formData'] = Form::where('id', $id)->with('landInfo','sampanna')->first();
        if(!empty($data['formData'])){
            return view('consultancy.view_sampanna',$data);
        }
        else {
            abort(404);
        }
    }

    public function get_edit_sampanna($form_id){
        $data['formData'] = Form::where('id',$form_id)->with(['floorInfo','buildingInfo','aaminPratibedan','dpc','landInfo','sampanna'])->first();

        if(empty($data['formData'])){
            abort(404);
        }
        else {
            return view('consultancy.edit_sampanna', $data);
        }
    }

    //function to get the details of form_building file to change house map
    public function get_housemap($id){
        $data['formData'] = FormBuildingFile::where('form_id',$id)->first();

        if(empty($data['formData'])){
            abort(404);
        }
        else {
            return view('consultancy.edit_housemap',$data);
        }
    }

    public function post_change_housemap(Request $request){
        $this->validate($request,[
            'form_id' => 'required'
        ]);


        if(isset($request->house_map)){
            $file = FormBuildingFile::where('form_id',$request->form_id)->first();
            Storage::delete('public/'.$file->house_map);
            $path = $request->file('house_map')->store('files/'.$request->form_id.'/building_file/house_map', 'public');
            $file->house_map = $path;

            $file->save();

            \Session::flash('success','Housemap Changed Successfully');
            return redirect()->route('consultancy.index');

        } else {
            return redirect()->back();
        }

    }
}
