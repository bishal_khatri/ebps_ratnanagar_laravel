<?php

namespace App\Http\Controllers;

use App\AaminPratibedan;
use App\DpcScan;
use App\Form;
use App\FormFloorInfo;
use App\FormLand;
use App\Helper\Tools;
use App\SandirSuchana;
use App\TippaniScan;
use App\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class EngineerController extends Controller
{
    public function skipdate($id){
        $scan = SandirSuchana::where('form_id' , $id)->first();
        if(!empty($scan)){
            $current_date = strtotime($scan->created_at);
            $newDate = date('Y-m-d H:i:s',strtotime('+20 days',$current_date));
            $scan->created_at = $newDate;
            $scan->save();

            return redirect()->route('engineer.view_sandhir',$id);
        }
        else {
            abort(404);
        }
    }

    public function index()
    {
        $data['ward'] = Ward::all();
        $level = [1,2,5,8,13];
        if (isset($_GET['ward_number'])){
            $data['ward_number'] = $_GET['ward_number'];
            $data['files'] = Form::whereIn('form_level',$level)
                ->where('proceeded_to','engineer')
                ->whereHas('landInfo', function($query){ $query->where('current_ward_number',$_GET['ward_number']);})
                ->with('landInfo')
                ->orderBy('id','desc')
                ->get();
            // dd($data);
        }
        return view('engineer.index',$data);
    }

    public function check_form_level($form_id)
    {
        $form = Form::find($form_id);

        if(!empty($form)){
            $form_level = $form->form_level;
            if ($form_level == 1) {
                $data['formData'] = Form::where('id', $form_id)->with('landInfo', 'floorInfo', 'buildingInfo', 'buildingFile')->first();
                if (is_null($data['formData'])) {
                    $data['formData'] = '';
                }
                return view('engineer.view_form', $data);
            }
            elseif ($form_level == 5) {
                $data['formData'] = AaminPratibedan::where('form_id', $form_id)->first();
                $data['form'] = Form::where('id', $form_id)->where('is_draft', 0)->where('is_rejected', 0)->where('proceeded_to', 'engineer')->where('form_level', 5)->with('landInfo')->first();

                return view('engineer.view_aamin_pratibedan', $data);
            }
            elseif ($form_level == 8) {
                $data['formData'] = Form::where('id', $form_id)->where('is_draft',0)->where('is_rejected',0)->where('proceeded_to', 'engineer')->where('form_level' , 8)->with('dpc')->first();
                return view('engineer.view_dpc', $data);
            }
        }
        else{
            abort(404);
        }
    }


    public function view_rajashow_form($form_id)
    {
        $data['floorInfo'] = FormFloorInfo::where('form_id',$form_id)->first();
        if(!empty($data['floorInfo'])){
            $data['landInfo'] = FormLand::where('form_id',$form_id)->first();
            $data['personalInfo'] = Form::where('id',$form_id)->with('rajashow')->first();
            return view('engineer.view_rajashow',$data);
        }
        else {
            abort(404);
        }
    }

    public function sandhir_suchana($id)
    {
        $fileData = Form::select_file('id',$id);

        if(!empty($fileData)){
            // proceed to print page if sandhir is not uploaded i.e. formLevel=1
            if ($fileData->form_level==1){

                return view('engineer.sandhir_suchana')->with('fileData',$fileData);
            }
            else{
                return redirect()->route('engineer.view_sandhir',$id);
            }
        }
        else{
            abort(404);
        }
    }

    public function reject_form(Request $request)
    {
        $form = Form::find($request->form_id);

        if(!is_null($form)){
            // store reject log
            Tools::rejectLog($form->id, $request->rejected_message, $form->form_level,1);

            $form->is_rejected = 1;
            $form->rejected_message = $request->rejected_message;
            $form->proceeded_to = 'consultancy';
            $form->rejected_by = Auth::user()->id;
            $form->rejected_at = date('Y-m-d H:i:s');
            $form->form_level = 1;
            $form->save();

            Session::flash('success','File Rejected Successfully');
            return redirect()->back();
        }
        else{
            abort(404);
        }
    }

    public function sandhir_upload(Request $request)
    {
        $this->validate($request, [
            'sandhir_file' => 'required'
        ]);

        $sandhir_file = $request->file('sandhir_file')->store('files/'.$request->form_id.'/sandhir_suchana', 'public');

        $sandhir = new SandirSuchana();
        $sandhir->form_id = $request->form_id;
        $sandhir->sandhir_suchana = $sandhir_file;
        $sandhir->save();

        $form = Form::find($request->form_id);
        $form->proceeded_to = 'aamin';
        $form->is_draft = 0;
        $form->is_rejected = 0;
        $form->form_level = 2;
        $form->updated_at = date('Y-m-d H:i:s');
        $form->updated_by = Auth::user()->id;
        $form->rejected_at = null;
        $form->rejected_message = null;
        $form->save();

        Session::flash('success','File Uploaded Successfully');
        return redirect()->route('engineer.view_sandhir', $request->form_id);
    }

    public function view_sandhir($id)
    {
        $data['sandhir'] = SandirSuchana::where('form_id',$id)->first();

        if(!empty($data['sandhir'])){
            $created_at = new \DateTime($data['sandhir']->created_at);
            $current_date = new \DateTime();
            $interval = $created_at->diff($current_date);
            switch ($interval->days){
                case 0:
                    $data['days_left'] = '१५';
                    break;
                case 1:
                    $data['days_left'] = '१४';
                    break;
                case 2:
                    $data['days_left'] = '१३';
                    break;
                case 3:
                    $data['days_left'] = '१२';
                    break;
                case 4:
                    $data['days_left'] = '११';
                    break;
                case 5:
                    $data['days_left'] = '१०';
                    break;
                case 6:
                    $data['days_left'] = '९';
                    break;
                case 7:
                    $data['days_left'] = '८';
                    break;
                case 8:
                    $data['days_left'] = '७';
                    break;
                case 9:
                    $data['days_left'] = '६';
                    break;
                case 10:
                    $data['days_left'] = '५';
                    break;
                case 11:
                    $data['days_left'] = '४';
                    break;
                case 12:
                    $data['days_left'] = '३';
                    break;
                case 13:
                    $data['days_left'] = '२';
                    break;
                case 14:
                    $data['days_left'] = '१';
                    break;
                case 15:
                    $data['days_left'] = '०';
                    break;
                default:
                    $data['days_left'] = '०';
            }

            $data['form_id'] = $id;

            return view('engineer.view_sandhir',$data);
        }
        else{
            abort(404);
        }

    }

    public function tippani_adesh($id){
        $data['formData'] = Form::where('id' ,$id)->with('landInfo','floorInfo','buildingInfo','buildingFile','aaminPratibedan')->first();

        if(!empty($data['formData'])){
            return view('engineer.tippani_adesh', $data);

        }
        else{
            abort(404);
        }
    }

    public function store_tippani( Request $request){
        $this->validate($request, [
            'tippani_scan' => 'required',
            'asthai_scan' => 'required'

        ]);

        $tippani_path = $request->file('tippani_scan')->store('files/'.$request->form_id.'/tippani_scan', 'public');
        $asthai_path = $request->file('asthai_scan')->store('files/'.$request->form_id.'/asthai_scan', 'public');

        $scan = new TippaniScan();
        $scan->form_id = $request->form_id;
        $scan->asthai_scan = $asthai_path;
        $scan->tippani_scan = $tippani_path;

        $scan->created_by = Auth::user()->id;

        $scan->save();

        $form = Form::find($request->form_id);
        $form->proceeded_to = 'consultancy';
        $form->is_draft = 0;
        $form->is_rejected = 0;
        $form->form_level = 6;
        $form->updated_at = date('Y-m-d H:i:s');
        $form->updated_by = Auth::user()->id;
        $form->rejected_at = null;
        $form->rejected_message = null;
        $form->save();

        Session::flash('success','Tippani and Esthai Notice Uploaded Successfully');
        return redirect()->route('engineer.uploaded_tippani_adesh',$request->form_id);

    }

    public function get_uploaded_tippani_adesh($form_id){
        $data['action'] = 'consultancy';

        $path = TippaniScan::where('form_id',$form_id)->first();
        $tippani_path = $path->tippani_scan;
        $asthai_path = $path->asthai_scan;

        $data['tippani_path'] = $tippani_path;
        $data['asthai_path'] = $asthai_path;
        return view('engineer.view_show_message',$data);
    }

    //dpc upload certificate of tippani controller
    public function get_print_dpc(Request $request)
    {
        $data['formData'] = Form::where('id' ,$request->form_id)->with('landInfo','floorInfo','buildingInfo','buildingFile','aaminPratibedan','dpc')->first();
//        dd($data);
        return view('engineer.upload_dpc', $data);
    }

    public function store_dpc(Request $request){
        $this->validate($request, [
            'dpc_scan' => 'required',
        ]);

        $dpc_path = $request->file('dpc_scan')->store('files/'.$request->form_id.'/dpc_scan', 'public');

        $scan = new DpcScan();
        $scan->form_id = $request->form_id;
        $scan->dpc_scan = $dpc_path;
        $scan->slug ='dpc-tippani';
        $scan->created_by = Auth::user()->id;

        $scan->save();

        $form = Form::find($request->form_id);
        $form->proceeded_to = 'main-engineer';
        $form->is_draft = 0;
        $form->is_rejected = 0;
        $form->form_level = 9;
        // $form->slug = 'dpc';
        $form->updated_at = date('Y-m-d H:i:s');
        $form->updated_by = Auth::user()->id;
        $form->rejected_at = null;
        $form->rejected_message = null;
        $form->save();

        Session::flash('success',' Tippani Adhesh Uploaded Successfully');
        return redirect()->route('engineer.uploaded_dpc',$request->form_id);

    }

    public function get_uploaded_dpc($form_id){
        $scan = DpcScan::where('form_id',$form_id)->where('slug','dpc-tippani')->first();
        $dpc_path = $scan->dpc_scan;
        $data['action'] = 'main-engineer';
        $data['dpc_path'] = $dpc_path;
        return view('engineer.view_show_message',$data);
    }

    public function get_print_sampanna($form_id){
        $data['formData'] = Form::where('id',$form_id)->with(['landInfo','sampanna','buildingInfo', 'floorInfo'])->first();

        if(!empty($data['formData'])){
            return view('engineer.view_sampanna', $data);
        }
        else {
            abort(404);
        }
        // dd($data);

    }


    public function proceed_sampanna(Request $request){
        $this->validate($request , [
            'form_id' => 'required'
        ]);

        $form = Form::find($request->form_id);
        $form->proceeded_to = 'main-engineer';
        $form->is_draft = 0;
        $form->is_rejected = 0;
        $form->form_level = 14;
        $form->updated_at = date('Y-m-d H:i:s', time());
        $form->updated_by = Auth::user()->id;
        $form->save();

        Session::flash('success','निर्माण सम्पन्न प्रमाण पत्र Proceeded Successfully ');
        return redirect()->route('engineer.view_sampanna',$request->form_id);

    }

    public function get_uploaded_sampanna($form_id){
        echo $form_id;
        $data['action'] = 'main-engineer';
        return view('engineer.view_show_message',$data);
    }

}
