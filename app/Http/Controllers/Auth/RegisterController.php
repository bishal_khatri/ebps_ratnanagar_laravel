<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\AuthPermission;
use App\AuthPermissionUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $data['permissions'] = AuthPermission::all();
        return view('auth.register',$data);
    }


    public function showEditForm($id){
        $data['formData'] = User::find($id);
        $data['permissions'] = AuthPermission::orderBy('code_name')->get();
        $data['permission_user'] = AuthPermissionUser::where('user_id' , $id)->first();
//        dd($data);
        return view('auth.edit',$data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'selected_permission' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if(isset($data['avatar'])){
            $avatar_path = $data['avatar']->store('files/avatar_file', 'public');
        } else {
            $avatar_path = null;
        }

        //checking permisson through its id.
        if($data['selected_permission'] != 1){
             $data['consultancy_name'] = null ;
             $data['consultancy_address'] = null;
             $data['consultancy_license'] = null;
             $data['consultancy_license_number'] = null;
             $data['staff_id'] = null;
             $data['extension_number'] = null;
        }

        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'contact' => $data['contact'],
            'address' => $data['address'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_super' => 0,
            'consultancy_name' => $data['consultancy_name'],
            'consultancy_address' => $data['consultancy_address'],
            'consultancy_license' => $data['consultancy_license'],
            'consultancy_license_number' => $data['consultancy_license_number'],
            'avatar' => $avatar_path,
        ]);
    }

    public function register(Request $request)
    {
        if($request->action == 'edit')
        {
            $this->validate($request, [
                'id' => 'required',
                'action' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'contact' => 'required',
                'address' => 'required',
//                'email' => 'required',
//                'old_password' => 'required',
//                'password' => 'required|string|min:6|confirmed',
//                'password_confirmation' => 'required',
                'selected_permission' => 'required',

            ]);


            $data = $request->all();

            if( $data['selected_permission'] != 1){
                $data['consultancy_name'] = null ;
                $data['consultancy_address'] = null;
                $data['consultancy_license'] = null;
                $data['consultancy_license_number'] = null;
                $data['staff_id'] = null;
                $data['extension_number'] = null;
            }




            $user = User::find($request->id);

            if (isset($data['avatar'])){
                Storage::delete('public/'.$user->avatar);
                $avatar_path = $data['avatar']->store('files/avatar_file', 'public');
            }

            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->contact = $data['contact'];

            $user->address  = $data['address'];
//            $user->email = $data['email'];

            if(isset($data['password'])){
                $user->password = $data['password'];
            }

            $user->is_super = 0;

            $user->consultancy_name = $data['consultancy_name'];
            $user->consultancy_address = $data['consultancy_address'];
            $user->consultancy_license = $data['consultancy_license'];

            $user->consultancy_license_number = $data['consultancy_license_number'];
            $user->staff_id = $data['staff_id'];
            $user->extension_number = $data['extension_number'];

            if(isset($avatar_path)){
                $user->avatar = $avatar_path;
            }

            $user->updated_at = date('Y-m-d h:i:s');

            $user->save();


            //        retrieving permission code_name from id
            $permission = AuthPermission::find($request->selected_permission);

            //adding permission to auth_permission_user table.
            $form =  AuthPermissionUser::where('user_id',$request->id)->first();
            $form->permission_id = $request->selected_permission;
            $form->permission = $permission->code_name;
            $form->save();


            Session::flash('success','User Updated Successfully');
            return redirect()->route('user.index');
        }

        else {
            $this->validator($request->all())->validate();
            event(new Registered($user = $this->create($request->all())));


//        retrieving permission code_name from id
            $permission['user_id'] = $user->id;
            $permission['auth_permission'] = AuthPermission::find($request->selected_permission);

            //adding permission to auth_permission_user table.
            $form = new AuthPermissionUser();
            $form->user_id = $permission['user_id'];
            $form->permission_id = $request->selected_permission;
            $form->permission = $permission['auth_permission']->code_name;
            $form->save();

        Session::flash('success','User Added Successfully');
            return redirect()->route('user.index');
        }


    }


    public function index()
    {
        $data['users'] = User::where('is_super',0)->get();
        return view('auth.index', $data);
    }

    public function delete(Request $request)
    {

        $user = User::find($request->id);
        $user->delete();
        Session::flash('success','User Deleted Successfully.');
    }

}
