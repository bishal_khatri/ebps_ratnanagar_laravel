<?php

namespace App\Http\Controllers\Api;

use App\Form;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Tools;

class FileController extends Controller
{
    public function index(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'darta_number' => 'required',
        ]);
        if ($validator->fails()){
            $responseData = Tools::setResponse('fail','Parameter Missing','','');
        }
        else{
            $form = Form::where('darta_number',$request->darta_number)->first();
            if(!empty($form)){
                $data['form_level'] = Tools::form_levelconverter($form->form_level);
                $data['proceeded_to']= $form->proceeded_to;

                $responseData = Tools::setResponse('success','Data Found',$data,'');
            }
            else{
                $responseData = Tools::setResponse('fail','Data Not Found','','');
            }
        }
        return response()->json($responseData);

    }


}
