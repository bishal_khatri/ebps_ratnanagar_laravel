<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\AaminPratibedan;
use Illuminate\Http\Request;
use App\Ward;
use App\Form;
use App\SandirSuchana;
use App\AaminPratibedanScan;
use Illuminate\Support\Facades\Storage;
use Session;


class AaminController extends Controller
{

    public function index(){
        $data['ward'] = Ward::all();

        if (isset($_GET['ward_number'])){
            $data['ward_number'] = $_GET['ward_number'];
            $data['files'] = Form::get_files(['fl.current_ward_number'=>$data['ward_number'],'proceeded_to'=>'aamin', 'form_level' => '2','is_rejected' => 0,'is_draft' => 0]);
        }

        return view('aamin.index', $data);
    }

    /**
     * function to view and validate 15 days after sandhir upload or print amin pratibedan.
     */
    public function get_sandhir($id)
    {
        $data['sandhir'] = SandirSuchana::where('form_id', $id)->first();
        if(!empty($data['sandhir'])){
            $created_at = new \DateTime($data['sandhir']->created_at);
            $current_date = new \DateTime();
            $interval = $created_at->diff($current_date);


            if ($interval->days >= 15) {

                $data['formData'] = Form::where('id', $id)->where('is_draft',0)->where('is_rejected',0)->with('landInfo')->first();
//            dd($data);

                $aamin = AaminPratibedan::where('form_id',$id)->first();
                if(!empty($aamin)){
                    return redirect()->route('aamin.view_aamin',$id);
                }

                return view('aamin.aamin_pratibedan' , $data);
            }
            else {
                switch ($interval->days) {
                    case 0:
                        $data['days_left'] = '१५';
                        break;
                    case 1:
                        $data['days_left'] = '१४';
                        break;
                    case 2:
                        $data['days_left'] = '१३';
                        break;
                    case 3:
                        $data['days_left'] = '१२';
                        break;
                    case 4:
                        $data['days_left'] = '११';
                        break;
                    case 5:
                        $data['days_left'] = '१०';
                        break;
                    case 6:
                        $data['days_left'] = '९';
                        break;
                    case 7:
                        $data['days_left'] = '८';
                        break;
                    case 8:
                        $data['days_left'] = '७';
                        break;
                    case 9:
                        $data['days_left'] = '६';
                        break;
                    case 10:
                        $data['days_left'] = '५';
                        break;
                    case 11:
                        $data['days_left'] = '४';
                        break;
                    case 12:
                        $data['days_left'] = '३';
                        break;
                    case 13:
                        $data['days_left'] = '२';
                        break;
                    case 14:
                        $data['days_left'] = '१';
                        break;
                    case 15:
                        $data['days_left'] = '०';
                        break;
                    default:
                        $data['days_left'] = 'n/a';
                }

                return view('aamin.view_sandhir', $data);
            }
        }
        else {
            abort(404);
        }

    }

    public function store_aaminPratibedan(Request $request){

        $this->validate($request , [
            'form_id' => 'required' ,
            'field_owner_name' => 'required' ,
            'building_type' => 'required' ,
            'electricity_line' => 'required' ,

            'river' => 'required' ,
            'bato_category' => 'required' ,
            'bato_length' => 'required' ,
            'bato_direction' => 'required' ,

            'bato_area' => 'required' ,
            'mohoda_front' => 'required' ,
            'mohoda_back' => 'required' ,
            'mohoda_left' => 'required' ,

            'mohoda_right' => 'required' ,
            'chadnu_parne_duri_purba' => 'required' ,
            'chadnu_parne_duri_paschim' => 'required' ,
            'chadnu_parne_duri_utar' => 'required' ,

            'chadnu_parne_duri_dakshin' => 'required' ,
            'chadnu_parne_setback_purba' => 'required' ,
            'chadnu_parne_setback_paschim' => 'required' ,
            'chadnu_parne_setback_utar' => 'required' ,

            'chadnu_parne_setback_dakshin' => 'required' ,
            'napi_length' => 'required' ,
            'napi_width' => 'required' ,
            'napi_field_area' => 'required' ,


            'created_at_nepali' => 'required'


        ]);

        //action = add then insert.
        if($request->action == 'add') {
            $form = new AaminPratibedan;

        }
        //action = 'edit' then update.
        else {
            $form =  AaminPratibedan::where('form_id',$request->form_id)->first();
        }

        $form->form_id = $request->form_id;
        $form->bato_category = $request->bato_category;
        $form->bato_length = $request->bato_length;
        $form-> bato_direction = $request->bato_direction;

        $form->bato_area = $request->bato_area;
        $form->mohoda_front = $request->mohoda_front;
        $form-> mohoda_back = $request->mohoda_back;
        $form->mohoda_left = $request->mohoda_left;

        $form->mohoda_right = $request->mohoda_right;
        $form->chadnu_parne_duri_purba = $request->chadnu_parne_duri_purba;
        $form->chadnu_parne_duri_paschim = $request->chadnu_parne_duri_paschim;
        $form->chadnu_parne_duri_utar = $request->chadnu_parne_duri_utar;

        $form->chadnu_parne_duri_dakshin  = $request->chadnu_parne_duri_dakshin;
        $form->chadnu_parne_setback_purba = $request->chadnu_parne_setback_purba;
        $form->chadnu_parne_setback_paschim = $request->chadnu_parne_setback_paschim;
        $form->chadnu_parne_setback_utar = $request->chadnu_parne_setback_utar;

        $form->chadnu_parne_setback_dakshin = $request->chadnu_parne_setback_dakshin;
        $form->napi_length = $request->napi_length;
        $form->napi_width = $request->napi_width;
        $form->napi_field_area = $request->napi_field_area;

        $form->siteko_napi_length = '';
        $form->siteko_napi_width = '';
        $form->siteko_napi_field_area = '';

        $form->created_at_nepali = $request->created_at_nepali;

        if(isset($request->other)){
            $form->other = $request->other;
        }

        if($request->electricity_line == "भएको") {
            $form->hitension_wire_distance = $request->hitension_wire_distance;
            $form->electricity_volt = $request->electricity_volt;
        }

        if($request->river = "भएको"){
            $form->river_side_bata_chodnu_parne_duri = $request->river_side_bata_chodnu_parne_duri;

        }
        if($request->action == 'edit'){
            $form->updated_at = date('Y-m-d H:i:s');
            $form->updated_by = Auth::user()->id;
        }

        $form->created_by  = Auth::user()->id;
        $form->save();

        if($request->token == 'administration'){

            //redirect to administration  File management edit list
            Session::flash('success','File Updated Successfully');
            return redirect()->route('administration.get_edit' , $request->form_id);
        } else {

            if($request->action == 'add') {
                Session::flash('success','File Uploaded Successfully');

            }
            else {
                Session::flash('success','File Updated Successfully');
            }

            return redirect()->route('aamin.view_aamin' , $request->form_id.'?action='.$request->action);

        }
    }

    //to view all details of aamin pratibedan form.
    public function get_aaminPratibedan($id){
        $data['formData'] = AaminPratibedan::where('form_id',$id)->first();
        if(!empty($data['formData'])){
            $data['action'] = isset($_GET['action']) ? $_GET['action']: 'add';
            $data['form'] = Form::where('id', $id)->where('is_draft',0)->where('proceeded_to', 'aamin')->where('form_level' , 2)->with('landInfo')->first();
            return view('aamin.print_aamin_pratibedan', $data);
        } else{
            abort(404);
        }
    }

    //to show all the rejected files.
    public function get_rejected_file(){
        $data['formData'] = Form::where('is_draft',0)->where('is_rejected',1)->where('form_level' , 2)->where('proceeded_to','aamin')->with('landInfo')->get();
        return view('aamin.list_rejected',$data);
    }

    public function editAaminPratibedan($id){
        if(isset($_GET['token'])){
            $data['formData'] = Form::where('id', $id)->where('is_draft',0)->with('landInfo','aaminPratibedan')->first();
            $data['token'] = 'administration';
        }
        else{
            $data['formData'] = Form::where('id', $id)->where('is_draft',0)->where('is_rejected',1)->where('form_level' , '2')->where('proceeded_to' , 'aamin')->with('landInfo','aaminPratibedan')->first();
            $data['token'] = 'aamin';
        }

        if(!empty($data['formData'])){

            return view('aamin.edit_aaminpratibedan' , $data);
        }
        else{
            abort(404);
        }
    }

    public function store_aaminPratibedanUpload(Request $request){

        $this->validate($request, [
            'form_id' => 'required',
            'aamin_pratibedan_scan' => 'required',
            'action' => 'required'
        ]);

        if($request->action == 'add'){
            $scan = new AaminPratibedanScan();

        }
        else {
            $scan = AaminPratibedanScan::where('form_id',$request->form_id)->first();
            Storage::delete('public/'.$scan->aamin_pratibedan_scan);

        }
        $aamin_pratibedan_scanpath = $request->file('aamin_pratibedan_scan')->store('files/'.$request->form_id.'/aaminPratibedan_scan', 'public');

        $scan->form_id = $request->form_id;
        $scan->aamin_pratibedan_scan = $aamin_pratibedan_scanpath;
        $scan->created_by = Auth::user()->id;

        $scan->save();

        $form = Form::find($request->form_id);
        $form->form_level = 3;
        $form->proceeded_to = 'sub-engineer';
        $form->is_draft = 0;
        $form->updated_at = date('Y-m-d H:i:s');
        $form->updated_by = Auth::user()->id;
        $form->is_rejected = 0;
        $form->rejected_message = null;
        $form->rejected_by = null;
        $form->rejected_at = null;
        $form->save();

        $data['path'] = $aamin_pratibedan_scanpath;

        if($request->action == 'add') {
            Session::flash('success', 'Aamin Pratibedan File  Uploaded Successfully');
        }
        else
        {
            Session::flash('success','Aamin Pratibedan File  Updated Successfully');
        }
        return redirect()->route('aamin.view_uploaded_aaminpratibedan',$request->form_id);
    }

    public function getUploadedaaminpratibedan($id){
        $data['formData'] = AaminPratibedanScan::where('form_id',$id)->first();

        if(!empty($data['formData'])){
            return view('aamin.view_aamin_pratibedan',$data);
        }
        else{
            abort(404);
        }

    }
}
