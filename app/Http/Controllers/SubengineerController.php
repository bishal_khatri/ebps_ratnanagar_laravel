<?php

namespace App\Http\Controllers;

use App\Dpc;
use App\Helper\Tools;
use App\RajashowSacn;
use Illuminate\Support\Facades\Auth;
use App\Form;
use App\FormFloorInfo;
use App\FormLand;
use App\Rajashow;
use App\Ward;
use App\AaminPratibedan;
use App\DpcScan;
use App\SampannaScan;
use Illuminate\Http\Request;
use Session;

class SubengineerController extends Controller
{
    public function index()
    {
        $level = [1,2,15];
        $data['ward'] = Ward::all();
        if (isset($_GET['ward_number'])){

            $data['ward_number'] = $_GET['ward_number'];
            $data['files'] = Form::get_files(['fl.current_ward_number'=>$data['ward_number'],'proceeded_to'=>'sub-engineer','is_rejected'=>0,'is_draft'=>0]);
            // $data['files'] = Form::whereIn('form_level',$level)
            //                 ->where('proceeded_to','sub-engineer')
            //                 ->whereHas('landInfo', function($query){ $query->where('current_ward_number',$_GET['ward_number']);})
            //                 ->with('landInfo')
            //                 ->get();
        }
        return view('sub_engineer.index',$data);
    }

    public function get_aamin_pratibaden($id)
    {
        $data['formData'] = AaminPratibedan::where('form_id',$id)->first();

        if(!empty($data['formData'])){
            $data['form'] = Form::where('id', $id)->where('is_draft',0)->where('is_rejected',0)->where('proceeded_to', 'sub-engineer')->where('form_level' , 3)->with('landInfo','aaminpratibedanscan')->first();
            return view('sub_engineer.aamin_pratibaden', $data);
        }
        else{
            abort(404);
        }
    }

    public function get_rajashow($form_id)
    {
        $data['floorInfo'] = FormFloorInfo::where('form_id',$form_id)->first();

        if(!empty($data['floorInfo'])){
            $data['landInfo'] = FormLand::where('form_id',$form_id)->first();
            $data['personalInfo'] = Form::find($form_id);

            if(isset($_GET['token'])){
                $data['rajashow'] = Rajashow::where('form_id',$form_id)->first();
                $data['action'] = 'edit';
                $data['token'] = 'administration';
                return view('sub_engineer.edit_rajashow',$data);
            }
            else{
                $data['action'] = 'add';
                return view('sub_engineer.rajashow',$data);
            }
        }
        else{
            abort(404);
        }
    }

    public function post_rajashow(Request $request)
    {
        $this->validate($request,[
            'created_at_nepali' => 'required',
        ]);
        $underground_floor_rate = !isset($request->underground_floor_rate) ? 0 : $request->underground_floor_rate;
        $underground_floor_amount = !isset($request->underground_floor_amount) ? 0 : $request->underground_floor_amount;

        $ground_floor_rate = !isset($request->ground_floor_rate) ? 0 : $request->ground_floor_rate;
        $ground_floor_amount = !isset($request->ground_floor_amount) ? 0 : $request->ground_floor_amount;

        $first_floor_rate = !isset($request->first_floor_rate) ? 0 : $request->first_floor_rate;
        $first_floor_amount = !isset($request->first_floor_amount) ? 0 : $request->first_floor_amount;

        $second_floor_rate = !isset($request->second_floor_rate) ? 0 : $request->second_floor_rate;
        $second_floor_amount = !isset($request->second_floor_amount) ? 0 : $request->second_floor_amount;

        $third_floor_rate = !isset($request->third_floor_rate) ? 0 : $request->third_floor_rate;
        $third_floor_amount = !isset($request->third_floor_amount) ? 0 : $request->third_floor_amount;

        $fourth_floor_rate = !isset($request->fourth_floor_rate) ? 0 : $request->fourth_floor_rate;
        $fourth_floor_amount = !isset($request->fourth_floor_amount) ? 0 : $request->fourth_floor_amount;

        $floorInfo = FormFloorInfo::where('form_id', $request->form_id)->first();
        $floorInfo->underground_floor_area = $request->underground_floor_area;
        $floorInfo->ground_floor_area = $request->ground_floor_area;
        $floorInfo->first_floor_area = $request->first_floor_area;
        $floorInfo->second_floor_area = $request->second_floor_area;
        $floorInfo->third_floor_area = $request->third_floor_area;
        $floorInfo->fourth_floor_area = $request->fourth_floor_area;
        $floorInfo->save();

        if($request->action == 'add'){
            $rajashow = new Rajashow();
        }
        else {
            $rajashow = Rajashow::where('form_id',$request->form_id)->first();
        }
        $rajashow->underground_floor_rate = $underground_floor_rate;
        $rajashow->underground_floor_amount = $underground_floor_amount;

        $rajashow->ground_floor_rate = $ground_floor_rate;
        $rajashow->ground_floor_amount = $ground_floor_amount;

        $rajashow->first_floor_rate = $first_floor_rate;
        $rajashow->first_floor_amount = $first_floor_amount;

        $rajashow->second_floor_rate = $second_floor_rate;
        $rajashow->second_floor_amount = $second_floor_amount;

        $rajashow->third_floor_rate = $third_floor_rate;
        $rajashow->third_floor_amount = $third_floor_amount;

        $rajashow->fourth_floor_rate = $fourth_floor_rate;
        $rajashow->fourth_floor_amount = $fourth_floor_amount;

        $rajashow->total = $request->total;
        $rajashow->other_amount = $request->other_amount;
        $rajashow->grand_total = $request->grand_total;
        $rajashow->created_at_nepali = $request->created_at_nepali;

        $rajashow->created_by = Auth::user()->id;
        $rajashow->form_id = $request->form_id;
        $rajashow->save();

        // change form status
        $form = form::find($request->form_id);
        $form->form_level = 4;
        $form->updated_by = Auth::user()->id;
        $form->save();

        if($request->action == 'add'){
            \Session::flash('success','Rajashow Uploaded Successfully');
            return redirect()->route('subengineer.rajashow.view',$request->form_id);
        } else{
            \Session::flash('success','Rajashow Updated Successfully');
            return redirect()->route('administration.get_edit',$request->form_id);
        }
    }

    public function display_rajashow($form_id)
    {
        $data['floorInfo'] = FormFloorInfo::where('form_id',$form_id)->first();

        if(!empty($data['floorInfo'])){
            $data['landInfo'] = FormLand::where('form_id',$form_id)->first();
            $data['personalInfo'] = Form::where('id', $form_id)->with('rajashow')->first();
            return view('sub_engineer.rajashow_display',$data);
        }
        else {
            abort(404);
        }
    }

    public function reject_form(Request $request){
        $form = Form::find($request->form_id);
        if (!is_null($form)){
            // store reject log
            Tools::rejectLog($form->id, $request->rejected_message, $form->form_level,2);

            $form->is_rejected = 1;
            $form->rejected_message = $request->rejected_message;
            $form->proceeded_to = 'aamin';
            $form->rejected_by = Auth::user()->id;
            $form->rejected_at = date('Y-m-d H:i:s');
            $form->form_level = 2;
            $form->save();

            \Session::flash('success','File has been Rejected Successfully');
            return redirect()->route('subengineer.index');
        } else{
            abort(404);
        }
    }

    public function upload_rajashow_print(Request $request)
    {
        $this->validate($request, [
            'rajashow_scan' => 'required'
        ]);
        $path = $request->file('rajashow_scan')->store('files/'.$request->form_id.'/rajashow_scan', 'public');
        $scan = new RajashowSacn();
        $scan->form_id = $request->form_id;
        $scan->created_by = Auth::user()->id;
        $scan->rajashow = $path;
        $scan->save();


        $form = Form::find($request->form_id);
        $form->proceeded_to ='engineer';
        $form->is_draft = 0;
        $form->is_rejected = 0;
        $form->form_level = 5;
        $form->updated_at = date('Y-m-d H:i:s');
        $form->updated_by = Auth::user()->id;
        $form->rejected_at = null;
        $form->rejected_message = null;
        $form->save();

        \Session::flash('success','Rajashow File Uploaded Successfully');
        return redirect()->route('subengineer.displayrajashow',$request->form_id);
    }

    public function get_uploaded_rajashow($form_id)
    {
        $data['action'] = 'show';
        $data['floorInfo'] = FormFloorInfo::where('form_id',$form_id)->first();
        $data['landInfo'] = FormLand::where('form_id',$form_id)->first();
        $data['personalInfo'] = Form::where('id',$form_id)->with('rajashow')->first();
        return view('sub_engineer.rajashow_display',$data);
    }

    public function get_dpc($id)
    {
        $data['formData'] = Form::where('id', $id)->where('is_draft',0)->where('is_rejected',0)->where('proceeded_to', 'sub-engineer')->where('form_level' , 7)->with('dpc')->with('dpcscan')->first();

        if(!empty($data['formData'])){
            $data['dpcScan'] = DpcScan::where('form_id',$id)->where('slug' ,'upload_dpc_form')->first();
            return view('sub_engineer.view_dpc',$data);
        }
        else {
            abort(404);
        }

    }

    public function post_proceeddpc(Request $request)
    {
        $form = Form::find($request->form_id);
        $form->form_level = 8;
        $form->proceeded_to = 'engineer';
        $form->updated_by = Auth::user()->id;
        $form->save();
        \Session::flash('success','File has been Proceeded Successfully');
        return redirect()->route('subengineer.view_proceeddpc',$request->form_id);

    }

    public function get_proceeddpc($id)
    {
        $data['dpcScan'] = DpcScan::where('form_id',$id)->where('slug' ,'upload_dpc_form')->first();
        $data['action'] = 'engineer';
        return view('sub_engineer.show_message',$data);
    }

    public function post_rejectdpc(Request $request)
    {
        $form = Form::find($request->form_id);
        if (!is_null($form)){
            // store reject log
            Tools::rejectLog($form->id, $request->rejected_message, $form->form_level,6);

            $form->is_rejected = 1;
            $form->rejected_message = $request->rejected_message;
            $form->proceeded_to = 'consultancy';
            $form->rejected_by = Auth::user()->id;
            $form->rejected_at = date('Y-m-d H:i:s');
            $form->form_level = 6;
            $form->save();

            \Session::flash('success','File Rejected Successfully');
            return redirect()->route('subengineer.index');
        }else{
            abort(404);
        }
    }

    public function get_upload_superstructure($id)
    {
        $data['form_id'] = $id;

        if(!empty($data['form_id'])){
            return view('sub_engineer.upload_superstructure',$data);
        }
        else{
            abort(404);
        }
    }

    public function post_superstructure(Request $request)
    {
        $this->validate($request, [
            'superstructure_certificate_scan' => 'required'
        ]);

        $path = $request->file('superstructure_certificate_scan')->store('files/'.$request->form_id.'/dpc_scan', 'public');
        $scan = new DpcScan();
        $scan->form_id = $request->form_id;
        $scan->slug = 'superstructure';
        $scan->created_by = Auth::user()->id;
        $scan->dpc_scan = $path;
        $scan->save();

        $form = Form::find($request->form_id);
        $form->form_level = 11;
        $form->proceeded_to = 'consultancy';
        $form->updated_by = Auth::user()->id;
        $form->save();
        \Session::flash('success','Superstructure Certificate Uploaded Successfully.');
        return redirect()->route('subengineer.view_uploaded',$request->form_id);
    }

    public function get_uploaded_superstructure($form_id)
    {
        $data['action'] ='consultancy';

        //finding by slug = superstructure from dpcscan
        $path = DpcScan::where('form_id',$form_id)->where('slug','superstructure')->first();
        $data['superstructure_path'] = $path->dpc_scan;
        return view('sub_engineer.show_message',$data);
    }

    public function get_sampanna($id)
    {
        $data['formData'] = Form::where('id',$id)->with('landInfo', 'sampanna')->first();

        if(!empty($data['formData'])){
            $data['sampannaScan'] = SampannaScan::where('form_id',$id)->first();
            return view('sub_engineer.view_sampanna',$data);
        }
        else{
            abort(404);
        }
    }

    public function get_proceedSampanna($id)
    {
        $form = Form::find($id);

        if(!empty($form)){
            $form->form_level = 13;
            $form->proceeded_to = 'engineer';
            $form->updated_by = Auth::user()->id;
            $form->save();

            $data['action'] ='engineer';
            $data['formData'] = Form::where('id',$id)->with('landInfo', 'sampanna')->first();
            $data['sampannaScan'] = SampannaScan::where('form_id',$id)->first();
            return view('sub_engineer.view_sampanna',$data);
        }
        else{
            abort(404);
        }
    }

    //function to enter rejected file after being passed by modal.
    public function post_rejectsampanna(Request $request)
    {
        $this->validate($request, [
            'form_id' => 'required',
            'rejected_message' => 'required'
        ]);

        $form = Form::find($request->form_id);
        $form->form_level = 11;
        $form->proceeded_to = 'consultancy';
        $form->rejected_message = $request->rejected_message;
        $form->rejected_by = Auth::user()->id;
        $form->rejected_at = date('Y-m-d h:i:s', time());
        $form->is_rejected = 1;
        $form->save();

        \Session::flash('success','Sampanna File  Rejected Successfully');
        return redirect()->route('subengineer.view_rejectsampanna',$request->form_id);


    }

    //function to view the file after being rejected.
    public function get_rejectsampanna($id)
    {
        $data['formData'] = Form::where('id',$id)->with('landInfo', 'sampanna')->first();

        if(!empty($data['formData'])){
            $data['action'] ='consultancy';
            $data['sampannaScan'] = SampannaScan::where('form_id',$id)->first();
            return view('sub_engineer.view_sampanna',$data);
        }
        else{
            abort(404);
        }
    }

    public function get_upload_sampanna($form_id)
    {
        $data['formData'] = Form::find($form_id);

        if(!empty($data['formData'])){
            return view('sub_engineer.upload_sampanna',$data);
        }
        else {
            abort(404);
        }
    }

    public function post_upload_sampanna(Request $request)
    {
        $this->validate($request,[
            'form_id' => 'required',
            'sampanna_tippani' => 'required',
            'sampanna_certificate' => 'required',
        ]);
        $tippani_path = $request->file('sampanna_tippani')->store('files/'.$request->form_id.'/sampanna_scan/tippani', 'public');
        $certificate_path = $request->file('sampanna_certificate')->store('files/'.$request->form_id.'/sampanna_scan/certificate', 'public');
        $sampanna = SampannaScan::where('form_id',$request->form_id)->first();
        $sampanna->tippani = $tippani_path;
        $sampanna->certificate = $certificate_path;
        $sampanna->created_by = Auth::user()->id;
        $sampanna->created_at = date('Y-m-d H:i:s');
        $sampanna->save();
        $form = Form::find($request->form_id);
        $form->form_level = 16;
        $form->proceeded_to = 'consultancy';
        $form->rejected_message = '';
        $form->rejected_by = null;
        $form->rejected_at = null;
        $form->is_rejected = 0;
        $form->is_draft = 0;
        $form->is_completed = 1;
        $form->updated_by = Auth::user()->id;
        $form->save();

        \Session::flash('success','Sampanna Tippani and Certificate Uploaded Successfully');
        return redirect()->route('subengineer.view_sampanna_file',$request->form_id);
    }

    public function view_sampanna_file($form_id)
    {
        $data['formData'] = Form::where('id',$form_id)->with('sampanna_scan')->first();

        if(!empty($data['formData'])){
            return view('sub_engineer.view_sampanna_file',$data);
        }
        else {
            abort(404);
        }
    }

}
