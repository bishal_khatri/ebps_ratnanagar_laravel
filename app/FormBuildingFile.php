<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormBuildingFile extends Model
{
    protected $table = 'form_buildingfiles';
    protected $primaryKey = 'buildingfiles_id';
    public $timestamps = false;
    public $fillable = ['form_id'];

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
