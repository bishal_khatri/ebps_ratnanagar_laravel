<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AaminPratibedanScan extends Model
{
    protected $table = 'aamin_pratibedan_scan';
    public $timestamps = false;
    protected $primaryKey = 'aamin_pratibedan_id';

    public function form(){
        return $this->belongsTo('App\Form');
    }

}
