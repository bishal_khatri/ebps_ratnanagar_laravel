<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = 'form_personalinfo';

    public static function get_files(array $data=null)
    {
        $sql = \DB::table('form_personalinfo as fp')
                    ->join('form_buildinginfo as fb','fb.form_id','fp.id')
                    ->join('form_buildingfiles as fbf','fbf.form_id','fp.id')
                    ->join('form_floorinfo as ff','ff.form_id','fp.id')
                    ->join('form_landinfo as fl','fl.form_id','fp.id')
                    ->orderBy('fp.id','desc');
        foreach($data as $key=>$value){
            $sql=$sql->where($key,$value);
        }
        $sql = $sql->get();

        return $sql;
    }

    public static function select_file($column, $data)
    {
        return \DB::table('form_personalinfo as fp')
            ->leftjoin('form_buildinginfo as fb','fb.form_id','fp.id')
            ->leftjoin('form_buildingfiles as fbf','fbf.form_id','fp.id')
            ->leftjoin('form_floorinfo as ff','ff.form_id','fp.id')
            ->leftjoin('form_landinfo as fl','fl.form_id','fp.id')
            ->where($column,$data)
            ->first();
    }

    public function landInfo()
    {
        return $this->hasOne('App\FormLand', 'form_id');
    }

    public function floorInfo()
    {
        return $this->hasOne('App\FormFloorInfo','form_id');
    }

    public function buildingInfo()
    {
        return $this->hasOne('App\FormBuildingInfo','form_id');
    }

    public function buildingFile()
    {
        return $this->hasOne('App\FormBuildingFile','form_id');
    }

    public function sandirSuchana()
    {
        return $this->hasOne('App\SandirSuchana','form_id');
    }


    public function aaminPratibedan(){
        return $this->hasOne('App\AaminPratibedan', 'form_id');
    }


    public function rajashow()
    {
        return $this->hasOne('App\Rajashow','form_id');
    }

    public function tippaniscan(){
        return $this->hasOne('App\TippaniScan', 'form_id');
    }

    public function dpc(){
        return $this->hasOne('App\Dpc', 'form_id');
    }

    public function dpcscan(){
        return $this->hasOne('App\DpcScan', 'form_id');
    }

    public function aaminpratibedanscan(){
        return $this->hasOne('App\AaminPratibedanScan', 'form_id');
    }

    public function sampanna(){
        return $this->hasOne('App\FormSampanna', 'form_id');
    }

    public function sampanna_scan(){
        return $this->hasOne('App\SampannaScan', 'form_id');
    }

    public function last_reject_log(){
        return $this->hasOne('App\RejectLog', 'form_id')->latest();
    }
}

