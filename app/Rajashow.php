<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rajashow extends Model
{
    protected $table = 'rajashow';
    protected $primaryKey = 'rajashow_id';
    public $fillable = ['form_id'];

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
