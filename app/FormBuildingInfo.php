<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormBuildingInfo extends Model
{
    protected $table= 'form_buildinginfo';
    public $timestamps = false;
    protected $primaryKey = 'buildinginfo_id';
    public $fillable = ['form_id'];

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
