<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SampannaScan extends Model
{
    protected $table = 'sampanna_scan';
    public $timestamps = false;
    protected $primaryKey = 'sampanna_scan_id';

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
