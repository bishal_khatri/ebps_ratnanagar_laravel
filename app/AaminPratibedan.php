<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AaminPratibedan extends Model
{
    protected $table= 'aamin_pratibedan';
    public $timestamps = false;

    public function form(){
        return $this->belongsTo('App\Form');
    }
}
