<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFloorInfo extends Model
{
	 protected $table= 'form_floorinfo';
     public $timestamps = false;
    protected $primaryKey = 'floorinfo_id';
    public $fillable = ['form_id'];

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
