<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TippaniScan extends Model
{
    protected $table = 'tippani_scan';
    public $timestamps = false;

    public function form(){
        return $this->belongsTo('App\Form');
    }

}
