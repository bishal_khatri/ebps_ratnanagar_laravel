-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2019 at 11:40 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebps`
--

-- --------------------------------------------------------

--
-- Table structure for table `aamin_pratibedan`
--

CREATE TABLE `aamin_pratibedan` (
  `id` int(20) NOT NULL,
  `form_id` int(11) NOT NULL,
  `bato_category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_direction` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_front` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_back` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_left` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_right` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_purba` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_paschim` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_utar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_dakshin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_purba` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_paschim` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_utar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_dakshin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_width` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_field_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_width` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_field_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at_nepali` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `other` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hitension_wire_distance` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `electricity_volt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `river_side_bata_chodnu_parne_duri` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aamin_pratibedan_scan`
--

CREATE TABLE `aamin_pratibedan_scan` (
  `aamin_pratibedan_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `aamin_pratibedan_scan` varchar(100) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `code_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permissions`
--

INSERT INTO `auth_permissions` (`id`, `display_name`, `code_name`) VALUES
(1, 'Consultancy', 'consultancy'),
(2, 'Engineer', 'engineer'),
(3, 'Sub Engineer', 'sub-engineer'),
(4, 'Aamin', 'aamin'),
(5, 'Main Engineer', 'main-engineer'),
(6, 'administration', 'administration');

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission_user`
--

CREATE TABLE `auth_permission_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `permission` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission_user`
--

INSERT INTO `auth_permission_user` (`id`, `user_id`, `permission_id`, `permission`) VALUES
(1, 13, 6, 'administration'),
(4, 14, 2, 'engineer'),
(5, 15, 3, 'sub-engineer'),
(8, 21, 1, 'consultancy'),
(10, 23, 1, 'consultancy'),
(11, 32, 1, 'consultancy'),
(12, 26, 1, 'consultancy'),
(13, 20, 1, 'consultancy'),
(14, 16, 2, 'engineer'),
(15, 17, 3, 'sub-engineer'),
(16, 18, 4, 'aamin'),
(17, 19, 5, 'main-engineer'),
(18, 24, 1, 'consultancy'),
(19, 28, 1, 'consultancy'),
(20, 22, 1, 'consultancy'),
(21, 33, 1, 'consultancy'),
(24, 31, 1, 'consultancy'),
(25, 34, 4, 'aamin'),
(26, 35, 1, 'consultancy'),
(27, 36, 1, 'consultancy'),
(28, 37, 6, 'administration'),
(29, 38, 1, 'consultancy'),
(30, 39, 6, 'administration'),
(31, 40, 1, 'consultancy'),
(32, 41, 2, 'engineer'),
(33, 41, 1, 'consultancy'),
(34, 42, 1, 'consultancy'),
(35, 43, 1, 'consultancy'),
(36, 44, 1, 'consultancy');

-- --------------------------------------------------------

--
-- Table structure for table `dpc`
--

CREATE TABLE `dpc` (
  `dpc_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `building_category` varchar(255) NOT NULL,
  `building_structure` varchar(255) NOT NULL,
  `building_length` varchar(255) NOT NULL,
  `building_width` varchar(255) NOT NULL,
  `foundation_size` varchar(255) NOT NULL,
  `setback_purba` varchar(255) NOT NULL,
  `setback_pachim` varchar(255) NOT NULL,
  `setback_utar` varchar(255) NOT NULL,
  `setback_dakshin` varchar(255) NOT NULL,
  `mohoda_right` varchar(255) NOT NULL,
  `mohoda_left` varchar(255) NOT NULL,
  `strap_beam_swkrit_anusar` varchar(255) NOT NULL,
  `strap_beam` varchar(255) NOT NULL,
  `plinth_height` varchar(255) NOT NULL,
  `pillar_size` varchar(255) NOT NULL,
  `pillar_rod_size` varchar(255) NOT NULL,
  `pillar_rod_number` varchar(255) NOT NULL,
  `pillar_churi_size` varchar(255) NOT NULL,
  `tie_beam_size_swkrit_anusar` varchar(255) NOT NULL,
  `tie_beam_size` varchar(255) NOT NULL,
  `hodding_board_swkrit_anusar` varchar(255) NOT NULL,
  `hodding_board` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dpc_scan`
--

CREATE TABLE `dpc_scan` (
  `dpc_scan_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `dpc_scan` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `form_buildingfiles`
--

CREATE TABLE `form_buildingfiles` (
  `buildingfiles_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `lalpurja` varchar(255) NOT NULL,
  `citizenship` varchar(255) NOT NULL,
  `blueprint` varchar(255) NOT NULL,
  `tero_rashid` varchar(255) NOT NULL,
  `house_map` varchar(255) NOT NULL,
  `naamsari` varchar(255) NOT NULL,
  `ward_sifaris` varchar(255) DEFAULT NULL,
  `sampanna_patra` varchar(255) DEFAULT NULL,
  `other_document_1` varchar(255) DEFAULT NULL,
  `other_document_1_description` text CHARACTER SET utf8,
  `other_document_2` varchar(255) DEFAULT NULL,
  `other_document_2_description` text CHARACTER SET utf8,
  `other_document_3` varchar(255) DEFAULT NULL,
  `other_document_3_description` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_buildingfiles`
--

INSERT INTO `form_buildingfiles` (`buildingfiles_id`, `form_id`, `lalpurja`, `citizenship`, `blueprint`, `tero_rashid`, `house_map`, `naamsari`, `ward_sifaris`, `sampanna_patra`, `other_document_1`, `other_document_1_description`, `other_document_2`, `other_document_2_description`, `other_document_3`, `other_document_3_description`) VALUES
(1, 1, 'files/1/building_file/lalpurja/QCuvBGwu8PCdwyqcLHZsa3Q8MTbcZT8MvdvjMA2G.jpeg', 'files/1/building_file/citizenship/IqHFjqZhsWvKRrdsrmjieFq9V0euDWog6VLDtwjH.jpeg', 'files/1/building_file/blueprint/GVqVhw4HkQm0vH0Kxlo25IVCuQOcN3omgJa40VSh.jpeg', 'files/1/building_file/tero_rashid/mq7cxT0SJnzXdqJOVZhD2S7fIUjzbDwL1G90trg3.jpeg', 'files/1/building_file/house_map/7rnR1PeetB69iqQWQ4wIWhMUrL7mdZYK7OuFDsjt.pdf', '', 'files/1/building_file/ward_sifaris/U2ghDESDHdJ7cl5K3ArIhHzONsDyZNbA0DkofMxv.jpeg', NULL, 'files/1/building_file/other_document_1/uxcyyW3rl4xWnNRxF8P1GBUfGVG58yhMfSEgUoNb.pdf', 'STRUCTURAL REPORT', '', NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_buildinginfo`
--

CREATE TABLE `form_buildinginfo` (
  `buildinginfo_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `building_category` varchar(50) NOT NULL,
  `building_structure` varchar(50) NOT NULL,
  `plith_height` varchar(50) NOT NULL,
  `foundation_size` varchar(50) NOT NULL,
  `pillar_size` varchar(50) NOT NULL,
  `pillar_rod_size` varchar(50) NOT NULL,
  `pillar_rod_number` varchar(50) NOT NULL,
  `pillar_churi_size` varchar(50) NOT NULL,
  `left_right_chadne_duri` varchar(50) NOT NULL,
  `setback` varchar(50) NOT NULL,
  `east_border` varchar(50) NOT NULL,
  `west_border` varchar(50) NOT NULL,
  `north_border` varchar(50) NOT NULL,
  `south_border` varchar(50) NOT NULL,
  `building_length` varchar(50) NOT NULL,
  `building_breadth` varchar(20) NOT NULL,
  `building_height` varchar(50) NOT NULL,
  `number_of_room` varchar(50) NOT NULL,
  `number_of_door` varchar(50) NOT NULL,
  `number_of_window` varchar(50) NOT NULL,
  `number_of_satar` varchar(50) NOT NULL,
  `number_of_bathroom` varchar(50) NOT NULL,
  `number_of_channel_gate` varchar(50) NOT NULL,
  `septic_tank` varchar(50) NOT NULL,
  `roof` varchar(50) NOT NULL,
  `jodai` varchar(50) NOT NULL,
  `building_purpose` varchar(50) NOT NULL,
  `house_owner_name` varchar(255) NOT NULL,
  `citizenship_number` varchar(50) NOT NULL,
  `district` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `ground_area_coverage` varchar(255) NOT NULL,
  `floor_area_ratio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_buildinginfo`
--

INSERT INTO `form_buildinginfo` (`buildinginfo_id`, `form_id`, `building_category`, `building_structure`, `plith_height`, `foundation_size`, `pillar_size`, `pillar_rod_size`, `pillar_rod_number`, `pillar_churi_size`, `left_right_chadne_duri`, `setback`, `east_border`, `west_border`, `north_border`, `south_border`, `building_length`, `building_breadth`, `building_height`, `number_of_room`, `number_of_door`, `number_of_window`, `number_of_satar`, `number_of_bathroom`, `number_of_channel_gate`, `septic_tank`, `roof`, `jodai`, `building_purpose`, `house_owner_name`, `citizenship_number`, `district`, `phone`, `ground_area_coverage`, `floor_area_ratio`) VALUES
(1, 1, 'ख', 'फ्रेम', '२\'-६\"', '५\'-६\"*५\'-६\'\' *५\'-६\'\'', '१२\"*१२\"', '८-१६ यम यम /४-१२ यम यम + ४-१६ यम यम', '८', '८ यम यम', '१०\'', '२मी', 'सन्दिप  खनाल', 'साखा  पिच', 'लक्ष्मी आचार्य', 'अन्छुल  मिया', '४३\'-०\"', '३०\'-०\"', '२२\'-६\"', '१०', '१४', '१०', 'छैन', '२', 'छैन', 'सेफ्टी ट्यांकी', 'आरसिसि', 'सिमेन्ट', 'आवासीय', 'जलिल  मिया', '३४३०२५/११०', 'चितवन', '९८६५१७१२७२', '०.३५', '०.७०'),
(2, 2, 'ग', 'फ्रेम', '३\'-० \"', '५\'६ \"*५\'६ \"', '१२\"*१२\'\'', '४-१२,४ -१६ यम यम', '८', '८ यम यम', '६\'-४\'\'', '६ +२ मीटर', 'सुर्य लम्साल', 'ग्राबेल बाटो', 'कमला थापा', 'कमला  अधिकारी', '२८\'', '२७\'', '२३\'-० \"', '६', '१२', '८', 'छैन', '२', 'छैन', 'सेफ्टी ट्यांकी', 'आरसिसि', 'सिमेन्ट', 'आवासीय', 'सरिता पौडेल', '३५३०२० /२५०', 'चितवनसुर्य लम्साल', '९८४५१४०६८५', '०.२९', '०.५८');

-- --------------------------------------------------------

--
-- Table structure for table `form_floorinfo`
--

CREATE TABLE `form_floorinfo` (
  `no_of_floor` int(11) NOT NULL,
  `floorinfo_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `underground_floor_area` float NOT NULL,
  `ground_floor_area` float NOT NULL,
  `first_floor_area` float DEFAULT NULL,
  `second_floor_area` float DEFAULT NULL,
  `third_floor_area` float DEFAULT NULL,
  `fourth_floor_area` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_floorinfo`
--

INSERT INTO `form_floorinfo` (`no_of_floor`, `floorinfo_id`, `form_id`, `underground_floor_area`, `ground_floor_area`, `first_floor_area`, `second_floor_area`, `third_floor_area`, `fourth_floor_area`) VALUES
(2, 1, 1, 0, 1290, 1290, NULL, NULL, NULL),
(2, 2, 2, 0, 756, 756, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_landinfo`
--

CREATE TABLE `form_landinfo` (
  `landinfo_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `ward_number` varchar(255) NOT NULL,
  `sabik_ward_number` varchar(255) NOT NULL,
  `current_ward_number` varchar(255) NOT NULL,
  `kitta_number` varchar(255) NOT NULL,
  `sabik_kitta_number` varchar(255) NOT NULL,
  `field_area` varchar(255) NOT NULL,
  `village_name` varchar(255) NOT NULL,
  `sabik_kittama_ghar` varchar(255) NOT NULL,
  `haak_pugeko` varchar(255) NOT NULL,
  `jagga_mohada` varchar(255) NOT NULL,
  `electricity_line` varchar(255) NOT NULL,
  `river` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_landinfo`
--

INSERT INTO `form_landinfo` (`landinfo_id`, `form_id`, `ward_number`, `sabik_ward_number`, `current_ward_number`, `kitta_number`, `sabik_kitta_number`, `field_area`, `village_name`, `sabik_kittama_ghar`, `haak_pugeko`, `jagga_mohada`, `electricity_line`, `river`) VALUES
(1, 1, '१३', '९  ख', '१३', '१७४८,१७४७', 'पन्चकन्या -९ ख', '३३८.६ वर्ग.मि', 'साखा पिच बाटो', 'नभएको', 'पुस्तौनी', 'पक्षिम्', 'नभएको', 'नभएको'),
(2, 2, 'र न पा -१०', 'पन्चकन्य /६क', '१०', '२२९५', '२२९५', '०-०-१४', 'सिबालय  टोल', 'नभएको', 'राजिनामा', 'पच्छिम', 'नभएको', 'नभएको');

-- --------------------------------------------------------

--
-- Table structure for table `form_personalinfo`
--

CREATE TABLE `form_personalinfo` (
  `id` int(11) NOT NULL,
  `sambandha_ma` varchar(50) CHARACTER SET utf8 NOT NULL,
  `option_1` varchar(50) CHARACTER SET utf8 NOT NULL,
  `option_2` varchar(50) CHARACTER SET utf8 NOT NULL,
  `building_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `building_type_slug` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sambodhan` varchar(50) CHARACTER SET utf8 NOT NULL,
  `field_owner_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `field_owner_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `field_owner_age` varchar(50) CHARACTER SET utf8 NOT NULL,
  `field_owner_job` varchar(255) CHARACTER SET utf8 NOT NULL,
  `father_husband_option` varchar(50) CHARACTER SET utf8 NOT NULL,
  `father_husband_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `grandfather_option` varchar(50) CHARACTER SET utf8 NOT NULL,
  `grandfather_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `form_level` int(11) NOT NULL,
  `proceeded_to` varchar(20) NOT NULL,
  `is_rejected` int(11) NOT NULL,
  `rejected_message` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `rejected_by` varchar(11) DEFAULT NULL,
  `rejected_at` timestamp NULL DEFAULT NULL,
  `is_draft` int(11) NOT NULL,
  `is_completed` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_nepali` varchar(50) CHARACTER SET utf8 NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `darta_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_personalinfo`
--

INSERT INTO `form_personalinfo` (`id`, `sambandha_ma`, `option_1`, `option_2`, `building_type`, `building_type_slug`, `sambodhan`, `field_owner_name`, `field_owner_address`, `field_owner_age`, `field_owner_job`, `father_husband_option`, `father_husband_name`, `grandfather_option`, `grandfather_name`, `form_level`, `proceeded_to`, `is_rejected`, `rejected_message`, `rejected_by`, `rejected_at`, `is_draft`, `is_completed`, `created_by`, `created_at`, `created_at_nepali`, `updated_at`, `updated_by`, `darta_number`) VALUES
(1, 'मैले', 'छु', 'गर्दछु', 'नयाँ घर निर्माण', 'new_building', 'श्री', 'जलिल  मिया', 'र न पा १३', '५७', 'कृषि', 'बाबु', 'अशिन मिया', 'बाजे', 'वालु मिया', 1, 'engineer', 0, NULL, NULL, NULL, 0, 0, 31, '2019-06-03 07:05:33', 'सोम, जेठ २०, २०७६', '2019-06-03 07:21:27', NULL, '1930601195'),
(2, 'मैले', 'छु', 'गर्दछु', 'नयाँ घर निर्माण', 'new_building', 'सुश्री', 'सरिता पौडेल', 'र न पा-१०', '३५', 'जागिर', 'बाबु', 'रामचन्द्र पौडेल', 'बाजे', 'खगेशोर', 0, 'consultancy', 0, NULL, NULL, NULL, 1, 0, 22, '2019-06-04 09:09:31', 'मंगल, जेठ २१, २०७६', '2019-06-04 09:27:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_sampanna`
--

CREATE TABLE `form_sampanna` (
  `sampanna_id` int(20) NOT NULL,
  `form_id` int(11) NOT NULL,
  `field_area` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `site_plan_area` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ground_coverage` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ground_coverage_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ground_coverage_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ground_coverage_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `talla_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `talla_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `talla_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `height_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `height_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `height_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `floor_area_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `floor_area_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `floor_area_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `swikrit_vanda_badi_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `swikrit_vanda_badi_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `swikrit_vanda_badi_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `r_o_w_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `r_o_w_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `r_o_w_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `setback_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `setback_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `setback_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `electricity_line` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `electricity_line_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `electricity_line_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `electricity_line_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `river_side_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `river_side_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `river_side_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `entry_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `consultancy_post` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `consultancy_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ravi.tiwari2064@gmail.com', '$2y$10$FBhzmfyWUPokigpgU1lv1ePr6hHpYNzeMs/3Y7ibKpRWtWjh3BSrK', '2018-12-04 10:54:58'),
('amin@gmail.com', '$2y$10$nxxJy01s8zPZbn9VuyHyI.A25nCiYyXE4SWL7hLJ0UOvxBMMX7O3i', '2019-01-01 05:49:59'),
('competentengineering@gmail.com', '$2y$10$fMcDKonUIiuKNRW5Xtu/xOo3KsPoY1fAqFQid/ci4L.Fxe8.QeNtO', '2019-01-30 06:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `rajashow`
--

CREATE TABLE `rajashow` (
  `rajashow_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `underground_floor_rate` float NOT NULL,
  `underground_floor_amount` float NOT NULL,
  `ground_floor_rate` float DEFAULT NULL,
  `ground_floor_amount` float DEFAULT NULL,
  `first_floor_rate` float DEFAULT NULL,
  `first_floor_amount` float DEFAULT NULL,
  `second_floor_rate` float DEFAULT NULL,
  `second_floor_amount` float DEFAULT NULL,
  `third_floor_rate` float DEFAULT NULL,
  `third_floor_amount` float DEFAULT NULL,
  `fourth_floor_rate` float DEFAULT NULL,
  `fourth_floor_amount` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `other_amount` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at_nepali` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rajashow_scan`
--

CREATE TABLE `rajashow_scan` (
  `rajashow_scan_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `rajashow` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sampanna_scan`
--

CREATE TABLE `sampanna_scan` (
  `sampanna_scan_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `form` varchar(255) NOT NULL,
  `tippani` varchar(255) NOT NULL,
  `certificate` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sandhirsuchana`
--

CREATE TABLE `sandhirsuchana` (
  `id` int(11) NOT NULL,
  `sandhir_suchana` varchar(255) NOT NULL,
  `form_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tippani_scan`
--

CREATE TABLE `tippani_scan` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `asthai_scan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tippani_scan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_super` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultancy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultancy_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultancy_license` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultancy_license_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `contact`, `address`, `email`, `email_verified_at`, `password`, `last_login_at`, `created_by`, `is_super`, `remember_token`, `consultancy_name`, `consultancy_address`, `consultancy_license`, `consultancy_license_number`, `staff_id`, `extension_number`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Bishal', 'Khatri', '9842721343', 'Kathmandu', 'bishal.khatri343@gmail.com', NULL, '$2y$10$GL4it4AE1qmN2QFd2Qu3muLnvI3E/OxfJNz.cXlv7hzvRoeaJ1EbK', '2019-06-03 09:18:02', NULL, 1, '8QTVPBJ7rYtceLmaIDHmma4nDNDGL3H7sQdYogNJwjauSkbxa20RQBTkzepS', '', '', '', '', '', '', 'upload_image/1/e2BViS6P19XNUcRtTJxz95XxDFSItydzPlIaA2BX.png', '2018-09-10 23:29:31', '2019-06-03 09:18:02'),
(13, 'Ravi', 'Tiwari', '0', 'Ratnanagar', 'ravi.tiwari2064@gmail.com', NULL, '$2y$10$aGv3x/VhyQj4Vps1UppzZOl7/MO3YtHkdn6/RmNCBTAjycwnSIbBi', '2019-04-04 06:06:47', NULL, 0, 'Y1k0N7clCu15N1pFdjKFodep6esE7x9hvhglst7KM5OHVoLiPeZhznAxKQqm', NULL, NULL, NULL, NULL, NULL, NULL, 'upload_image/13/IavtTRbnJVvEaSayV7g0tSCeOZBEYvSZzVhifMr4.png', '2018-12-04 04:50:15', '2019-04-04 06:06:47'),
(16, 'Namesh', 'Gurung', '9845633495', 'Bharatpur', 'engineer@gmail.com', NULL, '$2y$10$5XI/17LutIa7ZdLB1EmXnutbjpm2QiNx2XZnHDAdBicLIDP9ICpom', '2019-06-04 09:06:36', NULL, 0, '5ihK3twXrWqZcGTzUkmWal30uLPI5lrWoKFVEXCvAhYVk8W8R02eVWuWg8eM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-05 06:28:59', '2019-06-04 09:06:36'),
(17, 'Mohan', 'Timalsina', '9855060514', 'Ratnanagar', 'subengineer@gmail.com', NULL, '$2y$10$cFGjSsIidVq8xp2La4ayvu80JsgYGrt68ZuxR5Ip3zushRDYwx8O2', '2019-06-03 10:25:13', NULL, 0, '6TUncsGYe740GbvMxzqAOXV8oSdfASCF2CdMpDZVYVPvKkz24WApsertHvfH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-05 06:29:44', '2019-06-03 10:25:13'),
(19, 'Bijaya', 'Ranamagar', '9801101400', 'Kathmandu', 'mainengineer@gmail.com', NULL, '$2y$10$YkiJYQGthPRixkSisfxILe9I5Zym14QwN5K56wNaqqbLAu3tJhvX.', '2018-12-06 07:28:30', NULL, 0, 'f9D7DFELguqjT2cIA2aPlEqrHT4GZO0joPQRwWeluqiJP0kGZdiWbKqPrpcD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-05 06:32:49', '2018-12-06 07:28:30'),
(20, 'Trisha', 'Engineering Consultancy', '9855072699', 'Bharatpur', 'trishaeng2018@gmail.com', NULL, '$2y$10$u7UTNqPaGSxoaSFsE90AROyx64JKeYc4qpQVAnljEB07iwQyCW/Cq', '2019-04-03 05:50:59', NULL, 0, 'bfSbeT1kmidfwZXl0IhCWpjqFzKDhNQmye4CIVdqh3qD5fpepCGHmlzDxpi5', NULL, NULL, NULL, NULL, NULL, NULL, 'upload_image/20/fs1QZXlXY31ji0oizBoYyNpEiXmYHSuKyyVwhszo.png', '2018-12-05 09:43:32', '2019-04-03 05:50:59'),
(21, 'Binay', 'Achary', '9845711238', 'Ratnanagar', 'unitedengineering@gmail.com', NULL, '$2y$10$F.WzJ7mU25jdjSSx0jsvmu1vy.kgrTQijDcUigcTpGKgQ8M4LCfLS', '2019-04-04 05:35:09', NULL, 0, 'xodKo5R4poVPa9xSidlgSoCDRulAWjN798qIfusffnB3CegGtreeAXd5N4Sl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:01:52', '2019-04-04 05:35:09'),
(22, 'sesmic', 'Engineering consultancy', '9841811995', 'Ratnanagar', 'sesmicengineering@gmail.com', NULL, '$2y$10$7ovKaDFKDZU5sf2RazL4nuI0WeIvi3t6q7Uz6TmiKwvhHeYCVqzNq', '2019-06-04 09:08:10', NULL, 0, '38FPlEPKd1Fxw5hazyVptTC0fBC3H1qIk1fcdmo6lgIZZeUEnxjMQsWqGBh0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:03:52', '2019-06-04 09:08:10'),
(23, 'A.s', 'Engineering Cosultant', '9845046361', 'Ratnanagar-2', 'asengineering@gmail.com', NULL, '$2y$10$7.C8OfUn1XWGLAePyAy4mOL7pPQyLRU8.RkJ0oacUwIv7FI31shdC', '2019-02-20 08:22:08', NULL, 0, '93iEfXt45X8lhYqwWY9o1neRqHuxnm1iKoHrHpRszWgOIRu72he5ZchDiAfA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:05:27', '2019-02-20 08:22:08'),
(24, 'Image', 'Engineering Consultancy', '9855062992', 'Ratnanagar-1', 'imageengineering@gmail.com', NULL, '$2y$10$QmBRmDdQG1rPInwB9BMe3eb8KajsQZ7cvGV863ti/RKpdgUd5Etvy', '2019-04-04 09:04:09', NULL, 0, 'R5lMOc2IGqYx89XBcjnQl41uHM6rqIM2sDplzBjwWrMgPM2oK1rxujuTksMR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:06:37', '2019-04-04 09:04:09'),
(26, 'Samrachana', 'Engineering Consultancy', '9855080657', 'Ratnanagar-16', 'samrachanaengineering@gmail.com', NULL, '$2y$10$1dnTM4Og74IcDFQJ9hXHi.UaPXcWCdozQHVILViZSwId9ALYAZqm.', '2019-03-22 07:36:06', NULL, 0, 'hFgCvOPO2eafVxaMkoxFNKYOtKppj4gfHh45Y3lDCTCV7krBFiuuz3JTCE5m', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:11:04', '2019-03-22 07:36:06'),
(27, 'Comat', 'Engineering Consultancy', '9855080005', 'Khairani-8', 'comatengineering@gmail.com', NULL, '$2y$10$tHstMaaq.SYV03GNBaW8becPGYnmIH6sgfgVeo18vB.7f5CIvjcIW', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:12:14', '2018-12-06 07:12:14'),
(28, 'Devine', 'Engineering Consultancy', '9855080382', 'Ratnanagar-1', 'devineengineering@gmail.com', NULL, '$2y$10$JYcPdMCvjqWqQObVgEO6uOZcj1jSUUMe.ispSdexwtKJDFpXLlyN6', '2019-04-01 05:06:25', NULL, 0, 'uECdwP5yl4cDtzdMTX1o4jPbAcIDvgYsmAudwLa1gVvdksEydGekuMtDNRwQ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:13:22', '2019-04-01 05:06:25'),
(29, 'lofty', 'developers', '9851170614', 'Kathmandu', 'loftyengineering@gmail.com', NULL, '$2y$10$8dJoSuPX.27ICWjdLRduPOaqsmM1I6qMd5sy3q5/F7FQPATBygg6C', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:14:19', '2018-12-06 07:14:19'),
(30, 'Barsha', 'Engineering Consultancy', '056527597', 'Bharatpur-12', 'barshaengineering@gmail.com', NULL, '$2y$10$bjsA/BtNRHm2pBLF5zU/w.i0KlFXcXXXYtXVHuSGxeYQNKwBz5SJ.', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:15:58', '2018-12-06 07:15:58'),
(31, 'Mb', 'Engineering Consultancy', '9845067592', 'Ratnanagar-14', 'mbengineering@gmail.com', NULL, '$2y$10$ccKtX6meqlguzBv5r89OEOmtwTWp/xNbskmgCIYKGIbPcvRdnh54.', '2019-06-03 06:31:39', NULL, 0, 'njbP2Kvy6iEPkGVSKHvjVqtsNx0SchMnE4EbiFvpK3Lfz8ql5ahEAu7X3iZQ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:17:07', '2019-06-03 06:31:39'),
(32, 'Salin', 'Engineering Consultancy', '123456', 'Ratnanagar-12', 'salinengineering@gmail.com', NULL, '$2y$10$UVGqaA.aI76n2RCUit5WwOCFqKBr04AwdZbgRfp.Edz/KYH8208qu', '2019-03-11 05:43:30', NULL, 0, '9V9vWDGkr2MvRou8Qog99bU1rzXv31mzk0q7Z8U8Qf10JRXFx07R0SWFbZvK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:18:41', '2019-03-11 05:43:30'),
(34, 'Krishna', 'Magar', '9855053277', 'Kalika', 'amin@gmail.com', NULL, '$2y$10$/bh0dc/gG0pGn5I6wGSUMu0OHm7RPBydLOycGLuoNvPpSuP9IUMcm', '2019-01-22 06:26:44', NULL, 0, 'TnauOG8V3SCNe1VUKwD8eZhMb86gjzKQwym9eHtrHIwmatsnpn1wSxn9IImh', NULL, NULL, NULL, NULL, NULL, NULL, 'files/avatar_file/c3Kx53w8d2qz3PTIo1jAR14EYKTgon14SSHLRubu.jpeg', '2019-01-01 05:57:04', '2019-01-22 06:26:44'),
(35, 'Ashish', 'Baral', '9851172674', 'Ratnanagar-10,Bhanuchowk', 'ratnanagarbuilders10@gmail.com', NULL, '$2y$10$8VX2fdcTdUOf9Mst/RXW8uLXwnZtCxnxCWRuUPKb1vsC/Uimwuthu', '2019-01-18 06:55:19', NULL, 0, NULL, 'Ratnanagar Builders and Engineering', 'Ratnanagar-1', '16628', '35', NULL, NULL, 'files/avatar_file/QkKGP36zv9rlHGsWETQS8zsiJB17EHrfIKNxL2WK.jpeg', '2019-01-11 06:50:06', '2019-01-18 06:55:19'),
(36, 'Binod', 'Badu', '9857039404', 'bharatpur-7', 'bb.thirdpolengineering@gmail.com', NULL, '$2y$10$ljbtckiar3cNYaP5JcwGt.jXOGtDV0i4Bg4E2TH2NaeY2FDXJdnHu', '2019-03-27 08:45:53', NULL, 0, 'Uq72A45zX3IdCaLMSLqwHPIHNhxtvQy2PYURjyaFZmh4j0OJt7tYVnN1oOBY', 'Third pole engineering consultancy', 'bharatpur-7', '14000', '029', NULL, NULL, 'files/avatar_file/lWWQikWDI5Xpr67eKF73O2GRqVNT6WTnBtGDpgvj.jpeg', '2019-01-18 07:02:42', '2019-03-27 08:45:53'),
(39, 'developer', 'account', '9843166988', 'thecho, kathmandu', 'lamaprabin05@gmail.com', NULL, '$2y$10$y9RQub.7WWD7w5nt9os2yOYjfkeQA9dMGED39Dx00471qeOCpJVRq', '2019-01-22 05:14:08', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-22 05:13:43', '2019-01-22 05:15:21'),
(40, 'Ramkrishna', 'Dhital', '9856042752', 'Ratnanagar-2', 'maxpvt2014@gmail.com', NULL, '$2y$10$GhLTFv6DxrOSZVoLfVEPge7CoYO496WUMOnuaZ9uglpGAMTFv8MiO', '2019-04-20 07:23:17', NULL, 0, '6RVMTBfAYx0MHWufvAGamkrtzYWcQgHWQHfmNlMGlT5d6fgJXBq2n0ywpZXa', 'Max Engineering', 'Ratnanagar-2', '7215', '1400', NULL, NULL, 'files/avatar_file/GEmngKZ8EIxF5GrUOivAorPF4jg1DQiEkXXq8So8.jpeg', '2019-01-22 05:20:15', '2019-04-20 07:23:17'),
(41, 'Kapil', 'Paneru', '9851190018', 'Bharatpur-9, sharadpur', 'cecnepal18@gmail.com', NULL, '$2y$10$4OZt2keJEJjoeB5fkCrThO41jPQiyXVyp.RDdVAOUCb0.2M2iaeOq', '2019-02-20 08:49:58', NULL, 0, '4plWrOCPAhk9NjF5hP8GA7Ex7PwdXiWqHI5fCF2Bds171f7bAE9cBBk9IGWL', 'competent engineering consultancy', 'Bharatpur-9', '184943', '184943', NULL, NULL, 'files/avatar_file/UBAExSakIqqUImT7KQqIollX6X3gQ5YPdLpx4rI1.jpeg', '2019-02-02 16:46:59', '2019-02-20 08:49:58'),
(42, 'Niranjan', 'Neupane', '9845417627', 'Ratnanagar-11', 'neupaneniranjan51@gmail.com', NULL, '$2y$10$LWqKzAaBBdLLZWqQwy8JtOcMIRxBI8iceP/AmN2rYR.9XwvFJgXWW', '2019-03-22 13:28:07', NULL, 0, NULL, 'Unique Creation Engineering Consultancy', 'Ratnanagar-11', '32', '15788 \'A\'', NULL, NULL, NULL, '2019-03-01 06:21:28', '2019-03-22 13:28:07'),
(43, 'Ashok', 'pokhrel', '9867534576', 'Ratnanagar-11', 'kumargeeree@gmail.com', NULL, '$2y$10$I/lSs9S375eC4pNpJBBZq.rSRPx0N7ih9Jdp3uTUwDJP97dFRfQoq', '2019-03-25 09:46:06', NULL, 0, NULL, 'Gudban Engineering research and multi disiplinary consultant', 'Ratnanagar-11', '2195', '119', NULL, NULL, NULL, '2019-03-24 07:16:09', '2019-03-25 09:46:06'),
(44, 'Bharat Kumar', 'Acharya', '9855060856', 'Bharatpur-10,Chitwan', 'acme.engineering2015@gmail.com', NULL, '$2y$10$vtKcqK7Rcgxs8Tzz36tFBOLGUKN0SeY3jAMCyNow/psMocPLl.JKe', '2019-04-05 09:55:33', NULL, 0, 'ewCzn489xtBsLm0OYTLb3m0PMe0Hwbz8P1CPsZhoeZtOM4BEo1AFkVh5V4Dv', 'Acme Engineering Consultancy', 'Bharatpur-10,Chiwtwan', '12', '1727  Civil A', NULL, NULL, NULL, '2019-04-02 08:28:07', '2019-04-05 09:55:33');

-- --------------------------------------------------------

--
-- Table structure for table `ward`
--

CREATE TABLE `ward` (
  `id` int(11) NOT NULL,
  `ward_number` varchar(5) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ward`
--

INSERT INTO `ward` (`id`, `ward_number`) VALUES
(1, '१'),
(2, '२'),
(3, '३'),
(4, '४'),
(5, '५'),
(6, '६'),
(7, '७'),
(8, '८'),
(9, '९'),
(10, '१०'),
(11, '११'),
(12, '१२'),
(13, '१३'),
(14, '१४'),
(15, '१५'),
(16, '१६');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aamin_pratibedan`
--
ALTER TABLE `aamin_pratibedan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aamin_pratibedan_scan`
--
ALTER TABLE `aamin_pratibedan_scan`
  ADD PRIMARY KEY (`aamin_pratibedan_id`);

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_permission_user`
--
ALTER TABLE `auth_permission_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dpc`
--
ALTER TABLE `dpc`
  ADD PRIMARY KEY (`dpc_id`);

--
-- Indexes for table `dpc_scan`
--
ALTER TABLE `dpc_scan`
  ADD PRIMARY KEY (`dpc_scan_id`);

--
-- Indexes for table `form_buildingfiles`
--
ALTER TABLE `form_buildingfiles`
  ADD PRIMARY KEY (`buildingfiles_id`);

--
-- Indexes for table `form_buildinginfo`
--
ALTER TABLE `form_buildinginfo`
  ADD PRIMARY KEY (`buildinginfo_id`);

--
-- Indexes for table `form_floorinfo`
--
ALTER TABLE `form_floorinfo`
  ADD PRIMARY KEY (`floorinfo_id`);

--
-- Indexes for table `form_landinfo`
--
ALTER TABLE `form_landinfo`
  ADD PRIMARY KEY (`landinfo_id`);

--
-- Indexes for table `form_personalinfo`
--
ALTER TABLE `form_personalinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_sampanna`
--
ALTER TABLE `form_sampanna`
  ADD PRIMARY KEY (`sampanna_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `rajashow`
--
ALTER TABLE `rajashow`
  ADD PRIMARY KEY (`rajashow_id`);

--
-- Indexes for table `rajashow_scan`
--
ALTER TABLE `rajashow_scan`
  ADD PRIMARY KEY (`rajashow_scan_id`);

--
-- Indexes for table `sampanna_scan`
--
ALTER TABLE `sampanna_scan`
  ADD PRIMARY KEY (`sampanna_scan_id`);

--
-- Indexes for table `sandhirsuchana`
--
ALTER TABLE `sandhirsuchana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tippani_scan`
--
ALTER TABLE `tippani_scan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `ward`
--
ALTER TABLE `ward`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aamin_pratibedan`
--
ALTER TABLE `aamin_pratibedan`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aamin_pratibedan_scan`
--
ALTER TABLE `aamin_pratibedan_scan`
  MODIFY `aamin_pratibedan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `auth_permission_user`
--
ALTER TABLE `auth_permission_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `dpc`
--
ALTER TABLE `dpc`
  MODIFY `dpc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dpc_scan`
--
ALTER TABLE `dpc_scan`
  MODIFY `dpc_scan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_buildingfiles`
--
ALTER TABLE `form_buildingfiles`
  MODIFY `buildingfiles_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `form_buildinginfo`
--
ALTER TABLE `form_buildinginfo`
  MODIFY `buildinginfo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `form_floorinfo`
--
ALTER TABLE `form_floorinfo`
  MODIFY `floorinfo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `form_landinfo`
--
ALTER TABLE `form_landinfo`
  MODIFY `landinfo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `form_personalinfo`
--
ALTER TABLE `form_personalinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `form_sampanna`
--
ALTER TABLE `form_sampanna`
  MODIFY `sampanna_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rajashow`
--
ALTER TABLE `rajashow`
  MODIFY `rajashow_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rajashow_scan`
--
ALTER TABLE `rajashow_scan`
  MODIFY `rajashow_scan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sampanna_scan`
--
ALTER TABLE `sampanna_scan`
  MODIFY `sampanna_scan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sandhirsuchana`
--
ALTER TABLE `sandhirsuchana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tippani_scan`
--
ALTER TABLE `tippani_scan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `ward`
--
ALTER TABLE `ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
