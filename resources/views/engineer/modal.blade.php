<div class="modal fade" id="reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="{{ route('engineer.rejectForm') }}" method="post">
				@csrf
				<input type="hidden" name="form_id" id="form_id">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel1">अस्वीकृत गर्नुहोस </h4> </div>
				<div class="modal-body">
					<div class="form-group">
						<label for="message-text" class="control-label">अस्वीकृत गर्ने कारण </label>
						<textarea name="rejected_message" class="form-control" rows="10" id="message-text1" placeholder="अस्वीकृत गर्ने कारण लेखनुहोस|"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-info">Reject</button>
				</div>
			</form>
		</div>
	</div>
</div>

{{--file upload for sandhirsuchana--}}
<div class="modal fade" id="uploadSandhir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('engineer.sandhir_upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="form_id" id="form_id_sandhir">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">संधियारको फाईल अपलोड</h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="other_document_2" class="control-label">
                            संधियारको फाईल अपलोड गर्नुहोस|
                        </label>
                        <input type="file" name="sandhir_file" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                        @if($errors->has('sandhir_file'))
                            <span class="text-danger">{{ $errors->first('sandhir_file') }}</span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{--file upload for tippani_adesh --}}
<div class="modal fade" id="uploadTippani" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('engineer.tippani_upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="form_id" id="form_id_tippani">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">रजशोव  फाईल अपलोड</h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="other_document_2" class="control-label">
                            टिप्पणी आदेश
                        </label>
                        <input type="file" name="tippani_scan" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                        @if($errors->has('tippani_scan'))
                            <span class="text-danger">{{ $errors->first('tippani_scan') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="other_document_2" class="control-label">
                            अस्थायी सूचना
                        </label>
                        <input type="file" name="asthai_scan" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                        @if($errors->has('asthai_scan'))
                            <span class="text-danger">{{ $errors->first('asthai_scan') }}</span>
                        @endif
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{--file upload for dpc certificate --}}
<div class="modal fade" id="uploadDpc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('engineer.dpc_upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="form_id" id="form_id_dpc">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >DPC  फाईल अपलोड</h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="dpc_scan" class="control-label">
                            टिप्पणी आदेश
                        </label>
                        <input type="file" name="dpc_scan" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                        @if($errors->has('dpc_scan'))
                            <span class="text-danger">{{ $errors->first('tippani_scan') }}</span>
                        @endif
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>


