@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/css/nepaliDatePicker.css') }}" >
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/dpc_print.css') }}">
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <style>
        .table>tbody>tr>td {
            padding: 5px 3px !important;
        }

    </style>
@endsection
@section('page_title')
    सँधियार सूचना
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>टिप्पणी र आदेश</h4>
@endsection

@section('content')
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        <!-- PRINT PAGE 1 START -->
                        <div class="row print-page break-page ">
                            <div class="col-md-12">
                                @include('layouts._partials.print-head')
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <h2 class="text-center hide-on-print">टिप्पणी र आदेश</h2>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="font-size:19px;text-indent:5% ;line-height:2.6; margin-top:25px; text-align:justify;">

                                        प्रस्तुत  विषयमा रत्ननगर नगरपालिका वडा नं
                                            <span class="border_line"> {{ $formData->landInfo->current_ward_number ?? 'n/a' }}</span>
                                            निवासी
                                            <span class="border_line">
                                                    {{ $formData->sambodhan ?? 'n/a' }} {{ $formData->field_owner_name ?? 'n/a' }}
                                                </span>
                                            को  यस नगरपालिका वडा नं
                                            <span class="border_line">{{ $formData->landInfo->current_ward_number ?? 'n/a' }}</span>
                                            को साबिक
                                            <span class="border_line">{{ $formData->landInfo->village_name ?? 'n/a' }}</span>
                                            गा.वि.स वडा नं
                                            <span class="border_line">{{ $formData->landInfo->sabik_ward_number ?? 'n/a' }}</span>
                                            को कि.नं
                                            <span class="border_line">{{ $formData->landInfo->sabik_kitta_number ?? 'n/a' }}</span>
                                            को ज.वि
                                            <span class="border_line">{{ $formData->landInfo->field_area ?? 'n/a' }}</span>
                                                को
                                            जग्गामा जग्गा विकाश तथा भवन संहिता २०६० र २०७२ बमोजिम
                                            <span class="border_line">{{ $formData->buildingInfo->building_category ?? 'n/a' }}</span>
                                            वर्ग भवन वा घरको दोश्रो चरण सम्बन्धमा प्राबिधिकबाट पेश
                                            गरेको प्रतिवेदन अनुसार भवन निर्माण गर्नको लागि स्थायी इजाजत पत्र (Superstructure) दिन मिल्ने भएकोले अवाश्यक निर्णयको लागि यो
                                            टिप्पणी पेश गरेको छु  |
                                            </p>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- PRINT PAGE 1 END -->

                        <!-- PRINT PAGE 2 -->
                        <div class="row print-page " style="display:none" >
                            <div class="col-md-12">
                                @include('layouts._partials.print-head')

                                <table class="table table-bordered table-sm table-striped table-hover">
                                    <tr style="padding-top:40px;">
                                        <td colspan="9" class="text-center" style="margin:0px !important;font-size:18px;">
                                            भवन निर्माण स्थायी इजाजत पत्र  (प्लिन्थ लेभल Superstructure को लागि)
                                        </td>
                                    </tr>
                                    <tr >
                                        <td colspan="7">
                                            <p style="line-height:30px;font-size:12px;text-align:justify;font-weight:normal;text-indent:50px;">
                                                <span class="border_line">
								                     <strong>{{ $formData->sambodhan }}&nbsp;{{ $formData->field_owner_name }} </strong>
							                    </span>
                                                ले रत्ननगर नगरपालिका वडा नम्बर
                                                <span class="border_line"><strong>{{ $formData->landInfo->current_ward_number }}</strong></span>
                                                अन्तर्गत
                                                <span class="border_line"><strong>{{ $formData->landInfo->village_name }}</strong></span>
                                                टोल ...... मा रहेको साविक...... गा.वि.स. वडा नम्बर ......... कित्ता नम्बर ..... ज.वि. जम्मा ........
                                                को जग्गामा स्थानीय सरकार संकलन ऐन, नियम अनुसार नक्सा पास प्रक्रिया पुरा भैसकेको हुदा यसको पछिल्लो पानामा उल्लेखित शर्तहरु र
                                                यसै साथ दिइएको स्वीकृत नक्सा बमोजिम  <span class="border_line"><strong>{{ $formData->building_type }}</strong> </span> निर्माण गर्न नक्सा पास भएकोले
                                                यो प्रमाण-पत्र दिइएको छ | "जग्गा विकास तथा भवन मापदण्ड - २०६४ तथा राष्ट्रिय भवन संहिता -२०६० " बमोजिम निर्माण कार्य गर्नुहोला | स्वीकृत भएको नक्शा बमोजिम निर्माण कार्य गरिसकेको  पछि
                                                "निर्माण सम्पन्न प्रमाण पत्र "  अनिवार्य लिनुपर्ने छ |
                                            </p>
                                        </td>
                                    </tr>




                                </table>

                                <table class="table table1 table-bordered table-sm table-striped table-hover">
                                    <tr>
                                        <td colspan="9" class="text-center font-weight-bold">
                                            निर्माण स्वीकृत भएको विवरण
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" class="font-weight-bold">
                                            जग्गा विकास तथा भवन मापदण्ड - २०६४
                                        </td>
                                    </tr>
                                    <tr class="text-center" >
                                        <th style="width: 10px;">क्र.स</th>
                                        <th style="width:auto;">विवरण</th>
                                        <th style="width:auto;">स्वीकृत अनुसार</th>
                                        <th ></th>
                                        <th style="width:1px;">क्र.स</th>
                                        <th style="width:auto;">विवरण</th>
                                        <th style="width:100px;">स्वीकृत अनुसार</th>
                                    </tr>

                                    <tr>
                                        <td>१.</td>
                                        <td>लम्बाई (रनिंग फिट)</td>
                                        <td>{{ $formData->aaminPratibedan->napi_length }}&nbsp;फिट</td>
                                        <td></td>
                                        <td>८.</td>
                                        <td>प्लिन्थ एरिया (वर्ग फीट)</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>२.</td>
                                        <td>चौडाई (रनिंग फिट)</td>
                                        <td>{{ $formData->aaminPratibedan->napi_width }} &nbsp;फिट</td>
                                        <td></td>
                                        <td>(क).</td>
                                        <td> बेसमेन्ट</td>
                                        <td>{{ $formData->floorInfo->underground_floor_area }} &nbsp;sq.ft</td>

                                    </tr>

                                    <tr>
                                        <td>३.</td>
                                        <td>कुल उचाई (रनिंग फिट)</td>
                                        <td>{{ $formData->buildingInfo->building_height }}&nbsp;फिट</td>
                                        <td></td>
                                        <td>(ख).</td>
                                        <td>जमिन तल्ला</td>
                                        <td>{{ $formData->floorInfo->ground_floor_area }}&nbsp;sq.ft</td>
                                    </tr>

                                    <tr>
                                        <td>४.</td>
                                        <td>सेट व्यक</td>
                                        <td>{{ $formData->buildingInfo->setback }}</td>
                                        <td></td>
                                        <td>(ग).</td>
                                        <td>प्रथम तल्ला</td>
                                        <td>
                                        {{ (!empty($formData->floorInfo->first_floor_area)) ? $formData->floorInfo->first_floor_area : 0 }}&nbsp;sq.ft
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>५.</td>
                                        <td>सडकको चौडाई</td>
                                        <td>{{ $formData->aaminPratibedan->bato_length }}</td>
                                        <td></td>
                                        <td>(घ).</td>
                                        <td>दोश्रो तल्ला</td>
                                        <td>
                                            {{ (!empty($formData->floorInfo->second_floor_area)) ? $formData->floorInfo->second_floor_area : 0 }}&nbsp;sq.ft</td>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>६.</td>
                                        <td>कम्पाउन्ड वाल(रनिंग फिट)</td>
                                        <td>'n/a'</td>
                                        <td></td>
                                        <td>(ङ).</td>
                                        <td>तेश्रो तल्ला</td>
                                        <td>
                                            {{ (!empty($formData->floorInfo->third_floor_area)) ? $formData->floorInfo->third_floor_area : 0 }}&nbsp;sq.ft</td>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>७.</td>
                                        <td>भवनको प्रयोजन </td>
                                        <td>{{ $formData->buildingInfo->building_purpose }}</td>
                                        <td></td>
                                        <td>(च).</td>
                                        <td>चौथो तल्ला</td>
                                        <td>
                                            {{ (!empty($formData->floorInfo->fourth_floor_area)) ? $formData->floorInfo->fourth_floor_area : 0 }}&nbsp;sq.ft</td>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="7" class="font-weight-bold">
                                        राष्ट्रिय भवन संहिता - २०६०
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>क्र.स</td>
                                        <td>विवरण</td>
                                        <td>स्वीकृत अनुसार</td>
                                        <td></td>
                                        <td>क्र.स</td>
                                        <td>विवरण</td>
                                        <td>स्वीकृत अनुसार</td>
                                    </tr>

                                    <tr>
                                        <td>१.</td>
                                        <td>भवनको बर्गिकरण</td>
                                        <td>{{ $formData->buildingInfo->building_category }}</td>
                                        <td></td>
                                        <td>४.</td>
                                        <td>पिलरको साईज़ (इन्च)</td>
                                        <td>{{ $formData->buildingInfo->pillar_size }}&nbsp;इन्च</td>
                                    </tr>

                                    <tr>
                                        <td>२.</td>
                                        <td>तल्ला संख्या</td>
                                        <td>{{ $formData->floorInfo->no_of_floor }}&nbsp;तल्ला</td>
                                        <td></td>
                                        <td>५.</td>
                                        <td>पिलरमा प्रयोग गर्ने डण्डीको साईज़ / संख्या</td>
                                        <td>{{ $formData->buildingInfo->pillar_rod_size }}&nbsp;एम.एम - {{ $formData->buildingInfo->pillar_rod_number }}</td>
                                    </tr>

                                    <tr>
                                        <td>३.</td>
                                        <td>भवनको स्ट्रक्चर सिस्टम</td>
                                        <td>{{ $formData->buildingInfo->building_structure }}</td>
                                        <td></td>
                                        <td>६.</td>
                                        <td>कंक्रिट ब्याण्डहरु</td>
                                        <td>'n/a'</td>
                                    </tr>

                                    <tr>
                                        <td colspan="7">
                                            - नक्शा स्वीकृत अगावै निर्माण कार्य भएको भए सो को विवरण
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="7">
                                            - यसमा नक्शा पास दस्तुर जम्मा रू ...............................................र.नं.................................<br>
                                            मिति ............................................................को रसिद बाट प्राप्त भइसकेको छ |
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="7">
                                            - स्वकृत नक्सा अनुसार हाल.........................................तल्ला निर्माण गरिन्छ | <br><br>
                                            भविष्यमा मापदण्ड अनुसार थप ....................................तल्ला निर्माण गर्न सकिन्छ |
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="7" class="text-center">
                                            आफ्नो घर आगाडि न्यूनतम दुइ रुख रोप्न प्रतिवद्धतामा  हस्ताक्षर गर्दछु
                                        </td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td >................................</td>
                                        <td class="text-center" colspan="2">
                                            .........................................<br>पेश गर्ने
                                        </td>
                                        <td class="text-center" colspan="2">
                                            ......................................<br>जाँच गर्ने
                                        </td>
                                        <td class="text-center"  colspan="2">
                                            ........................................<br>स्वीकृत गर्ने
                                        </td>

                                    </tr>

                                        </tbody>
                                </table>


                            </div>
                        </div>
                        <!-- PRINT PAGE 2 END -->


                        <div class="row">
                            <div class="col-md-offset-5 col-md-12">
                                <a class="btn btn-success print-window hide-on-print"><i class="fa fa-print"></i>&nbsp;Print</a>
                                <a data-toggle="modal" data-id="{{ $formData->id }}" data-target="#uploadDpc" class="btn btn-primary hide-on-print">Upload</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('engineer.modal')
@endsection

@section('scripts')
    <!-- this should go after your </body> -->
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <script>

        $("#clear-bth").on("click", function(event) {
            $(".bod-picker").val('');
        });

        $('.print-window').click(function() {

            window.print();
        });

        $('#print-date').on('blur',function() {
            $("#msg").hide();
        });

        $(document).ready(function() {
            $('.dropify').dropify();

        });

        $('#uploadDpc').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#form_id_dpc").val(id);
        });
    </script>

@endsection
