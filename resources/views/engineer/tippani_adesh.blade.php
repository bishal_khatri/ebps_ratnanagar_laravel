@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/css/nepaliDatePicker.css') }}" >
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/sandhir_print.css') }}">
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <style>
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 5px 5px;
        }
    </style>
@endsection
@section('page_title')
    सँधियार सूचना
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>टिप्पणी र आदेश</h4>
@endsection

@section('content')
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        <!-- PRINT PAGE 1 START -->
                        <div class="row print-page break-page">
                            <div class="col-md-12">
                                @include('layouts._partials.print-head')
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <h2 class="text-center hide-on-print">टिप्पणी र आदेश</h2>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <h3 class="text-center hide-on-print "><b>नक्सा पास स्वीकृत सम्बन्धमा</b></h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="font-size:19px;text-indent:5% ;line-height:2.6; text-align: justify;">
                                            रत्ननगर नगरपालिका वडा नं
                                            <span class="border_line"> {{ $formData->landInfo->current_ward_number ?? 'n/a' }}</span>
                                            बस्ने
                                            <span class="border_line">
                                                    {{ $formData->sambodhan ?? 'n/a' }} {{ $formData->field_owner_name ?? 'n/a' }}
                                                </span>
                                            को  यस रत्ननगर न.पा वडा नं
                                            <span class="border_line">{{ $formData->landInfo->current_ward_number ?? 'n/a' }}</span>
                                            साविक
                                            <span class="border_line">{{ $formData->landInfo->sabik_ward_number ?? 'n/a' }}</span>
                                            कित्ता नं
                                            <span class="border_line">{{ $formData->landInfo->kitta_number ?? 'n/a' }}</span>
                                            ज.वि जग्गा.....................को
                                            पूर्व
                                            <span class="border_line">{{ $formData->buildingInfo->east_border ?? 'n/a' }}</span>
                                            पश्चिम
                                            <span class="border_line">{{ $formData->buildingInfo->west_border ?? 'n/a' }}</span>
                                            उत्तर
                                            <span class="border_line">{{ $formData->buildingInfo->north_border ?? 'n/a' }}</span>
                                            दक्षिण
                                            <span class="border_line">{{ $formData->buildingInfo->south_border ?? 'n/a' }}</span>
                                            यति चार किल्ला भित्रको जग्गामा लम्बाई
                                            <span class="border_line">{{ $formData->buildingInfo->building_length ?? 'n/a' }}</span>
                                            चौडाई
                                            <span class="border_line">{{ $formData->buildingInfo->building_breadth ?? 'n/a' }}</span>
                                            उचाई
                                            <span class="border_line">{{ $formData->buildingInfo->building_height ?? 'n/a' }}</span>
                                            को
                                            <span class="border_line">{{ $formData->building_type ?? 'n/a' }}</span>
                                            बनाउनको लागि जग्गाको ज.ध प्रमाणपत्रको प्रतिलिपि, नागरिता प्रमाण पत्रको प्रतिलिपि, जग्गाको  नक्शा  र चर्पी, सेफ्टी टंकी, सकपिट समेत देखिने गरी नक्सा पेश गर्नुभएकोमा मिति.....................................मा प्रकाशित र मिति.............................मा सम्बन्धित ठाउँमा गै सर्जमिन /मुचुल्का समेत भई आएकोमा कोही कसैको पनि खिचोलो  सन्धीसर्पन सम्बन्धी निवेदन पर्न नआएको हुँदा सो................................................................को नक्सा पास स्वीकृत गरिदिन उचित देखिएकोले निर्णयार्थ श्रीमान्मा पेश गरेको छु | जो आदेश |    <br><br><br>
                                        </p>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                        <!-- PRINT PAGE 1 END -->
                        
                        <!-- PRINT PAGE 2 START -->
                        <div class="row print-page " style="display:none;">
                            <div class="col-md-12">
                                @include('layouts._partials.print-head')
                                <table class="table table-bordered table-sm table-striped table-hover table-responsive">
                                    <tr class="d-print-table-row">
                                        <td style="font-size:19px;padding-top:13px;padding-bottom:3px;" colspan="6" class="text-center">
                                            प्लिन्थ लेभलसम्म निर्माण कार्यको ईजाजत पत्र
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <th colspan="6">
                                            <p style="line-height:35px;font-size:19px;text-align:justify;text-indent:50px;">
                                                तपाई
                                                <span style="font-weight: bold;">
								        {{ $formData->sambodhan ?? 'n/a' }}&nbsp;{{ $formData->field_owner_name ?? 'n/a' }}
							                </span>
                                                ले रनगर नगरपािलका वडा नम्बर हालको
                                                <span style="font-weight: bold;">{{ $formData->landInfo->current_ward_number ?? 'n/a' }}</span>
                                                अन्तर्गत
                                                <span style="font-weight: bold;">{{ $formData->landInfo->village_name ?? 'n/a' }} </span>
                                                टोलमा रहेको कित्ता नम्बर
                                                <span style="font-weight: bold;">{{ $formData->landInfo->kitta_number ?? 'n/a' }}</span>
                                                को क्षेत्रफल
                                                <span style="font-weight: bold;">{{ $formData->landInfo->field_area ?? 'n/a' }}</span>
                                                जग्गामा भवन निर्माण स्वकृतिको लागि मिति
                                                <span style="font-weight: bold;"> {{ $formData->created_at_nepali ?? 'n/a' }} </span> मा दरखास्त सहित नक्शा पेश गर्नुभएकोमा
                                                स्थानीय स्वायत्त शासन ऐन २०५५ को दफा १५०,१५२,१५३,१५५ अनुसार प्रक्रिया पुरा भइ यस कार्यालयको
                                                मिति.........................................को निर्णय बमोजिम प्रथम चरणमा "जग्गा विकास तथा भवन मापदण्ड-२०६४ तथा
                                                राष्ट्रिय भवन संहिता - २०६० " बमोजिम निम्न अनुसार डी.पी.सी लेभल सम्म मात्र निर्माण कार्य गर्नुहोला | डी.पी.सी सम्मको निर्माण कार्य सकिएपछि सबै भन्दा
                                                माथिको स्वकृति (भवन निर्माण स्थायी पत्र ) को लागि कन्सलटेन्ट / इन्जिनीरबाट डी.पी.सी निर्माण कार्यको फिल्ड प्रतिवेदन लिनु भै उपस्थितहुन जानकारी गराइन्छ  |
                                        </th>
                                    </tr>
                                </table>
                                
                                <table class="table table-bordered table-sm table-striped table-hover table-responsive">
                                    <tr>
                                        <td colspan="6" class="text-center font-weight-bold" style="font-size:19px;padding-top:13px;padding-bottom:3px;">
                                            निर्माण स्वीकृत भएको विवरण
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="font-size:19px;padding-top:13px;padding-bottom:3px;">
                                            जग्गा विकास तथा भवन मापदण्ड
                                        </td>
                                    </tr>
                                </table>
                                
                                <table class="table table-bordered table-sm table-striped table-hover table-responsive">
                                    <tr class="text-center">
                                        <td>क्र.सं</td>
                                        <td>विवरण</td>
                                        <td>स्वीकृत अनुसार</td>
                                        <td>क्र.सं</td>
                                        <td>विवरण</td>
                                        <td>स्वीकृत अनुसार</td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <td>१.</td>
                                        <td>लम्बाई </td>
                                        <td>{{ $formData->buildingInfo->building_length ?? 'n/a' }}&nbsp;फिट</td>
                                        <td>४.</td>
                                        <td>दायाँ / बायाँ छोड्ने दुरी (रनिङ फिट)</td>
                                        <td>{{ $formData->buildingInfo->left_right_chadne_duri ?? 'n/a' }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>२.</td>
                                        <td>चौडाई</td>
                                        <td>{{ $formData->buildingInfo->building_breadth ?? 'n/a' }}&nbsp;फिट</td>
                                        <td>५.</td>
                                        <td>प्लिन्थ लेभलको उचाई</td>
                                        <td>{{ $formData->buildingInfo->plith_height ?? 'n/a' }}</td>
                                    </tr>
                                    <tr>
                                        <td>३.</td>
                                        <td>सेट ब्यक (सडकको चौडाई सहित)</td>
                                        <td>
                                            पूर्व : {{ $formData->aaminPratibedan->chadnu_parne_setback_purba ?? 'n/a' }}
                                            पश्चिम : {{ $formData->aaminPratibedan->chadnu_parne_setback_paschim ?? 'n/a' }}
                                            उत्तर : {{ $formData->aaminPratibedan->chadnu_parne_setback_utar ?? 'n/a' }}
                                            दक्षिण : {{ $formData->aaminPratibedan->chadnu_parne_setback_dakshin ?? 'n/a' }}
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-sm table-striped table-hover table-responsive">
                                    
                                    <tr>
                                        <td colspan="6" style="font-size:19px;padding-top:13px;padding-bottom:3px;">
                                            राष्ट्रिय भवन संहिता
                                        </td>
                                    </tr>
                                    <tr class="text-center">
                                        <td>क्र.सं</td>
                                        <td>विवरण</td>
                                        <td>स्वीकृत अनुसार</td>
                                        <td>क्र.सं</td>
                                        <td>विवरण</td>
                                        <td>स्वीकृत अनुसार</td>
                                    </tr>
                                    <tr>
                                        <td>१.</td>
                                        <td>भवनको बर्गिकरण</td>
                                        <td>{{ $formData->buildingInfo->building_category ?? 'n/a' }}</td>
                                        <td>४.</td>
                                        <td>पिलरको साईज़ (इन्च)</td>
                                        <td>{{ $formData->buildingInfo->pillar_size ?? 'n/a' }}</td>
                                    </tr>
                                    <tr>
                                        <td>२.</td>
                                        <td>भवन स्ट्रक्चर</td>
                                        <td>{{ $formData->buildingInfo->building_structure ?? 'n/a' }}</td>
                                        <td>५.</td>
                                        <td>पिलरमा प्रयोग गर्ने डण्डी को साईज़</td>
                                        <td>{{ $formData->buildingInfo->pillar_rod_size ?? 'n/a' }}</td>
                                    </tr>
                                    <tr>
                                        <td>३.</td>
                                        <td>जगको साईज़ (फिट)</td>
                                        <td>{{ $formData->buildingInfo->foundation_size ?? 'n/a' }}</td>
                                        <td>६.</td>
                                        <td>पिलरमा प्रयोग गर्ने डण्डी को संख्या</td>
                                        <td>{{ $formData->buildingInfo->pillar_rod_number ?? 'n/a' }}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>७.</td>
                                        <td>पिलरको चुरी साईज़</td>
                                        <td>{{ $formData->buildingInfo->pillar_churi_size ?? 'n/a' }}</td>
                                    </tr>
                                    <tr>
                                        <!-- <td style="padding-top:30px;">
											__________________
											<br>
											पेशगर्ने फाँटवाला
										</td>
										<td style="padding-top:30px;">
											_________________________________
											<br>स्थलगत निरीक्षक&nbsp;(सव इन्जिनियर)
										</td>
										<td style="padding-top:30px;">
											_____________________________
											<br>जाँच गर्ने &nbsp;(इन्जिनियर)
										</td> -->
                                        <td colspan="4">
                                            बोधार्थ <br>
                                            भू-कम्प सुरक्षा उपशाखा , र.न.प कार्यालय
                                        </td>
                                        
                                        <td style="padding-top:50px;">
                                            _______________
                                            <!-- <br>सिफारिस गर्ने&nbsp;(महाशाखा प्रमुख) -->
                                        </td>
                                        <td style="padding-top:50px;">
                                            _______________
                                            <!-- <br>सिफारिस गर्ने&nbsp;(महाशाखा प्रमुख) -->
                                        </td>
                                </table>
                            
                            </div>
                        </div>
                        <!-- PRINT PAGE 2 END -->
                        
                        <div class="row pull-right">
                            <div class="col-md-12">
                                <a class="btn btn-success print-window hide-on-print"><i class="fa fa-print"></i>&nbsp;Print</a>
                                <a data-toggle="modal" data-id="{{ $formData->id }}" data-target="#uploadTippani" class="btn btn-primary hide-on-print">Upload</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
    @include('engineer.modal')
@endsection

@section('scripts')
    <!-- this should go after your </body> -->
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'assets/js/nepaliDatePicker.js') }}" ></script>
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <script>
        $(".bod-picker").nepaliDatePicker({
            dateFormat: "%D, %M %d, %y",
            closeOnDateSelect: true
        });

        $("#clear-bth").on("click", function(event) {
            $(".bod-picker").val('');
        });

        $('.print-window').click(function() {

            window.print();

            // var print_date = $("#print-date").val();
            // if (print_date.length==0){
            //     $("#msg").show();
            // }
            // else{
            //     window.print();
            // }
        });

        $('#print-date').on('blur',function() {
            $("#msg").hide();
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });

        $('#uploadTippani').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#form_id_tippani").val(id);
        });
    </script>

@endsection
