@extends('layouts.app')
@section('css')
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/css/nepaliDatePicker.css') }}" >
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/sandhir_print.css') }}">
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@endsection
@section('page_title')
	सँधियार सूचना
@endsection
@section('right_button')
	<a class="btn btn-info pull-right" href="{{ URL::previous() }}" style="margin-top: 3px;">Back</a>
@stop
@section('content-title')
	<h4>सँधियार सूचना</h4>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12 col-lg-12 col-sm-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-12">
						<!-- PRINT PAGE 1 START -->
						<div class="row print-page break-page">
							<div class="col-md-12">
								@include('layouts._partials.print-head')
								<div class="row">
									<div class="col-md-12 col-lg-12 col-sm-12">
										<h2 class="text-center hide-on-print">सँधियार सूचना</h2>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6">
										<p>पत्र संख्या : यो श.वि.शा</p>
										<p>चलानी नम्बर :</p>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 text-right">
										<p class="pull-right sandhir_date">मिति: <input style="border: none !important;" type="text" id="print-date" name="date" class="bod-picker" placeholder="मिति चयन गर्नुहोस ">
											<button id="clear-bth hide-on-print" onclick="" class="btn btn-default btn-sm hide-on-print">Clear</button>
											<br>
											<span class="text-danger text-center hide-on-print pull-left" style="display: none;" id="msg">मिति चयन गर्नुहोस</span>
										</p>
									</div>
									<div class="row">
										<div class="col-md-12 col-lg-12 col-sm-12 text-center " >
											<h3 class="sandhir_title">{{ $fileData->current_ward_number ?? 'n/a' }} नं. वडा कार्यालय</h3>
											<h4>रत्ननगर, चितवन, ३. नं प्रदेश, नेपाल</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 text-center">
											<h3>बिषय : १५ दिने सूचना टाँस सम्बन्धमा</h3>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<p style="text-indent: 5%; line-height: 2.6;text-align: justify;">यस रत्ननगर न.पा वडा नं {{ $fileData->current_ward_number ?? 'n/a' }} बस्ने
                                                {{ $fileData->sambodhan ?? 'n/a' }}&nbsp;{{ $fileData->field_owner_name ?? 'n/a' }} ले ए वार्ड नं
                                                {{ $fileData->current_ward_number ?? 'n/a' }} कित्ता नं {{ $fileData->kitta_number ?? 'n/a' }} मा
                                                {{ $fileData->building_type ?? 'n/a' }}
                                                बनाउन नक्सा पास स्वीकृत लागि दरखास्त पर्न आएकोले सो समन्धी प्रकासित १५ दिने सूचना यसैसाथ संलग्न छ |
												उक्त सूचना त्यस वडा समितिको कार्यालय र घर निर्माण स्थल मा टाँस गरि सोको मुचुल्का पठाइदिनु हुन अनुरोध गर्दछु |
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- PRINT PAGE 1 END -->

						<!-- PRINT PAGE 2 START -->
						<div class="row print-page break-page" style="display: none;">
							<div class="col-md-12">
								@include('layouts._partials.print-head')
								<div class="row">
									<div class="row">
										<div class="col-md-12 col-lg-12 col-sm-12 text-center " >
											<h3 class="sandhir_title">{{ $fileData->current_ward_number ?? 'n/a' }} नं. वडा कार्यालय</h3>
											<h4>रत्ननगर, चितवन, ३. नं प्रदेश, नेपाल</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<p style="text-indent: 5%; line-height: 2.6;text-align: justify;">
												<span>{{ $fileData->village_name ?? 'n/a' }}</span>&nbsp;बस्ने
												<span>
										            {{ $fileData->sambodhan ?? 'n/a' }}&nbsp;{{ $fileData->field_owner_name ?? 'n/a' }}
												</span>
												ले रत्ननगर नगरपालिका वडा नं
												<span>{{ $fileData->current_ward_number ?? 'n/a' }}</span>
												को साबिक
												<span>{{ $fileData->current_ward_number ?? 'n/a' }}</span>
												गा.प. वडा नं
												<span>{{ $fileData->current_ward_number ?? 'n/a' }}</span>
												कित्ता नं
												<span>{{ $fileData->kitta_number ?? 'n/a' }}</span>
												क्षेत्रफ़ल्
												<span>{{ $fileData->field_area ?? 'n/a' }}</span>
												को पूर्व
												<span>{{ $fileData->east_border ?? 'n/a' }}</span>
												पश्चिम
												<span>{{ $fileData->west_border ?? 'n/a' }}</span>
												उत्तर
												<span>{{ $fileData->north_border ?? 'n/a' }}</span>
												दक्षिण
												<span>{{ $fileData->south_border ?? 'n/a' }}</span>
												यति  चार किल्लाभित्रको जग्गामा तपसीलमा उल्लेख भए बमोजिमको निर्माण कार्य गर्नको लागि नक्सा पास गरी पाउ भनि मिति <span> {{ $fileData->created_at_nepali }} </span>
												मा निवेदन दिनुभएकोले सोको लागि स्थानीय संचालन एनको परिछेद ७ को दफा ३१ को 'क' अनुसार सो बनाउने घरको साँध संधियार को कोही कसैलाई पिरमर्का पर्ने भए आफुलाई मर्का परेको सबै विवरण खुलाई यो सुचना प्रकाशित भेएको मितिले १५ दिनभित्र यस नगरपालिका कार्यालयमा उजुर बाजुर गर्नुहुन यो सुचना प्रकाशित गरिएको छ | म्यादभित्र पर्न नआएका उजुरप्रति कुनै कारबाही गरिने छैन |
											</p>
											<br>
											<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 text-center">
											<h4>तपसील</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<p>निर्माण कार्यको किसिम : {{ $fileData->building_type ?? 'n/a' }}</p>
										</div>
										<div class="col-md-6">
											<p>लम्बाई : {{ $fileData->building_length ?? 'n/a' }}</p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<p>चौडाई : {{ $fileData->building_breadth ?? 'n/a' }}</p>
										</div>
										<div class="col-md-6">
											<p>उचाई : {{ $fileData->building_height ?? 'n/a' }}</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- PRINT PAGE 2 END -->
						
						<!-- PRINT PAGE 3 START -->
						<div class="row print-page break-page" style="display: none;">
							<div class="col-md-12">
								@include('layouts._partials.print-head')
								<div class="row">
									<div class="row">
										<div class="col-md-12 col-lg-12 col-sm-12 text-center " >
											<h3 class="sandhir_title">{{ $fileData->current_ward_number ?? 'n/a' }} नं. वडा कार्यालय</h3>
											<h4>रत्ननगर, चितवन, ३. नं प्रदेश, नेपाल</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<p style="text-indent: 5%; line-height: 2.6;text-align: justify;">
												रत्ननगर नगरपालिका वार्ड नं
												<span>{{ $fileData->current_ward_number ?? 'n/a' }}</span>
												बस्ने
												<span>
													{{ $fileData->sambodhan ?? 'n/a' }}&nbsp;{{ $fileData->field_owner_name ?? 'n/a' }}
												</span>
												ले साबिक
												<span>{{ $fileData->sabik_ward_number ?? 'n/a' }}</span>
												हालको वार्ड नं
												<span>{{ $fileData->current_ward_number ?? 'n/a' }}</span>
												कित्ता नं
												<span>{{ $fileData->kitta_number ?? 'n/a' }}</span>
												क्षेत्रफ़ल्
												<span>{{ $fileData->field_area ?? 'n/a' }}</span>
												को जग्गामा
												<span>{{ $fileData->building_type ?? 'n/a' }}</span>
												बनाउन नक्सापास गरी पाँउ भनि निवेदन दिनुभएको हुँदा उक्त निर्माण कार्य गर्दा कोही कसैलाई सन्धी सर्पन पिर मर्का पर्ने भए यस सूचनाको म्यादभित्र आफुलाई परेको पिर मार्काको विवरण सहित दरखास्त दिने बारेको सूचना यस निर्माण स्थल
												<span>{{ $fileData->building_purpose ?? 'n/a' }}</span>
												मा टाँसी मुचुल्कामा सही छाप गरी वार्ड सिमितिको कार्यालय मार्फत र.न.पा कार्यालयमा चढायौ
											</p>
											<br>
											<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 text-center">
											<h4>तपसील</h4>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											र.न.पा वडा नं...............................................बस्ने बर्ष............................................का................................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											र.न.पा वडा नं...............................................बस्ने बर्ष............................................का................................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											र.न.पा वडा नं...............................................बस्ने	बर्ष............................................का................................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											<h4>का.ता</h4>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											रत्ननगर न.पा.......................................................................................
										</div>
									</div>
									<br>
									
									<div class="row">
										<div class="col-md-12 text-center">
											इति सम्बत २०.......साल.........महिना..........गते..................रोज....................शुभम
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<!-- PRINT PAGE 3 END -->
						
						<!-- PRINT PAGE 4 START -->
						<div class="row print-page" style="display: none;">
							<div class="col-md-12">
								@include('layouts._partials.print-head')
								<div class="row">
									<div class="row">
										<div class="col-md-12 col-lg-12 col-sm-12 text-center " >
											<h3 class="sandhir_title">{{ $fileData->current_ward_number ?? 'n/a' }} नं. वडा कार्यालय</h3>
											<h4>रत्ननगर, चितवन, ३. नं प्रदेश, नेपाल</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<p style="text-indent: 5%; line-height: 2.6;text-align: justify;">
												लिखितम यस रत्ननगर नगरपालिका वार्ड नं
												<span class="border_line">{{ $fileData->current_ward_number ?? 'n/a' }}</span>
												बस्ने
												<span class="border_line">
													{{ $fileData->sambodhan ?? 'n/a' }}&nbsp;{{ $fileData->field_owner_name ?? 'n/a' }}
												</span>
												साबिक
												<span class="border_line">{{ $fileData->sabik_ward_number ?? 'n/a' }}</span>
												हालको वार्ड नं
												<span class="border_line">{{ $fileData->current_ward_number ?? 'n/a' }}</span>
												कित्ता नं
												<span class="border_line">{{ $fileData->kitta_number ?? 'n/a' }}</span>
												ज.वि
												<span class="border_line">{{ $fileData->field_area ?? 'n/a' }}</span>
												को पूर्व
												<span class="border_line">{{ $fileData->east_border ?? 'n/a' }}</span>
												पश्चिम
												<span class="border_line">{{ $fileData->west_border ?? 'n/a' }}</span>
												उत्तर
												<span class="border_line">{{ $fileData->north_border ?? 'n/a' }}</span>
												दक्षिण
												<span class="border_line">{{ $fileData->south_border ?? 'n/a' }}</span>
												यति चार किल्लाभित्रको जग्गामा
												<span class="border_line">{{ $fileData->building_type ?? 'n/a' }}</span>
												निमित दरखास्त पेश हुन आएकोमा १५ दिने सूचना प्रक्राशित गर्दा कोही कसैले पनि हालसम्म उजुर गर्न नआएकोले सोको नक्सा पासको लागि सर्जमिन मुचुल्का उठाउनुपर्ने भएकोमा सोको लागि साँध संधियार, छर छिमिकीएकस्थान भेला गराई खटाई आउनुभएका डोर मार्फत सोधनी हुँदा ततसम्बन्धमाव्यहोरा यो छ कि उक्त कि.नं
												<span class="border_line">{{ $fileData->kitta_number ?? 'n/a' }}</span>
												ज.वि
												<span class="border_line">{{ $fileData->field_area ?? 'n/a' }}</span>
												को जग्गामा कोही कसैको खिचोला नभएको,कोही कसैलाई संधिसर्पन,पीरमर्का नपर्ने देखिएको हुँदा सो
												<span class="border_line">{{ $fileData->building_purpose ?? 'n/a' }}&nbsp;भवन</span>
												को नक्शा पास गरिदिएमा ठीक छ,व्यहोरा ठीक साँचो हो भनी यस मुचुल्कामा सहीछाप गरी खटाई आउनुभएका डोर मार्फत र.न.पा कार्यालयमा चढायौ
											</p>
											<br>
											<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 text-center">
											<h4>तपसील</h4>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											१) र.न.पा वडा नं.........................................बस्ने बर्ष......................................का.........................................										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											२) र.न.पा वडा नं.........................................बस्ने बर्ष......................................का.........................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											३) र.न.पा वडा नं.........................................बस्ने बर्ष......................................का.........................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											४) र.न.पा वडा नं.........................................बस्ने बर्ष......................................का.........................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											५) र.न.पा वडा नं.........................................बस्ने बर्ष......................................का.........................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											<h4>सम्बन्धित</h4>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											र.न.पा वडा नं.........................................बस्ने बर्ष......................................का.........................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											<h4>रोह्बरु</h4>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											र.न.पा वडा नं.........................................बस्ने बर्ष......................................का.........................................
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											<h4>काम तामेल गर्ने</h4>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12 text-center">
											र.न.पा कार्यालयका
											इति सम्बत २०.........साल..........महिना...........गते..................रोज....................शुभम
										</div>
									</div>
								
								</div>
							</div>
						</div>
						<!-- PRINT PAGE 4 END -->
						<hr>
						<div class="row">
							<div class="col-lg-2 col-sm-4 col-xs-12 hide-on-print">
								<a class="btn btn-block btn-info text-white print-window"><i class="fa fa-print fa-fw"></i>&nbsp;Print</a>
							</div>
							<div class="col-lg-2 col-sm-4 col-xs-12 pull-right hide-on-print">
								<a data-toggle="modal" data-id="{{ $fileData->id }}" data-target="#uploadSandhir" class="btn btn-block btn-info text-white">Upload</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('engineer.modal')
@endsection

@section('scripts')
	<!-- this should go after your </body> -->
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'assets/js/nepaliDatePicker.js') }}" ></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
	<script>
        $(".bod-picker").nepaliDatePicker({
            dateFormat: "%D, %M %d, %y",
            closeOnDateSelect: true
        });

        $("#clear-bth").on("click", function(event) {
            $(".bod-picker").val('');
        });

        $('.print-window').click(function() {
            var print_date = $("#print-date").val();
            if (print_date.length==0){
                $("#msg").show();
            }
            else{
                window.print();
            }
        });

        $('#print-date').on('blur',function() {
	        $("#msg").hide();
        });
	</script>
	<script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });

        $('#uploadSandhir').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#form_id_sandhir").val(id);
        });
	</script>

@endsection
