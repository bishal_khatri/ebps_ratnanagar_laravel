@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
@endsection
@section('page_title')
	Files listing
@endsection
@section('right_button')
@stop
@section('content-title')
	<h2>
		फाईल खोज्नुहोस
	</h2>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<form action="{{ route('engineer.index') }}" method="get">
							@csrf
							<div class="row">
								<div class="col-md-3">
									<strong>फाईल खोज्नुहोस</strong>
								</div>
								<div class="col-md-7">
									<select class="form-control select2" name="ward_number">
										<option value="">वडा नं * </option>
										@if (isset($ward))
											@foreach($ward as $value)
												<option value="{{ $value->ward_number }}">{{ $value->ward_number }}</option>
											@endforeach
										@endif
									</select>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-default">Search</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							@if(isset($ward_number))
								<div class="panel-heading">वडा नं. <strong>{{ $ward_number }}</strong> का फाईल</div>
							@else
								<div class="panel-heading">वडा नं. चयन गर्नुहोस</div>
							@endif
							<div class="table-responsive">
								<table id="form_id" class="display nowrap dataTable table-bordered">
									<thead>
									<tr>
										<th width="50" class="text-center">#</th>
										<th width="150" class="text-center">दर्ता नं.</th>
										<th class="text-center">जग्गा धनीको नाम थर</th>
										<th class="text-center">ठेगाना</th>
										<th class="text-center">वडा नं</th>
										<th class="text-center">निर्माण किसिम</th>
										<th class="text-center">दर्ता मिति</th>
										<th class="text-center"></th>
									</tr>
									</thead>
									<tbody>
									@if (isset($files))
										@foreach($files as $value)
											<tr>
												<td class="text-center">{{ $loop->iteration }}</td>
												<td class="text-center">{{ $value->darta_number }}</td>
												<td class="text-center">{{ $value->sambodhan }}&nbsp;{{ $value->field_owner_name }}</td>
												<td class="text-center">{{ $value->field_owner_address }}</td>
												<td class="text-center">{{ $value->current_ward_number }}</td>
												<td class="text-center">{{ $value->building_type }}</td>
												<td class="text-center">{{ $value->created_at_nepali }}</td>
												<td class="text-center">
													@if($value->form_level == 8)
														<a href="{{ route('engineer.check_form',$value->id) }}" type="button" class="btn btn-info btn-block btn-sm">DPC</a>
													@elseif($value->form_level == 13)
														<a href="{{route('engineer.get_print_sampanna',$value->id) }}" class="btn btn-info btn-block btn-sm">Sampanna</a>
													@else
														<a href="{{ route('engineer.check_form',$value->id) }}" type="button" class="btn btn-info btn-block btn-sm">Proceed</a>
													@endif
												</td>
											</tr>
										@endforeach
									@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
	<script>
        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            //Bootstrap-TouchSpin

            $(document).ready( function () {
                $('#form_id').DataTable({"bSort" : false});
            } );
        });
	</script>
@endsection
