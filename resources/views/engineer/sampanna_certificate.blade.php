@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/sampanna_print.css') }}">
    <style type="text/css">
    	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    		padding: 0px 0px;
		}
		.table-bordered, .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
   		 border-top: none;
		}
		td{
			padding-top: 2px;
			padding-bottom: 2px;
		}
		th{
			padding-top: 2px;
			padding-bottom: 2px;
		}
		.square {
		  height: 150px;
		  width: 150px;
		  border: 1px solid black;
		}
    </style>
@endsection
@section('page_title')
	सम्पन्न फारम
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>सम्पन्न फारम</h4>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-wrapper collapse in" aria-expanded="true">
					
				</div>
			</div>
		</div>
	</div>


	<button onclick="window.print()">Print</button>
@endsection

@section('scripts')

@endsection
