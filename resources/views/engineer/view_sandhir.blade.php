@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
	
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/css/animated-masonry-gallery.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.css') }}" />
@endsection
@section('page_title')
	सँधियार सूचना
@endsection
@section('right_button')
	<a class="btn btn-info pull-right" href="{{ route('engineer.index') }}" style="margin-top: 3px;">Back</a>
@stop
@section('content-title')
	<h4>
		सँधियार सूचना
	</h4>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<h3 class="box-title text-center text-danger">
							@if($days_left == 'n/a')
								procedeed to aamin
							@else
								अब {{ $days_left }} दिन बाकी
							@endif
						</h3>
						<hr>
						<div class="row text-center">
							<div class="col-md-12 ">
								<div id="gallery">
									<div id="gallery-content">
										<a href="{{ asset(STATIC_DIR.'storage/'.$sandhir->sandhir_suchana) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="सँधियार सूचना">
											<img src="{{ asset(STATIC_DIR.'storage/'.$sandhir->sandhir_suchana) }}" class="all studio" alt="gallery" width="500"/>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	{{--@include('permission.model')--}}
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/animated-masonry-gallery.js') }}"></script>
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/jquery.isotope.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.js') }}"></script>
	
	<script>
        $(document).ready(function($) {
            // delegate calls to data-toggle="lightbox"
            $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                event.preventDefault();
                return $(this).ekkoLightbox({
                    onShown: function() {
                        if (window.console) {
                            return console.log('Checking our the events huh?');
                        }
                    },
                    onNavigate: function(direction, itemIndex) {
                        if (window.console) {
                            return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                        }
                    }
                });
            });
        });
	</script>
@endsection
