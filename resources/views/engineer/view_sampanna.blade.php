@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/sampanna_print.css') }}">
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/css/nepaliDatePicker.css') }}" >

	<style type="text/css">
		
     .table>tbody>tr>td {
            padding: 8px 5px !important;
        }
	</style>

@endsection
@section('page_title')
	सम्पन्न फारम
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>सम्पन्न फारम</h4>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-wrapper collapse in" aria-expanded="true">
					<div class="panel-body">
						  <form method="post" action="{{ route('engineer.proceed_sampanna') }}">
				                    	@csrf
						<!-- page 1 print start -->
						{{--<div class="row break-page"  >	--}}
                			{{--@include('layouts._partials.print-head')--}}
							{{--<table class="table table-sm table-bordered table-hover edit-table" style="font-size: 16px;>--}}
								{{--<tr class="d-none d-print-table-row text-center font-weight-bold">--}}
									{{--<td colspan="5">--}}
										{{--निर्माण सम्पन्न प्रतिवेदन सम्बन्धमा--}}
									{{--</td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td colspan="5">--}}
										{{--<p>महोदय,</p>--}}
										{{--<p style="text-indent:40px;">--}}
											{{--उपरोक्त सम्बन्धमा {{ $formData->buildingInfo->district ?? '............' }}--}}
											{{--जिल्ला {{ $formData->landInfo->village_name ?? '............' }} गा.वि.स / न.पा--}}
											{{--वडा नं {{ $formData->landInfo->current_ward_number ?? '............' }} बस्ने--}}

											{{--<span class="border_line">{{ $formData->sambodhan ?? '............' }} --}}

											{{--{{ $formData->field_owner_name ?? '............' }}</span> --}}
											{{--को--}}
											{{--र.न.पा वडा नं {{ $formData->landInfo->current_ward_number ?? '............' }}--}}
											{{--साबिक --}}
											{{--कि.नं {{ $formData->landInfo->kitta_number ?? '............' }} मा पास भएको {{ $formData->building_type ?? '............' }}को निर्माण सम्पन्न प्रमाण पत्र पाऊ भनि मिति ........................दर्ता नं --}}

											{{--{{ $formData->id ?? '............' }} मा दिनु भएको निबेदन तोक आदेश भए अनुसार उक्त निर्माण कार्यको स्थल गत निरक्षणमा सम्पूर्ण नाप जाँच गरि यो निर्माण सम्पनको प्रतिवेदन पेश गरको छु |--}}
										{{--</p>--}}
									{{--</td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td class="font-weight-bold" colspan="5">मापदण्ड</td>--}}
								{{--</tr>--}}
								{{----}}
								{{--<tr>--}}
									{{--<td>१.</td>--}}
									{{--<td>ज.ध.प्र.पु. क्षेत्रफ़ल्</td>--}}
									{{--<td colspan="3">--}}
										{{--{{ $formData->sampanna->field_area ?? 'n/a' }}--}}
									{{--</td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td>२.</td>--}}
									{{--<td>साईट प्लान क्षेत्रफ़ल्</td>--}}
									{{--<td colspan="3">--}}
										{{--{{ $formData->sampanna->site_plan_area ?? 'n/a' }}--}}
									{{--</td>--}}
								{{--</tr>--}}
								{{--<tr class="text-center">--}}
									{{--<td>क्र.सं.</td>--}}
									{{--<td>विवरण</td>--}}
									{{--<td>मापदण्ड अनुसार</td>--}}
									{{--<td>स्विकृत अनुसार</td>--}}
									{{--<td>निर्माण स्थिति </td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td>१.</td>--}}
									{{--<td>--}}
										{{--ग्रउण्ड कभरेज --}}
										{{--{{ $formData->sampanna->ground_coverage ?? 'n/a' }}--}}
										 {{--%  --}}
									{{--</td>--}}

									{{--<td>--}}
										{{--{{ $formData->sampanna->ground_coverage_mapdanda ?? 'n/a' }} %--}}
										{{----}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->ground_coverage_swikrit_anusar ?? 'n/a' }} %--}}
									{{--</td>--}}

									{{--<td>--}}
										{{--{{ $formData->sampanna->ground_coverage_nirman_esthithi ?? 'n/a' }} %--}}
									{{--</td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td>२.</td>--}}
									{{--<td>तला</td>--}}

									{{--<td>--}}
										{{--{{ $formData->sampanna->talla_mapdanda ?? 'n/a' }} --}}
									{{--</td>--}}

									{{--<td>--}}
										{{--{{ $formData->sampanna->talla_swikrit_anusar ?? 'n/a' }} --}}
									{{--</td>--}}

									{{--<td>--}}
										{{--{{ $formData->sampanna->talla_nirman_esthithi ?? 'n/a' }}--}}
									{{--</td>--}}

								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td>३.</td>--}}
									{{--<td>उचाई</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->height_mapdanda ?? 'n/a' }} फि--}}
									{{--</td>--}}
									{{----}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->height_swikrit_anusar ?? 'n/a' }} फि--}}
									{{--</td>--}}

									{{--<td>--}}
										{{--{{ $formData->sampanna->height_nirman_esthithi ?? 'n/a' }} फि--}}
									{{--</td>--}}

								{{--</tr>--}}

								{{--<tr>--}}
									{{--<td>४.</td>--}}
									{{--<td>कूल फ्लोर एरिया</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->floor_area_mapdanda ?? 'n/a' }} व.फि--}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->floor_area_swikrit_anusar ?? 'n/a' }} व.फि--}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->floor_area_nirman_esthithi ?? 'n/a' }} व.फि--}}
									{{--</td>--}}
								{{--</tr>--}}

								{{--<tr>--}}
									{{--<td>५.</td>--}}
									{{--<td>स्विकृत भन्दा बदी</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->swikrit_vanda_badi_mapdanda ?? 'n/a' }}	--}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->swikrit_vanda_badi_swikrit_anusar ?? 'n/a' }} --}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->swikrit_vanda_badi_nirman_esthithi ?? 'n/a' }} --}}
									{{--</td>--}}
								{{--</tr>--}}

								{{--<tr>--}}
									{{--<td>६.</td>--}}
									{{--<td>आर. ओ. डब्लु</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->r_o_w_mapdanda ?? 'n/a' }} मि--}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->r_o_w_swikrit_anusar ?? 'n/a' }} मि--}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->r_o_w_nirman_esthithi ?? 'n/a' }} मि--}}
									{{--</td>--}}
								{{--</tr>--}}

								{{--<tr>--}}
									{{--<td>७.</td>--}}
									{{--<td>सेट ब्यक</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->setback_mapdanda ?? 'n/a' }} मि--}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->setback_swikrit_anusar ?? 'n/a' }} मि--}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{ $formData->sampanna->setback_nirman_esthithi ?? 'n/a' }} मि--}}
									{{--</td>--}}
								{{--</tr>--}}

								{{--<tr>--}}
									{{--<td>८.</td>--}}
									{{--<td>--}}
										{{--बिधुत लाईन --}}
										{{--{{ $formData->sampanna->electricity_line ?? 'n/a' }} के.भि--}}
									{{--</td>--}}
									{{--<td>{{ $formData->sampanna->electricity_line_mapdanda ?? 'n/a' }} फि</td>--}}
									{{--<td>{{ $formData->sampanna->electricity_line_swikrit_anusar ?? 'n/a' }} फि</td>--}}
									{{--<td>{{ $formData->sampanna->electricity_line_nirman_esthithi ?? 'n/a' }} फि</td>--}}
								{{--</tr>--}}

								{{--<tr>--}}
									{{--<td>९.</td>--}}
									{{--<td>नदि किनार</td>--}}
									{{--<td>{{ $formData->sampanna->river_side_mapdanda ?? 'n/a' }} फि</td>--}}
									{{--<td>{{ $formData->sampanna->river_side_swikrit_anusar ?? 'n/a' }} फि</td>--}}
									{{--<td>{{ $formData->sampanna->river_side_nirman_esthithi ?? 'n/a' }} फि</td>--}}
									{{----}}
								{{--</tr>--}}

								{{--<tr>--}}
									{{--<td colspan="5">--}}
										{{--कैफियत : स्वीकृत नक्शा भन्दा विपरित  भए त्यसको विवरण :--}}
									{{--</td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td colspan="3">--}}
										{{--कन्सल्टेन्टको तर्फबाट : <br>--}}
										{{--नाम : {{ $formData->sampanna->entry_name}} <br>--}}
										{{--पद : {{ $formData->sampanna->consultancy_post}} <br>--}}
										{{--कन्सल्टेन्सी : {{ $formData->sampanna->consultancy_name}} <br>--}}
										{{--<!-- सही : -->--}}
									{{--</td>--}}
									{{--<td colspan="2">--}}
										{{--कार्यालयको प्राविधिक शाखाको तर्फबाट : <br>--}}
										{{--नाम : ...................... <br>--}}
										{{--पद : ...................... <br>--}}
										{{--सही :......................--}}
									{{--</td>--}}
								{{--</tr>--}}
								{{----}}
							{{--</table>--}}
							{{----}}
						{{--</div>--}}
						<!-- end page print 1 -->
					
						<!-- page print 2 start -->
							<div class="row print-page break-page" >
								<div class="col-md-12">
                					@include('layouts._partials.print-head')

									<table class="table table-sm table-bordered">
					                    <tr>
					                        <td colspan="4" class="text-center">
					                            निर्माण सम्पन्न प्रमाण पत्र सम्बन्धमा
					                        </td>
					                    </tr>
					                        
					                    <tr>
					                        <td colspan="3">
					                                पत्र संख्या : <br>
					                                चलानी नम्बर : 
					                        </td>
					                        <td class="text-right">
					                              मिति : <input type="text" id="print-date" class="form-contril input-box bod-picker" required placeholder="--मिति चयन अनिवार्य--" style="border:0px;width:180px" >

                                                <br>
                                                <span class="text-danger text-center hide-on-print pull-right" style="display: none; margin-right:130px;" id="msg">मिति चयन गर्नुहोस</span>					                          </td>

					                    </tr>

				                        <tr>
				                          	<td colspan="4">
				                              	<p style="line-height:50px;font-size:16px;text-indent:50px;">
                                                    उपयुक्त बिषयमा रत्ननगर नगरपालिका वडा नं

				                                  	<span class="border_line">
				                                    	{{ $formData->landInfo->current_ward_number ?? 'n/a' }} 
				                                	</span>

				                                	बस्ने 
				                                	<span class="border_line">
				                                    	{{ $formData->sambodhan ?? 'n.a' }}&nbsp; {{ $formData->field_owner_name ?? 'n.a'}}ले
				                                  	</span> 

				                                  	आफ्नो नाममा रहेको साविक 

				                                  	<span class="border_line">
				                                  		{{ $formData->landInfo->sabik_ward_number ?? 'n.a' }}	
				                                  	</span> 

				                                  	वडा नं 

				                                  	<span class="border_line"> 
				                                  		{{ $formData->landInfo->current_ward_number ?? 'n.a' }}
				                                	</span> 

				                                    हाल रत्ननगर न.पा. वडा नं 

				                                    <span class="border_line">
				                                    	{{ $formData->landInfo->current_ward_number ?? 'n.a' }}		
				                                    </span> 

				                                    मा पर्ने कित्ता.नं 

				                                    <span class="border_line">
				                                    	{{ $formData->landInfo->kitta_number ?? 'n.a' }}
				                                    </span> 

				                                    ज.वि
				                                    <span>
				                                    	{{ $formData->landInfo->field_area ?? 'n/a' }}
				                                    </span> 

				                                    जग्गामा प्रथम चरणको डि.पि.सि  र दोश्रो चरणको नक्सा पास स्वीकृति लिई घर निर्माण कार्य सम्पन्न गरी सकेको प्राविधिक प्रतिवेदन बाट देखिने आएकोले निजलाई निर्माण सम्पन्न भएको प्रमाण-पत्र दिन मिल्ने भएकोले श्रीमान समक्ष निर्णयार्थ यो टिप्पणी पेश गरेको छु |
				                              	</p>
				                          	</td>
				                        </tr>

				                        <tr>
				                            <td colspan="4" class="text-right" style="padding-top:50px;">
				                                  ..............................................<br>
				                                  <p style="margin-right: 35px;">टिप्पणी पेश गर्ने</p>
				                            </td>
				                        </tr>

				                  
										<tr class="d-block-table-row d-print-none hide-on-print">
				                          	<td colspan="4">
				                                <center>
				                                	<input type="hidden" name="form_id" value="{{ $formData->id }}">
				                                    <button type="button" name="button"  class="btn btn-primary print-window hide-on-print">
				                                        <i class="fa fa-print"></i>&nbsp;Print
				                                    </button>
				                                    <button type="submit"  class="btn btn-info">
				                                        Proceed 
				                                    </button>

				                                </center>
				                          	</td>
					                    </tr>
                    				</table>
								</div>	
							</div>
						<!-- end of page print 2  -->
				        

						<!-- page print 3  -->
						<div class="row print-page" style="display: none;">
							<div class="col-md-12">
								<div class="panel-body">
        				@include('layouts._partials.print-head')
						<table class="table table-sm table-responsive">
							<tr class="text-center font-weight-bold">
								<td colspan="5" >
									<h2 class="text-danger"><span style="border: 1px solid red; padding: 4px;">घर निर्माण सम्पन्न प्रमाण पत्र</span></h2>
								</td>
							</tr>
							<tr>
								<td >
									श्रीमान/ श्रीमती /सुश्री    {{ $formData->field_owner_name ?? 'n/a' }}
								</td>
								{{-- <td rowspan="2" class="square pull-right"></td> --}}
							</tr>
							<tr>
								<td >
									ठेगाना   {{ $formData->field_owner_address ?? 'n/a' }}
								</td>
							</tr>
							<tr>
								<td >
									महोदय,
								</td>
							</tr>
							<tr>
								<td>
									<p style="text-indent: 5%">तपाइले यस नगरपालिकामा निम्न विवरण बमोजिमको निर्माण कार्य पूरा गर्नु भएको प्रमाणित गरिन्छ।</p>
								</td>
							</tr>
							<table style="width: 100%;" border="1px">
								<tr>
									<th colspan="2" class="text-center" >वडा नं.</th>
									<th class="text-center">कि. नं.</th>
									<th class="text-center">छेत्रफल</th>
									<th class="text-center">नक्सा पास मिति</th>
									<th class="text-center">क्षेत्र</th>
								</tr>
								<tr>
									<td class="text-center">हाल</td>
									<td class="text-center">साविक</td>
									<td class="text-center"></td>
									<td class="text-center"></td>
									<td class="text-center"></td>
									<td class="text-center"></td>
								</tr>
								<tr>
									<td class="text-center">{{ $formData->landInfo->current_ward_number ?? 'n/a'}}</td>
									<td class="text-center">{{ $formData->landInfo->sabik_ward_number ?? 'n/a'}}</td>
									<td class="text-center">{{ $formData->landInfo->kitta_number ?? 'n/a' }}</td>
									<td class="text-center">{{ $formData->landInfo->field_area ?? 'n/a' }}</td>
									<td class="text-center">{{ $formData->created_at_nepali ?? 'n/a' }}</td>
									<td class="text-center"></td>
								</tr>
							</table>
							<tr>
								<td colspan="5">
									<h4>मापदण्ड </h4> 
								</td>
							</tr>
							<table style="width: 100%; margin-top: 10px;" border="1px">
							    <tr align="center" >
							        <th rowspan="2" class="text-center">सि. नं.</th>
							        <th rowspan="2" class="text-center">विवरण</th>
							        <th rowspan="2" class="text-center">मापदण्ड आनुसार</th>
							        <th rowspan="2" class="text-center">स्वीकृत आनुसार </th>
							        <th style="padding:2.5px;" colspan="2" class="text-center">निर्माण स्तिथि </th>
							        <th rowspan="2" class="text-center">कैफियत</th>
							    </tr>
							    <tr>
							        <th class="text-center">साविक</th>
							        <th class="text-center">स्तिथि</th>
							    </tr>
							    <tr>
							    	<td class="text-center">१</td>
							    	<td class="text-center">ग्रउण्ड कभरेज {{ $formData->sampanna->ground_coverage ?? 'n/a' }}</td>
							    	<td class="text-center">{{ $formData->sampanna->ground_coverage_mapdanda ?? 'n/a' }} %</td>
							    	<td class="text-center">{{ $formData->sampanna->ground_coverage_swikrit_anusar ?? 'n/a' }} %</td>
							    	<td class="text-center"></td>
							    	<td class="text-center">{{ $formData->sampanna->ground_coverage_nirman_esthithi ?? 'n/a' }}</td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center">२</td>
							    	<td class="text-center">आर ओ डब्लु (ROW)</td>
							    	<td class="text-center">{{ $formData->sampanna->r_o_w_mapdanda ?? 'n/a' }} मि</td>
							    	<td class="text-center">{{ $formData->sampanna->r_o_w_swikrit_anusar ?? 'n/a' }} मि</td>
							    	<td class="text-center"> </td>
							    	<td class="text-center">{{ $formData->sampanna->r_o_w_nirman_esthithi ?? 'n/a' }}</td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center">३</td>
							    	<td class="text-center">सेट ब्यक (मि / फि)</td>
							    	<td class="text-center">{{ $formData->sampanna->setback_mapdanda ?? 'n/a' }} मि</td>
							    	<td class="text-center">{{ $formData->sampanna->setback_swikrit_anusar ?? 'n/a' }} मि</td>
							    	<td class="text-center"></td>
							    	<td class="text-center">{{ $formData->sampanna->setback_nirman_esthithi ?? 'n/a' }}</td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center">४</td>
							    	<td class="text-center">विधुत लाइन {{ $formData->sampanna->electricity_line ?? 'n/a' }} (KV)</td>
							    	<td class="text-center">{{ $formData->sampanna->electricity_line_mapdanda ?? 'n/a' }} फि</td>
							    	<td class="text-center">{{ $formData->sampanna->electricity_line_swikrit_anusar ?? 'n/a' }} फि</td>
							    	<td class="text-center"></td>
							    	<td class="text-center">{{ $formData->sampanna->electricity_line_nirman_esthithi ?? 'n/a' }}</td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center">५</td>
							    	<td class="text-center">नदी किनार </td>
							    	<td class="text-center">{{ $formData->sampanna->river_side_mapdanda ?? 'n/a' }} फि</td>
							    	<td class="text-center">{{ $formData->sampanna->river_side_swikrit_anusar ?? 'n/a' }} फि</td>
							    	<td class="text-center"></td>
							    	<td class="text-center">{{ $formData->sampanna->river_side_nirman_esthithi	 ?? 'n/a' }}</td>
							    	<td class="text-center"></td>
							    </tr>
							</table>

							<table style="width: 100%; margin-top: 10px;" border="1px">
							    <tr align="center" >
							        <th class="text-center">निर्माण भएको भवनको विवरण</th>
							        <th class="text-center">जमिन</th>
							        <th class="text-center">प्रथम</th>
							        <th class="text-center">दोस्रो</th>
							        <th class="text-center">तेस्रो</th>
							        <th class="text-center">चौथो</th>
							        <th class="text-center">बेसमेन्ट</th>
							    </tr>
							    
							    <tr>
							    	<td class="text-center">लम्बाई</td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center">चौडाई</td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center">उचाई</td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center">छेत्रफल</td>
							    	<td class="text-center">{{ $formData->floorInfo->underground_floor_area ?? 'n/a' }}</td>
							    	<td class="text-center">{{ $formData->floorInfo->ground_floor_area ?? 'n/a' }}</td>
							    	<td class="text-center">{{ $formData->floorInfo->first_floor_area ?? '' }}</td>
							    	<td class="text-center">{{ $formData->floorInfo->second_floor_area ?? '' }}</td>
							    	<td class="text-center">{{ $formData->floorInfo->third_floor_area ?? '' }}</td>
							    	<td class="text-center">{{ $formData->floorInfo->fourth_floor_area ?? '' }}</td>
							    </tr>
							</table>

							<table style="width: 100%; margin-top: 10px;" border="1px">
							    <tr align="center" >
							        <th class="text-center">भवन प्रयोजन</th>
							        <th class="text-center">घरको बनौट</th>
							        <th class="text-center">बाटोको किसिम</th>
							        <th class="text-center">निकास </th>
							        <th class="text-center">कैफियत</th>
							    </tr>
							    <tr>
							    	<td class="text-center">{{ $formData->buildingInfo->building_purpose ?? 'n/a' }}</td>
							    	<td class="text-center">{{ $formData->buildingInfo->building_structure ?? 'n/a' }}</td>
							    	<td class="text-center"></td>
							    	<td class="text-center"> {{ $formData->buildingInfo->septic_tank ?? 'n/a' }} </td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center"></td>
							    	<td class="text-center">{{ $formData->buildingInfo->jodai ?? 'n/a' }}</td>
							    	<td class="text-center"><td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center"></td>
							    	<td class="text-center">{{ $formData->buildingInfo->roof ?? 'n/a' }}</td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    </tr>
							    <tr>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    	<td class="text-center"></td>
							    </tr>
							</table>

							<table style="width: 100%; margin-top: 30px;" border="0px">
								<tr>
									<td class="text-center">.....................................................................................</td>
									<td class="text-center">.....................................................................................</td>
									<td class="text-center">.....................................................................................</td>
									<td class="text-center">.....................................................................................</td>
								</tr>
								<tr>
									<td class="text-center">स. इञ्जिनियर / अ. स. इञ्जिनियर</td>
									<td class="text-center">इञ्जिनियर</td>
									<td class="text-center">प्रमुख</td>
									<td class="text-center">मिति</td>
								</tr>
							</table>
						</table>
						
					</div>
							</div>
						</div>




				        </form>   

					</div>
					{{-- panel-body --}}

				</div>
				<!-- end panel-wrapper -->
			</div>
			<!-- end panel -->
		
		</div>
		<!-- end col-md-12-->
	</div>
	<!-- end row -->
	
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset(STATIC_DIR.'assets/js/nepaliDatePicker.js') }}" ></script>
			

<script>
    $(".bod-picker").nepaliDatePicker({
        dateFormat: "%D, %M %d, %y",
        closeOnDateSelect: true
    });


    $('.print-window').click(function() {
        var print_date = $("#print-date").val();
        if (print_date.length==0){
            $("#msg").show();
        }
        else{
            window.print();
        }
    });

    $('#print-date').on('blur',function() {
        $("#msg").hide();
    });
</script>

@endsection
