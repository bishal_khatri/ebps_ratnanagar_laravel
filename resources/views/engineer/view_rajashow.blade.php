@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/rajashow_print.css') }}">
@endsection
@section('page_title')
    रजशोव
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>रजशोव </h4>
@endsection

@section('content')
    <div class="row print-page">
        <div class="col-md-12 col-sm-12 col-lg-12">
            <div class="white-box">
                @include('layouts._partials.print-head')
                <table class="table table-responsive">
                    <tbody>
                    <tr>
                        <td>
                            जग्गा धनिको नाम थर : {{ $personalInfo->sambodhan }} {{ $personalInfo->field_owner_name }}
                        </td>
                        <td>
                            निर्माण किसिम : {{ $personalInfo->building_type }}
                        </td>
                        <td>
                            दर्ता नम्बर : <span>{{ $personalInfo->darta_number }}</span>
                        </td>
                        <td>
                            वडा नं : <span>{{ $landInfo->current_ward_number }}</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th style="width: 150px;" class="text-center">तल्ला</th>
                        <th class="text-center">क्षेत्रफल(बर्ग फिट)</th>
                        <th class="text-center">दर रु (Rate)</th>
                        <th class="text-center">रकम रु(Amount)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($floorInfo->underground_floor_area))
                        <tr>
                            <td>भुमिगत तल्ला</td>
                            <td><input type="text" class="form-control input-number" name="underground_floor_area" value="{{ $floorInfo->underground_floor_area }}" readonly></td>
                            <td><input type="text" name="underground_floor_rate" class="form-control input-number" value="{{ $personalInfo->rajashow->underground_floor_rate }}" readonly></td>
                            <td><input type="text" name="underground_floor_amount" class="form-control" value="{{ $personalInfo->rajashow->underground_floor_amount }}" readonly></td>
                        </tr>
                    @endif
                    @if(!empty($floorInfo->ground_floor_area))
                        <tr>
                            <td>भुइ तल्लाा</td>
                            <td><input type="text" class="form-control input-number" name="ground_floor_area" value="{{ $floorInfo->ground_floor_area }}" readonly></td>
                            <td><input type="text" name="ground_floor_rate" class="form-control input-number" value="{{ $personalInfo->rajashow->ground_floor_rate }}" readonly></td>
                            <td><input type="text" name="ground_floor_amount" class="form-control" value="{{ $personalInfo->rajashow->ground_floor_amount }}" readonly></td>
                        </tr>
                    @endif
                    @if(!empty($floorInfo->first_floor_area))
                        <tr>
                            <td>पहिलो तल्ला</td>
                            <td>
                                <input type="text" class="form-control input-number" name="first_floor_area" value="{{ $floorInfo->first_floor_area }}" readonly>
                            </td>
                            <td>
                                <input type="text" name="first_floor_rate" class="form-control input-number" value="{{ $personalInfo->rajashow->first_floor_rate }}" readonly>
                            </td>
                            <td>
                                <input type="text" name="first_floor_amount" class="form-control" value="{{ $personalInfo->rajashow->first_floor_amount }}" readonly>
                            </td>
                        </tr>
                    @endif
                    @if(!empty($floorInfo->second_floor_area))
                        <tr>
                            <td>
                                दोश्रो तल्ला
                            </td>
                            <td>
                                <input type="text" class="form-control input-number" name="second_floor_area" value="{{ $floorInfo->second_floor_area }}" readonly>
                            </td>
                            <td>
                                <input type="text" name="second_floor_rate" class="form-control input-number" value="{{ $personalInfo->rajashow->second_floor_rate }}" readonly>
                            </td>
                            <td>
                                <input type="text" name="second_floor_amount" class="form-control" value="{{ $personalInfo->rajashow->second_floor_amount }}" readonly>
                            </td>
                        </tr>
                    @endif
                    @if(!empty($floorInfo->third_floor_area))
                        <tr>
                            <td>
                                तेश्रो तल्ला
                            </td>
                            <td>
                                <input type="text" class="form-control input-number" name="third_floor_area" value="{{ $floorInfo->third_floor_area }}" readonly>
                            </td>
                            <td>
                                <input type="text" name="third_floor_rate" class="form-control input-number" value="{{ $personalInfo->rajashow->third_floor_rate }}" readonly>
                            </td>
                            <td>
                                <input type="text" name="third_floor_amount" class="form-control" value="{{ $personalInfo->rajashow->third_floor_amount }}" readonly>
                            </td>
                        </tr>
                    @endif
                    @if(!empty($floorInfo->fourth_floor_area))
                        <tr>
                            <td>
                                चौथो तल्ला
                            </td>
                            <td>
                                <input type="text" class="form-control input-number" name="fourth_floor_area" value="{{ $floorInfo->fourth_floor_area }}" readonly>
                            </td>
                            <td>
                                <input type="text" name="fourth_floor_rate" class="form-control input-number" value="{{ $personalInfo->rajashow->fourth_floor_rate }}" readonly>
                            </td>
                            <td>
                                <input type="text" name="fourth_floor_amount" class="form-control" value="{{ $personalInfo->rajashow->fourth_floor_amount }}" readonly>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td>
                            जम्मा नक्शा पास दस्तुर
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" name="total" class="form-control" readonly value="{{ $personalInfo->rajashow->total }}" >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            अन्य धरौटी
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" name="other_amount" class="form-control" value="{{ $personalInfo->rajashow->other_amount }}" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            कूल जम्मा
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" name="grand_total" class="form-control" id="grand_total" value="{{ $personalInfo->rajashow->grand_total }}" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            दस्तुर हिसाब गर्नेको नाम
                        </td>
                        <td>
                            <input type="text" class="form-control" readonly value="{{ Auth::user()->first_name ?? '' }}&nbsp;{{ Auth::user()->last_name ?? '' }}">
                        </td>
                        <td></td>
                        <td >
                            <input class="form-control" type="text" id="print-date" value="{{ $personalInfo->rajashow->created_at_nepali }}" readonly>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-2 pull-right">
                        <a href="{{ route('engineer.tippani_adesh',$floorInfo->form_id) }}" type="button" class="btn btn-block btn-outline btn-primary">Next</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sub_engineer.modal')
@endsection

@section('scripts')
    <script>
        $('#uploadRajashow').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#form_id_rejashow").val(id);
        });
    </script>
@endsection
