@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/css/animated-masonry-gallery.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.css') }}" />

@endsection
@section('page_title')
	सँधियार सूचना
@endsection
@section('right_button')
@stop
@section('content-title')
	<h2>
		@if(isset($dpc_path))
			<h2> DPC टिप्पणी </h2>
		@else
			<h2>टिप्पणी र आदेश</h2>
		@endif
		@endsection
		
		@section('content')
			<div class="preloader">
				<svg class="circular" viewBox="25 25 50 50">
					<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
				</svg>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-12 col-sm-12">
					<div class="white-box">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-offset-3 col-md-6">
									@if(isset($action))
										@if($action == 'main-engineer')
											<h3 class="box-title text-center text-danger">
												Proceeded to Main Engineer
											</h3>
										@elseif($action == 'consultancy')
											<h3 class="box-title text-center text-danger">
												Proceeded to Consultancy for DPC
											</h3>
										@endif
									@endif
								</div>
							</div>
						</div>
						<hr>
						<div id="gallery" class="text-center">
							<div id="gallery-content">
								@if(isset($tippani_path))
									<a href="{{ asset(STATIC_DIR.'storage/'.$tippani_path) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="अमिन  प्रतिवेदन">
										<img src="{{ asset(STATIC_DIR.'storage/'.$tippani_path) }}" class="all studio" alt="gallery" width="500"/>
									</a>
									
									<a href="{{ asset(STATIC_DIR.'storage/'.$asthai_path) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="अमिन  प्रतिवेदन">
										<img src="{{ asset(STATIC_DIR.'storage/'.$asthai_path) }}" class="all studio" alt="gallery" width="500"/>
									</a>
								@endif
								@if(isset($dpc_path))
									<a href="{{ asset(STATIC_DIR.'storage/'.$dpc_path) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="Dpc टिप्पणी र आदेश">
										<img src="{{ asset(STATIC_DIR.'storage/'.$dpc_path) }}" class="all studio" alt="gallery" width="500"/>
									</a>
								@endif
							</div>
						</div>
						
						<div class="clearfix"></div>
					
					</div>
				
				</div>
			</div>
		@endsection
		
		@section('scripts')
			<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/animated-masonry-gallery.js') }}"></script>
			<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/jquery.isotope.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.js') }}"></script>
			
			<script>
                $(document).ready(function($) {
                    // delegate calls to data-toggle="lightbox"
                    $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                        event.preventDefault();
                        return $(this).ekkoLightbox({
                            onShown: function() {
                                if (window.console) {
                                    return console.log('Checking our the events huh?');
                                }
                            },
                            onNavigate: function(direction, itemIndex) {
                                if (window.console) {
                                    return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                                }
                            }
                        });
                    });
                });
			</script>

@endsection
