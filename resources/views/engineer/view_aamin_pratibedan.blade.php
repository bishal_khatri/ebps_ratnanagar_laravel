@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/css/nepaliDatePicker.css') }}" >
@endsection
@section('page_title')
    अमिन प्रतिवेदन
@endsection
@section('right_button')
@stop
@section('content-title')
    <h2>
        अमिन प्रतिवेदन
    </h2>
@endsection

@section('content')
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>

    <form  method="post"  action="">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="white-box">
                    @csrf

                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field_owner_name" class="control-label">
                                    जग्गा धनी को नाम थर :
                                </label>
                                <input type="text" name="field_owner_name" readonly class="form-control" value="{{ !empty($form->sambodhan) && !empty($form->field_owner_name) ? $form->sambodhan.' ' .$form->field_owner_name : 'n/a'  }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="building_type" class="control-label">
                                    निर्माण किसिम :
                                </label>
                                <input type="text" name="building_type" readonly class="form-control" value="{{ !empty($form->building_type) ? $form->building_type : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="electricity_line" class="control-label">
                                    बिजुली को तार :
                                </label>
                                <input type="text" name="electricity_line" readonly class="form-control" value="{{ !empty($form->landInfo->electricity_line) ? $form->landInfo->electricity_line : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="river" class="control-label">
                                    नदि किनारा :
                                </label>
                                <input type="text" name="river" readonly class="form-control" value="{{ !empty($form->landInfo->river ) ? $form->landInfo->river : 'n/a'}}">
                            </div>
                        </div>

                    </div>
                    <!--/ first row-->

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bato_category" class="control-label">
                                    १.बाटोको वर्ग
                                </label>
                                <input type="text" name="bato_category" class="form-control"   readonly value="{{ !empty($formData->bato_category) ? $formData->bato_category : 'n/a'  }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bato_length" class="control-label">
                                    २.बाटो को चौडाई
                                </label>
                                <input type="text" name="bato_length" placeholder="बाटो को चौडाई"  class="form-control" readonly  value="{{ !empty($formData->bato_length) ? $formData->bato_length : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bato_direction" class="control-label">
                                    ३.बाटो को दिशा
                                </label>
                                <input type="text" name="bato_direction" class="form-control" readonly  value="{{ !empty($formData->bato_direction) ? $formData->bato_direction : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bato_area" class="control-label">
                                    ४.जग्गा रहेको क्षेत्र / उपक्षेत्र
                                </label>
                                <input type="text" name="bato_area" class="form-control" readonly  value="{{ !empty($formData->bato_area) ? $formData->bato_area : 'n/a' }}">
                            </div>
                        </div>

                    </div>
                    <!--/ second row-->

                    <p>५.जग्गा को मोहोडा / पिछाड</p>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mohoda_front" class="control-label">
                                    अगाडी
                                </label>
                                <input type="text" name="mohoda_front" class="form-control" readonly  value="{{ !empty($formData->mohoda_front) ? $formData->mohoda_front : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mohoda_back" class="control-label">
                                    पछाडी
                                </label>
                                <input type="text" name="mohoda_back" class="form-control" readonly  value="{{ !empty($formData->mohoda_back) ? $formData->mohoda_back : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mohoda_left" class="control-label">
                                    दायाँ
                                </label>
                                <input type="text" name="mohoda_left" class="form-control" readonly  value="{{ !empty($formData->mohoda_left) ? $formData->mohoda_left : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mohoda_right" class="control-label">
                                    बायाँ
                                </label>
                                <input type="text" name="mohoda_right" class="form-control" readonly  value="{{ !empty($formData->mohoda_right) ? $formData->mohoda_right : 'n/a' }}">
                            </div>
                        </div>

                    </div>
                    {{--third row--}}

                    <p>६.बाटोको केन्द्रबिन्दुबाट छाड्नु पर्ने दुरी</p>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_duri_purba" class="control-label">
                                    पूर्व
                                </label>
                                <input type="text" name="chadnu_parne_duri_purba"  class="form-control" readonly  value="{{ !empty($formData->chadnu_parne_duri_purba) ? $formData->chadnu_parne_duri_purba : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_duri_paschim" class="control-label">
                                    पश्चिम
                                </label>
                                <input type="text" name="chadnu_parne_duri_paschim" class="form-control" readonly  value="{{ !empty($formData->chadnu_parne_duri_paschim) ? $formData->chadnu_parne_duri_paschim : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_duri_utar" class="control-label">
                                    उत्तर
                                </label>
                                <input type="text" name="chadnu_parne_duri_utar" class="form-control" readonly  value="{{ !empty($formData->chadnu_parne_duri_utar) ? $formData->chadnu_parne_duri_utar : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_duri_dakshin" class="control-label">
                                    दक्षिण
                                </label>
                                <input type="text" name="chadnu_parne_duri_dakshin" class="form-control" readonly  value="{{ !empty($formData->chadnu_parne_duri_dakshin) ? $formData->chadnu_parne_duri_dakshin : 'n/a' }}">
                            </div>
                        </div>
                    </div>
                    {{--fourth row--}}

                    <p>७.छाड्नुपर्ने न्यूनतम् सेट ब्याक</p>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_setback_purba" class="control-label">
                                    पूर्व
                                </label>
                                <input type="text" name="chadnu_parne_setback_purba" class="form-control" readonly  value="{{ !empty($formData->chadnu_parne_setback_purba) ? $formData->chadnu_parne_setback_purba : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_setback_paschim" class="control-label">
                                    पश्चिम
                                </label>
                                <input type="text" name="chadnu_parne_setback_paschim"  class="form-control" readonly  value="{{ !empty($formData->chadnu_parne_setback_paschim) ? $formData->chadnu_parne_setback_paschim : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_setback_utar" class="control-label">
                                    उत्तर
                                </label>
                                <input type="text" name="chadnu_parne_setback_utar"  class="form-control" readonly  value="{{ !empty($formData->chadnu_parne_setback_utar) ? $formData->chadnu_parne_setback_utar : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_setback_dakshin" class="control-label">
                                    दक्षिण
                                </label>
                                <input type="text" name="chadnu_parne_setback_dakshin"   class="form-control" readonly  value="{{ !empty($formData->chadnu_parne_setback_dakshin) ? $formData->chadnu_parne_setback_dakshin : 'n/a' }}">
                            </div>
                        </div>

                    </div>
                    {{--fifth row--}}

                    <p>८.नापी नक्शा अनुसार जग्गाको</p>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="napi_length" class="control-label">
                                    लम्बाई
                                </label>
                                <input type="text" name="napi_length" class="form-control" readonly  value="{{ !empty($formData->napi_length) ? $formData->napi_length : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="napi_width" class="control-label">
                                    चौडाई
                                </label>
                                <input type="text" name="napi_width" class="form-control" readonly  value="{{ !empty($formData->napi_width) ? $formData->napi_width : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="napi_field_area" class="control-label">
                                    क्षेत्रफ़ल्
                                </label>
                                <input type="text" name="napi_field_area" class="form-control" readonly  value="{{ !empty($formData->napi_field_area) ? $formData->napi_field_area : 'n/a' }}">
                            </div>
                        </div>
                    </div>
                    {{--sixth row--}}

                    {{--<p>९.साईटको नापी अनुसार जग्गाको</p>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="siteko_napi_length" class="control-label">--}}
                                    {{--लम्बाई--}}
                                {{--</label>--}}
                                {{--<input type="text" name="siteko_napi_length" class="form-control" readonly  value="{{ !empty($formData->siteko_napi_length) ? $formData->siteko_napi_length : 'n/a' }}">--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="siteko_napi_width" class="control-label">--}}
                                    {{--चौडाई--}}
                                {{--</label>--}}
                                {{--<input type="text" name="siteko_napi_width" class="form-control" readonly  value="{{ !empty($formData->siteko_napi_width) ? $formData->siteko_napi_width : 'n/a' }}">--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="siteko_napi_field_area" class="control-label">--}}
                                    {{--क्षेत्रफ़ल्--}}
                                {{--</label>--}}
                                {{--<input type="text" name="siteko_napi_field_area" class="form-control" readonly  value="{{ !empty($formData->siteko_napi_field_area) ? $formData->siteko_napi_field_area : 'n/a' }}">--}}
                            {{--</div>--}}

                        {{--</div>--}}

                    {{--</div>--}}
                    {{--seventh row--}}


                    <div class="row">
                        @if($form->landInfo->electricity_line == "भएको")
                            <div class="col-md-3">
                                <div class="form-group ">
                                    <label for="hitension_wire_distance" class="control-label">
                                        हाईटेन्सन को तार बाटछाड्नु पर्ने दुरी
                                    </label>
                                    <input type="text" name="hitension_wire_distance" class="form-control" readonly  value="{{ !empty($formData->hitension_wire_distance) ? $formData->hitension_wire_distance : 'n/a' }}">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group ">
                                    <label for="electricity_volt" class="control-label">
                                        भोल्ट
                                    </label>
                                    <input type="text" name="electricity_volt" class="form-control" readonly  value="{{ !empty($formData->electricity_volt) ? $formData->electricity_volt : 'n/a' }}">
                                </div>
                            </div>
                        @endif

                        @if($form->landInfo->river == "भएको")
                            <div class="col-md-3">
                                <div class="form-group ">
                                    <label for="river_side_bata_chodnu_parne_duri" class="control-label">
                                        नदि भएको किनारा बाट छाड्नु पर्ने दुरी
                                    </label>
                                    <input type="text" name="river_side_bata_chodnu_parne_duri" class="bod-picker form-control" readonly  value="{{ !empty($formData->river_side_bata_chodnu_parne_duri) ? $formData->river_side_bata_chodnu_parne_duri : 'n/a' }}">
                                </div>
                            </div>
                        @endif
                    </div>
                    {{--eight row--}}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="created_at_nepali" class="control-label">
                                    मिति &nbsp;
                                </label>
                                <input type="text" name="created_at_nepali" class="bod-picker form-control" readonly  value="{{ !empty($formData->created_at_nepali) ? $formData->created_at_nepali : 'n/a' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="other" class="control-label">
                                    अन्य &nbsp;
                                </label>
                                <input type="text" name="other"   class=" form-control"  readonly  value="{{ !empty($formData->other) ? $formData->other : '' }}">
                            </div>
                        </div>
                    </div>
                    {{--end of nine row --}}

                    {{--ten row--}}
                    <div class="row">
                        <div class="col-md-2 pull-right">
                            <a href="{{ route('engineer.view_rajashow',$formData->form_id) }}" type="button" class="btn btn-block btn-outline btn-primary">Next</a>
                        </div>
                    </div>
                    {{--nine row--}}
                </div>
            </div>
        </div>
    </form>

@endsection

@section('scripts')
    <!-- this should go after your </body> -->
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'assets/js/nepaliDatePicker.js') }}" ></script>
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <script>
        $(".bod-picker").nepaliDatePicker({
            dateFormat: "%D, %M %d, %y",
            closeOnDateSelect: true
        });


    </script>
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });


    </script>

@endsection
