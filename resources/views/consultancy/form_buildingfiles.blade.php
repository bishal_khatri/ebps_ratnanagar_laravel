@extends('layouts.app')
@section('css')
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">

@endsection
@section('page_title')
	नयाँ नक्शा दरखास्त फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>नयाँ नक्शा दरखास्त फाईल</h4>
@endsection

@section('content')
	<div class="row">
		<!-- tbl form_buildlingfiles -->
		<div class="col-md-12">
			{{-- {{ dd($errors) }} --}}
			<form  method="post" action="{{ route('consultancy.store_form_buildingfiles') }}" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="page" value="4" >
				<input type="hidden" name="form_id" value="{{ $formData->id }}" >
				
				<div class="col-md-12">
					<div class="panel panel-info">
						<div class="panel-wrapper collapse in" aria-expanded="true">
							<div class="panel-body">
								<div class="form-body">
									<div class="row">
										<p style="font-size: 25px; text-align: center"> समंन्धित कागज पत्र</p>
										<hr>
										<div class="col-md-3">
											<div class="form-group ">
												<label for="lalpurja" class="control-label">
													१.लालपुर्जा&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="file" name="lalpurja" id="input-file-now" class="dropify"  accept=".jpg,.png,.bmp,.jpeg" required/>
												@if($errors->has('lalpurja'))
													<span class="text-danger">{{ $errors->first('lalpurja') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="citizenship" class="control-label">
													२.नागरिकता&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="file" name="citizenship" id="input-file-now" class="dropify"  accept=".jpg,.png,.bmp,.jpeg" required/>
												@if($errors->has('citizenship'))
													<span class="text-danger">{{ $errors->first('citizenship') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="blueprint" class="control-label">
													३.नापी नक्शा&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												
												<input type="file" name="blueprint" id="input-file-now" class="dropify"  accept=".jpg,.png,.bmp,.jpeg" required/>
												@if($errors->has('blueprint'))
													<span class="text-danger">{{ $errors->first('blueprint') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="house_map" class="control-label">
													४.घरको नक्शा&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="file" name="house_map" id="input-file-now" class="dropify"  accept=".pdf" required/>
												@if($errors->has('house_map'))
													<span class="text-danger">{{ $errors->first('house_map') }}</span>
												@endif
											</div>
										</div>
									
									
									</div>
									<!--/ end of first row-->
									
									
									<!-- second row -->
									<div class="row">
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="tero_rashid" class="control-label">
													५.तीरो तिरेको रशिद <sup class="text-danger">(अनिवार्य)</sup>
												</label>
												
												<input type="file" name="tero_rashid" id="input-file-now" class="dropify" required  accept=".jpg,.png,.jpeg,.bmp"/>
												@if($errors->has('tero_rashid'))
													<span class="text-danger">{{ $errors->first('tero_rashid') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="naamsari" class="control-label">
													६.नाम सारीको कागज
												</label>
												<input type="file" name="naamsari" id="input-file-now" class="dropify"  accept=".jpg,.png,.bmp,.jpeg"/>
												@if($errors->has('naamsari'))
													<span class="text-danger">{{ $errors->first('naamsari') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<label for="ward_sifaris" class="control-label">
													७.वडा को सीफरीस
												</label>
												
												<input type="file" name="ward_sifaris" id="input-file-now" class="dropify"  accept=".jpg,.png,.gif,.jpeg"/>
												@if($errors->has('ward_sifaris'))
													<span class="text-danger">{{ $errors->first('ward_sifaris') }}</span>
												@endif
											</div>
										</div>
										
										
										{{--<div class="col-md-3">--}}
										{{--<div class="form-group ">--}}
										{{--<label for="sampanna_patra" class="control-label">--}}
										{{--८.सम्पन्न प्रमाण पत्र--}}
										{{--</label>--}}
										{{----}}
										{{--<input type="file" name="sampanna_patra" id="input-file-now" class="dropify"  accept=".jpg,.png,.gif,.jpeg"/>--}}
										{{--@if($errors->has('sampanna_patra'))--}}
										{{--<span class="text-danger">{{ $errors->first('sampanna_patra') }}</span>--}}
										{{--@endif--}}
										{{--</div>--}}
										{{--</div>--}}
									
									
									
									</div>
									<!--/ end of second row-->
									
									<!-- third row -->
									<div class="row">
										<p style="font-size: 25px; text-align: center"> अन्य  कागज पत्र</p>
										<hr>
										
										<div class="col-md-4">
											<div class="form-group ">
												<label for="other_document_1" class="control-label">
													१.अन्य विवरण १
												</label>
												<input type="file" name="other_document_1" id="input-file-now" class="dropify"  accept=".doc,.docx,.pdf"/>
												@if($errors->has('other_document_1'))
													<span class="text-danger">{{ $errors->first('other_document_1') }}</span>
												@endif
											</div>
											<div class="form-group ">
												<label for="other_document_1_description" class="control-label">
													अन्य विवरण १ को  विवरण
												</label>
												<input type="text" name="other_document_1_description" class="form-control" placeholder="अन्य विवरण १ को विवरण" />
												@if($errors->has('other_document_1_description'))
													<span class="text-danger">{{ $errors->first('other_document_1_description') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group ">
												<label for="other_document_2" class="control-label">
													२.अन्य विवरण २
												</label>
												<input type="file" name="other_document_2" id="input-file-now" class="dropify"  accept=".doc,.docx,.pdf,.jpeg,.jpg,.png"/>
												@if($errors->has('other_document_2'))
													<span class="text-danger">{{ $errors->first('other_document_2') }}</span>
												@endif
											</div>
											<div class="form-group ">
												<label for="other_document_2_description" class="control-label">
													अन्य विवरण २ को  विवरण
												</label>
												<input type="text" name="other_document_2_description" class="form-control" placeholder="अन्य विवरण २ को विवरण" />
												@if($errors->has('other_document_2_description'))
													<span class="text-danger">{{ $errors->first('other_document_2_description') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group ">
												<label for="other_document_3" class="control-label">
													३.अन्य विवरण ३
												</label>
												<input type="file" name="other_document_3" id="input-file-now" class="dropify"  accept=".doc,.docx,.pdf,.jpeg,.jpg,.png"/>
												@if($errors->has('other_document_3'))
													<span class="text-danger">{{ $errors->first('other_document_3') }}</span>
												@endif
											</div>
											<div class="form-group ">
												<label for="other_document_3_description" class="control-label">
													अन्य विवरण ३ को  विवरण
												</label>
												<input type="text" name="other_document_3_description" class="form-control" placeholder="अन्य विवरण ३ को विवरण" />
												@if($errors->has('other_document_3_description'))
													<span class="text-danger">{{ $errors->first('other_document_3_description') }}</span>
												@endif
											</div>
										</div>
									</div>
									<!--/ end of third row-->
									<!-- last row -->
									<hr>
									<div class="row">
										<div class="col-lg-2 col-sm-4 col-xs-12 pull-left">
											<a href="{{route('consultancy.add_form_floorinfo',$formData->id)}}" class="btn btn-block btn-info text-white">Back</a>
										</div>
										<div class="col-lg-2 col-sm-4 col-xs-12 pull-right">
											<button type="submit" class="btn btn-block btn-info">Submit</button>
										</div>
									</div>
									<!-- end of third row -->
								</div>
								{{-- end form-body --}}
							</div>
							{{-- panel-body --}}
						</div>
						{{-- end panel-wrapper --}}
					</div>
					{{-- end panel --}}
				</div>
				{{-- end col-md-12 --}}
			</form>
			<!-- end form -->
		</div>
		<!-- end of form_buildlingfiles -->
	</div>
	
@include('permission.model')
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
	<script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });
	</script>

@endsection
