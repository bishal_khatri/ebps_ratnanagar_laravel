@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
@endsection
@section('page_title')
	अस्वीकृत फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>अस्वीकृत फाईल</h4>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<div class="table-responsive">
								<table id="form_id" class="display nowrap dataTable table-bordered">
									<thead>
									<tr>
										<th width="50" class="text-center">S.N.</th>
										<th width="150" class="text-center">दर्ता नं.</th>
										<th class="text-center">जग्गा धनीको नाम थर</th>
										<th class="text-center">ठेगाना</th>
										<th class="text-center">वडा नं</th>
										<th class="text-center">निर्माण किसिम</th>
										<th class="text-center">दर्ता मिति</th>
										<th class="text-center"></th>
									</tr>
									</thead>
									<tbody>
									@if (isset($formData))
										@foreach($formData as $value)
											<tr>
												<td class="text-center">{{ $loop->iteration }}</td>
												<td class="text-center">{{ $value->darta_number }}</td>
												<td class="text-center">{{ $value->sambodhan ?? '' }}&nbsp;{{ $value->field_owner_name ?? '' }}</td>
												<td class="text-center">{{ $value->field_owner_address ?? '' }}</td>
												<td class="text-center">{{ $value->landInfo->current_ward_number ?? '' }}</td>
												<td class="text-center">{{ $value->building_type ?? '' }}</td>
												<td class="text-center">{{ $value->created_at_nepali ?? '' }}</td>
												<td class="text-center">
													<a class="btn btn-warning btn-block btn-sm" data-toggle="modal" data-target="#myModal" >Error</a>
													@if($value->form_level == 6)
														<a href="{{ route('consultancy.rejected.edit_dpcform',$value->id) }}" type="button" class="btn btn-info btn-block btn-sm">Edit DPC</a>
													@elseif($value->form_level == 11)
														<a href="{{ route('consultancy.rejected.edit_sampanna',$value->id) }}" type="button" class="btn btn-info btn-block btn-sm">Edit Sampanna</a>
													@else
														<a href="{{ route('consultancy.rejected.edit_form',$value->id) }}" type="button" class="btn btn-info btn-block btn-sm">Edit</a>
													@endif
												</td>
											
											</tr>
										@endforeach
									@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('consultancy.modal')
	@include('permission.model')
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
	<script>

        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            //Bootstrap-TouchSpin

            $(document).ready( function () {
                $('#form_id').DataTable({"bSort" : false});
            });
        });
	
	
	</script>
@endsection
