@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	सम्पन्न फारम
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>सम्पन्न फारम</h4>
@endsection

@section('content')
	<div class="row">
		<!-- tbl of form_landinfo -->
		<div class="col-md-12">
			
			<form  method="post"  action="{{ route('consultancy.store_form_sampanna') }}">
				@csrf
				<input type="hidden" name="page" value="2" id="">
				<input type="hidden" name="form_id" value= {{ $formData->id }} >
                <input type="hidden" name="action" value= 'add'>

				<div class="col-md-12">
					<div class="panel panel-info">
						
						<div class="panel-wrapper collapse in" aria-expanded="true">
							<div class="panel-body">
								
								<table class="table table-sm table-bordered table-hover">
									<tr class="d-none d-print-table-row text-center font-weight-bold">
										<td colspan="5">
											निर्माण सम्पन्न प्रतिवेदन सम्बन्धमा
										</td>
									</tr>
									<tr>
										<td colspan="5">
											<p>महोदय,</p>
											<p style="text-indent:60px;">
												उपरोक्त सम्बन्धमा {{ $formData->buildingInfo->district ?? '............' }}
												जिल्ला {{ $formData->landInfo->village_name ?? '............' }} गा.वि.स / न.पा
												वडा नं {{ $formData->landInfo->current_ward_number ?? '............' }} बस्ने

												<span class="border_line">{{ $formData->sambodhan ?? '............' }} 

												{{ $formData->field_owner_name ?? '............' }}</span> 
												को
												र.न.पा वडा नं {{ $formData->landInfo->current_ward_number ?? '............' }}
												साबिक 
												कि.नं {{ $formData->landInfo->kitta_number ?? '............' }} मा पास भएको {{ $formData->building_type ?? '............' }}को निर्माण सम्पन्न प्रमाण पत्र पाऊ भनि मिति ........................दर्ता नं 

												{{ $formData->darta_number ?? '............' }} मा दिनु भएको निबेदन तोक आदेश भए अनुसार उक्त निर्माण कार्यको स्थल गत निरक्षणमा सम्पूर्ण नाप जाँच गरि यो निर्माण सम्पनको प्रतिवेदन पेश गरको छु |
											</p>
										</td>
									</tr>
									<tr>
										<td class="font-weight-bold" colspan="5">मापदण्ड</td>
									</tr>
									
									<tr>
										<td>१.</td>
										<td>ज.ध.प्र.पु. क्षेत्रफ़ल्</td>
										<td colspan="3">
											<input type="text" name="field_area" value="{{old('field_area') }}">
											<span>ब.फि</span>
											@if($errors->has('field_area'))
													<span class="text-danger">{{ $errors->first('field_area') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>२.</td>
										<td>साईट प्लान क्षेत्रफ़ल्</td>
										<td colspan="3">
											<input type="text" name="site_plan_area" value="{{old('site_plan_area') }}">
											<span>ब.फि</span>
											@if($errors->has('site_plan_area'))
													<span class="text-danger">{{ $errors->first('site_plan_area') }}</span>
											@endif
										</td>
									</tr>
									<tr class="text-center">
										<td>क्र.सं.</td>
										<td>विवरण</td>
										<td>मापदण्ड अनुसार</td>
										<td>स्विकृत अनुसार</td>
										<td>निर्माण स्थिति </td>
									</tr>
									<tr>
										<td>१.</td>
										<td>
											ग्रउण्ड कभरेज
											<input type="text" name="ground_coverage" style="width:35px;" value="{{old('ground_coverage') }}">
											<span>%</span>

											@if($errors->has('ground_coverage'))
											<br>
													<span class="text-danger">{{ $errors->first('ground_coverage') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="ground_coverage_mapdanda" placeholder="व.फि" value="{{old('ground_coverage_mapdanda') }}">
											@if($errors->has('ground_coverage_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('ground_coverage_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="ground_coverage_swikrit_anusar" placeholder="व.फि" value="{{old('ground_coverage_swikrit_anusar') }}">
			
											@if($errors->has('ground_coverage_swikrit_anusar'))
											<br>
													<span class="text-danger">{{ $errors->first('ground_coverage_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="ground_coverage_nirman_esthithi" placeholder="व.फि" value="{{old('ground_coverage_nirman_esthithi') }}">
											@if($errors->has('ground_coverage_nirman_esthithi'))
											<br>
													<span class="text-danger">{{ $errors->first('ground_coverage_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>२.</td>
										<td>तला</td>
										<td>
											<input type="text" name="talla_mapdanda" value="{{old('talla_mapdanda') }}">
											@if($errors->has('talla_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('talla_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="talla_swikrit_anusar" value="{{old('talla_swikrit_anusar') }}">
											@if($errors->has('talla_swikrit_anusar'))
											<br>
													<span class="text-danger">{{ $errors->first('talla_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="talla_nirman_esthithi" value="{{old('talla_nirman_esthithi') }}">
											@if($errors->has('talla_nirman_esthithi'))
											<br>
													<span class="text-danger">{{ $errors->first('talla_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>३.</td>
										<td>उचाई</td>
										<td>
											<input type="text" name="height_mapdanda" placeholder="फि" value="{{old('height_mapdanda') }}">
											@if($errors->has('height_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('height_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="height_swikrit_anusar" placeholder="फि" value="{{old('height_swikrit_anusar') }}">
											@if($errors->has('height_swikrit_anusar'))
											<br>
													<span class="text-danger">{{ $errors->first('height_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="height_nirman_esthithi" placeholder="फि"value="{{old('height_nirman_esthithi') }}">
											@if($errors->has('height_nirman_esthithi'))
											<br>
													<span class="text-danger">{{ $errors->first('height_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>४.</td>
										<td>कूल फ्लोर एरिया</td>
										<td>
											<input type="text" name="floor_area_mapdanda" placeholder="व.फि" value="{{old('floor_area_mapdanda') }}">
											@if($errors->has('floor_area_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('floor_area_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="floor_area_swikrit_anusar" placeholder="व.फि" value="{{old('floor_area_swikrit_anusar') }}">
											@if($errors->has('floor_area_swikrit_anusar'))
											<br>
													<span class="text-danger">{{ $errors->first('floor_area_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="floor_area_nirman_esthithi" placeholder="व.फि" value="{{old('floor_area_nirman_esthithi') }}">
											@if($errors->has('floor_area_nirman_esthithi'))
											<br>
													<span class="text-danger">{{ $errors->first('floor_area_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>५.</td>
										<td>स्विकृत भन्दा बदी</td>
										<td>
											<input type="text" name="swikrit_vanda_badi_mapdanda" placeholder="व.फि" value="{{old('swikrit_vanda_badi_mapdanda') }}">
											@if($errors->has('swikrit_vanda_badi_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('swikrit_vanda_badi_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="swikrit_vanda_badi_swikrit_anusar" placeholder="व.फि" value="{{old('swikrit_vanda_badi_swikrit_anusar') }}">
											@if($errors->has('swikrit_vanda_badi_swikrit_anusar'))
											<br>
													<span class="text-danger">{{ $errors->first('swikrit_vanda_badi_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="swikrit_vanda_badi_nirman_esthithi" placeholder="व.फि" value="{{old('swikrit_vanda_badi_nirman_esthithi') }}">
											@if($errors->has('swikrit_vanda_badi_nirman_esthithi'))
											<br>
													<span class="text-danger">{{ $errors->first('swikrit_vanda_badi_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>६.</td>
										<td>आर. ओ. डब्लु</td>
										<td>
											<input type="text" name="r_o_w_mapdanda" placeholder="मि" value="{{old('r_o_w_mapdanda') }}">
											@if($errors->has('r_o_w_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('r_o_w_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="r_o_w_swikrit_anusar" placeholder="मि" value="{{old('r_o_w_swikrit_anusar') }}">
											@if($errors->has('r_o_w_swikrit_anusar'))
											<br>
													<span class="text-danger">{{ $errors->first('r_o_w_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="r_o_w_nirman_esthithi" placeholder="मि" value="{{old('r_o_w_nirman_esthithi') }}">
											@if($errors->has('r_o_w_nirman_esthithi'))
											<br>
													<span class="text-danger">{{ $errors->first('r_o_w_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>७.</td>
										<td>सेट ब्यक</td>
										<td>
											<input type="text" name="setback_mapdanda" placeholder="मि" value="{{old('setback_mapdanda') }}">
											@if($errors->has('setback_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('setback_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="setback_swikrit_anusar" placeholder="मि" value="{{old('setback_swikrit_anusar') }}">
											@if($errors->has('setback_swikrit_anusar'))
													<span class="text-danger">{{ $errors->first('setback_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="setback_nirman_esthithi" placeholder="मि" value="{{old('setback_nirman_esthithi') }}">
											@if($errors->has('setback_nirman_esthithi'))
											<br>
													<span class="text-danger">{{ $errors->first('setback_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>८.</td>
										<td>
											बिधुत लाईन 
											<input type="text" name="electricity_line" value="{{ old('electricity_line') }}" style="width:35px;">
											<span>के.भि</span>

											@if($errors->has('electricity_line'))
											<br>
													<span class="text-danger">{{ $errors->first('electricity_line') }}</span>
											@endif

										</td>
										<td>
											<input type="text" name="electricity_line_mapdanda" placeholder="फि"value="{{old('electricity_line_mapdanda') }}">
											@if($errors->has('electricity_line_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('electricity_line_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="electricity_line_swikrit_anusar" placeholder="फि" value="{{old('electricity_line_swikrit_anusar') }}">
											@if($errors->has('electricity_line_swikrit_anusar'))
													<span class="text-danger">{{ $errors->first('electricity_line_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="electricity_line_nirman_esthithi" placeholder="फि"value="{{old('electricity_line_nirman_esthithi') }}">
											@if($errors->has('electricity_line_nirman_esthithi'))
											<br>
													<span class="text-danger">{{ $errors->first('electricity_line_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td>९.</td>
										<td>नदि किनार</td>
										<td>
											<input type="text" name="river_side_mapdanda" placeholder="फि" value="{{old('river_side_mapdanda') }}">
											@if($errors->has('river_side_mapdanda'))
											<br>
													<span class="text-danger">{{ $errors->first('river_side_mapdanda') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="river_side_swikrit_anusar" placeholder="फि" value="{{old('river_side_swikrit_anusar') }}">
											@if($errors->has('river_side_swikrit_anusar'))
											<br>
													<span class="text-danger">{{ $errors->first('river_side_swikrit_anusar') }}</span>
											@endif
										</td>
										<td>
											<input type="text" name="river_side_nirman_esthithi"" placeholder="फि" value="{{old('river_side_nirman_esthithi') }}">
											@if($errors->has('river_side_nirman_esthithi'))
											<br>
														<span class="text-danger">{{ $errors->first('river_side_nirman_esthithi') }}</span>
											@endif
										</td>
									</tr>
									<tr>
										<td colspan="5">
											कैफियत : स्वीकृत नक्शा भन्दा विपरित  भए त्यसको विवरण :
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<strong>कन्सल्टेन्टको तर्फबाट</strong> 
										</td>

										<td > <strong>ठेकेदार तर्फबाट</strong></td>
										<td >
											<strong>कार्यालयको प्राविधिक शाखाको तर्फबाट </strong>
										</td>
									</tr>

									<tr>
										<td></td>
										<td >
											नाम : &nbsp; 
											<input type="text" name="entry_name" value="{{ old('entry_name') }}">
											@if($errors->has('entry_name'))
											<br>
												<span class="text-danger">{{ $errors->first('entry_name') }}</span>
											@endif

										</td>

										<td>
											नाम : ...........................
										</td>

										<td >
											नाम : ...........................
										</td>
										
									</tr>

									<tr>
										<td></td>
										<td>
											पद : &nbsp;
											<input type="text" name="consultancy_post" value="{{ old('consultancy_post') }}">
											
											@if($errors->has('consultancy_post'))
												<br>
												<span class="text-danger">{{ $errors->first('consultancy_post') }}</span>
											@endif

										</td>

										<td>सहि: ...........................</td>

										<td>
											पद : ...........................

										</td>
									</tr>

									<tr>
										<td></td>

										<td>
											कन्सल्टेन्सी: <input type="text" name="consultancy_name" value="{{ old('consultancy_name') }} "> 

											@if($errors->has('consultancy_name'))
												<br>
												<span class="text-danger">{{ $errors->first('consultancy_name') }}</span>
											@endif

										</td>

										<td></td>

										<td>सहि: ...........................</td>

									</tr>

									<tr>
										<td></td>
										<td>सहि: ...........................</td> 
										
									</tr>
									<tr class="d-block-table-row d-print-none">
										<td colspan="5">
											<center>
												<button type="submit" class="btn btn-primary"  >
													Proceed
												</button>
											</center>
										</td>
									</tr>
								</table>
								{{-- end form-body --}}
							</div>
							{{-- panel-body --}}
						</div>
						{{-- end panel-wrapper --}}
					</div>
					{{-- end panel --}}
				
				</div>
				{{-- end col-md-12 --}}
			
			</form>
			<!-- end form -->
		
		</div>
		<!-- end form_landinfo -->
	</div>
	
@endsection

@section('scripts')

@endsection
