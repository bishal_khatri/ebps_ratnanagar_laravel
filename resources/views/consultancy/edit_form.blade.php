@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <style>
        .img-rounded {
            border-radius: 6px;
            padding: 5px;
        }

        .option1{
            background-color: transparent;
            padding: 1px;
            width: 20px;
            border: none;
            font-size: 14px;
            color: #333;
            text-align: center;"
        }

        .option2{
            background-color: transparent;
            padding: 1px;
            width: 45px;
            border: none;
            font-size: 14px;
            color: #333;
            text-align: center;"
        }
    </style>
@endsection
@section('page_title')
    नयाँ नक्शा दरखास्त फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>नयाँ नक्शा दरखास्त फाईल</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="panel panel-info">
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            @if($action == 'administration')
                                <form action="{{ route('administration.post_edit_form') }}" method="post" enctype="multipart/form-data">
                            @else
                                        <form action="{{ route('consultancy.rejected.post_edit_form') }}" method="post" enctype="multipart/form-data">
                            @endif
                                    @csrf
                                <input type="hidden" value="{{$formData->id}}" name="form_id">
                                <input type="hidden" value="{{$action}}" name="action">

                                <div class="form-body">
                                    @if($action == 'administration')
                                        <a href="{{ route('administration.get_edit',$formData->id) }}" class="btn btn-primary btn-outline"><i class="fa fa-arrow-circle-left"></i>&nbsp;Back</a>
                                    <hr>
                                    @endif
                                    <h3 style="padding:0px;margin:0px;font-size:19px;">तपसिल</h3>

                                    <hr>
                                    <p style="text-indent: 5%">
                                        उपरोक्त सम्बन्धमा
                                        <select name="sambandha_ma" required id="option" data-toggle="tooltip" title="अनिबार्य" data-placement="top" class="required">
                                            <option value="मैले" @if($formData->sambandha_ma == 'मैले') selected @endif >मैले</option>
                                            <option value="हामीले" @if($formData->sambandha_ma == 'हामीले') selected @endif>हामीले</option>
                                        </select>

                                        तपशीलमा उल्लेखित जग्गामा देहाय बमोजिमको

                                        <select required name="building_type"  onchange="showSampana(this)" 	data-toggle="tooltip" title="अनिबार्य">
                                            <option value="नयाँ घर निर्माण" @if($formData->building_type == 'नयाँ घर निर्माण') selected @endif>नयाँ घर निर्माण</option>
                                            <option value="तल्ला थप" @if($formData->building_type == 'तल्ला थप') selected @endif>तल्ला थप</option>
                                        </select>

                                        कार्यका लागि घर नक्शा सहितको दरखास्त फारम साथै जग्गामा हक पुगेको आवश्यक कागजात प्रमाण समेत राखी पेश गरेको

                                        <strong>
                                            <input id="option_1"  readonly name="option_1" class="option1">
                                        </strong>

                                        । उक्त भवन निर्माण कार्यका लागि नक्शा पाश स्वीकृत गरी पाउन अनुरोध

                                        <strong>
                                            <input id="option_2" readonly name="option_2" class="option2">
                                        </strong>

                                        । यस दरखास्तमा लेखिएको व्यहोरा ठिक साँचो हो । झुठा ठहरे कानुन बमोजिम सहुँला बुझाउँला ।
                                    </p>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="sambodhan" class="control-label">सम्बोधन  <sup class="text-danger">(अनिवार्य)</sup> </label>
                                                <select class="form-control" name="sambodhan" required>
                                                    <option value="श्री" @if($formData->sambodhan =='श्री') selected @endif>श्री </option>
                                                    <option value="सुश्री" @if($formData->sambodhan=='सुश्री') selected @endif>सुश्री</option>
                                                </select>

                                                @if($errors->has('sambodhan'))
                                                    <span class="text-danger">{{ $errors->first('sambodan') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="field_owner_name" class="control-label">१.जग्गा धनी को नाम थर&nbsp;<sup class="text-danger">(अनिवार्य)</sup> </label>
                                                <input type="text" name="field_owner_name" id="owner_name" required="" class="form-control"  value="{{ !empty($formData->field_owner_name) ? $formData->field_owner_name : 'n/a' }}">

                                                @if($errors->has('field_owner_name'))
                                                    <span class="text-danger">{{ $errors->first('field_owner_name') }}</span>
                                                @endif
                                            </div>

                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="field_owner_address" class="control-label">
                                                    २.जग्गा धनीको ठेगाना&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="field_owner_address"  required='' class="form-control"  value="{{ !empty($formData->field_owner_address) ? $formData->field_owner_address : 'n/a' }}">

                                                @if($errors->has('field_owner_address'))
                                                    <span class="text-danger">{{ $errors->first('field_owner_address') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="field_owner_age" class="control-label">
                                                    ३.बर्ष&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="field_owner_age"  required='' class="form-control"  value="{{ !empty($formData->field_owner_age) ? $formData->field_owner_age : 'n/a' }}">

                                                @if($errors->has('field_owner_age'))
                                                    <span class="text-danger">{{ $errors->first('field_owner_age') }}</span>
                                                @endif



                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    {{-- end of first row --}}

                                    {{-- start of 2nd row --}}
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="father_husband_option" class="control-label">
                                                    ४.बाबु / पति&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select class="form-control" name="father_husband_option" required>
                                                    <option value="बाबु" @if($formData->father_husband_option =='बाबु') selected @endif>बाबु </option>
                                                    <option value="पति" @if($formData->father_husband_option =='पति') selected @endif>पति</option>
                                                </select>

                                                @if($errors->has('father_husband_option'))
                                                    <span class="text-danger">{{ $errors->first('father_husband_option') }}</span>
                                                @endif
                                            </div></div>


                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="father_husband_name" class="control-label">
                                                    ५.बाबु/पतिको नाम, थर&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="father_husband_name" required=""  class="form-control" value="{{ !empty($formData->father_husband_name) ? $formData->father_husband_name : 'n/a' }}">

                                                @if($errors->has('father_husband_name'))
                                                    <span class="text-danger">{{ $errors->first('father_husband_name') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="grandfather_option" class="control-label">
                                                    ६.बाजे / ससुरा&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <select class="form-control" name="grandfather_option" required>
                                                    <option value="बाजे" @if($formData->grandfather_option =='बाजे') selected @endif>बाजे </option>
                                                    <option value="ससुरा" @if($formData->grandfather_option =='ससुरा') selected @endif>ससुरा</option>
                                                </select>

                                                @if($errors->has('grandfather_option'))
                                                    <span class="text-danger">{{ $errors->first('grandfather_option') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="grandfather_name" class="control-label">
                                                    ७.बाजे / ससुरा नाम, थर&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="grandfather_name"  required='' class="form-control" value="{{ !empty($formData->grandfather_name) ? $formData->grandfather_name : 'n/a' }}">

                                                @if($errors->has('grandfather_name'))
                                                    <span class="text-danger">{{ $errors->first('grandfather_name') }}</span>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <!--/ end of second row-->

                                    {{-- start of 2nd row --}}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="field_owner_job" class="control-label">
                                                    ८.जग्गा धनीको पेशा/व्यवसाय &nbsp;<sup class="text-danger">(अनिवार्य) </sup>
                                                </label>
                                                <input type="text" name="field_owner_job"  required='' class="form-control" value="{{ !empty($formData->field_owner_job) ? $formData->field_owner_job : 'n/a' }}">

                                                @if($errors->has('field_owner_job'))
                                                    <span class="text-danger">{{ $errors->first('field_owner_job') }}</span>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <!--/ end of third row-->
                                    {{-- form_personalinfo END --}}
                                    <hr>
                                    <br>
                                    {{-- form_landinfo START --}}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="kitta_number" class="control-label">
                                                    १.हाल सर्भे कित्ता नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="kitta_number"  required="" class="form-control" value="{{ $formData->landInfo->kitta_number ?? 'n/a' }}">

                                                @if($errors->has('kitta_number'))
                                                    <span class="text-danger">{{ $errors->first('kitta_number') }}</span>
                                                @endif

                                            </div>

                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="sabik_kitta_number" class="control-label">
                                                    २.साविक कित्ता नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>

                                                </label>

                                                <input type="text" name="sabik_kitta_number"  required="" class="form-control" value="{{ $formData->landInfo->sabik_kitta_number ?? 'n/a'  }}">

                                                @if($errors->has('sabik_kitta_number'))
                                                    <span class="text-danger">{{ $errors->first('sabik_kitta_number') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="sabik_ward_number" class="control-label">
                                                    ३.साविक गा.वि.स / वडा नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="sabik_ward_number"  required='' class="form-control" value="{{ $formData->landInfo->sabik_ward_number ?? 'n/a'  }}">

                                                @if($errors->has('sabik_ward_number'))
                                                    <span class="text-danger">{{ $errors->first('sabik_ward_number') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="field_area" class="control-label">
                                                    ४.जग्गा क्षेत्रफल &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="field_area"  required='' class="form-control" value="{{ $formData->landInfo->field_area ?? 'n/a'  }}">

                                                @if($errors->has('field_area'))
                                                    <span class="text-danger">{{ $errors->first('field_area') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <!--/row-->
                                    {{-- end of first row --}}

                                    {{-- start of 2nd row --}}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="village_name" class="control-label">
                                                    ५.बाटो / मार्ग / टोलको नाम&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="village_name"  required="" class="form-control" value="{{ $formData->landInfo->village_name ?? 'n/a' }}">

                                                @if($errors->has('village_name'))
                                                    <span class="text-danger">{{ $errors->first('village_name') }}</span>
                                                @endif
                                            </div>

                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="jagga_mohada" class="control-label">
                                                    ६.जग्गाको मोहोडा&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="jagga_mohada"  required="" class="form-control" value="{{ $formData->landInfo->jagga_mohada ?? 'n/a' }}">

                                                @if($errors->has('jagga_mohada'))
                                                    <span class="text-danger">{{ $errors->first('jagga_mohada') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="ward_number" class="control-label">
                                                    ७.जग्गा भएको वडा नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="ward_number"  required='' class="form-control" value="{{ $formData->landInfo->ward_number ?? 'n/a' }}">

                                                @if($errors->has('ward_number'))
                                                    <span class="text-danger">{{ $errors->first('ward_number') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="current_ward_number" class="control-label">
                                                    ८.हालको वडा नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select name="current_ward_number" id="" class="form-control" required>
                                                    @if(isset($ward_number))
                                                        @foreach($ward_number as $value)
                                                            <option value="{{ $value->ward_number }}" @if($formData->landinfo->current_ward_number == $value->ward_number) selected @endif>{{ $value->ward_number }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                                @if($errors->has('current_ward_number'))
                                                    <span class="text-danger">{{ $errors->first('current_ward_number') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <!--/ end of second row-->

                                    {{-- start of 2nd row --}}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="sabik_kittama_ghar" class="control-label">
                                                    ९.हालको/साविक कित्तामा घर
                                                </label>

                                                <select class="form-control" name="sabik_kittama_ghar" required>
                                                    <option value="भएको" @if($formData->landInfo->sabik_kittama_ghar =='भएको') selected @endif>भएको</option>
                                                    <option value="नभएको" @if($formData->landInfo->sabik_kittama_ghar =='नभएको') selected @endif>नभएको</option>

                                                </select>

                                                @if($errors->has('sabik_kittama_ghar'))
                                                    <span class="text-danger">{{ $errors->first('sabik_kittama_ghar') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="haak_pugeko" class="control-label">
                                                    १०.जग्गामा हक पुगेको बिवरण&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <select class="form-control" name="haak_pugeko" required>
                                                    <option value="पुस्तौनी" @if($formData->landInfo->haak_pugeko =='पुस्तौनी') selected @endif>पुस्तौनी</option>
                                                    <option value="राजिनामा" @if($formData->landInfo->haak_pugeko =='राजिनामा') selected @endif>राजिनामा</option>
                                                    <option value="बकसपत्र" @if($formData->landInfo->haak_pugeko =='बकसपत्र') selected @endif>बकसपत्र</option>
                                                    <option value="हाल साविक" @if($formData->landInfo->haak_pugeko =='हाल साविक&nbsp;(हा.सा.)') selected @endif>हाल साविक&nbsp;(हा.सा.)</option>
                                                    <option value="गुठी" @if($formData->landInfo->haak_pugeko =='गुठी') selected @endif>गुठी</option>
                                                    <option value="अंशबण्डा" @if($formData->landInfo->haak_pugeko =='अंशबण्डा') selected @endif>अंशबण्डा</option>
                                                </select>

                                                @if($errors->has('haak_pugeko'))
                                                    <span class="text-danger">{{ $errors->first('haak_pugeko') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="electricity_line" class="control-label">
                                                    ११.हाईटेन्सनको  तार नजिक&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <select class="form-control" name="electricity_line" required>
                                                    <option value="भएको" @if($formData->landInfo->electricity_line =='भएको') selected @endif>भएको</option>
                                                    <option value="नभएको" @if($formData->landInfo->electricity_line =='भएको') selected @endif>नभएको</option>
                                                </select>

                                                @if($errors->has('electricity_line'))
                                                    <span class="text-danger">{{ $errors->first('electricity_line') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="river" class="control-label">
                                                    १२.नदी / नहर / कुलो&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select class="form-control" name="river" required>
                                                    <option value="नभएको" @if($formData->landInfo->river =='नभएको') selected @endif>नभएको</option>
                                                    <option value="नदी" @if($formData->landInfo->river=='नदी') selected @endif>नदी</option>
                                                    <option value="नहर" @if($formData->landInfo->river=='नहर') selected @endif>नहर</option>
                                                </select>

                                                @if($errors->has('river'))
                                                    <span class="text-danger">{{ $errors->first('river') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    {{-- form_landinfo END --}}

                                    <hr>
                                    <br>
                                    {{-- form_buildinginfo START --}}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="building_type" class="control-label">
                                                    १.घर निर्माणको वर्ग&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select class="form-control" name="building_category" required>
                                                    <option value="क" @if($formData->building_category =='क') selected @endif>वर्ग (क)</option>
                                                    <option value="ख" @if($formData->building_category =='ख') selected @endif>वर्ग (ख)</option>
                                                    <option value="ग" @if($formData->building_category =='ग') selected @endif>वर्ग (ग)</option>
                                                    <option value="घ" @if($formData->building_category =='घ') selected @endif>वर्ग (घ)</option>
                                                </select>

                                                @if($errors->has('building_category'))
                                                    <span class="text-danger">{{ $errors->first('building_category') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="building_structure" class="control-label">
                                                    २.भवनको स्ट्रक्चर&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select class="form-control" name="building_structure" required>
                                                    <option value="फ्रेम" @if($formData->buildingInfo->building_structure =='फ्रेम') selected @endif>फ्रेम</option>
                                                    <option value="वाल" @if($formData->buildingInfo->building_structure =='वाल') selected @endif>वाल</option>
                                                </select>

                                                @if($errors->has('building_structure'))
                                                    <span class="text-danger">{{ $errors->first('building_structure') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="sabik_ward_number" class="control-label">
                                                    ३.प्लिन्थ लेभलको उचाई&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="plith_height"  required='' class="form-control" value="{{ $formData->buildingInfo->plith_height ?? 'n/a' }}">

                                                @if($errors->has('plith_height'))
                                                    <span class="text-danger">{{ $errors->first('plith_height') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="foundation_size" class="control-label">
                                                    ४.जगको साईज &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="foundation_size"  required='' class="form-control" value="{{ $formData->buildingInfo->foundation_size ?? 'n/a' }}">

                                                @if($errors->has('foundation_size'))
                                                    <span class="text-danger">{{ $errors->first('foundation_size') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    {{-- end of first row --}}

                                    {{-- start of 2nd row --}}
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="pillar_size" class="control-label">
                                                    ५.पिलरको साईज़&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="pillar_size"  required="" class="form-control" value="{{ $formData->buildingInfo->pillar_size ?? 'n/a' }}">

                                                @if($errors->has('pillar_size'))
                                                    <span class="text-danger">{{ $errors->first('pillar_size') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="pillar_rod_size" class="control-label">
                                                    ६.पिलरको डण्डी साईज़&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="pillar_rod_size"  required="" class="form-control" value="{{ $formData->buildingInfo->pillar_rod_size ?? 'n/a' }}">

                                                @if($errors->has('pillar_rod_size'))
                                                    <span class="text-danger">{{ $errors->first('pillar_rod_size') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="pillar_rod_number" class="control-label">
                                                    ७.पिलरको डण्डी साईज़&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="pillar_rod_number"  required="" class="form-control" value="{{ $formData->buildingInfo->pillar_rod_number ?? 'n/a' }}">

                                                @if($errors->has('pillar_rod_number'))
                                                    <span class="text-danger">{{ $errors->first('pillar_rod_number') }}</span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="pillar_churi_size" class="control-label">
                                                    ८.पिलरको चुरीको साईज़&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="pillar_churi_size"  required='' class="form-control" value="{{ $formData->buildingInfo->pillar_churi_size ?? 'n/a' }}">

                                                @if($errors->has('pillar_churi_size'))
                                                    <span class="text-danger">{{ $errors->first('pillar_churi_size') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <!--/ end of second row-->

                                    {{-- start of third row --}}
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="left_right_chadne_duri" class="control-label">
                                                    ९.दायाँ / बायाँ छोड्ने दुरी&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="left_right_chadne_duri"   required='' class="form-control" value="{{ $formData->buildingInfo->left_right_chadne_duri ?? 'n/a' }}">

                                                @if($errors->has('left_right_chadne_duri'))
                                                    <span class="text-danger">{{ $errors->first('left_right_chadne_duri') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="setback" class="control-label">
                                                    १०.सेट ब्याक &nbsp;<sup class="text-danger">(अनिवार्य) </sup>
                                                </label>
                                                <input type="text" name="setback"  required=''  class="form-control" value="{{ $formData->buildingInfo->setback ?? 'n/a' }}">

                                                @if($errors->has('setback'))
                                                    <span class="text-danger">{{ $errors->first('setback') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end of third row -->

                                    <hr>

                                    <!-- fourth row started -->
                                    <div class="row">
                                        <p style="font-size: 25px; text-align: center"> ११.जग्गाको चार किल्ला विवरण</p>
                                        <hr>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="east_border" class="control-label">
                                                    पूर्व&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="east_border"  required='' class="form-control" value="{{ $formData->buildingInfo->east_border ?? 'n/a' }}">

                                                @if($errors->has('east_border'))
                                                    <span class="text-danger">{{ $errors->first('east_border') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="setback" class="control-label">
                                                    पश्चिम&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="west_border"  required='' class="form-control" value="{{ $formData->buildingInfo->west_border ?? 'n/a' }}">
                                                @if($errors->has('west_border'))
                                                    <span class="text-danger">{{ $errors->first('west_border') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="north_border" class="control-label">
                                                    उत्तर &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="north_border"  required='' class="form-control" value="{{ $formData->buildingInfo->north_border ?? 'n/a' }}">

                                                @if($errors->has('north_border'))
                                                    <span class="text-danger">{{ $errors->first('north_border') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="south_border" class="control-label">
                                                    दक्षिण &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>


                                                <input type="text" name="south_border"  required='' class="form-control" value="{{ $formData->buildingInfo->south_border ?? 'n/a' }}">

                                                @if($errors->has('south_border'))
                                                    <span class="text-danger">{{ $errors->first('south_border') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end of fourth row -->

                                    <hr>
                                    <!-- start fifth row -->
                                    <div class="row">
                                        <p style="font-size: 25px; text-align: center"> १२.निर्माण कार्यको विवरण</p>
                                        <hr>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="building_length" class="control-label">
                                                    लम्बाई (फिट मा) &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="building_length"   required='' class="form-control" value="{{ $formData->buildingInfo->building_length ?? 'n/a' }}">

                                                @if($errors->has('building_length'))
                                                    <span class="text-danger">{{ $errors->first('building_length') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="building_breadth" class="control-label">
                                                    चौडाई (फिट मा) &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="building_breadth"  required='' class="form-control" value="{{ $formData->buildingInfo->building_breadth ?? 'n/a' }}">

                                                @if($errors->has('building_breadth'))
                                                    <span class="text-danger">{{ $errors->first('building_breadth') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="building_height" class="control-label">
                                                    उचाई (फिट मा)  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="building_height"  required='' class="form-control" value="{{ $formData->buildingInfo->building_height ?? 'n/a' }}">

                                                @if($errors->has('building_height'))
                                                    <span class="text-danger">{{ $errors->first('building_height') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="number_of_door" class="control-label">
                                                    ढोका संख्या &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="number_of_door"  required='' class="form-control" value="{{ $formData->buildingInfo->number_of_door ?? 'n/a' }}">

                                                @if($errors->has('number_of_door'))
                                                    <span class="text-danger">{{ $errors->first('number_of_door') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                    <!--/ end of fifth row-->

                                    <!-- start sixth row -->
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="number_of_door" class="control-label">
                                                    कोठा संख्या  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="number_of_room"  required='' class="form-control" value="{{ $formData->buildingInfo->number_of_room ?? 'n/a' }}">

                                                @if($errors->has('number_of_room'))
                                                    <span class="text-danger">{{ $errors->first('number_of_room') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="number_of_window" class="control-label">
                                                    झ्याल संख्या  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="number_of_window"  required='' class="form-control" value="{{ $formData->buildingInfo->number_of_window ?? 'n/a' }}">

                                                @if($errors->has('number_of_window'))
                                                    <span class="text-danger">{{ $errors->first('number_of_window') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="number_of_bathroom" class="control-label">
                                                    शौचालय/बाथरुम   &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="number_of_bathroom"  required='' class="form-control" value="{{ $formData->buildingInfo->number_of_bathroom ?? 'n/a' }}">

                                                @if($errors->has('number_of_bathroom'))
                                                    <span class="text-danger">{{ $errors->first('number_of_bathroom') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="septic_tank" class="control-label">
                                                    ढल / सेफ्टिटंकी
                                                </label>

                                                <input type="text" name="septic_tank"  required='' class="form-control" value="{{ $formData->buildingInfo->septic_tank ?? 'n/a' }}">

                                                @if($errors->has('septic_tank'))
                                                    <span class="text-danger">{{ $errors->first('septic_tank') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                    <!--/ end of sixth row-->

                                    <!-- start seven row -->
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="number_of_channel_gate" class="control-label">
                                                    च्यानल गेट संख्या &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select name="number_of_channel_gate" class="form-control" required >
                                                    <option value="छैन" @if($formData->buildingInfo->number_of_channel_gate  =='छैन') selected @endif>छैन</option>
                                                    <option value="०१" @if($formData->buildingInfo->number_of_channel_gate  =='०१') selected @endif>०१</option>
                                                    <option value="०२" @if($formData->buildingInfo->number_of_channel_gate  =='०२') selected @endif>०२</option>
                                                    <option value="०३" @if($formData->buildingInfo->number_of_channel_gate  =='०३') selected @endif>०३</option>
                                                    <option value="०४" @if($formData->buildingInfo->number_of_channel_gate  =='०४') selected @endif>०४</option>
                                                    <option value="०५" @if($formData->buildingInfo->number_of_channel_gate  =='०५') selected @endif>०५</option>
                                                </select>

                                                @if($errors->has('number_of_channel_gate'))
                                                    <span class="text-danger">{{ $errors->first('number_of_channel_gate') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="number_of_satar" class="control-label">
                                                    सटर संख्या  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select name="number_of_satar" class="form-control" required >
                                                    <option value="छैन" @if($formData->buildingInfo->number_of_satar  =='छैन') selected @endif>छैन</option>
                                                    <option value="०१" @if($formData->buildingInfo->number_of_satar  =='०१') selected @endif>०१</option>
                                                    <option value="०२" @if($formData->buildingInfo->number_of_satar  =='०२') selected @endif>०२</option>
                                                    <option value="०३" @if($formData->buildingInfo->number_of_satar  =='०३') selected @endif>०३</option>
                                                    <option value="०४" @if($formData->buildingInfo->number_of_satar  =='०४') selected @endif>०४</option>
                                                    <option value="०५" @if($formData->buildingInfo->number_of_satar  =='०५') selected @endif>०५</option>
                                                </select>

                                                @if($errors->has('number_of_satar'))
                                                    <span class="text-danger">{{ $errors->first('number_of_satar') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="roof" class="control-label">
                                                    छाना   &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <select name="roof" required class="form-control">
                                                    <option value="आरसिसि" @if($formData->buildingInfo->roof == 'आरसिसि') selected @endif>आरसिसि</option>
                                                    <option value="जस्ता" @if($formData->buildingInfo->roof == 'जस्ता') selected @endif>जस्ता</option>
                                                    <option value="ट्रस" @if($formData->buildingInfo->roof == 'ट्रस') selected @endif>ट्रस</option>
                                                    <option value="अन्य" @if($formData->buildingInfo->roof == 'अन्य') selected @endif>अन्य</option>
                                                </select>

                                                @if($errors->has('roof'))
                                                    <span class="text-danger">{{ $errors->first('roof') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="jodai" class="control-label">
                                                    जोडाई &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <select name="jodai" required class="form-control">
                                                    <option  selected>{{ $formData->buildingInfo->jodai ?? 'n/a' }}
                                                    <option value="सिमेन्ट" @if($formData->buildingInfo->jodai == 'सिमेन्ट') selected @endif>सिमेन्ट</option>
                                                    <option value="माटो" @if($formData->buildingInfo->jodai == 'माटो') selected @endif>माटो</option>
                                                    <option value="अन्य" @if($formData->buildingInfo->jodai == 'अन्य') selected @endif>अन्य</option>
                                                </select>

                                                @if($errors->has('jodai'))
                                                    <span class="text-danger">{{ $errors->first('jodai') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <!--/ end of seven row-->

                                    <!-- start eight row -->
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="building_purpose" class="control-label">
                                                    प्रयोजन &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select name="building_purpose" class="form-control" required >
                                                    <option value="व्यवसायीक" @if($formData->buildingInfo->building_purpose == 'व्यवसायीक') selected @endif>व्यवसायीक</option>
                                                    <option value="आवासीय" @if($formData->buildingInfo->building_purpose == 'आवासीय') selected @endif>आवासीय</option>
                                                    <option value="सार्वजनिक" @if($formData->buildingInfo->building_purpose == 'सार्वजनिक') selected @endif>सार्वजनिक</option>
                                                    <option value="शैक्षिक" @if($formData->buildingInfo->building_purpose == 'शैक्षिक') selected @endif>शैक्षिक</option>
                                                    <option value="औध्योगिक" @if($formData->buildingInfo->building_purpose == 'औध्योगिक') selected @endif>औध्योगिक</option>
                                                    <option value="अन्य" @if($formData->buildingInfo->building_purpose == 'अन्य') selected @endif>अन्य</option>
                                                </select>

                                                @if($errors->has('building_purpose'))
                                                    <span class="text-danger">{{ $errors->first('building_purpose') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end of eight row -->

                                    <hr>
                                    <p style="text-indent: 5%">माथि उल्लेखित जग्गामा <strong>{{ $formData->building_type ?? 'n/a' }}</strong> गर्न प्रस्ताव गरिएको संरचना भूकम्प सुरक्षात्मक बनाउन सरकारी आवश्यक नक्शा डिजाइन, आवश्यक कागजात सहित यो निवेदन पेश गरेको छु । प्राविधिक रुपमा तथा निर्माणबाट भूकम्प भए वा साधारण सुरक्षाको कमीले हुनसक्ने सम्पूर्ण जोखिम प्रति
                                        जिम्मेवार
                                        | यस रत्ननगर नगरपालिका बाट समय समयमा दिइने निर्देशन मापदण्ड समेत पालना गर्ने
                                        तथा आवश्यक परेको बेला त्यस कार्यालयमा उपस्थित हुनेछु ।</p>
                                    <hr>

                                    <!-- start of ninth row -->
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="house_owner_name" class="control-label">
                                                    घरधनी/प्रतिनिधिको नाम/थर
                                                </label>

                                                <input type="text" readonly  name="house_owner_name" id="field_owner_name"  required='' class="form-control" value="{{ $formData->buildingInfo->house_owner_name ?? 'n/a' }}">

                                                @if($errors->has('house_owner_name'))
                                                    <span class="text-danger">{{ $errors->first('house_owner_name') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="citizenship_number" class="control-label">
                                                    ना.प्र. प. नं  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="citizenship_number"  required='' class="form-control" value="{{ $formData->buildingInfo->citizenship_number ?? 'n/a' }}">

                                                @if($errors->has('citizenship_number'))
                                                    <span class="text-danger">{{ $errors->first('citizenship_number') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="district" class="control-label">
                                                    ना.प्र. प जारी जिल्ला
                                                </label>

                                                <input type="text" name="district"  required=''  class="form-control" value="{{ $formData->buildingInfo->district ?? 'n/a' }}">

                                                @if($errors->has('district'))
                                                    <span class="text-danger">{{ $errors->first('district') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="phone" class="control-label">
                                                    मोबाइल नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="phone"  required=''  class="form-control" value="{{ $formData->buildingInfo->phone ?? 'n/a' }}">

                                                @if($errors->has('phone'))
                                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="ground_area_coverage" class="control-label">
                                                    जमिन ओगटेको प्रतिशत &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>

                                                <input type="text" name="ground_area_coverage"  required='' class="form-control" value="{{ $formData->buildingInfo->ground_area_coverage ?? 'n/a' }}">

                                                @if($errors->has('ground_area_coverage'))
                                                    <span class="text-danger">{{ $errors->first('ground_area_coverage') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="floor_area_ratio" class="control-label">
                                                    FAR(Floor Area Ratio)&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="floor_area_ratio"  required='' class="form-control" value="{{ $formData->buildingInfo->floor_area_ratio ?? 'n/a' }}">

                                                @if($errors->has('floor_area_ratio'))
                                                    <span class="text-danger">{{ $errors->first('floor_area_ratio') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    {{-- form_buildinginfo END --}}
                                    <hr>
                                    <br>
                                    {{-- form_floorinfo START --}}
                                    <div class="row">
                                        <p style="font-size: 25px; text-align: center"> जग्गाको चार किल्ला विवरण</p>
                                        <hr>
                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="floor_number" class="control-label">
                                                    १.तल्ला संख्या&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select class="form-control" name="no_of_floor" id="floor_number"  required>
                                                    {{--<option value="3"  selected >३</option>--}}

                                                    <option value="1" @if($formData->floorInfo->no_of_floor == '1') selected @endif>१</option>
                                                    <option value="2" @if($formData->floorInfo->no_of_floor == '2') selected @endif>२</option>
                                                    <option value="3" @if($formData->floorInfo->no_of_floor == '3') selected @endif>३</option>
                                                    <option value="4" @if($formData->floorInfo->no_of_floor == '4') selected @endif>४</option>
                                                    <option value="5" @if($formData->floorInfo->no_of_floor == '5') selected @endif>५</option>
                                                </select>

                                                @if($errors->has('floor_number'))
                                                    <span class="text-danger">{{ $errors->first('floor_number') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3" id="floor1" >
                                            <div class="form-group ">
                                                <label for="underground_floor_area" class="control-label">
                                                    २.भूमिगत तल्ला &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="number" id="floor1"  name="underground_floor_area" placeholder="Enter value in sq.ft English" required='' class="form-control" value="{{ $formData->floorInfo->underground_floor_area ?? '' }}">

                                                @if($errors->has('underground_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('underground_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3" id="floor2" >
                                            <div class="form-group ">
                                                <label for="ground_floor_area" class="control-label">
                                                    ३.भुइँ तल्ला &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="number"  name="ground_floor_area"  placeholder="Enter value in sq.ft English" required='' class="form-control" value="{{ $formData->floorInfo->ground_floor_area ?? '' }}">

                                                @if($errors->has('ground_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('ground_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="col-md-3" id="floor3"
                                             @if(empty($formData->floorInfo->first_floor_area)) style="display:none;" @endif>
                                            <div class="form-group ">
                                                <label for="first_floor_area" class="control-label">
                                                    ४.पहिलो तल्ला &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="number" name="first_floor_area"  placeholder="Enter value in sq.ft English"  class="form-control" value="{{ $formData->floorInfo->first_floor_area ?? '' }}">

                                                @if($errors->has('first_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('first_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    {{-- end of first row --}}

                                    {{-- start of 2nd row --}}
                                    <div class="row">
                                        <div class="col-md-3" id="floor4" @if(empty($formData->floorInfo->second_floor_area)) style="display:none;" @endif>
                                            <div class="form-group ">
                                                <label for="second_floor_area" class="control-label">
                                                    ५.दोश्रो तल्ला  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="number" name="second_floor_area"  placeholder="Enter value in sq.ft English" class="form-control" value="{{ $formData->floorInfo->second_floor_area ?? '' }}">

                                                @if($errors->has('second_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('second_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3" id="floor5" @if(empty($formData->floorInfo->third_floor_area)) style="display:none;" @endif>
                                            <div class="form-group ">
                                                <label for="third_floor_area" class="control-label">
                                                    ६.तेश्रो तल्ला&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="number" name="third_floor_area"   placeholder="Enter value in sq.ft English" class="form-control" value="{{ $formData->floorInfo->third_floor_area ?? '' }}">

                                                @if($errors->has('third_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('third_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-3" id="floor6" @if(empty($formData->floorInfo->fourth_floor_area)) style="display:none;" @endif>
                                            <div class="form-group ">
                                                <label for="fourth_floor_area" class="control-label">
                                                    ७.चौथो तल्ला&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="number" name="fourth_floor_area"  placeholder="Enter value in sq.ft English" class="form-control" value="{{ $formData->floorInfo->fourth_floor_area ?? '' }}">

                                                @if($errors->has('fourth_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('fourth_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ end of second row-->
                                    {{-- form_floorinfo END --}}
                                    <hr>
                                    <br>
                                    {{-- form_buildingfiles START --}}
                                    <div class="row">
                                        <p style="font-size: 25px; text-align: center"> समंन्धित कागज पत्र</p>
                                        <hr>
                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <label for="lalpurja" class="control-label">
                                                        १.लालपुर्जा
                                                    </label>

                                                    <div class="row">
                                                        <input type="file" name="lalpurja" id="input-file-now-custom-1" class="dropify" accept=".jpg,.png,.bmp,.jpeg" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->lalpurja) }}" />
                                                    </div>

                                                    @if($errors->has('lalpurja'))
                                                        <span class="text-danger">{{ $errors->first('lalpurja') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <label for="citizenship" class="control-label">
                                                        २.नागरिकता
                                                    </label>
                                                    <div class="row">
                                                        <input type="file" name="citizenship" id="input-file-now-custom-1" class="dropify" accept=".jpg,.png,.bmp,.jpeg" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->citizenship) }}" />
                                                    </div>

                                                    @if($errors->has('citizenship'))
                                                        <span class="text-danger">{{ $errors->first('citizenship') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <label for="blueprint" class="control-label">
                                                        ३.नापी नक्शा
                                                    </label>

                                                    <div class="row">
                                                        <input type="file" name="blueprint" id="input-file-now-custom-1" class="dropify" accept=".jpg,.png,.bmp,.jpeg" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->blueprint) }}" />
                                                    </div>

                                                    @if($errors->has('blueprint'))
                                                        <span class="text-danger">{{ $errors->first('blueprint') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <label for="house_map" class="control-label">
                                                        ४.घरको नक्शा
                                                    </label>
                                                    <div class="row">
                                                        <input type="file" name="house_map" id="input-file-now-custom-1" class="dropify" accept=".pdf" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->house_map) }}" />
                                                    </div>

                                                    @if($errors->has('house_map'))
                                                        <span class="text-danger">{{ $errors->first('house_map') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                    </div>
                                    <!--/ end of first row-->


                                    <!-- second row -->
                                    <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <label for="tero_rashid" class="control-label">
                                                        ५.तीरो तिरेको रशिद
                                                    </label>
                                                    <div class="row">
                                                        <input type="file" name="tero_rashid" id="input-file-now-custom-1" class="dropify" accept=".jpg,.png,.jpeg,.bmp" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->tero_rashid) }}" />
                                                    </div>


                                                    @if($errors->has('tero_rashid'))
                                                        <span class="text-danger">{{ $errors->first('tero_rashid') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <label for="naamsari" class="control-label">
                                                        ६.नाम सारीको कागज
                                                    </label>
                                                    <div class="row">
                                                        <input type="file" name="naamsari" id="input-file-now-custom-1" class="dropify" accept=".jpg,.png,.bmp,.jpeg" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->naamsari) }}" />
                                                    </div>
                                                    @if($errors->has('naamsari'))
                                                        <span class="text-danger">{{ $errors->first('naamsari') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <label for="ward_sifaris" class="control-label">
                                                        ७.वडा को सीफरीस
                                                    </label>
                                                    <div class="row">
                                                        <input type="file" name="ward_sifaris" id="input-file-now-custom-1" class="dropify" accept=".jpg,.png,.gif,.jpeg" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->ward_sifaris) }}" />
                                                    </div>

                                                    @if($errors->has('ward_sifaris'))
                                                        <span class="text-danger">{{ $errors->first('ward_sifaris') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                        {{--@if(!empty ($formData->buildingFile->sampanna_patra))--}}
                                            {{--<div class="col-md-3">--}}
                                                {{--<div class="form-group ">--}}
                                                    {{--<label for="sampanna_patra" class="control-label">--}}
                                                        {{--८.सम्पन्न प्रमाण पत्र--}}
                                                    {{--</label>--}}
                                                    {{--<div class="row">--}}
                                                        {{--<input type="file" name="sampanna_patra" id="input-file-now-custom-1" class="dropify" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->sampanna_patra) }}" />--}}
                                                    {{--</div>--}}

                                                    {{--@if($errors->has('sampanna_patra'))--}}
                                                        {{--<span class="text-danger">{{ $errors->first('sampanna_patra') }}</span>--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@endif--}}
                                    </div>
                                    <!--/ end of second row-->

                                    <!-- third row -->
                                    <div class="row">

                                        <p style="font-size: 25px; text-align: center"> अन्य  कागज पत्र</p>
                                        <hr>

                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label for="other_document_1" class="control-label">
                                                    १.अन्य विवरण  १
                                                </label>
                                                <div class="row">
                                                    <input type="file" name="other_document_1" id="input-file-now-custom-1" class="dropify" accept=".doc,.docx,.pdf" @if(!empty($formData->buildingFile->other_document_1)) data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->other_document_1) }}" @endif />
                                                </div>

                                                @if($errors->has('other_document_1'))
                                                    <span class="text-danger">{{ $errors->first('other_document_1') }}</span>
                                                @endif

                                            </div>

                                            <div class="form-group ">
                                                <label for="other_document_1_description" class="control-label">
                                                    अन्य विवरण १ को  विवरण
                                                </label>

                                                <input type="text" value="{{ (!empty($formData->buildingFile->other_document_1_description)) ? $formData->buildingFile->other_document_1_description : '' }}" name="other_document_1_description" class="form-control" placeholder="अन्य विवरण १ को विवरण" />

                                                @if($errors->has('other_document_1_description'))
                                                    <span class="text-danger">{{ $errors->first('other_document_1_description') }}</span>
                                                @endif
                                            </div>


                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label for="other_document_2" class="control-label">
                                                    २.अन्य विवरण  २

                                                </label>
                                                <div class="row">
                                                    <input type="file" name="other_document_2" id="input-file-now-custom-1" class="dropify" accept=".doc,.docx,.pdf,.jpeg,.jpg,.png" @if(!empty($formData->buildingFile->other_document_2)) data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->other_document_2) }}" @endif />
                                                </div>

                                                @if($errors->has('other_document_2'))
                                                    <span class="text-danger">{{ $errors->first('other_document_2') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group ">
                                                <label for="other_document_2_description" class="control-label">
                                                    अन्य विवरण २ को  विवरण
                                                </label>

                                                <input type="text" value="{{ (!empty($formData->buildingFile->other_document_2_description)) ? $formData->buildingFile->other_document_2_description : '' }}" name="other_document_2_description" class="form-control" placeholder="अन्य विवरण २ को विवरण" />

                                                @if($errors->has('other_document_2_description'))
                                                    <span class="text-danger">{{ $errors->first('other_document_2_description') }}</span>
                                                @endif
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label for="other_document_3" class="control-label">
                                                    ३.अन्य विवरण  ३
                                                </label>
                                                <div class="row">
                                                    <input type="file" name="other_document_3" id="input-file-now-custom-1" class="dropify"  accept=".doc,.docx,.pdf,.jpeg,.jpg,.png" @if(!empty($formData->buildingFile->other_document_3)) data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->other_document_3) }}" @endif />
                                                </div>

                                                @if($errors->has('other_document_3'))
                                                    <span class="text-danger">{{ $errors->first('other_document_3') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group ">
                                                <label for="other_document_3_description" class="control-label">
                                                    अन्य विवरण ३ को  विवरण
                                                </label>

                                                <input type="text" value="{{ (!empty($formData->buildingFile->other_document_3_description)) ? $formData->buildingFile->other_document_3_description : '' }}" name="other_document_3_description" class="form-control" placeholder="अन्य विवरण ३ को विवरण" />

                                                @if($errors->has('other_document_3_description'))
                                                    <span class="text-danger">{{ $errors->first('other_document_3_description') }}</span>
                                                @endif
                                            </div>

                                        </div>

                                        {{-- form_buildingfiles END --}}
                                    </div>
                                </div>
                            {{-- end form-body --}}
                        </div>
                        {{-- panel-body --}}
    
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-sm-4 col-xs-12 pull-right">
                                <button type="submit" class="btn btn-block btn-info">Update</button>
                            </div>
                        </div>
                        </form>
                    </div>
                    {{-- end panel --}}


                </div>

            </div>
        </div>
    </div>
    </form>
    </div>


    @include('permission.model')
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <script>
        $("#owner_name").on ('change',  function( e ){
            data = $('#owner_name').val();
            $('#field_owner_name').val(data);
        });

        $("#option").on ('change',  function( e ){
            var data = $('#option').val();

            if( data == 'मैले') {
                $('#option_1').val('छु');
                $('#option_2').val('छौं');
            }
            else {
                $('#option_1').val('छौं');
                $('#option_2').val('गर्दछौं');
            }
        });

        $(document).ready(function( e ){
            var data = $('#option').val();

            if( data == 'मैले') {
                $('#option_1').val('छु');
                $('#option_2').val('गर्दछु');
            }
            else {
                $('#option_1').val('छौं');
                $('#option_2').val('गर्दछौं');
            }
        });


        // $("#floor_number").on ('change',  function( e ){
            var data = $('#floor_number').val();

            if(data == 1) {
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').hide();
                $('#floor4').hide();
                $('#floor5').hide();
                $('#floor6').hide();
                $('#floor7').hide();

            }

            if(data == 2) {
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').hide();
                $('#floor5').hide();
                $('#floor6').hide();
                $('#floor7').hide();
            }

            if(data == 3){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').hide();
                $('#floor6').hide();
                $('#floor7').hide();
            }

            if(data == 4){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').show();
                $('#floor6').hide();
                $('#floor7').hide();
            }

            if(data == 5){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').show();
                $('#floor6').show();
                $('#floor7').hide();
            }
        // });

        $("#floor_number").on ('change',  function( e ){
        var data = $('#floor_number').val();

        if(data == 1) {
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').hide();
            $('#floor4').hide();
            $('#floor5').hide();
            $('#floor6').hide();
            $('#floor7').hide();

        }

        if(data == 2) {
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').hide();
            $('#floor5').hide();
            $('#floor6').hide();
            $('#floor7').hide();
        }

        if(data == 3){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').hide();
            $('#floor6').hide();
            $('#floor7').hide();
        }

        if(data == 4){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').show();
            $('#floor6').hide();
            $('#floor7').hide();
        }

        if(data == 5){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').show();
            $('#floor6').show();
            $('#floor7').hide();
        }
        });


        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({

            });
            // Used events
            var drEvent = $('#input-file-events').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });
            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });

    </script>
@endsection
