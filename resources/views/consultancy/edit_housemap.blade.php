@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <style>
        .img-rounded {
            border-radius: 6px;
            padding: 5px;
        }

        .option1{
            background-color: transparent;
            padding: 1px;
            width: 20px;
            border: none;
            font-size: 14px;
            color: #333;
            text-align: center;"
        }

        .option2{
            background-color: transparent;
            padding: 1px;
            width: 45px;
            border: none;
            font-size: 14px;
            color: #333;
            text-align: center;"
        }
    </style>
@endsection
@section('page_title')
    नयाँ नक्शा दरखास्त फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>नयाँ नक्शा दरखास्त फाईल</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="panel panel-info">
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form action="{{ route('consultancy.change_housemap') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" value="{{$formData->form_id}}" name="form_id">
                                <div class="form-body">



                                    {{-- form_buildingfiles START --}}
                                    <div class="row">
                                        <p style="font-size: 25px; text-align: center"> समंन्धित कागज पत्र</p>
                                        <hr>

                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="house_map" class="control-label">
                                                    <p style="font-size: 20px;">घरको नक्शा </p>
                                                </label>
                                                <div class="row">
                                                    <input type="file" name="house_map" id="input-file-now-custom-1"  class="dropify" accept=".pdf" data-default-file="{{ asset(STATIC_DIR.'storage/'.$formData->house_map) }}" />
                                                </div>

                                                @if($errors->has('house_map'))
                                                    <span class="text-danger">{{ $errors->first('house_map') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{-- end form-body --}}
                                <div class="row">
                                    <div class="col-md-4 ">

                                        <a href="{{ route('consultancy.index') }}" class="btn btn-info btn-outline m-r-5"><i class="fa fa-reply"></i> Back</a>
                                        <button type="submit" class="btn btn-primary btn-outline m-r-5">Update</button>
                                    </div>
                                </div>

                            </form>
                    </div>
                    {{-- end panel --}}


                </div>

            </div>
        </div>
    </div>
    </form>
    </div>


    @include('permission.model')
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <script>
        $("#option").on ('change',  function( e ){
            var data = $('#option').val();

            if( data == 'मैले') {
                $('#option_1').val('छु');
                $('#option_2').val('छौं');
            }
            else {
                $('#option_1').val('छौं');
                $('#option_2').val('गर्दछौं');
            }
        });

        $(document).ready(function( e ){
            var data = $('#option').val();

            if( data == 'मैले') {
                $('#option_1').val('छु');
                $('#option_2').val('छौं');
            }
            else {
                $('#option_1').val('छौं');
                $('#option_2').val('गर्दछौं');
            }
        });


        // $("#floor_number").on ('change',  function( e ){
        var data = $('#floor_number').val();

        if(data == 1) {
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').hide();
            $('#floor4').hide();
            $('#floor5').hide();
            $('#floor6').hide();
            $('#floor7').hide();

        }

        if(data == 2) {
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').hide();
            $('#floor5').hide();
            $('#floor6').hide();
            $('#floor7').hide();
        }

        if(data == 3){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').hide();
            $('#floor6').hide();
            $('#floor7').hide();
        }

        if(data == 4){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').show();
            $('#floor6').hide();
            $('#floor7').hide();
        }

        if(data == 5){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').show();
            $('#floor6').show();
            $('#floor7').hide();
        }
        // });

        $("#floor_number").on ('change',  function( e ){
            var data = $('#floor_number').val();

            if(data == 1) {
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').hide();
                $('#floor4').hide();
                $('#floor5').hide();
                $('#floor6').hide();
                $('#floor7').hide();

            }

            if(data == 2) {
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').hide();
                $('#floor5').hide();
                $('#floor6').hide();
                $('#floor7').hide();
            }

            if(data == 3){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').hide();
                $('#floor6').hide();
                $('#floor7').hide();
            }

            if(data == 4){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').show();
                $('#floor6').hide();
                $('#floor7').hide();
            }

            if(data == 5){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').show();
                $('#floor6').show();
                $('#floor7').hide();
            }
        });


        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({

            });
            // Used events
            var drEvent = $('#input-file-events').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });
            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });

    </script>
@endsection
