@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
    Edit D.P.C
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>DPC बारे संबन्धित प्राविधिकको प्रतिवेदन</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        @if($action == 'administration')
                            <a href="{{ route('administration.get_edit',$formData->id) }}" class="btn btn-info btn-outline"><i class="fa fa-arrow-circle-left"></i>&nbsp;Back</a>
                            <hr>
                        @endif

                        @if(empty($formData->dpc))
                            <p>DPC भरेको छैन |  </p>

                        @else

                        <p>घर / जग्गाधनीको नाम : {{ $formData->field_owner_name }}</p>
                        <p>ठेगाना : {{ $formData->field_owner_address }}</p>
                        <p>अस्थाई चरण अनुमति दिईएको मिति :</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        @if($action == 'consultancy')
                            <form  method="post"  action="{{ route('consultancy.post_dpc_form') }}">
                        @else
                             <form  method="post"  action="{{ route('administration.post_dpc_form') }}">
                        @endif
                            @csrf
                            <input type="hidden" value="{{ $action}}" name="redirect">
                            <input type="hidden" value="{{ $form_id}}" name="form_id">
                            <input type="hidden" value="{{ $formData->dpc->dpc_id }}" name="dpc_form_id">
                            <input type="hidden" value="edit" name="action">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-sm table-hover">
                                        <tr class="text-center">
                                            <td>क्र.स</td>
                                            <td>विवरण</td>
                                            <td>प्रथम चरणमा स्वीकृत दिईएको </td>
                                            <td>नाप जांचबाट दखिएको</td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>१.</td>
                                            <td>भवनको वर्गीकरण</td>
                                            <td>
                                                <input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->building_category ?? 'n/a' }}" readonly>
                                            </td>
                                            <td>
                                                <input type="text" name="building_category" class="form-control input-box" placeholder="अनिवार्य" value="{{ (!empty($formData->dpc->building_category)) ? $formData->dpc->building_category : '' }}" required>
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>२.</td>
                                            <td>भवनको स्ट्रक्चर सिस्टम</td>
                                            <td>
                                                <input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->building_structure ?? 'n/a' }}" readonly>
                                            </td>
                                            <td>
                                                <input type="text" name="building_structure" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->building_structure)) ? $formData->dpc->building_structure : '' }}">
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>३.</td>
                                            <td>भवनको लम्बाई</td>
                                            <td>
                                                <input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->building_length ?? 'n/a' }} फीट" readonly>
                                            </td>
                                            <td>
                                                <input type="text" name="building_length" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->building_length)) ? $formData->dpc->building_length : '' }}">
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>४.</td>
                                            <td>भवनको चौडाई</td>
                                            <td>
                                                <input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->building_breadth ?? 'n/a' }} फीट" readonly>
                                            </td>
                                            <td>
                                                <input type="text" name="building_width" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->building_width)) ? $formData->dpc->building_width : '' }}">
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>५.</td>
                                            <td>जगको साईज़</td>
                                            <td>
                                                <input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->foundation_size ?? 'n/a' }} फीट" readonly>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control input-box" name="foundation_size" placeholder="अनिवार्य" value="{{ (!empty($formData->dpc->foundation_size)) ? $formData->dpc->foundation_size : '' }}" required>
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>६.</td>
                                            <td>सेट व्यक सडकको चौडाई सहित</td>
                                            <td>
                                                <input type="text" class="form-control input-box" value="पूर्व : {{ $formData->aaminPratibedan->chadnu_parne_setback_purba ?? 'n/a' }} फीट" readonly><br>
                                                <input type="text" class="form-control input-box" value="पश्चिम : {{ $formData->aaminPratibedan->chadnu_parne_setback_paschim ?? 'n/a' }} फीट" readonly><br>
                                                <input type="text" class="form-control input-box" value="उत्तर : {{ $formData->aaminPratibedan->chadnu_parne_setback_utar ?? 'n/a' }} फीट" readonly><br>
                                                <input type="text" class="form-control input-box" value="दक्षिण : {{ $formData->aaminPratibedan->chadnu_parne_setback_dakshin ?? 'n/a' }} फीट" readonly>

                                            </td>
                                            <td>
                                                <input type="text" name="setback_purba" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->setback_purba)) ? $formData->dpc->setback_purba : '' }}" ><br>
                                                <input type="text" name="setback_pachim" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->setback_pachim)) ? $formData->dpc->setback_pachim : '' }}"><br>
                                                <input type="text" name="setback_utar" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->setback_utar)) ? $formData->dpc->setback_utar : '' }}"><br>
                                                <input type="text" name="setback_dakshin" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->setback_dakshin)) ? $formData->dpc->setback_dakshin : '' }}">
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>७.</td>
                                            <td>दायाँ / बायाँ छोड्ने दुरी (फीटमा )</td>
                                            <td>
                                                <input type="text" value="दायाँ : {{ $formData->aaminPratibedan->mohoda_right ?? 'n/a' }} फीट" class="form-control input-box" readonly><br>
                                                <input type="text" value="बायाँ : {{ $formData->aaminPratibedan->mohoda_left ?? 'n/a' }} फीट" class="form-control input-box" readonly>
                                            </td>
                                            <td>
                                                <input type="text" name="mohoda_right" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->mohoda_right)) ? $formData->dpc->mohoda_right : '' }}" ><br>
                                                <input type="text" name="mohoda_left" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->mohoda_left)) ? $formData->dpc->mohoda_left : '' }}" >

                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>८.</td>
                                            <td>जग बिम / strap Beam</td>
                                            <td>
                                                <input type="text" name="strap_beam_swkrit_anusar" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->strap_beam_swkrit_anusar)) ? $formData->dpc->strap_beam_swkrit_anusar : '' }}">
                                            </td>
                                            <td>
                                                <input type="text" name="strap_beam" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->strap_beam)) ? $formData->dpc->strap_beam : '' }}" >
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>९.</td>
                                            <td>प्लिन्थको उचाई, / सडकको लेभलबाट</td>
                                            <td><input type="text" value="{{ $formData->buildingInfo->plith_height ?? 'n/a' }}" class="form-control input-box" readonly></td>
                                            <td>
                                                <input type="text" name="plinth_height" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->plinth_height)) ? $formData->dpc->plinth_height : '' }}"">
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>१०.</td>
                                            <td>पिलरको साईज़</td>
                                            <td><input type="text" value="{{ $formData->buildingInfo->pillar_size ?? 'n/a' }}" class="form-control input-box" readonly></td>
                                            <td>
                                                <input type="text" name="pillar_size" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->pillar_size)) ? $formData->dpc->pillar_size : '' }}">
                                            </td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>११.</td>
                                            <td>पिलर डण्डीको साईज़</td>
                                            <td><input type="text" value="{{ $formData->buildingInfo->pillar_rod_size ?? 'n/a' }}&nbsp;एम.एम" class="form-control input-box" readonly></td>
                                            <td>
                                                <input type="text"  name="pillar_rod_size" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->pillar_rod_size)) ? $formData->dpc->pillar_rod_size : '' }}">
                                            </td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>१२.</td>
                                            <td>पिलर डण्डीको संख्या</td>
                                            <td><input type="text" value="{{ $formData->buildingInfo->pillar_rod_number ?? 'n/a' }}&nbsp;एम.एम" class="form-control input-box" readonly></td>
                                            <td>
                                                <input type="text" name="pillar_rod_number" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->pillar_rod_number)) ? $formData->dpc->pillar_rod_number : '' }}">
                                            </td>
                                        </tr>

                                        <tr class="text-center">
                                            <td>१३.</td>
                                            <td>पिलर चुरीको साईज़</td>
                                            <td><input type="text" value="{{ $formData->buildingInfo->pillar_churi_size ?? 'n/a' }}" class="form-control input-box" readonly></td>
                                            <td>
                                                <input type="text" name="pillar_churi_size" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->pillar_churi_size)) ? $formData->dpc->pillar_churi_size : '' }}">
                                            </td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>१४.</td>
                                            <td>टाई बिमको साईज़</td>
                                            <td>
                                                <input type="text" name="tie_beam_size_swkrit_anusar" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->tie_beam_size_swkrit_anusar)) ? $formData->dpc->tie_beam_size_swkrit_anusar : '' }}">
                                            </td>
                                            <td>
                                                <input type="text" name="tie_beam_size" class="form-control input-box" placeholder="अनिवार्य" required value="{{ (!empty($formData->dpc->tie_beam_size)) ? $formData->dpc->tie_beam_size : '' }}">
                                            </td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>१५.</td>
                                            <td>होर्डिङ बोर्ड (क र ख वर्गको लागि)</td>
                                            <td>
                                                <input type="radio" name="hodding_board_swkrit_anusar" value="भएको" @if($formData->dpc->hodding_board_swkrit_anusar == "भएको") checked @endif required>&nbsp&nbsp;&nbsp;भएको&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="hodding_board_swkrit_anusar" value="नभएको" @if($formData->dpc->hodding_board_swkrit_anusar == "नभएको") checked @endif >&nbsp;&nbsp;&nbsp;नभएको
                                            </td>
                                            <td>
                                                <input type="text" name="hodding_board" class="form-control input-box" placeholder="अनिवार्य" value="{{ (!empty($formData->dpc->hodding_board)) ? $formData->dpc->hodding_board : '' }}" required>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4">
                                                <p style="line-height:30px;font-size:18px;text-indent:300px;">
                                                    माथि उल्लेखित व्यहोरा ठीक साँचो हो , झुटा ठहरे कानुन बमोजिम साहुला बुझाउला |
                                                </p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">
                                                नाँप जाँच गर्ने प्राविधिकको नाम / दरखास्त : <input type="text" name="uploaded_by" class="form-control input-box" value="{{ Auth::user()->first_name }}&nbsp;{{ Auth::user()->last_name }}" readonly>
                                            </td>
                                            <td class="text-center"  colspan="2">
                                                <p style="margin-top:35px; ">जग्गा धनी मञ्जुर प्राप्त प्रतिनिधिको दरखास्त : .............................................................</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-5">
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection
