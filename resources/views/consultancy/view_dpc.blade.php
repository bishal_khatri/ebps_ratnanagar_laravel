@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	D.P.C
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>DPC बारे संबन्धित प्राविधिकको प्रतिवेदन</h4>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-offset-3 col-md-6">
							<h3 class="box-title text-center text-danger">
								Proceeded to Proceeded to {{ $formData->proceeded_to ?? '' }}
							</h3>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<p>घर / जग्गाधनीको नाम : {{ $formData->field_owner_name }}</p>
						<p>ठेगाना : {{ $formData->field_owner_address }}</p>
						<p>अस्थाई चरण अनुमति दिईएको मिति :</p>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<form  method="post"  action="{{ route('consultancy.post_dpc_form') }}">
							@csrf
							<input type="hidden" value="{{ $formData->id }}" name="form_id">
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered table-sm table-hover">
										<tr class="text-center">
											<td>क्र.स</td>
											<td>विवरण</td>
											<td>प्रथम चरणमा स्वीकृत दिईएको </td>
											<td>नाप जांचबाट दखिएको</td>
										</tr>
										
										<tr class="text-center">
											<td>१.</td>
											<td>भवनको वर्गीकरण</td>
											<td>
												<input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->building_category ?? 'n/a' }}" readonly>
											</td>
											<td>
												<input type="text" name="building_category" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->building_category ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>२.</td>
											<td>भवनको स्ट्रक्चर सिस्टम</td>
											<td>
												<input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->building_structure ?? 'n/a' }}" readonly>
											</td>
											<td>
												<input type="text" name="building_structure" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->building_structure ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>३.</td>
											<td>भवनको लम्बाई</td>
											<td>
												<input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->building_length ?? 'n/a' }} फीट" readonly>
											</td>
											<td>
												<input type="text" name="building_length" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->building_length ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>४.</td>
											<td>भवनको चौडाई</td>
											<td>
												<input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->building_breadth ?? 'n/a' }} फीट" readonly>
											</td>
											<td>
												<input type="text" name="building_width" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->building_width ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>५.</td>
											<td>जगको साईज़</td>
											<td>
												<input type="text" class="form-control input-box" value="{{ $formData->buildingInfo->foundation_size ?? 'n/a' }} फीट" readonly>
											</td>
											<td>
												<input type="text" class="form-control input-box" name="foundation_size" placeholder="अनिवार्य" value="{{ $formData->dpc->foundation_size ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>६.</td>
											<td>सेट व्यक सडकको चौडाई सहित</td>
											<td>
												<input type="text" class="form-control input-box" value="पूर्व : {{ $formData->aaminPratibedan->chadnu_parne_setback_purba ?? 'n/a' }} फीट" readonly><br>
												<input type="text" class="form-control input-box" value="पश्चिम : {{ $formData->aaminPratibedan->chadnu_parne_setback_paschim ?? 'n/a' }} फीट" readonly><br>
												<input type="text" class="form-control input-box" value="उत्तर : {{ $formData->aaminPratibedan->chadnu_parne_setback_utar ?? 'n/a' }} फीट" readonly><br>
												<input type="text" class="form-control input-box" value="दक्षिण : {{ $formData->aaminPratibedan->chadnu_parne_setback_dakshin ?? 'n/a' }} फीट" readonly>
											
											</td>
											<td>
												<input type="text" name="setback_purba" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->setback_purba ?? 'n/a' }}" readonly><br>
												<input type="text" name="setback_pachim" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->setback_pachim ?? 'n/a' }}" readonly><br>
												<input type="text" name="setback_utar" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->setback_utar ?? 'n/a' }}" readonly><br>
												<input type="text" name="setback_dakshin" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->setback_dakshin ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>७.</td>
											<td>दायाँ / बायाँ छोड्ने दुरी (फीटमा )</td>
											<td>
												<input type="text" value="दायाँ : {{ $formData->aaminPratibedan->mohoda_right ?? 'n/a' }} फीट" class="form-control input-box" readonly><br>
												<input type="text" value="बायाँ : {{ $formData->aaminPratibedan->mohoda_left ?? 'n/a' }} फीट" class="form-control input-box" readonly>
											</td>
											<td>
												<input type="text" name="mohoda_right" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->mohoda_right ?? 'n/a' }}" readonly><br>
												<input type="text" name="mohoda_left" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->mohoda_left ?? 'n/a' }}" readonly>
											
											</td>
										</tr>
										
										<tr class="text-center">
											<td>८.</td>
											<td>जग बिम / strap Beam</td>
											<td>
												<input type="text" name="strap_beam_swkrit_anusar" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->strap_beam_swkrit_anusar ?? 'n/a' }}" readonly>
											</td>
											<td>
												<input type="text" name="strap_beam" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->strap_beam ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>९.</td>
											<td>प्लिन्थको उचाई, / सडकको लेभलबाट</td>
											<td><input type="text" value="{{ $formData->buildingInfo->plith_height ?? 'n/a' }}" class="form-control input-box" readonly></td>
											<td>
												<input type="text" name="plinth_height" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->plinth_height ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>१०.</td>
											<td>पिलरको साईज़</td>
											<td><input type="text" value="{{ $formData->buildingInfo->pillar_size ?? 'n/a' }}" class="form-control input-box" readonly></td>
											<td>
												<input type="text" name="pillar_size" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->pillar_size ?? 'n/a' }}" readonly>
											</td>
										</tr>
										<tr class="text-center">
											<td>११.</td>
											<td>पिलर डण्डीको साईज़</td>
											<td><input type="text" value="{{ $formData->buildingInfo->pillar_rod_size ?? 'n/a' }}&nbsp;एम.एम" class="form-control input-box" readonly></td>
											<td>
												<input type="text"  name="pillar_rod_size" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->pillar_rod_size ?? 'n/a' }}" readonly>
											</td>
										</tr>
										<tr class="text-center">
											<td>१२.</td>
											<td>पिलर डण्डीको संख्या</td>
											<td><input type="text" value="{{ $formData->buildingInfo->pillar_rod_number ?? 'n/a' }}&nbsp;एम.एम" class="form-control input-box" readonly></td>
											<td>
												<input type="text" name="pillar_rod_number" class="form-control input-box" placeholder="अनिवार्य"  value="{{ $formData->dpc->pillar_rod_number ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr class="text-center">
											<td>१३.</td>
											<td>पिलर चुरीको साईज़</td>
											<td><input type="text" value="{{ $formData->buildingInfo->pillar_churi_size ?? 'n/a' }}" class="form-control input-box" readonly></td>
											<td>
												<input type="text" name="pillar_churi_size" class="form-control input-box" placeholder="अनिवार्य"  value="{{ $formData->dpc->pillar_churi_size ?? 'n/a' }}" readonly>
											</td>
										</tr>
										<tr class="text-center">
											<td>१४.</td>
											<td>टाई बिमको साईज़</td>
											<td>
												<input type="text" name="tie_beam_size_swkrit_anusar" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->tie_beam_size_swkrit_anusar ?? 'n/a' }}" readonly>
											</td>
											<td>
												<input type="text" name="tie_beam_size" class="form-control input-box" placeholder="अनिवार्य"  value="{{ $formData->dpc->tie_beam_size ?? 'n/a' }}" readonly>
											</td>
										</tr>
										<tr class="text-center">
											<td>१५.</td>
											<td>होर्डिङ बोर्ड (क र ख वर्गको लागि)</td>
											<td>
												<input type="radio" disabled name="hodding_board_swkrit_anusar" value="भएको" @if($formData->dpc->hodding_board_swkrit_anusar=='भएको') checked @endif>&nbsp&nbsp;&nbsp;भएको&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" disabled name="hodding_board_swkrit_anusar" value="नभएको" @if($formData->dpc->hodding_board_swkrit_anusar=='नभएको') checked @endif>&nbsp;&nbsp;&nbsp;नभएको
											</td>
											<td>
												<input type="text" name="hodding_board" class="form-control input-box" placeholder="अनिवार्य" value="{{ $formData->dpc->hodding_board ?? 'n/a' }}" readonly>
											</td>
										</tr>
										
										<tr>
											<td colspan="4">
												<p style="line-height:30px;font-size:18px;" class="text-center">
													माथि उल्लेखित व्यहोरा ठीक साँचो हो , झुटा ठहरे कानुन बमोजिम साहुला बुझाउला |
												</p>
											</td>
										</tr>
										
										<tr>
											<td colspan="2">
												नाँप जाँच गर्ने प्राविधिकको नाम / दरखास्त : <input type="text" name="uploaded_by" class="form-control input-box" value="{{ Auth::user()->first_name }}&nbsp;{{ Auth::user()->last_name }}" readonly>
											</td>
											<td class="text-center"  colspan="2">
												<p style="margin-top:35px; " class="text-center">जग्गा धनी मञ्जुर प्राप्त प्रतिनिधिको दरखास्त : .............................................................</p>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')

@endsection
