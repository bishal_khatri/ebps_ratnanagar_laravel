@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	नयाँ नक्शा दरखास्त फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>नयाँ नक्शा दरखास्त फाईल</h4>
@endsection

@section('content')
	<div class="row">
		<!-- tbl of form_buildinginfo -->
		<div class="col-md-12">
			<form  method="post"  action="{{ route('consultancy.store_form_buildinginfo') }}">
				@csrf
				<input type="hidden" name="page" value="3" id="">
				<input type="hidden" name="form_id" value= "{{ $formData->id }} ">
				
				<div class="col-md-12">
					<div class="panel panel-info">
						<div class="panel-wrapper collapse in" aria-expanded="true">
							<div class="panel-body">
								<div class="form-body">
									<div class="row">
										<p style="font-size: 25px; text-align: center">घरको विवरण</p>
										<hr>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="building_category" class="control-label">
													१.घर निर्माणको वर्ग&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select class="form-control" name="building_category" required>
													<option disabled selected>--कुनै एक चयन गर्नुहोस--

													<option value="क" @if(old('building_category')=='क') selected @endif
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_category == 'क' )
																selected
															@endif
														@elseif(old('building_category')=='क')
																selected
														@endif
													>वर्ग (क)
													</option>

													<option value="ख" @if(old('building_category')=='ख') selected @endif
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_category == 'ख' )
																selected
															@endif
														@elseif(old('building_category')=='ख')
																selected
														@endif
													>वर्ग (ख)
													</option>

													<option value="ग" @if(old('building_category')=='ग') selected @endif
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_category == 'ग' )
																selected
															@endif
														@elseif(old('building_category')=='ग')
																selected
														@endif
													>वर्ग (ग)
													</option>

													<option value="घ" @if(old('building_category')=='घ') selected @endif
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_category == 'घ' )
																selected
															@endif
														@elseif(old('building_category')=='घ')
																selected
														@endif
													>वर्ग (घ)
													</option>

												</select>
												@if($errors->has('building_category'))
													<span class="text-danger">{{ $errors->first('building_category') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<label for="building_structure" class="control-label">
													२.भवनको स्ट्रक्चर&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select class="form-control" name="building_structure" required>
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="फ्रेम" @if(old('building_structure')=='फ्रेम') selected @endif
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_structure == 'फ्रेम' )
																selected
															@endif
														@elseif(old('building_structure')=='फ्रेम')
																selected
														@endif
													>फ्रेम</option>

													<option value="वाल" @if(old('building_structure')=='वाल') selected @endif
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_structure == 'वाल' )
																selected
															@endif
														@elseif(old('building_structure')=='वाल')
																selected
														@endif
													>वाल
													</option>
												</select>
												@if($errors->has('building_structure'))
													<span class="text-danger">{{ $errors->first('building_structure') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="sabik_ward_number" class="control-label">
													३.प्लिन्थ लेभलको उचाई&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="plith_height"  required='' class="form-control" value="{{ $formData->buildinginfo->plith_height ?? old('plith_height') }}" placeholder="प्लिन्थ लेभलको उचाई(नेपालीमा)">
												@if($errors->has('plith_height'))
													<span class="text-danger">{{ $errors->first('plith_height') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="foundation_size" class="control-label">
													४.जगको साईज &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="foundation_size" required='' class="form-control" value="{{$formData->buildinginfo->foundation_size ?? old('foundation_size') }}" placeholder="जगको साईज(नेपालीमा)">
												@if($errors->has('foundation_size'))
													<span class="text-danger">{{ $errors->first('foundation_size') }}</span>
												@endif
											</div>
										</div>
									</div>
									{{-- end of first row --}}
									
									{{-- start of 2nd row --}}
									<div class="row">
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="pillar_size" class="control-label">
													५.पिलरको साईज़&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="pillar_size" required="" class="form-control" value="{{$formData->buildinginfo->pillar_size ?? old('pillar_size') }}" placeholder="पिलरको साईज़(नेपालीमा)">
												@if($errors->has('pillar_size'))
													<span class="text-danger">{{ $errors->first('pillar_size') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="pillar_rod_size" class="control-label">
													६.पिलरको डण्डी साईज़&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="pillar_rod_size" required="" class="form-control" value="{{$formData->buildinginfo->pillar_rod_size ?? old('pillar_rod_size') }}" placeholder="पिलरको डण्डी साईज़(नेपालीमा)">
												@if($errors->has('pillar_rod_size'))
													<span class="text-danger">{{ $errors->first('pillar_rod_size') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="pillar_rod_number" class="control-label">
													७.पिलरको डण्डी संख्या &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="pillar_rod_number" required="" class="form-control" value="{{$formData->buildinginfo->pillar_rod_number ?? old('pillar_rod_number') }}" placeholder="पिलरको डण्डी साईज़(नेपालीमा)">
												@if($errors->has('pillar_rod_number'))
													<span class="text-danger">{{ $errors->first('pillar_rod_number') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="pillar_churi_size" class="control-label">
													८.पिलरको चुरीको साईज़&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="pillar_churi_size"  required='' class="form-control" value="{{$formData->buildinginfo->pillar_churi_size ?? old('pillar_churi_size') }}" placeholder="पिलरको चुरीको साईज़(नेपालीमा)">
												@if($errors->has('pillar_churi_size'))
													<span class="text-danger">{{ $errors->first('pillar_churi_size') }}</span>
												@endif
											</div>
										</div>
									
									</div>
									<!--/ end of second row-->
									
									{{-- start of third row --}}
									<div class="row">
										<div class="col-md-3">
											<div class="form-group ">
												<label for="left_right_chadne_duri" class="control-label">
													९.दायाँ / बायाँ छोड्ने दुरी&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="left_right_chadne_duri"  required='' class="form-control" value="{{$formData->buildinginfo->left_right_chadne_duri ?? old('left_right_chadne_duri') }}" placeholder="दायाँ / बायाँ छोड्ने दुरी(नेपालीमा)">
												@if($errors->has('left_right_chadne_duri'))
													<span class="text-danger">{{ $errors->first('left_right_chadne_duri') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="setback" class="control-label">
													१०.सेट ब्याक <sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="setback"  required='' class="form-control" value="{{$formData->buildinginfo->setback ?? old('setback') }}" placeholder="सेट ब्याक(नेपालीमा)">
												@if($errors->has('setback'))
													<span class="text-danger">{{ $errors->first('setback') }}</span>
												@endif
											</div>
										</div>
									</div>
									<!-- end of third row -->
									<hr>
									<!-- fourth row started -->
									<div class="row">
										<p style="font-size: 25px; text-align: center"> ११.जग्गाको चार किल्ला विवरण</p>
										<hr>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="east_border" class="control-label">
													पूर्व&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="east_border"  required='' class="form-control" value="{{$formData->buildinginfo->east_border ?? old('east_border') }}" placeholder="(नेपालीमा)">
												@if($errors->has('east_border'))
													<span class="text-danger">{{ $errors->first('east_border') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="west_border" class="control-label">
													पश्चिम&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="west_border"  required='' class="form-control" value="{{$formData->buildinginfo->west_border ?? old('west_border') }}" placeholder="(नेपालीमा)">
												@if($errors->has('west_border'))
													<span class="text-danger">{{ $errors->first('west_border') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="north_border" class="control-label">
													उत्तर &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="north_border"  required='' class="form-control" value="{{$formData->buildinginfo->north_border ?? old('north_border') }}" placeholder="(नेपालीमा)">
												@if($errors->has('north_border'))
													<span class="text-danger">{{ $errors->first('north_border') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="south_border" class="control-label">
													दक्षिण &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="south_border"  required='' class="form-control" value="{{$formData->buildinginfo->south_border ?? old('south_border') }}" placeholder="(नेपालीमा)">
												@if($errors->has('south_border'))
													<span class="text-danger">{{ $errors->first('south_border') }}</span>
												@endif
											</div>
										</div>
									
									</div>
									<!-- end of fourth row -->
									
									<hr>
									<!-- start fifth row -->
									<div class="row">
										<p style="font-size: 25px; text-align: center"> १२.निर्माण कार्यको विवरण</p>
										<hr>
										<div class="col-md-3">
											<div class="form-group ">
												<label for="building_length" class="control-label">
													लम्बाई (फिट मा) &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="building_length"  required='' class="form-control" value="{{$formData->buildinginfo->building_length ?? old('building_length') }}" placeholder="(नेपालीमा)">
												@if($errors->has('building_length'))
													<span class="text-danger">{{ $errors->first('building_length') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="building_breadth" class="control-label">
													चौडाई (फिट मा) &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="building_breadth"  required='' class="form-control" value="{{$formData->buildinginfo->building_breadth ?? old('building_breadth') }}" placeholder="(नेपालीमा)">
												@if($errors->has('building_breadth'))
													<span class="text-danger">{{ $errors->first('building_breadth') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="building_height" class="control-label">
													उचाई (फिट मा)  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="building_height"  required='' class="form-control" value="{{$formData->buildinginfo->building_height ?? old('building_height') }}" placeholder="(नेपालीमा)">
												@if($errors->has('building_height'))
													<span class="text-danger">{{ $errors->first('building_height') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="number_of_door" class="control-label">
													ढोका संख्या &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="number_of_door"  required='' class="form-control" value="{{$formData->buildinginfo->number_of_door ?? old('number_of_door') }}" placeholder="(नेपालीमा)">
												@if($errors->has('number_of_door'))
													<span class="text-danger">{{ $errors->first('number_of_door') }}</span>
												@endif
											</div>
										</div>
									
									
									</div>
									<!--/ end of fifth row-->
									
									<!-- start sixth row -->
									<div class="row">
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="number_of_room" class="control-label">
													कोठा संख्या  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="number_of_room"  required='' class="form-control" value="{{$formData->buildinginfo->number_of_room ?? old('number_of_room') }}" placeholder="(नेपालीमा)">
												@if($errors->has('number_of_room'))
													<span class="text-danger">{{ $errors->first('number_of_room') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="number_of_window" class="control-label">
													झ्याल संख्या  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="number_of_window"  required='' class="form-control" value="{{$formData->buildinginfo->number_of_window ?? old('number_of_window') }}" placeholder="(नेपालीमा)">
												@if($errors->has('number_of_window'))
													<span class="text-danger">{{ $errors->first('number_of_window') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="number_of_bathroom" class="control-label">
													शौचालय/बाथरुम   &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="number_of_bathroom"  required='' class="form-control" value="{{$formData->buildinginfo->number_of_bathroom ?? old('number_of_bathroom') }}" placeholder="(नेपालीमा)">
												@if($errors->has('number_of_bathroom'))
													<span class="text-danger">{{ $errors->first('number_of_bathroom') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="septic_tank" class="control-label">
													ढल / सेफ्टिटंकी
												</label>
												<input type="text" name="septic_tank" required='' class="form-control" value="सेफ्टी ट्यांकी" readonly>
												@if($errors->has('septic_tank'))
													<span class="text-danger">{{ $errors->first('septic_tank') }}</span>
												@endif
											</div>
										</div>
									
									
									</div>
									<!--/ end of sixth row-->
									
									<!-- start seven row -->
									<div class="row">
										<div class="col-md-3">
											<div class="form-group ">
												<label for="number_of_channel_gate" class="control-label">
													च्यानल गेट संख्या &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select name="number_of_channel_gate" class="form-control" required >
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="छैन"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_channel_gate == 'छैन' )
																selected
															@endif
														@elseif(old('number_of_channel_gate')=='छैन')
																selected
														@endif
													>छैन
													</option>

													<option value="०१"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_channel_gate == '०१' )
																selected
															@endif
														@elseif(old('number_of_channel_gate')=='०१')
																selected
														@endif
													>०१
													</option>

													<option value="०२"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_channel_gate == '०२' )
																selected
															@endif
														@elseif(old('number_of_channel_gate')=='०२')
																selected
														@endif
													>०२
													</option>

													<option value="०३"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_channel_gate == '०३' )
																selected
															@endif
														@elseif(old('number_of_channel_gate')=='०३')
																selected
														@endif
													>०३
													</option>

													<option value="०४"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_channel_gate == '०४' )
																selected
															@endif
														@elseif(old('number_of_channel_gate')=='०४')
																selected
														@endif
													>०४
													</option>

													<option value="०५"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_channel_gate == '०५' )
																selected
															@endif
														@elseif(old('number_of_channel_gate')=='०५')
																selected
														@endif
													>०५
													</option>
												</select>
												@if($errors->has('number_of_channel_gate'))
													<span class="text-danger">{{ $errors->first('number_of_channel_gate') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="number_of_satar" class="control-label">
													सटर संख्या  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select name="number_of_satar" class="form-control" required >
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="छैन"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_satar == 'छैन' )
																selected
															@endif
														@elseif(old('number_of_satar')=='छैन')
																selected
														@endif
													>छैन
													</option>

													<option value="०१"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_satar == '०१' )
																selected
															@endif
														@elseif(old('number_of_satar')=='०१')
																selected
														@endif
													>०१
													</option>

													<option value="०२"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_satar == '०२' )
																selected
															@endif
														@elseif(old('number_of_satar')=='०२')
																selected
														@endif
													>०२
													</option>

													<option value="०३"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_satar == '०३' )
																selected
															@endif
														@elseif(old('number_of_satar')=='०३')
																selected
														@endif
													>०३
													</option>

													<option value="०४"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_satar == '०४' )
																selected
															@endif
														@elseif(old('number_of_satar')=='०४')
																selected
														@endif
													>०४
													</option>

													<option value="०५"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->number_of_satar == '०५' )
																selected
															@endif
														@elseif(old('number_of_satar')=='०५')
																selected
														@endif
													>०५
													</option>
												</select>
												@if($errors->has('number_of_satar'))
													<span class="text-danger">{{ $errors->first('number_of_satar') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="roof" class="control-label">
													छाना   &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select name="roof" required class="form-control">
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="आरसिसि"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->roof == 'आरसिसि' )
																selected
															@endif
														@elseif(old('roof')=='आरसिसि')
																selected
														@endif
													>आरसिसि
													</option>

													<option value="जस्ता"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->roof == 'जस्ता' )
																selected
															@endif
														@elseif(old('roof')=='जस्ता')
																selected
														@endif
													>जस्ता
													</option>

													<option value="ट्रस"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->roof == 'ट्रस' )
																selected
															@endif
														@elseif(old('roof')=='ट्रस')
																selected
														@endif
													>ट्रस
													</option>

													<option value="अन्य"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->roof == 'अन्य' )
																selected
															@endif
														@elseif(old('roof')=='अन्य')
																selected
														@endif
													>अन्य</option>

												</select>
												@if($errors->has('roof'))
													<span class="text-danger">{{ $errors->first('roof') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="jodai" class="control-label">
													जोडाई &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select name="jodai" required class="form-control">
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="सिमेन्ट"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->jodai == 'सिमेन्ट' )
																selected
															@endif
														@elseif(old('jodai')=='सिमेन्ट')
																selected
														@endif
													>सिमेन्ट
													</option>

													<option value="माटो"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->jodai == 'माटो' )
																selected
															@endif
														@elseif(old('jodai')=='माटो')
																selected
														@endif
													>माटो
													</option>

													<option value="अन्य"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->jodai == 'अन्य' )
																selected
															@endif
														@elseif(old('jodai')=='अन्य')
																selected
														@endif
													>अन्य
													</option>

												</select>
												@if($errors->has('jodai'))
													<span class="text-danger">{{ $errors->first('jodai') }}</span>
												@endif
											</div>
										</div>
									
									</div>
									<!--/ end of seven row-->
									
									<!-- start eight row -->
									<div class="row">
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="building_purpose" class="control-label">
													प्रयोजन &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select name="building_purpose" class="form-control" required >
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="व्यवसायीक"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_purpose == 'व्यवसायीक' )
																selected
															@endif
														@elseif(old('building_purpose')=='व्यवसायीक')
																selected
														@endif
													>व्यवसायीक
													</option>

													<option value="आवासीय"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_purpose == 'आवासीय' )
																selected
															@endif
														@elseif(old('building_purpose')=='आवासीय')
																selected
														@endif
													>आवासीय
													</option>

													<option value="सार्वजनिक"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_purpose == 'सार्वजनिक' )
																selected
															@endif
														@elseif(old('building_purpose')=='सार्वजनिक')
																selected
														@endif
													>सार्वजनिक
													</option>

													<option value="शैक्षिक"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_purpose == 'शैक्षिक' )
																selected
															@endif
														@elseif(old('building_purpose')=='शैक्षिक')
																selected
														@endif
													>शैक्षिक
													</option>

													<option value="औध्योगिक"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_purpose == 'औध्योगिक' )
																selected
															@endif
														@elseif(old('building_purpose')=='औध्योगिक')
																selected
														@endif
													>औध्योगिक
													</option>

													<option value="अन्य"
														@if(isset($formData->buildinginfo))
															@if($formData->buildinginfo->building_purpose == 'अन्य' )
																selected
															@endif
														@elseif(old('building_purpose')=='अन्य')
																selected
														@endif
													>अन्य
													</option>

												</select>
												@if($errors->has('building_purpose'))
													<span class="text-danger">{{ $errors->first('building_purpose') }}</span>
												@endif
											</div>
										</div>
									
									</div>
									<!-- end of eight row -->
									
									<hr>
									
									<p>माथि उल्लेखित जग्गामा
										@if($formData->building_type == 'तल्ला थप' )
											तल्ला थप
										@else
											नयाँ घर
										@endif
										निर्माण गर्न प्रस्ताव गरिएको संरचना भूकम्प सुरक्षात्मक बनाउन सरकारी आवश्यक नक्शा डिजाइन, आवश्यक कागजात सहित यो निवेदन पेश गरेको छु । प्राविधिक रुपमा तथा निर्माणबाट भूकम्प भए वा साधारण सुरक्षाको कमीले हुनसक्ने सम्पूर्ण जोखिम प्रति
										जिम्मेवार
										| यस रत्ननगर नगरपालिका बाट समय समयमा दिइने निर्देशन मापदण्ड समेत पालना गर्ने
										तथा आवश्यक परेको बेला त्यस कार्यालयमा उपस्थित हुनेछु ।</p>
									<hr>
									
									<!-- start of ninth row -->
									<div class="row">
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="house_owner_name" class="control-label">
													घरधनी/प्रतिनिधिको नाम/थर &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="house_owner_name"  required='' class="form-control" value="{{ $formData->field_owner_name  }}" placeholder="घरधनी/प्रतिनिधिको नाम/थर(नेपालीमा)">
												@if($errors->has('house_owner_name'))
													<span class="text-danger">{{ $errors->first('house_owner_name') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="citizenship_number" class="control-label">
													ना.प्र. प. नं  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="citizenship_number"  required='' class="form-control" value="{{ $formData->buildinginfo->citizenship_number ?? old('citizenship_number') }}" placeholder="(नेपालीमा)">
												@if($errors->has('citizenship_number'))
													<span class="text-danger">{{ $errors->first('citizenship_number') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="district" class="control-label">
													ना.प्र. प जारी जिल्ला &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="district"  required='' class="form-control" value="{{ $formData->buildinginfo->district ?? old('district') }}" placeholder="ना.प्र. प जारी जिल्ला(नेपालीमा)">
												@if($errors->has('district'))
													<span class="text-danger">{{ $errors->first('district') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="phone" class="control-label">
													मोबाइल नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="phone"  required='' class="form-control" value="{{ $formData->buildinginfo->phone ?? old('phone') }}" placeholder="मोबाइल नं(नेपालीमा)">
												@if($errors->has('phone'))
													<span class="text-danger">{{ $errors->first('phone') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="ground_area_coverage" class="control-label">
													जमिन ओगटेको प्रतिशत<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="ground_area_coverage"  required='' class="form-control" value="{{ $formData->buildinginfo->ground_area_coverage ?? old('ground_area_coverage') }}" placeholder="जमिन ओगटेको प्रतिशत(नेपालीमा)">
												@if($errors->has('ground_area_coverage'))
													<span class="text-danger">{{ $errors->first('ground_area_coverage') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="floor_area_ratio" class="control-label">
													FAR(Floor Area Ratio)&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="floor_area_ratio"  required='' class="form-control" value="{{ $formData->buildinginfo->floor_area_ratio ?? old('floor_area_ratio') }}" placeholder="(नेपालीमा)">
												@if($errors->has('floor_area_ratio'))
													<span class="text-danger">{{ $errors->first('floor_area_ratio') }}</span>
												@endif
											</div>
										</div>
									
									</div>
									
									<!--/ end of ninth row-->
									<hr>
									<div class="row">
										<div class="col-lg-2 col-sm-4 col-xs-12 pull-left">
											<a href="{{route('consultancy.add_form_landinfo',$formData->id)}}" class="btn btn-block btn-info text-white">Back</a>
										</div>
										<div class="col-lg-2 col-sm-4 col-xs-12 pull-right">
											<button class="btn btn-block btn-info">Next</button>
										</div>
									</div>
									<!-- end of fourth row -->
								</div>
								{{-- end form-body --}}
							</div>
							{{-- panel-body --}}
						</div>
						{{-- end panel-wrapper --}}
					</div>
					{{-- end panel --}}
				
				</div>
				{{-- end col-md-12 --}}
			
			</form>
			<!-- end form -->
		
		</div>
		<!-- end form_buldinginfo -->
	
	</div>
	
	@include('permission.model')
@endsection

@section('scripts')

@endsection
