@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
    नयाँ नक्शा दरखास्त फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>नयाँ नक्शा दरखास्त फाईल</h4>
@endsection

@section('content')
    <div class="row">
        <!-- tbl_floorinfo -->
        <div class="col-md-12">
            <form  method="post"  action="{{ route('consultancy.store_form_floorinfo') }}">
                @csrf
                <input type="hidden" name="page" value="4" >
                <input type="hidden" name="form_id" value="{{ $formData->id }}">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <div class="form-body">
                                    <!-- first row -->
                                    <div class="row">
                                        <p style="font-size: 25px; text-align: center">तल्ला विवरण</p>
                                        <hr>
                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <label for="no_of_floor" class="control-label">
                                                    १.तल्ला संख्या&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <select class="form-control" id="floor_number" name="no_of_floor" required>
                                                    <option disabled selected>--कुनै एक चयन गर्नुहोस--</option>
                                                    
                                                    <option value="1"
                                                            @if(isset($formData->floorinfo))
                                                            @if($formData->floorinfo->no_of_floor == '1' )
                                                            selected
                                                            @endif
                                                            @elseif(old('no_of_floor')=='1')
                                                            selected
                                                            @endif
                                                    >१
                                                    </option>
                                                    
                                                    <option value="2"
                                                            @if(isset($formData->floorinfo))
                                                            @if($formData->floorinfo->no_of_floor == '2' )
                                                            selected
                                                            @endif
                                                            @elseif(old('no_of_floor')=='2')
                                                            selected
                                                            @endif
                                                    >२
                                                    </option>
                                                    
                                                    <option value="3"
                                                            @if(isset($formData->floorinfo))
                                                            @if($formData->floorinfo->no_of_floor == '3' )
                                                            selected
                                                            @endif
                                                            @elseif(old('no_of_floor')=='3')
                                                            selected
                                                            @endif
                                                    >३
                                                    </option>
                                                    
                                                    <option value="4"
                                                            @if(isset($formData->floorinfo))
                                                            @if($formData->floorinfo->no_of_floor == '4' )
                                                            selected
                                                            @endif
                                                            @elseif(old('no_of_floor')=='4')
                                                            selected
                                                            @endif
                                                    >४
                                                    </option>
                                                    
                                                    <option value="5"
                                                            @if(isset($formData->floorinfo))
                                                            @if($formData->floorinfo->no_of_floor == '5' )
                                                            selected
                                                            @endif
                                                            @elseif(old('no_of_floor')=='5')
                                                            selected
                                                            @endif
                                                    >५
                                                    </option>
                                                
                                                </select>
                                                @if($errors->has('floor_number'))
                                                    <span class="text-danger">{{ $errors->first('floor_number') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-md-3" id="floor1" style="display:none;">
                                            <div class="form-group ">
                                                <label for="underground_floor_area" class="control-label">
                                                    २.भूमिगत तल्ला &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" id="floor1" name="underground_floor_area" placeholder="Enter value in sq.ft English" required='' class="form-control input-number" value="{{ $formData->floorinfo->underground_floor_area ?? old('underground_floor_area') }}">
                                                @if($errors->has('underground_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('underground_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3" id="floor2" style="display:none;">
                                            <div class="form-group ">
                                                <label for="ground_floor_area" class="control-label">
                                                    ३.भुइँ तल्ला &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text"  name="ground_floor_area" id="area2" placeholder="Enter value in sq.ft English" required='' class="form-control input-number" value="{{$formData->floorinfo->ground_floor_area ?? old('ground_floor_area') }}">
                                                @if($errors->has('ground_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('ground_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3" id="floor3" style="display:none;">
                                            <div class="form-group ">
                                                <label for="first_floor_area" class="control-label">
                                                    ४.पहिलो तल्ला &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="first_floor_area"  placeholder="Enter value in sq.ft English" class="form-control input-number" value="{{$formData->floorinfo->first_floor_area ?? old('first_floor_area') }}">
                                                @if($errors->has('first_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('first_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    {{-- end of first row --}}
                                    
                                    {{-- start of 2nd row --}}
                                    <div class="row">
                                        <div class="col-md-3" id="floor4" style="display:none;">
                                            <div class="form-group ">
                                                <label for="second_floor_area" class="control-label">
                                                    ५.दोश्रो तल्ला  &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="second_floor_area"  placeholder="Enter value in sq.ft English" class="form-control input-number" value="{{$formData->floorinfo->second_floor_area ?? old('second_floor_area') }}">
                                                @if($errors->has('second_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('second_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3" id="floor5" style="display:none;">
                                            <div class="form-group ">
                                                <label for="third_floor_area" class="control-label">
                                                    ६.तेश्रो तल्ला&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="third_floor_area"  placeholder="Enter value in sq.ft English" class="form-control input-number" value="{{$formData->floorinfo->third_floor_area ?? old('third_floor_area') }}">
                                                @if($errors->has('third_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('third_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-md-3" id="floor6" style="display:none;">
                                            <div class="form-group ">
                                                <label for="fourth_floor_area" class="control-label">
                                                    ७.चौथो तल्ला&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
                                                </label>
                                                <input type="text" name="fourth_floor_area"  placeholder="Enter value in sq.ft English" class="form-control input-number" value="{{$formData->floorinfo->fourth_floor_area ?? old('fourth_floor_area') }}">
                                                @if($errors->has('fourth_floor_area'))
                                                    <span class="text-danger">{{ $errors->first('fourth_floor_area') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ end of second row-->
                                    
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-2 col-sm-4 col-xs-12 pull-left">
                                            <a href="{{ route('consultancy.add_form_buildinginfo',$formData->id) }}" class="btn btn-block btn-info text-white">Back</a>
                                        </div>
                                        <div class="col-lg-2 col-sm-4 col-xs-12 pull-right">
                                            <button class="btn btn-block btn-info">Next</button>
                                        </div>
                                    </div>
                                    <!-- end of third row -->
                                
                                
                                </div>
                                {{-- end form-body --}}
                            </div>
                            {{-- panel-body --}}
                        </div>
                        {{-- end panel-wrapper --}}
                    </div>
                    {{-- end panel --}}
                
                </div>
                {{-- end col-md-12 --}}
            
            </form>
            <!-- end form -->
        
        </div>
        <!-- end of tbl_floorinfo -->
    
    </div>
    
    @include('permission.model')
@endsection

@section('scripts')
    <script>

        var data = $('#floor_number').val();

        if(data == 1) {
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').hide();
            $('#floor4').hide();
            $('#floor5').hide();
            $('#floor6').hide();

        }

        if(data == 2) {
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').hide();
            $('#floor5').hide();
            $('#floor6').hide();
        }

        if(data == 3){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').hide();
            $('#floor6').hide();
        }

        if(data == 4){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').show();
            $('#floor6').hide();
        }

        if(data == 5){
            $('#floor1').show();
            $('#floor2').show();
            $('#floor3').show();
            $('#floor4').show();
            $('#floor5').show();
            $('#floor6').show();
        }




        $("#floor_number").on ('change',  function( e ){
            var data = $('#floor_number').val();

            if(data == 1) {
                $('#floor1').show();
                $('#floor2').show();

                $('#floor3').hide();
                $('#floor4').hide();
                $('#floor5').hide();
                $('#floor6').hide();

                $('input[name=first_floor_area]').val(null);
                $('input[name=second_floor_area]').val(null);
                $('input[name=third_floor_area]').val(null);
                $('input[name=fourth_floor_area]').val(null);

            }

            if(data == 2) {
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();

                $('#floor4').hide();
                $('#floor5').hide();
                $('#floor6').hide();

                $('input[name=second_floor_area]').val(null);
                $('input[name=third_floor_area]').val(null);
                $('input[name=fourth_floor_area]').val(null);

            }

            if(data == 3){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').hide();
                $('#floor6').hide();

                $('input[name=third_floor_area]').val(null);
                $('input[name=fourth_floor_area]').val(null);
            }

            if(data == 4){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').show();
                $('#floor6').hide();

                $('input[name=fourth_floor_area]').val(null);

            }

            if(data == 5){
                $('#floor1').show();
                $('#floor2').show();
                $('#floor3').show();
                $('#floor4').show();
                $('#floor5').show();
                $('#floor6').show();
            }
        });

        $('.input-number').keypress(function(event) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    </script>

@endsection
