<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">अस्वीकृत गर्नुको कारण</h4> </div>
            <div class="modal-body">
                <p class="text-danger">{{ isset($value->rejected_message) ? $value->rejected_message : 'n/a' }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{--to upload dpc_form  signed one.--}}
<div class="modal fade" id="uploadDpc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('consultancy.uploaddpc') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="form_id" id="form_id_dpc">
                <input type="hidden" name="action" value="{{$action ?? ''}}">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >DPC  फाईल अपलोड</h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="dpc_scan" class="control-label">
                            DPC File:
                        </label>
                        <input type="file" name="upload_dpc" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                        @if($errors->has('dpc_scan'))
                            <span class="text-danger">{{ $errors->first('tippani_scan') }}</span>
                        @endif
                    </div>
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{--to upload sampanna_form signed one.--}}
<div class="modal fade" id="uploadSampanna" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('consultancy.uploadsampanna') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="form_id" id="form_id_sampanna">
                <input type="hidden" name="action" value="{{$action ?? ''}}">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >सम्पन्न  फाईल अपलोड</h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="dpc_scan" class="control-label">
                            सम्पन्न  फाईल :
                        </label>
                        <input type="file" name="upload_sampanna" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                        @if($errors->has('dpc_scan'))
                            <span class="text-danger">{{ $errors->first('tippani_scan') }}</span>
                        @endif
                    </div>
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">
                        @if(isset($action))
                            @if($action == 'edit')
                                Update
                            @else
                                Upload
                            @endif
                        @endif
                    
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

