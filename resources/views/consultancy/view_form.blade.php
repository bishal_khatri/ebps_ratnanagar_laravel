@extends('layouts.app')
@section('css')
	<style>
		.img-rounded {
			border-radius: 6px;
			padding: 5px;
		}
	</style>
@endsection
@section('page_title')
	नयाँ नक्शा दरखास्त फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>नयाँ नक्शा दरखास्त फाईल</h4>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row text-center">
					<div class="col-md-12">
						<h3 class="box-title text-center text-danger">
							@if($formData->is_completed == 1)
								!!! Your File has been Completed !!!
							@else
								Proceeded to {{ $formData->proceeded_to ?? '' }}
							@endif
						</h3>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-info">
							<div class="panel-wrapper collapse in" aria-expanded="true">
								<div class="panel-body">
									<div class="form-body">
										<h3 style="padding:0px;margin:0px;font-size:19px;">तपसिल <span class="label label-primary">दर्ता नं. : {{ $formData->darta_number }}</span></h3>
										<hr>
										<p style="text-indent: 5%">
											उपरोक्त सम्बन्धमा
											<strong>{{ $formData->sambandha_ma ?? 'n/a' }}</strong>
											
											तपशीलमा उल्लेखित जग्गामा देहाय बमोजिमको
											<strong>{{ $formData->building_type ?? 'n/a' }}</strong>
											
											कार्यका लागि घर नक्शा सहितको दरखास्त फारम साथै जग्गामा हक पुगेको आवश्यक कागजात प्रमाण समेत राखी पेश गरेको
											
											<strong>{{ $formData->option_1 ?? 'n/a' }}</strong>
											
											। उक्त भवन निर्माण कार्यका लागि नक्शा पाश स्वीकृत गरी पाउन अनुरोध
											
											<strong>{{ $formData->option_2 ?? 'n/a' }}</strong>
											
											। यस दरखास्तमा लेखिएको व्यहोरा ठिक साँचो हो । झुठा ठहरे कानुन बमोजिम सहुँला बुझाउँला ।
										</p>
										
										<hr>
										
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label for="sambodhan" class="control-label">सम्बोधन   </label>
													<select class="form-control" name="sambodhan" required>
														<option selected disabled="" readonly>{{ !empty($formData->sambodhan) ? $formData->sambodhan : 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="field_owner_name" class="control-label">१.जग्गा धनी को नाम थर&nbsp; </label>
													<input type="text" name="field_owner_name" required="" class="form-control" readonly value="{{ !empty($formData->field_owner_name) ? $formData->field_owner_name : 'n/a' }}">
												</div>
											
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="field_owner_address" class="control-label">
														२.जग्गा धनीको ठेगाना&nbsp;
													</label>
													<input type="text" name="field_owner_address"  required='' class="form-control" readonly value="{{ !empty($formData->field_owner_address) ? $formData->field_owner_address : 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="field_owner_age" class="control-label">
														३.बर्ष&nbsp;
													</label>
													
													<input type="text" name="field_owner_age"  required='' class="form-control" readonly value="{{ !empty($formData->field_owner_age) ? $formData->field_owner_age : 'n/a' }}">
												</div>
											</div>
										
										</div>
										<!--/row-->
										{{-- end of first row --}}
										
										{{-- start of 2nd row --}}
										<div class="row">
											
											<div class="col-md-3">
												<div class="form-group">
													<label for="father_husband_option" class="control-label">
														४.बाबु / पति&nbsp;
													</label>
													<select class="form-control" name="father_husband_option" required>
														<option disabled="" selected>{{ !empty($formData->father_husband_option) ? $formData->father_husband_option : 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="father_husband_name" class="control-label">
														५.बाबु/पतिको नाम, थर&nbsp;
													</label>
													
													<input type="text" name="father_husband_name" required="" readonly class="form-control" value="{{ !empty($formData->father_husband_name) ? $formData->father_husband_name : 'n/a' }}">
												</div>
											
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="grandfather_option" class="control-label">
														६.बाजे / ससुरा&nbsp;
													</label>
													<select class="form-control" name="grandfather_option" required>
														<option disabled="" selected>{{ !empty($formData->grandfather_option) ? $formData->grandfather_option : 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="grandfather_name" class="control-label">
														७.बाजे / ससुरा नाम, थर&nbsp;
													</label>
													<input type="text" name="grandfather_name" readonly required='' class="form-control" value="{{ !empty($formData->grandfather_name) ? $formData->grandfather_name : 'n/a' }}">
												</div>
											</div>
										</div>
										<!--/ end of second row-->
										
										{{-- start of 2nd row --}}
										<div class="row">
											<div class="col-md-3">
												<div class="form-group ">
													<label for="field_owner_job" class="control-label">
														८.जग्गा धनीको पेशा/व्यवसाय &nbsp;
													</label>
													<input type="text" name="field_owner_job" readonly required='' class="form-control" value="{{ !empty($formData->field_owner_job) ? $formData->field_owner_job : 'n/a' }}">
												</div>
											</div>
										</div>
										<!--/ end of third row-->
										{{-- form_personalinfo END --}}
										
										{{-- form_landinfo START --}}
										<div class="row">
											<div class="col-md-3">
												<div class="form-group ">
													<label for="kitta_number" class="control-label">
														१.हाल सर्भे कित्ता नं&nbsp;
													</label>
													
													<input type="text" name="kitta_number" readonly required="" class="form-control" value="{{ $formData->landInfo->kitta_number ?? 'n/a' }}">
												</div>
											
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label for="sabik_kitta_number" class="control-label">
														२.साविक कित्ता नं&nbsp;
													
													</label>
													
													<input type="text" name="sabik_kitta_number" readonly required="" class="form-control" value="{{ $formData->landInfo->sabik_kitta_number ?? 'n/a'  }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="sabik_ward_number" class="control-label">
														३.साविक गा.वि.स / वडा नं&nbsp;
													</label>
													
													<input type="text" name="sabik_ward_number" readonly required='' class="form-control" value="{{ $formData->landInfo->sabik_ward_number ?? 'n/a'  }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="field_area" class="control-label">
														४.जग्गा क्षेत्रफल &nbsp;
													</label>
													
													<input type="text" name="field_area" readonly required='' class="form-control" value="{{ $formData->landInfo->field_area ?? 'n/a'  }}">
												</div>
											</div>
										
										</div>
										<!--/row-->
										{{-- end of first row --}}
										
										{{-- start of 2nd row --}}
										<div class="row">
											<div class="col-md-3">
												<div class="form-group ">
													<label for="village_name" class="control-label">
														५.बाटो / मार्ग / टोलको नाम&nbsp;
													</label>
													<input type="text" name="village_name" readonly required="" class="form-control" value="{{ $formData->landInfo->village_name ?? 'n/a' }}">
												</div>
											
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="jagga_mohada" class="control-label">
														६.जग्गाको मोहोडा&nbsp;
													</label>
													<input type="text" name="jagga_mohada" readonly required="" class="form-control" value="{{ $formData->landInfo->jagga_mohada ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="ward_number" class="control-label">
														७.जग्गा भएको वडा नं&nbsp;
													</label>
													<input type="text" name="ward_number" readonly required='' class="form-control" value="{{ $formData->landInfo->ward_number ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="current_ward_number" class="control-label">
														८.हालको वडा नं&nbsp;
													</label>
													
													<input type="text" name="current_ward_number" readonly required='' class="form-control" value="{{ $formData->landInfo->current_ward_number ?? 'n/a' }}">
												</div>
											</div>
										
										</div>
										<!--/ end of second row-->
										
										{{-- start of 2nd row --}}
										<div class="row">
											<div class="col-md-3">
												<div class="form-group ">
													<label for="sabik_kittama_ghar" class="control-label">
														९.हालको/साविक कित्तामा घर
													</label>
													
													<select class="form-control" name="sabik_kittama_ghar" required>
														<option disabled selected>{{ $formData->landInfo->sabik_kittama_ghar ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="haak_pugeko" class="control-label">
														१०.जग्गामा हक पुगेको बिवरण&nbsp;
													</label>
													
													<select class="form-control" name="haak_pugeko" required>
														<option disabled selected>{{ $formData->landInfo->haak_pugeko ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="electricity_line" class="control-label">
														११.हाईटेन्सनको  तार नजिक&nbsp;
													</label>
													
													<select class="form-control" name="electricity_line" required>
														<option disabled selected>{{ $formData->landInfo->electricity_line ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="river" class="control-label">
														१२.नदी / नहर / कुलो&nbsp;
													</label>
													<select class="form-control" name="river" required>
														<option disabled selected>{{ $formData->landInfo->river ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
										</div>
										{{-- form_landinfo END --}}
										
										{{-- form_buildinginfo START --}}
										<div class="row">
											<div class="col-md-3">
												<div class="form-group ">
													<label for="building_type" class="control-label">
														१.घर निर्माणको वर्ग&nbsp;
													</label>
													<select class="form-control" name="father_husband_option" required>
														<option disabled selected>{{ !empty($formData->building_type) ? $formData->building_type : 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label for="building_structure" class="control-label">
														२.भवनको स्ट्रक्चर&nbsp;
													</label>
													<select class="form-control" name="building_structure" required>
														<option disabled selected>{{ $formData->buildingInfo->building_structure ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="sabik_ward_number" class="control-label">
														३.प्लिन्थ लेभलको उचाई&nbsp;
													</label>
													<input type="text" name="plith_height" readonly required='' class="form-control" value="{{ $formData->buildingInfo->plith_height ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="foundation_size" class="control-label">
														४.जगको साईज &nbsp;
													</label>
													<input type="text" name="foundation_size" readonly required='' class="form-control" value="{{ $formData->buildingInfo->foundation_size ?? 'n/a' }}">
												</div>
											</div>
										
										</div>
										{{-- end of first row --}}
										
										{{-- start of 2nd row --}}
										<div class="row">
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="pillar_size" class="control-label">
														५.पिलरको साईज़&nbsp;
													</label>
													<input type="text" name="pillar_size" readonly required="" class="form-control" value="{{ $formData->buildingInfo->pillar_size ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="pillar_rod_size" class="control-label">
														६.पिलरको डण्डी साईज़&nbsp;
													</label>
													<input type="text" name="pillar_rod_size" readonly required="" class="form-control" value="{{ $formData->buildingInfo->pillar_rod_size ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="pillar_rod_number" class="control-label">
														७.पिलरको डण्डी साईज़&nbsp;
													</label>
													<input type="text" name="pillar_rod_number" readonly required="" class="form-control" value="{{ $formData->buildingInfo->pillar_rod_number ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="pillar_churi_size" class="control-label">
														८.पिलरको चुरीको साईज़&nbsp;
													</label>
													<input type="text" name="pillar_churi_size" readonly required='' class="form-control" value="{{ $formData->buildingInfo->pillar_churi_size ?? 'n/a' }}">
												</div>
											</div>
										
										</div>
										<!--/ end of second row-->
										
										{{-- start of third row --}}
										<div class="row">
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="left_right_chadne_duri" class="control-label">
														९.दायाँ / बायाँ छोड्ने दुरी&nbsp;
													</label>
													<input type="text" name="left_right_chadne_duri"  readonly required='' class="form-control" value="{{ $formData->buildingInfo->left_right_chadne_duri ?? 'n/a' }}">
												</div>
											</div>
											
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="setback" class="control-label">
														१०.सेट ब्याक &nbsp;
													</label>
													<input type="text" name="setback"  required='' readonly class="form-control" value="{{ $formData->buildingInfo->setback ?? 'n/a' }}">
												</div>
											</div>
										</div>
										<!-- end of third row -->
										
										<hr>
										
										<!-- fourth row started -->
										<div class="row">
											<p style="font-size: 25px; text-align: center"> ११.जग्गाको चार किल्ला विवरण</p>
											<hr>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="east_border" class="control-label">
														पूर्व&nbsp;
													</label>
													<input type="text" name="east_border" readonly required='' class="form-control" value="{{ $formData->buildingInfo->east_border ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="setback" class="control-label">
														पश्चिम&nbsp;
													</label>
													<input type="text" name="west_border" readonly required='' class="form-control" value="{{ $formData->buildingInfo->west_border ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="north_border" class="control-label">
														उत्तर &nbsp;
													</label>
													<input type="text" name="north_border" readonly required='' class="form-control" value="{{ $formData->buildingInfo->north_border ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="south_border" class="control-label">
														दक्षिण &nbsp;
													</label>
													
													
													<input type="text" name="south_border" readonly required='' class="form-control" value="{{ $formData->buildingInfo->south_border ?? 'n/a' }}">
												</div>
											</div>
										
										</div>
										<!-- end of fourth row -->
										
										<hr>
										<!-- start fifth row -->
										<div class="row">
											<p style="font-size: 25px; text-align: center"> १२.निर्माण कार्यको विवरण</p>
											<hr>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="building_length" class="control-label">
														लम्बाई (फिट मा) &nbsp;
													</label>
													
													<input type="text" name="building_length" readonly  required='' class="form-control" value="{{ $formData->buildingInfo->building_length ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="building_breadth" class="control-label">
														चौडाई (फिट मा) &nbsp;
													</label>
													<input type="text" name="building_breadth" readonly required='' class="form-control" value="{{ $formData->buildingInfo->building_breadth ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="building_height" class="control-label">
														उचाई (फिट मा)  &nbsp;
													</label>
													<input type="text" name="building_height" readonly required='' class="form-control" value="{{ $formData->buildingInfo->building_height ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="number_of_door" class="control-label">
														ढोका संख्या &nbsp;
													</label>
													<input type="text" name="number_of_door" readonly required='' class="form-control" value="{{ $formData->buildingInfo->number_of_door ?? 'n/a' }}">
												</div>
											</div>
										
										
										</div>
										<!--/ end of fifth row-->
										
										<!-- start sixth row -->
										<div class="row">
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="number_of_door" class="control-label">
														कोठा संख्या  &nbsp;
													</label>
													
													<input type="text" name="number_of_room" readonly required='' class="form-control" value="{{ $formData->buildingInfo->number_of_room ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="number_of_window" class="control-label">
														झ्याल संख्या  &nbsp;
													</label>
													
													<input type="text" name="number_of_window" readonly required='' class="form-control" value="{{ $formData->buildingInfo->number_of_window ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="number_of_bathroom" class="control-label">
														शौचालय/बाथरुम   &nbsp;
													</label>
													<input type="text" name="number_of_bathroom" readonly required='' class="form-control" value="{{ $formData->buildingInfo->number_of_bathroom ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="septic_tank" class="control-label">
														ढल / सेफ्टिटंकी
													</label>
													
													<input type="text" name="septic_tank" readonly required='' class="form-control" value="{{ $formData->buildingInfo->septic_tank ?? 'n/a' }}">
												</div>
											</div>
										
										
										</div>
										<!--/ end of sixth row-->
										
										<!-- start seven row -->
										<div class="row">
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="number_of_channel_gate" class="control-label">
														च्यानल गेट संख्या &nbsp;
													</label>
													<select name="number_of_channel_gate" class="form-control" required >
														<option disabled selected>{{ $formData->buildingInfo->number_of_channel_gate ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="number_of_satar" class="control-label">
														सटर संख्या  &nbsp;
													</label>
													<select name="number_of_satar" class="form-control" required >
														<option disabled selected>{{ $formData->buildingInfo->number_of_satar ?? 'n/a' }}</option>
													
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="roof" class="control-label">
														छाना   &nbsp;
													</label>
													
													<select name="roof" required class="form-control">
														<option disabled selected>{{ $formData->buildingInfo->roof ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="jodai" class="control-label">
														जोडाई &nbsp;
													</label>
													
													<select name="jodai" required class="form-control">
														<option disabled selected>{{ $formData->buildingInfo->jodai ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
										
										</div>
										<!--/ end of seven row-->
										
										<!-- start eight row -->
										<div class="row">
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="building_purpose" class="control-label">
														प्रयोजन &nbsp;
													</label>
													<select name="building_purpose" class="form-control" required >
														<option disabled selected>{{ $formData->buildingInfo->building_purpose ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
										
										</div>
										<!-- end of eight row -->
										
										<hr>
										<p style="text-indent: 5%">माथि उल्लेखित जग्गामा <strong>{{ $formData->building_type ?? 'n/a' }}</strong> गर्न प्रस्ताव गरिएको संरचना भूकम्प सुरक्षात्मक बनाउन सरकारी आवश्यक नक्शा डिजाइन, आवश्यक कागजात सहित यो निवेदन पेश
											<strong>
												@if ($formData->sambandha_ma=='मैले')
													गरेको छु
												@else
													गरेका छौँ
												@endif
											</strong>
											। प्राविधिक रुपमा तथा निर्माणबाट भूकम्प भए वा साधारण सुरक्षाको कमीले हुनसक्ने सम्पूर्ण जोखिम प्रति
											जिम्मेवार
											<strong>{{ $formData->option_1 ?? 'n/a' }}</strong>
											| यस रत्ननगर नगरपालिका बाट समय समयमा दिइने निर्देशन मापदण्ड समेत पालना गर्ने
											तथा आवश्यक परेको बेला त्यस कार्यालयमा उपस्थित
											<strong>
												@if ($formData->sambandha_ma=='मैले')
													हुनेछु
												@else
													हुनेछौँ
												@endif
											</strong>
											।</p>
										<hr>
										
										<!-- start of ninth row -->
										<div class="row">
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="house_owner_name" class="control-label">
														घरधनी/प्रतिनिधिको नाम थर
													</label>
													
													<input type="text" name="house_owner_name" readonly required='' class="form-control" value="{{ $formData->buildingInfo->house_owner_name ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="citizenship_number" class="control-label">
														ना.प्र. प. नं  &nbsp;
													</label>
													
													<input type="text" name="citizenship_number" readonly required='' class="form-control" value="{{ $formData->buildingInfo->citizenship_number ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="district" class="control-label">
														ना.प्र. प जारी जिल्ला
													</label>
													
													<input type="text" name="district"  required='' readonly class="form-control" value="{{ $formData->buildingInfo->district ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="phone" class="control-label">
														मोबाइल नं&nbsp;
													</label>
													
													<input type="text" name="phone"  required='' readonly class="form-control" value="{{ $formData->buildingInfo->phone ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label for="ground_area_coverage" class="control-label">
														जमिन ओगटेको प्रतिशत &nbsp;
													</label>
													
													<input type="text" name="ground_area_coverage" readonly required='' class="form-control" value="{{ $formData->buildingInfo->ground_area_coverage ?? 'n/a' }}">
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group ">
													<label for="floor_area_ratio" class="control-label">FAR(Floor Area Ratio)&nbsp;</label>
													<input type="text" name="floor_area_ratio" readonly required='' class="form-control" value="{{ $formData->buildingInfo->floor_area_ratio ?? 'n/a' }}">
												</div>
											</div>
										</div>
										{{-- form_buildinginfo END --}}
										
										{{-- form_floorinfo START --}}
										<div class="row">
											<p style="font-size: 25px; text-align: center"> जग्गाको चार किल्ला विवरण</p>
											<hr>
											<div class="col-md-3">
												<div class="form-group ">
													<label for="floor_number" class="control-label">
														१.तल्ला संख्या&nbsp;
													</label>
													<select class="form-control" id="floor_number"  required>
														<option disabled selected>{{ $formData->floorInfo->no_of_floor ?? 'n/a' }}</option>
													</select>
												</div>
											</div>
											
											@if(!empty($formData->floorInfo->underground_floor_area))
												<div class="col-md-3" id="floor1">
													<div class="form-group ">
														<label for="underground_floor_area" class="control-label">
															२.भूमिगत तल्ला &nbsp;
														</label>
														<input type="number" id="floor1" readonly name="underground_floor_area" placeholder="Enter value in sq.ft English" required='' class="form-control" value="{{ $formData->floorInfo->underground_floor_area ?? 'n/a' }}">
													</div>
												</div>
											@endif
											
											@if(!empty($formData->floorInfo->ground_floor_area))
												<div class="col-md-3" id="floor2">
													<div class="form-group ">
														<label for="ground_floor_area" class="control-label">
															३.भुइँ तल्ला &nbsp;
														</label>
														<input type="number"  name="ground_floor_area" readonly placeholder="Enter value in sq.ft English" required='' class="form-control" value="{{ $formData->floorInfo->ground_floor_area ?? 'n/a' }}">
													</div>
												</div>
											@endif
											
											@if(!empty($formData->floorInfo->first_floor_area))
												<div class="col-md-3" id="floor3">
													<div class="form-group ">
														<label for="first_floor_area" class="control-label">
															४.पहिलो तल्ला &nbsp;
														</label>
														<input type="number" name="first_floor_area" readonly placeholder="Enter value in sq.ft English" required='' class="form-control" value="{{ $formData->floorInfo->first_floor_area ?? 'n/a' }}">
													</div>
												</div>
											@endif
										</div>
										{{-- end of first row --}}
										
										{{-- start of 2nd row --}}
										<div class="row">
											@if(!empty($formData->floorInfo->second_floor_area))
												<div class="col-md-3" id="floor4">
													<div class="form-group ">
														<label for="second_floor_area" class="control-label">
															५.दोश्रो तल्ला  &nbsp;
														</label>
														<input type="number" name="second_floor_area" readonly placeholder="Enter value in sq.ft English" required='' class="form-control" value="{{ $formData->floorInfo->second_floor_area ?? 'n/a' }}">
													</div>
												</div>
											@endif
											
											@if(!empty($formData->floorInfo->third_floor_area))
												<div class="col-md-3" id="floor5">
													<div class="form-group ">
														<label for="third_floor_area" class="control-label">
															६.तेश्रो तल्ला&nbsp;
														</label>
														<input type="number" name="third_floor_area" readonly  placeholder="Enter value in sq.ft English" required='' class="form-control" value="{{ $formData->floorInfo->third_floor_area ?? 'n/a' }}">
													</div>
												</div>
											@endif
											
											@if(!empty($formData->floorInfo->fourth_floor_area))
												<div class="col-md-3" id="floor6">
													<div class="form-group ">
														<label for="fourth_floor_area" class="control-label">
															७.चौथो तल्ला&nbsp;
														</label>
														<input type="number" name="fourth_floor_area" readonly placeholder="Enter value in sq.ft English" required='' class="form-control" value="{{ $formData->floorInfo->fourth_floor_area ?? 'n/a' }}">
													</div>
												</div>
											@endif
										</div>
										<!--/ end of second row-->
										{{-- form_floorinfo END --}}
										
										{{-- form_buildingfiles START --}}
										<div class="row">
											<p style="font-size: 25px; text-align: center"> समंन्धित कागज पत्र</p>
											<hr>
											@if(!empty($formData->buildingFile->lalpurja))
												<div class="col-md-3">
													<div class="form-group ">
														<label for="lalpurja" class="control-label">
															१.लालपुर्जा
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->lalpurja) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->lalpurja) }}"  alt="lalpurja" class="img-responsive img-rounded" width="250"></a>
														</div>
													</div>
												</div>
											@endif
											
											@if(!empty($formData->buildingFile->citizenship))
												<div class="col-md-3">
													<div class="form-group ">
														<label for="citizeship" class="control-label">
															२.नागरिकता
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->citizenship) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->citizenship) }}" alt="citizenship"  class="img-responsive img-rounded" width="250"></a>
														</div>
													</div>
												</div>
											@endif
											
											@if(!empty($formData->buildingFile->blueprint))
												<div class="col-md-3">
													<div class="form-group ">
														<label for="blueprint" class="control-label">
															३.नापी नक्शा
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->blueprint) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->blueprint) }}" alt="blueprint" class="img-responsive img-rounded" width="250"></a>
														</div>
													</div>
												</div>
											@endif
											
											@if(!empty($formData->buildingFile->house_map))
												<div class="col-md-3">
													<div class="form-group ">
														<label for="house_map" class="control-label">
															४.घरको नक्शा
														</label>
														
														<div class="row text-center">
															<img src="{{ asset(STATIC_DIR.'uploads/pdf.png') }}" alt="" class="img-responsive">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->house_map) }}" target="_blank" class="btn btn-primary btn-outline" style="margin-top: 55px;" >View</a>
														</div>
													</div>
												</div>
											@endif
										</div>
										<!--/ end of first row-->
										
										
										<!-- second row -->
										<div class="row">
											@if(!empty($formData->buildingFile->tero_rashid))
												<div class="col-md-3">
													<div class="form-group ">
														<label for="tero_rashid" class="control-label">
															५.तीरो तिरेको रशिद
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->tero_rashid) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->tero_rashid) }}" alt="tero_rashid" class="img-responsive img-rounded" width="250"></a>
														</div>
													</div>
												</div>
											@endif
											
											@if(!empty($formData->buildingFile->naamsari))
												<div class="col-md-3">
													<div class="form-group ">
														<label for="naamsari" class="control-label">
															६.नाम सारीको कागज
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->naamsari) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->naamsari) }}" alt="naamsari" class="img-responsive img-rounded" width="250"></a>
														</div>
													</div>
												</div>
											@endif
											
											@if(!empty($formData->buildingFile->ward_sifaris))
												<div class="col-md-3">
													<div class="form-group ">
														<label for="ward_sifaris" class="control-label">
															७.वडा को सीफरीस
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->ward_sifaris) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->ward_sifaris) }}" alt="ward_sifaris" class="img-responsive img-rounded" width="250"></a>
														</div>
													</div>
												</div>
											@endif
											
											@if(!empty ($formData->buildingFile->sampanna_patra))
												<div class="col-md-3">
													<div class="form-group ">
														<label for="sampanna_patra" class="control-label">
															८.सम्पन्न प्रमाण पत्र
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->sampanna_patra) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->sampanna_patra) }}" alt="sampanna_patra" class="img-responsive img-rounded" width="250"></a>
														</div>
													</div>
												</div>
											@endif
										</div>
										<!--/ end of second row-->
										
										<!-- third row -->
										<div class="row">
											
											@if(!empty($formData->buildingFile->other_document_1) || !empty($formData->buildingFile->other_document_2) || !empty($formData->buildingFile->other_document_3) )
												<p style="font-size: 25px; text-align: center"> अन्य  कागज पत्र</p>
												<hr>
											@endif
											
											@if(!empty($formData->buildingFile->other_document_1))
												<div class="col-md-4">
													<div class="form-group ">
														<label for="other_document_1" class="control-label">
															१.अन्य विवरण  १
														</label>
														<div class="row text-center">
															<img src="{{ asset(STATIC_DIR.'uploads/pdf.png') }}" alt="" class="img-responsive">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->other_document_1) }}" target="_blank" class="btn btn-primary btn-outline" style="margin-top: 55px; margin-left: -150px;" >view</a>
														</div>
														
														
														@if(!empty($formData->buildingFile->other_document_1_description))
															<hr>
															<p>विवरण : {{ $formData->buildingFile->other_document_1_description }}</p>
														@endif
													</div>
												</div>
											@endif
											
											@if(!empty($formData->buildingFile->other_document_2))
												<div class="col-md-4">
													<div class="form-group ">
														<label for="other_document_2" class="control-label">
															१.अन्य विवरण  २
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->other_document_2) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->other_document_2) }}" alt="other_document_2" class="img-responsive img-rounded" width="250"></a>
														</div>
														
														@if(!empty($formData->buildingFile->other_document_2_description))
															<hr>
															<p>विवरण : {{ $formData->buildingFile->other_document_2_description }}</p>
														@endif
													</div>
												</div>
											@endif
											
											@if(!empty($formData->buildingFile->other_document_3))
												<div class="col-md-4">
													<div class="form-group ">
														<label for="other_document_3" class="control-label">
															१.अन्य विवरण  ३
														</label>
														<div class="row">
															<a href="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->other_document_3) }}" target="_blank"><img src="{{ asset(STATIC_DIR.'storage/'.$formData->buildingFile->other_document_3) }}" alt="other_document_3" class="img-responsive img-rounded" width="250"></a>
														</div>
														
														@if(!empty($formData->buildingFile->other_document_3_description))
															<hr>
															<p>विवरण : {{ $formData->buildingFile->other_document_3_description }}</p>
														@endif
													
													</div>
												</div>
											@endif
											{{-- form_buildingfiles END --}}
										</div>
										{{-- end form-body --}}
									</div>
									{{-- panel-body --}}
								</div>
								{{-- end panel-wrapper --}}
							</div>
							{{-- end panel --}}
						
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	@include('permission.model')
@endsection

@section('scripts')

@endsection
