@extends('layouts.app')
@section('css')
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/css/nepaliDatePicker.css') }}" >
	
	<style type="text/css">
		.option1{
			background-color: transparent;
			padding: 1px;
			width: 20px;
			border: none;
			font-size: 14px;
			color: #333;
			text-align: center;"
		}
		
		.option2{
			background-color: transparent;
			padding: 1px;
			width: 45px;
			border: none;
			font-size: 14px;
			color: #333;
			text-align: center;"
		}
	</style>
@endsection
@section('page_title')
	नयाँ नक्शा दरखास्त फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>नयाँ नक्शा दरखास्त फाईल</h4>
@endsection

@section('content')
	<div class="row">
		<!-- tbl of form_personalinfo -->
		<div class="col-md-12">
			{{-- {{ dd($errors) }} --}}
			<form  method="post"  action="{{ route('consultancy.store_form_personalinfo') }}">
				@csrf
				<input type="hidden" name="page" value="1" id="">
                <input type="hidden" name="form_id" value="{{ $formData->id ?? ''}}" >

                <div class="col-md-12">
					<div class="panel panel-info">
						<div class="panel-wrapper collapse in" aria-expanded="true">
							<div class="panel-body">
								<div class="form-body">
									<p style="font-size: 25px; text-align: center">तपसिल</p>
									<hr>
									<p style="text-indent: 5%">
										उपरोक्त सम्बन्धमा
										<select name="sambandha_ma"  id="option" required data-toggle="tooltip" title="अनिबार्य" data-placement="top" class="required">
											<option disabled selected value="" class="selected">--चयन गर्नुहोस--</option>
											<option value="मैले"
												@if(isset($formData))
													@if($formData->sambandha_ma == 'मैले' )
														selected
													@endif
												@elseif(old('sambandha_ma')=='मैले')
													selected
												@endif
												>मैले
											</option>
											<option value="हामीले" @if(old('sambandha_ma')=='हामीले') selected @endif
												@if(isset($formData))
													@if($formData->sambandha_ma == 'हामीले' )
														selected
													@endif
												@elseif(old('sambandha_ma')=='हामीले')
														selected
												@endif
												>हामीले
											</option>
										</select>
										
										तपशीलमा उल्लेखित जग्गामा देहाय बमोजिमको
										
										<select name="building_type" id="building_type" required data-toggle="tooltip" title="अनिबार्य">
											<option disabled selected >--चयन गर्नुहोस--</option>
											<option value="नयाँ घर निर्माण"

													@if(isset($formData))
													@if($formData->building_type == 'नयाँ घर निर्माण' )
													selected
													@endif
													@elseif(old('building_type')=='नयाँ घर निर्माण')
													selected
													@endif

													>नयाँ घर निर्माण
											</option>

											<option value="तल्ला थप"

													@if(isset($formData))
														@if($formData->building_type == 'तल्ला थप' )
														selected
														@endif
													@elseif(old('building_type')=='तल्ला थप')
														selected
													@endif
												>तल्ला थप

											</option>
										</select>
										
										कार्यका लागि घर नक्शा सहितको दरखास्त फारम साथै जग्गामा हक पुगेको आवश्यक कागजात प्रमाण समेत राखी पेश गरेको
										
										<input id="option_1"  readonly name="option_1" class="option1">
										
										। उक्त भवन निर्माण कार्यका लागि नक्शा पाश स्वीकृत गरी पाउन अनुरोध
										
										<input id="option_2" readonly name="option_2" class="option2">
										
										। यस दरखास्तमा लेखिएको व्यहोरा ठिक साँचो हो । झुठा ठहरे कानुन बमोजिम सहुँला बुझाउँला ।
									</p>
									
									<hr>
									
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="sambodhan" class="control-label">सम्बोधन  <sup class="text-danger">(अनिवार्य)</sup> </label>
												<select class="form-control" name="sambodhan" required>
													<option selected="" disabled="">--कुनै एक चयन गर्नुहोस--</option>
													<option value="श्री"
															@if(isset($formData))
																@if($formData->sambodhan == 'श्री' )
																	selected
																@endif
															@elseif(old('sambodhan')=='श्री')
															selected
															@endif>श्री
													</option>
													<option value="सुश्री"
															@if(isset($formData))
															@if($formData->sambodhan == 'सुश्री' )
															selected
															@endif
															@elseif(old('sambodhan')=='सुश्री')
															selected
															@endif>सुश्री
													</option>
												</select>
												@if($errors->has('sambodhan'))
													<span class="text-danger">{{ $errors->first('sambodan') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="field_owner_name" class="control-label">१.जग्गा धनी को नाम थर&nbsp;<sup class="text-danger">(अनिवार्य)</sup> </label>
												<input type="text" name="field_owner_name" required="" class="form-control" value="{{ $formData->field_owner_name ??  old('field_owner_name') }}" placeholder="जग्गा धनी को नाम थर (नेपालीमा)">
												@if($errors->has('field_owner_name'))
													<span class="text-danger">{{ $errors->first('field_owner_name') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="field_owner_address" class="control-label">
													२.जग्गा धनीको ठेगाना&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="field_owner_address"  required='' class="form-control" value="{{ $formData->field_owner_address ?? old('field_owner_address') }}" placeholder="जग्गा धनीको ठेगाना(नेपालीमा)">
												@if($errors->has('field_owner_address'))
													<span class="text-danger">{{ $errors->first('field_owner_address') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="field_owner_age" class="control-label">
													३.बर्ष&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="field_owner_age"  required='' class="form-control" value="{{$formData->field_owner_age ?? old('field_owner_age') }}" placeholder="बर्ष(नेपालीमा)">
												@if($errors->has('field_owner_age'))
													<span class="text-danger">{{ $errors->first('field_owner_age') }}</span>
												@endif
											</div>
										</div>
									</div>
									<!--/row-->
									{{-- end of first row --}}
									
									{{-- start of 2nd row --}}
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="father_husband_option" class="control-label">
													४.बाबु / पति&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select class="form-control" name="father_husband_option" required>
													<option disabled="" selected>--कुनै एक चयन गर्नुहोस--</option>
													<option value="बाबु"
														@if(isset($formData))
															@if($formData->father_husband_option == 'बाबु' )
																selected
															@endif
														@elseif(old('father_husband_option')=='बाबु')
																selected
														@endif
														>बाबु
													</option>
													<option value="पति"
														@if(isset($formData))
															@if($formData->father_husband_option == 'पति' )
																selected
															@endif
														@elseif(old('father_husband_option')=='पति')
																selected
														@endif

													>पति</option>
												</select>
												@if($errors->has('father_husband_option'))
													<span class="text-danger">{{ $errors->first('father_husband_option') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="father_husband_name" class="control-label">
													५.बाबु/पतिको नाम, थर&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="father_husband_name" required="" class="form-control" value="{{$formData->father_husband_name ?? old('father_husband_name') }}" placeholder="बाबु/पतिको नाम, थर (नेपालीमा)">
												@if($errors->has('father_husband_name'))
													<span class="text-danger">{{ $errors->first('father_husband_name') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="grandfather_option" class="control-label">
													६.बाजे / ससुरा&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select class="form-control" name="grandfather_option" required>
													<option disabled="" selected>--कुनै एक चयन गर्नुहोस--</option>
													<option value="बाजे" @if(old('grandfather_option')=='बाजे') selected @endif
														@if(isset($formData))
															@if($formData->grandfather_option == 'बाजे' )
																selected
															@endif
														@elseif(old('grandfather_option')=='बाजे')
																selected
														@endif
													>बाजे
													</option>
													<option value="ससुरा" @if(old('grandfather_option')=='ससुरा') selected @endif
														@if(isset($formData))
															@if($formData->grandfather_option == 'ससुरा' )
																selected
															@endif
														@elseif(old('grandfather_option')=='ससुरा')
																selected
														@endif
														>ससुरा
													</option>
												</select>
												@if($errors->has('grandfather_option'))
													<span class="text-danger">{{ $errors->first('grandfather_option') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="grandfather_name" class="control-label">
													७.बाजे / ससुरा नाम, थर&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="grandfather_name"  required='' class="form-control" value="{{ $formData->grandfather_name ?? old('grandfather_name') }}" placeholder="बाजे / ससुरा नाम, थर(नेपालीमा)">
												@if($errors->has('grandfather_name'))
													<span class="text-danger">{{ $errors->first('grandfather_name') }}</span>
												@endif
											</div>
										</div>
									
									</div>
									<!--/ end of second row-->
									
									{{-- start of third row --}}
									<div class="row">
										<div class="col-md-3">
											<div class="form-group ">
												<label for="field_owner_job" class="control-label">
													८.जग्गा धनीको पेशा/व्यवसाय &nbsp;<sup class="text-danger">(अनिवार्य) </sup>
												</label>
												<input type="text" name="field_owner_job"  required='' class="form-control" value="{{$formData->field_owner_job ?? old('field_owner_job') }}" placeholder="जग्गा धनीको पेशा/व्यवसाय(नेपालीमा)">
												@if($errors->has('field_owner_job'))
													<span class="text-danger">{{ $errors->first('field_owner_job') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="created_at_nepali" class="control-label">
													९.मिति &nbsp;<sup class="text-danger">(अनिवार्य) </sup>
												</label>
												<input type="text" name="created_at_nepali"  required='' class="bod-picker form-control" placeholder="मिति चयन गर्नुहोस " value="{{ $formData->created_at_nepali ?? old('created_at_nepali') }}">
												@if($errors->has('created_at_nepali'))
													<span class="text-danger">{{ $errors->first('created_at_nepali') }}</span>
												@endif
											</div>
										</div>
									</div>
									<!--/ end of third row-->
									<hr>
									<div class="row">
										<div class="col-lg-2 col-sm-4 col-xs-12 pull-right">
											<button class="btn btn-block btn-info">Next</button>
										</div>
									</div>
								</div>
								{{-- end form-body --}}
							</div>
							{{-- panel-body --}}
						</div>
						{{-- end panel-wrapper --}}
					</div>
					{{-- end panel --}}
				
				</div>
				{{-- end col-md-12 --}}
			
			</form>
			<!-- end form -->
		
		</div>
	</div>
	<!-- end col-md-12 -->
	
	
	
	
	@include('permission.model')
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'assets/js/nepaliDatePicker.js') }}" ></script>
	
	<script>

        $(".bod-picker").nepaliDatePicker({
            dateFormat: "%D, %M %d, %y",
            closeOnDateSelect: true
        });

        $("#clear-bth").on("click", function(event) {
            $(".bod-picker").val('');
        });


        $("#option").on ('change',  function( e ){
            var data = $('#option').val();

            if( data == 'मैले') {
                $('#option_1').val('छु');
                $('#option_2').val('गर्दछु');
            }
            else {
                $('#option_1').val('छौं');
                $('#option_2').val('गर्दछौं');
            }
        });

        $(document).ready(function( e ){
            var data = $('#option').val();

            if( data == 'मैले') {
                $('#option_1').val('छु');
                $('#option_2').val('गर्दछु');
            }
            else {
                $('#option_1').val('छौं');
                $('#option_2').val('गर्दछौं');
            }
        });
	</script>

@endsection
