@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	नयाँ नक्शा दरखास्त फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>नयाँ नक्शा दरखास्त फाईल</h4>
@endsection

@section('content')
	<div class="row">
		<!-- tbl of form_landinfo -->
		<div class="col-md-12">
			
			<form  method="post"  action="{{ route('consultancy.store_form_landinfo') }}">
				@csrf
				<input type="hidden" name="page" value="2" id="">
				<input type="hidden" name="form_id" value= {{ $formData->id }} >
				
				<div class="col-md-12">
					<div class="panel panel-info">
						
						<div class="panel-wrapper collapse in" aria-expanded="true">
							<div class="panel-body">
								
								<div class="form-body">
									<p style="font-size: 25px; text-align: center">जग्गा विवरण</p>
									<hr>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group ">
												<label for="kitta_number" class="control-label">
													१.हाल सर्भे कित्ता नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="kitta_number" required="" class="form-control" value="{{ $formData->landinfo->kitta_number ?? old('kitta_number') }}" placeholder="हाल सर्भे कित्ता नं(नेपालीमा)">
												@if($errors->has('kitta_number'))
													<span class="text-danger">{{ $errors->first('kitta_number') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<label for="sabik_kitta_number" class="control-label">
													२.साविक कित्ता नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="sabik_kitta_number" required="" class="form-control" value="{{ $formData->landinfo->sabik_kitta_number ?? old('sabik_kitta_number') }}" placeholder="साविक कित्ता नं.(नेपालीमा)">
												@if($errors->has('sabik_kitta_number'))
													<span class="text-danger">{{ $errors->first('sabik_kitta_number') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="sabik_ward_number" class="control-label">
													३.साविक गा.वि.स / वडा नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												
												<input type="text" name="sabik_ward_number"  required='' class="form-control" value="{{ $formData->landinfo->sabik_ward_number ?? old('sabik_ward_number') }}" placeholder="साविक गा.वि.स / वडा नं.(नेपालीमा)">
												@if($errors->has('sabik_ward_number'))
													<span class="text-danger">{{ $errors->first('sabik_ward_number') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="field_area" class="control-label">
													४.जग्गा क्षेत्रफल &nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												
												<input type="text" name="field_area"  required='' class="form-control" value="{{ $formData->landinfo->field_area ?? old('field_area') }}" placeholder="जग्गा क्षेत्रफल(नेपालीमा)">
												@if($errors->has('field_area'))
													<span class="text-danger">{{ $errors->first('field_area') }}</span>
												@endif
											</div>
										</div>
									</div>
									<!--/row-->
									{{-- end of first row --}}
									
									{{-- start of 2nd row --}}
									<div class="row">
										<div class="col-md-3">
											<div class="form-group ">
												<label for="village_name" class="control-label">
													५.बाटो / मार्ग / टोलको नाम&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												
												<input type="text" name="village_name" required="" class="form-control" value="{{ $formData->landinfo->village_name ?? old('village_name') }}" placeholder="बाटो / मार्ग / टोलको नाम(नेपालीमा)">
												@if($errors->has('village_name'))
													<span class="text-danger">{{ $errors->first('village_name') }}</span>
												@endif
											</div>
										
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="jagga_mohada" class="control-label">
													६.जग्गाको मोहोडा&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="jagga_mohada" required="" class="form-control" value="{{ $formData->landinfo->jagga_mohada ?? old('jagga_mohada') }}" placeholder="जग्गाको मोहोडा(नेपालीमा)">
												@if($errors->has('jagga_mohada'))
													<span class="text-danger">{{ $errors->first('jagga_mohada') }}</span>
												@endif
											
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="ward_number" class="control-label">
													७.जग्गा भएको वडा नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<input type="text" name="ward_number"  required='' class="form-control" value="{{ $formData->landinfo->ward_number ?? old('ward_number') }}" placeholder="जग्गा भएको वडा नं(नेपालीमा)">
												@if($errors->has('ward_number'))
													<span class="text-danger">{{ $errors->first('ward_number') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="current_ward_number" class="control-label">
													८.हालको वडा नं&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select name="current_ward_number" id="" class="form-control" required>
													<option value="">--चयन गर्नुहोस--</option>
													@if(isset($ward_number))
														@foreach($ward_number as $value)
															<option value="{{ $value->ward_number }}"
																	{{--@if(old('current_ward_number')==$value->ward_number) selected @endif--}}

																@if(isset($formData->landinfo))
																	@if($formData->landinfo->current_ward_number == $value->ward_number )
																		selected
																	@endif
																@elseif(old('current_ward_number')== $value->ward_number)
																	selected
																@endif
																		>
																{{ $value->ward_number }}
															</option>
														@endforeach
													@endif
												</select>
												@if($errors->has('current_ward_number'))
													<span class="text-danger">{{ $errors->first('current_ward_number') }}</span>
												@endif
											</div>
										</div>
									</div>
									<!--/ end of second row-->
									
									{{-- start of 2nd row --}}
									<div class="row">
										<div class="col-md-3">
											<div class="form-group ">
												<label for="sabik_kittama_ghar" class="control-label">
													९.हालको /साविक कित्तामा घर &nbsp;<sup class="text-danger">(अनिवार्य) </sup>
												</label>
												<select class="form-control" name="sabik_kittama_ghar" required>
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>
                                                    <option value="भएको"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->sabik_kittama_ghar == 'भएको' )
																selected
															@endif
														@elseif(old('sabik_kittama_ghar')=='भएको')
																selected
														@endif
														>भएको
													</option>
                                                    <option value="नभएको"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->sabik_kittama_ghar == 'नभएको' )
																selected
															@endif
														@elseif(old('sabik_kittama_ghar')=='नभएको')
																selected
														@endif
														>नभएको
													</option>
                                                </select>
                                            @if($errors->has('sabik_kittama_ghar'))
													<span class="text-danger">{{ $errors->first('sabik_kittama_ghar') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="haak_pugeko" class="control-label">
													१०.जग्गामा हक पुगेको बिवरण&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select class="form-control" name="haak_pugeko" required>
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="पुस्तौनी"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->haak_pugeko == 'पुस्तौनी' )
																selected
															@endif
														@elseif(old('haak_pugeko')=='पुस्तौनी')
																selected
														@endif
													>पुस्तौनी
													</option>

													<option value="राजिनामा"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->haak_pugeko == 'राजिनामा' )
																selected
															@endif
														@elseif(old('haak_pugeko')=='राजिनामा')
																selected
														@endif
													>राजिनामा
													</option>

													<option value="बकसपत्र"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->haak_pugeko == 'बकसपत्र' )
																selected
															@endif
														@elseif(old('haak_pugeko')=='बकसपत्र')
																selected
														@endif
													>बकसपत्र
													</option>

													<option value="हाल साविक"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->haak_pugeko == 'हाल साविक' )
																selected
															@endif
														@elseif(old('haak_pugeko')=='हाल साविक')
																selected
														@endif
													>हाल साविक&nbsp;(हा.सा.)</option>

													<option value="गुठी"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->haak_pugeko == 'गुठी' )
																selected
															@endif
														@elseif(old('haak_pugeko')=='गुठी')
																selected
														@endif
													>गुठी</option>

													<option value="अंशबण्डा"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->haak_pugeko == 'अंशबण्डा' )
																selected
															@endif
														@elseif(old('haak_pugeko')=='अंशबण्डा')
																selected
														@endif
													>अंशबण्डा
													</option>

												</select>
												@if($errors->has('haak_pugeko'))
													<span class="text-danger">{{ $errors->first('haak_pugeko') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="electricity_line" class="control-label">
													११.हाईटेन्सनको  तार नजिक&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select class="form-control" name="electricity_line" required>
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="भएको"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->electricity_line == 'भएको' )
																selected
															@endif
														@elseif(old('electricity_line')=='भएको')
																selected
														@endif
													>भएको
													</option>

													<option value="नभएको" endif
														@if(isset($formData->landinfo))
															@if($formData->landinfo->electricity_line == 'नभएको' )
																selected
															@endif
														@elseif(old('electricity_line')=='नभएको')
																selected
														@endif
													>नभएको
													</option>

												</select>
												@if($errors->has('electricity_line'))
													<span class="text-danger">{{ $errors->first('electricity_line') }}</span>
												@endif
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group ">
												<label for="river" class="control-label">
													१२.नदी / नहर / कुलो&nbsp;<sup class="text-danger">(अनिवार्य)</sup>
												</label>
												<select class="form-control" name="river" required>
													<option disabled selected>--कुनै एक चयन गर्नुहोस--</option>

													<option value="नभएको"

														@if(isset($formData->landinfo))
															@if($formData->landinfo->river == 'नभएको' )
																selected
															@endif
														@elseif(old('river')=='नभएको')
																selected
														@endif
													>नभएको
													</option>

													<option value="नदी"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->river == 'नदी' )
																selected
															@endif
														@elseif(old('river')=='नदी')
																selected
														@endif
													>नदी
													</option>

													<option value="नहर"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->river == 'नहर' )
																selected
															@endif
														@elseif(old('river')=='नहर')
																selected
														@endif
													>नहर
													</option>


													<option value="कुलो"
														@if(isset($formData->landinfo))
															@if($formData->landinfo->river == 'कुलो' )
																selected
															@endif
														@elseif(old('river')=='कुलो')
																selected
														@endif
													>कुलो
													</option>


												</select>
												@if($errors->has('river'))
													<span class="text-danger">{{ $errors->first('river') }}</span>
												@endif
											</div>
										</div>
									
									</div>
									<!--/ end of third row-->
									<hr>
									<div class="row">
										<div class="col-lg-2 col-sm-4 col-xs-12 pull-left">
											<a href="{{ route('consultancy.add_form_personalinfo',$formData->id) }}" class="btn btn-block btn-info text-white">Back</a>
										</div>
										<div class="col-lg-2 col-sm-4 col-xs-12 pull-right">
											<button class="btn btn-block btn-info">Next</button>
										</div>
									</div>
								</div>
								{{-- end form-body --}}
							</div>
							{{-- panel-body --}}
						</div>
						{{-- end panel-wrapper --}}
					</div>
					{{-- end panel --}}
				
				</div>
				{{-- end col-md-12 --}}
			
			</form>
			<!-- end form -->
		
		</div>
		<!-- end form_landinfo -->
	</div>
	
	@include('permission.model')
@endsection

@section('scripts')

@endsection
