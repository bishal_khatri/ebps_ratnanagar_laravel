@extends('layouts.app')
@section('page_title')
	Dashboard
@endsection
@section('content-title')
	Dashboard
@endsection
@section('css')
	<link href= "{{ asset(STATIC_DIR.'plugins/full_calendar/fullcalendar.min.css') }}"  rel='stylesheet' />
	<link href= "{{ asset(STATIC_DIR.'assets/clock/style.css') }}"  rel='stylesheet' />
	<style>
		
		body {
			margin: 0px 0px -15px;
			padding: 0;
			font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
			font-size: 14px;
		}
		.bg-theme {
			background-color: #fff !important;
		}
		#loading {
			display: none;
			position: absolute;
			top: 10px;
			right: 10px;
		}
		#calendar {
			max-height: 900px;
			margin: 0 auto;
		}
		.fc-unthemed td.fc-today {
			background: #ff7676 !important;
			color: white;
		}
		.fc-toolbar.fc-header-toolbar {
			margin-bottom: 0em;
		}
		hr {
			display: block;
			border-top: 5px solid #ff7676;
			margin: 1em 0;
			padding: 0;
		}
		.fc .fc-row .fc-content-skeleton table, .fc .fc-row .fc-content-skeleton td, .fc .fc-row .fc-helper-skeleton td {
			background: 0 0;
			border-color: white;
		}
		.bg-title{
			display:none;
			
		}
		.clock {
			letter-spacing: 5px;
		}
		@media only screen and (max-width: 1500px) {
			.clock {
				letter-spacing: 2px !important;
			}
		}
		@media only screen and (max-width: 1300px) {
			.clock {
				letter-spacing: 0px !important;
			}
		}
		
	</style>
@endsection

@section('content')
	<div class="row" style="margin-top: 25px;">
		<div class="col-md-12 col-lg-12 col-sm-12">
			<div class="calendar-widget m-b-30">
				<div class="cal-left">
					<h1>{{ Carbon\Carbon::now()->format('d') }}</h1>
					<h4>{{ Carbon\Carbon::now()->format('l') }}</h4> <span></span>
					<h5>{{ Carbon\Carbon::now()->format('F Y') }}</h5>
					<div class="clock">
						<ul class="clock-ul">
							<li class="clock-li" id="hours"> </li>
							<li class="clock-li" id="point">:</li>
							<li class="clock-li" id="min"> </li>
							<li class="clock-li" id="point">:</li>
							<li class="clock-li" id="sec"> </li>
						</ul>
					</div>
					<div class="cal-btm-text"> <a href="">EBPS</a>
						<h5>Ratnanagar</h5>
					</div>
				</div>
				<div class="cal-right bg-theme">
					<div id='loading'>loading...</div>
					<div id='calendar'></div>
				</div>
			</div>
		</div>
	</div>

@endsection
@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/full_calendar/lib/moment.min.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/full_calendar/fullcalendar.min.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'assets/clock/clock.js') }}"></script>
	<script>
        $(document).ready(function() {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,listYear'
                },
            });
        });
	</script>
@endsection
