@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'jquery.dataTables.css') }}">
@endsection
@section('page_title')
	फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
	<h2>
		फाईल
	</h2>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<div class="table-responsive">
								<table id ="form_id" class="table table-hover manage-u-table" >
									<thead>
									<tr>
										<th width="150" class="text-center">S.N.</th>
										<th width="150" class="text-center">दर्ता नं.</th>
										<th>जग्गा धनीको नाम थर</th>
										<th>ठेगाना</th>
										<th>वडा नं</th>
										<th>निर्माण किसिम</th>
										<th>दर्ता मिति</th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									@if (isset($files))
										@foreach($files as $value)
									<tr>
										<td class="text-center">{{ $loop->iteration }}</td>
										<td class="text-center">{{ $value->darta_number }}</td>
										<td>{{ $value->sambodhan }}&nbsp;{{ $value->field_owner_name }}</td>
										<td>{{ $value->field_owner_address }}</td>
										<td>{{ $value->landInfo->current_ward_number }}</td>
										<td>{{ $value->building_type }}</td>
										<td>{{ $value->created_at_nepali }}</td>
										<td>
											<a href="{{ route('administration.get_edit',$value->id) }}" type="button" class="btn btn-link btn-sm m-r-5">Edit File</a>
                                        </td>
									</tr>
										@endforeach
										@endif
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

@endsection

@section('scripts')
	<script type="text/javascript" charset="utf8" src="{{ asset(STATIC_DIR.'jquery.dataTables.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            //Bootstrap-TouchSpin

        });

        $(document).ready( function () {
            $('#form_id').DataTable();
        } );

    </script>
@endsection
