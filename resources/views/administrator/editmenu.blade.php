@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
    File Edit
@endsection
@section('right_button')
@stop
@section('content-title')File Edit @endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Name : {{ $files->sambodhan }} {{ $files->field_owner_name }} <span class="label label-primary"> {{ $files->darta_number }}</span>
                    <a href="{{ route('administration.get_files') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-circle-left"></i>&nbsp;Back</a>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <p><strong>Proceeded To : </strong> <span class="label label-primary">{{ $files->proceeded_to }}</span></p>
                        <p><strong>Building Type : </strong> {{ $files->building_type }}</p>
                        <p><strong>Address : </strong> {{ $files->field_owner_address }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" >
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body text-center">
                        <h3>Consultancy Form</h3>
                        <a href="{{ route('administration.edit_form_administration',$form_id.'?token=administration') }}" class="btn btn-success m-t-10">Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body text-center">
                        <h3>Rajashow</h3>
                        <a href="{{ route('administration.rajashow',$form_id.'?token=administration') }}" class="btn btn-success m-t-10">Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body text-center">
                        <h3>DPC</h3>
                        <a href="{{ route('administration.edit_dpcform',$form_id.'?token=administration') }}" class="btn btn-success m-t-10">Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body text-center">
                        <h3>Amin Pratibedan Form</h3>
                        <a href="{{route('administration.edit_aaminPratibedan',$form_id.'?token=administration')}}" class="btn btn-success m-t-10">Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            //Bootstrap-TouchSpin

        });
    </script>
@endsection
