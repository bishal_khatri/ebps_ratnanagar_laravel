@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@endsection
@section('page_title')
    Files listing
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>फाईल खोज्नुहोस</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('subengineer.post_upload_sampanna') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="form_id" value="{{$formData->id}}">

                            {{--<div class="form-group">
                                <label for="sampanna_form" class="control-label">
                                    Sampanna form फाईल अपलोड गर्नुहोस।
                                </label>

                                <input type="file" name="sampanna_form" id="input-file-now" class="dropify"  accept=".pdf,.jpeg,.jpg,.png" required="" />
                                @if($errors->has('sampanna_form'))
                                    <span class="text-danger">{{ $errors->first('sampanna_form') }}</span>
                                @endif
                            </div>--}}

                            <div class="form-group">
                                <label for="sampanna_tippani" class="control-label">
                                    Sampanna tippani फाईल अपलोड गर्नुहोस।
                                </label>

                                <input type="file" name="sampanna_tippani" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png" required="" />
                                @if($errors->has('sampanna_tippani'))
                                    <span class="text-danger">{{ $errors->first('sampanna_tippani') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="sampanna_certificate" class="control-label">
                                    Sampanna certificate फाईल अपलोड गर्नुहोस।
                                </label>

                                <input type="file" name="sampanna_certificate" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png" required="" />
                                @if($errors->has('sampanna_certificate'))
                                    <span class="text-danger">{{ $errors->first('sampanna_certificate') }}</span>
                                @endif
                            </div>

                            <div class="row pull-right">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('permission.model')
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });
    </script>
@endsection
