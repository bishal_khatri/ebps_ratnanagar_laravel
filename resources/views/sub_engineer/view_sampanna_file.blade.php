@extends('layouts.app')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bower_components/gallery/css/animated-masonry-gallery.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bower_components/fancybox/ekko-lightbox.min.css') }}" />
@endsection
@section('page_title')
    Sampanna
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>SAMPANNA FILE LISTING</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        <p>घर / जग्गाधनीको नाम : {{ $formData->field_owner_name }}</p>
                        <p>ठेगाना : {{ $formData->field_owner_address }}</p>
                        <p>अस्थाई चरण अनुमति दिईएको मिति :</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div id="gallery">
                            <div id="galler y-header">
                                <div id="gallery-header-center">
                                    <div id="gallery-header-center-left">
                                    </div>
                                </div>
                            </div>
                            <div id="gallery-content">
                                <div id="gallery-content-center">
                                    <a href="{{ asset(STATIC_DIR.'storage/'.$formData->sampanna_scan->form) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="अमिन  प्रतिवेदन">
                                        <img src="{{ asset(STATIC_DIR.'storage/'.$formData->sampanna_scan->form) }}" class="all studio" alt="gallery" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <br>
                <br>
                {{--emd first row--}}

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div id="gallery">
                            <div id="gallery-header">
                                <div id="gallery-header-center">
                                    <div id="gallery-header-center-left">
                                    </div>
                                </div>
                            </div>
                            <div id="gallery-content">
                                <div id="gallery-content-center">
                                    <a href="{{ asset(STATIC_DIR.'storage/'.$formData->sampanna_scan->tippani) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="अमिन  प्रतिवेदन">
                                        <img src="{{ asset(STATIC_DIR.'storage/'.$formData->sampanna_scan->tippani) }}" class="all studio" alt="gallery" />
                                    {{--</a>--}}
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <br>
                <br>
                {{--2nd row end--}}


                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div id="gallery">
                            <div id="gallery-header">
                                <div id="gallery-header-center">
                                    <div id="gallery-header-center-left">
                                    </div>
                                </div>
                            </div>
                            <div id="gallery-content">
                                <div id="gallery-content-center">
                                    <a href="{{ asset(STATIC_DIR.'storage/'.$formData->sampanna_scan->certificate) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="अमिन  प्रतिवेदन">
                                        <img src="{{ asset(STATIC_DIR.'storage/'.$formData->sampanna_scan->certificate) }}" class="all studio" alt="gallery" />
                                    {{--</a>--}}
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sub_engineer.modal')
@endsection

@section('scripts')
   <script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/animated-masonry-gallery.js') }}"></script>
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/jquery.isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.js') }}"></script>
    {{--<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>--}}
    {{--<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>--}}
    <script>
        $(document).ready(function($) {
            // delegate calls to data-toggle="lightbox"
            $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                event.preventDefault();
                return $(this).ekkoLightbox({
                    onShown: function() {
                        if (window.console) {
                            return console.log('Checking our the events huh?');
                        }
                    },
                    onNavigate: function(direction, itemIndex) {
                        if (window.console) {
                            return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                        }
                    }
                });
            });
        });
    </script>
@endsection
