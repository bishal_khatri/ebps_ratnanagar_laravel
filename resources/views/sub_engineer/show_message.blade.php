@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/css/animated-masonry-gallery.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.css') }}" />
@endsection
@section('page_title')
	D.P.C
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>D.P.C </h4>
@endsection

@section('content')
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
		</svg>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-12">
						<h3 class="box-title text-center text-danger">
							@if(isset($action))
								@if($action == 'engineer')
									Proceeded to Engineer Section.
								@elseif($action == 'consultancy')
									Proceeded to Consultancy Section.
								@endif
							@endif
						</h3>
					</div>
				</div>
				<hr>
				<div class="row">
					@if(isset($dpcScan))
						<div class="row text-center">
							<div id="gallery">
								<div id="gallery-content">
									<a href="{{ asset(STATIC_DIR.'storage/'.$dpcScan->dpc_scan) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="अमिन  प्रतिवेदन">
										<img src="{{ asset(STATIC_DIR.'storage/'.$dpcScan->dpc_scan) }}" class="all studio" alt="gallery" width="500"/>
									</a>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					@endif
					
					@if(isset($superstructure_path))
						<div class="row text-center">
							<div id="gallery">
								<div id="gallery-content">
									<a href="{{ asset(STATIC_DIR.'storage/'.$superstructure_path) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="अमिन  प्रतिवेदन">
										<img src="{{ asset(STATIC_DIR.'storage/'.$superstructure_path) }}" class="all studio" alt="gallery" width="500"/>
									</a>
								</div>
							
							</div>
						</div>
						<div class="clearfix"></div>
					@endif
				
				</div>
			</div>
		</div>
	</div>
	
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/animated-masonry-gallery.js') }}"></script>
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/jquery.isotope.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.js') }}"></script>
	
	<script>
        $(document).ready(function($) {
            // delegate calls to data-toggle="lightbox"
            $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                event.preventDefault();
                return $(this).ekkoLightbox({
                    onShown: function() {
                        if (window.console) {
                            return console.log('Checking our the events huh?');
                        }
                    },
                    onNavigate: function(direction, itemIndex) {
                        if (window.console) {
                            return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                        }
                    }
                });
            });
        });
	</script>
@endsection
