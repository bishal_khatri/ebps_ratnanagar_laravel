<div class="modal fade" id="reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('subengineer.rejectForm') }}" method="post">
                @csrf
                <input type="hidden" name="form_id" id="form_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">अस्वीकृत गर्नुहोस </h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="control-label">अस्वीकृत गर्ने कारण </label>
                        <textarea name="rejected_message" class="form-control" rows="10" id="message-text1" placeholder="अस्वीकृत गर्ने कारण लेखनुहोस|"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Reject</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{--file upload --}}
<div class="modal fade" id="uploadRajashow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('subengineer.rajashow.upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="form_id" id="form_id_rejashow">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">रजशोव  फाईल अपलोड</h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="other_document_2" class="control-label">
                            रजशोव  फाईल अपलोड गर्नुहोस|
                        </label>
                        <input type="file" name="rajashow_scan" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                        @if($errors->has('rajashow_scan'))
                            <span class="text-danger">{{ $errors->first('rajashow_scan') }}</span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{--reject modal for dpc form--}}
<div class="modal fade" id="rejectdpc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('subengineer.rejectdpc') }}" method="post">
                @csrf
                <input type="hidden" name="form_id" id="dpc_form_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">अस्वीकृत गर्नुहोस </h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="control-label">अस्वीकृत गर्ने कारण </label>
                        <textarea name="rejected_message" class="form-control" rows="10" id="message-text1" placeholder="अस्वीकृत गर्ने कारण लेखनुहोस|"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Reject</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{--reject modal for sampanna form--}}
<div class="modal fade" id="rejectsampanna" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('subengineer.rejectsampanna') }}" method="post">
                @csrf
                <input type="hidden" name="form_id" id="sampanna_form_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">अस्वीकृत गर्नुहोस </h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="control-label">अस्वीकृत गर्ने कारण </label>
                        <textarea name="rejected_message" class="form-control" rows="10" id="message-text1" placeholder="अस्वीकृत गर्ने कारण लेखनुहोस|"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Reject</button>
                </div>
            </form>
        </div>
    </div>
</div>
