@extends('layouts.app')
@section('css')
    {{--<link href="../plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />--}}
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/css/nepaliDatePicker.css') }}" >
@endsection
@section('page_title')
    राजस्व
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>राजस्व </h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                @if($token == 'administration')
                    <a href="{{ route('administration.get_edit',$personalInfo->id) }}" class="btn btn-primary btn-outline" style="margin-bottom: 5px;"><i class="fa fa-arrow-circle-left"></i>&nbsp;Back</a>
                @endif
                @if(empty($rajashow))
                    <p>Data is empty</p>
                @else

                {{--@if($token == 'administration')--}}
                     <form action="@if($token == 'administration'){{ route('administration.rajashow.store') }}@else {{ route('subengineer.rajashow.store') }} @endif" method="post">
                {{--@else--}}
                     {{--<form action="{{ route('subengineer.rajashow.store') }}" method="post">--}}
                {{--@endif--}}
                     @csrf
                    <input type="hidden" name="form_id" value="{{ $personalInfo->id }}" id="">
                    <input type="hidden" name="action" value="edit">

                    <div class="row">

                        <div class="col-md-3">
                            जग्गा धनिको नाम थर : <p>{{ $personalInfo->sambodhan ?? '' }} {{ $personalInfo->field_owner_name ?? '' }}</p>
                        </div>
                        <div class="col-md-3">
                            निर्माण किसिम : <p>{{ $personalInfo->building_type ?? '' }}</p>
                        </div>
                        <div class="col-md-3">
                            दर्ता नम्बर : <p>{{ $personalInfo->id ?? '' }}</p>
                        </div>
                        <div class="col-md-3">
                            वडा नं : <p>{{ $landInfo->current_ward_number ?? '' }}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-3">
                            <p>तल्ला</p>
                        </div>
                        <div class="col-md-3">
                            क्षेत्रफल(बर्ग फिट)
                        </div>
                        <div class="col-md-3">
                            दर रु (Rate)
                        </div>
                        <div class="col-md-3">
                            रकम रु(Amount)
                        </div>
                    </div>
                    @if(isset($floorInfo->underground_floor_area))
                        <div class="row">
                            <div class="col-md-3">
                                भुमिगत तल्ला
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control input-number" name="underground_floor_area" value="{{ $floorInfo->underground_floor_area ?? '' }}" id="underground_floor_area">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="underground_floor_rate" class="form-control input-number" id="underground_floor_rate" value="{{ old('underground_floor_rate') ?? $rajashow->underground_floor_rate }}">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="underground_floor_amount" class="form-control" readonly id="underground_floor_amount" value="{{ old('underground_floor_amount') ?? $rajashow->underground_floor_amount }}">
                            </div>
                        </div>
                    @endif
                    <br>
                    @if(isset($floorInfo->ground_floor_area))
                        <div class="row">
                            <div class="col-md-3">
                                भुइ तल्ला
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control input-number" name="ground_floor_area" value="{{ $floorInfo->ground_floor_area ?? '' }}" id="ground_floor_area">
                            </div>

                            <div class="col-md-3">
                                <input type="text" name="ground_floor_rate" class="form-control input-number" id="ground_floor_rate" value="{{ old('ground_floor_rate') ?? $rajashow->ground_floor_rate }}">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="ground_floor_amount" class="form-control" readonly id="ground_floor_amount" value="{{ old('ground_floor_amount') ?? $rajashow->ground_floor_amount }}">
                            </div>
                        </div>
                    @endif
                    <br>
                    @if(isset($floorInfo->first_floor_area))
                        <div class="row">
                            <div class="col-md-3">
                                पहिलो तल्ला
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control input-number" name="first_floor_area" value="{{ $floorInfo->first_floor_area ?? '' }}" id="first_floor_area">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="first_floor_rate" class="form-control input-number" id="first_floor_rate" value="{{ old('first_floor_rate') ?? $rajashow->first_floor_rate }}">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="first_floor_amount" class="form-control" readonly id="first_floor_amount" value="{{ old('first_floor_amount') ?? $rajashow->first_floor_amount }}">
                            </div>
                        </div>
                    @endif
                    <br>
                    @if(isset($floorInfo->second_floor_area))
                        <div class="row">
                            <div class="col-md-3">
                                दोश्रो तल्ला
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control input-number" name="second_floor_area" value="{{ $floorInfo->second_floor_area ?? '' }}" id="second_floor_area">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="second_floor_rate" class="form-control input-number" id="second_floor_rate" value="{{ old('second_floor_rate') ?? $rajashow->second_floor_rate }}">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="second_floor_amount" class="form-control" readonly id="second_floor_amount" value="{{ old('second_floor_amount') ?? $rajashow->second_floor_amount }}">
                            </div>
                        </div>
                    @endif
                    <br>
                    @if(isset($floorInfo->third_floor_area))
                        <div class="row">
                            <div class="col-md-3">
                                तेश्रो तल्ला
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control input-number" name="third_floor_area" value="{{ $floorInfo->third_floor_area ?? '' }}" id="third_floor_area">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="third_floor_rate" class="form-control input-number" id="third_floor_rate" value="{{ old('third_floor_rate') ??  $rajashow->third_floor_rate }}">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="third_floor_amount" class="form-control" readonly id="third_floor_amount" value="{{ old('third_floor_amount') ?? $rajashow->third_floor_amount }}">
                            </div>
                        </div>
                    @endif
                    <br>
                    @if(isset($floorInfo->fourth_floor_area))
                        <div class="row">
                            <div class="col-md-3">
                                चौथो तल्ला
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control input-number" name="fourth_floor_area" value="{{ $floorInfo->fourth_floor_area ?? '' }}" id="fourth_floor_area">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="fourth_floor_rate" class="form-control input-number" id="fourth_floor_rate" value="{{ old('fourth_floor_rate') ?? $rajashow->fourth_floor_rate }}">
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="fourth_floor_amount" class="form-control" readonly id="fourth_floor_amount" value="{{ old('fourth_floor_amount') ?? $rajashow->fourth_floor_amount }}">
                            </div>
                        </div>
                    @endif
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            जम्मा नक्शा पास दस्तुर
                        </div>
                        <div class="col-md-3 pull-right">
                            <input type="text" name="total" class="form-control" readonly id="total" value="{{ old('total') ?? $rajashow->total }}">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            अन्य धरौटी
                        </div>
                        <div class="col-md-3 pull-right">
                            <input type="text" name="other_amount" class="form-control input-number" id="other_amount" value="{{ old('other_amount') ?? $rajashow->other_amount }}">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            कूल जम्मा
                        </div>
                        <div class="col-md-3 pull-right">
                            <input type="text" name="grand_total" class="form-control" readonly id="grand_total" value="{{ old('grand_total') ?? $rajashow->grand_total }}">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">दस्तुर हिसाब गर्नेको नाम</label>
                            <input type="text" class="form-control" readonly value="{{ Auth::user()->first_name ?? '' }}&nbsp;{{ Auth::user()->last_name ?? '' }}">
                        </div>
                        <div class="col-md-6">
                            <label for="">मिति</label>
                            <div class="form-group @if($errors->has('created_at_nepali')) has-error @endif">
                                <input type="text" id="print-date" name="created_at_nepali" class="bod-picker form-control" placeholder="मिति चयन गर्नुहोस" value="{{ old('created_at_nepali') ?? $rajashow->created_at_nepali }}">
                            </div>
                            @if($errors->has('created_at_nepali'))
                                <p class="text-danger">{{ $errors->first('created_at_nepali') }}</p>
                            @endif
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 ">
                            <button type="submit" class="btn btn-primary btn-outline pull-right">Update</button>
                        </div>
                    </div>
                </form>
            @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{--<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>--}}
    {{--<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>--}}
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'assets/js/nepaliDatePicker.js') }}" ></script>
    <script>
        $(".bod-picker").nepaliDatePicker({
            dateFormat: "%D, %M %d, %y",
            closeOnDateSelect: true
        });

        $('.input-number').keypress(function(event) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        jQuery(document).ready(function () {
            $(".input-number").on('keyup', function () {
                var underground_floor_area = parseFloat($('#underground_floor_area').val());
                var underground_floor_rate = parseFloat($('#underground_floor_rate').val());
                var underground_floor_amount = underground_floor_area * underground_floor_rate;

                if (isNaN(underground_floor_amount)){
                    underground_floor_amount = 0;
                }
                $('#underground_floor_amount').val(underground_floor_amount);

                // ground floor
                var ground_floor_area = parseFloat($('#ground_floor_area').val());
                var ground_floor_rate = parseFloat($('#ground_floor_rate').val());
                var ground_floor_amount = ground_floor_area * ground_floor_rate;

                if (isNaN(ground_floor_amount)){
                    ground_floor_amount = 0;
                }
                $('#ground_floor_amount').val(ground_floor_amount);

                // first floor
                var first_floor_area = parseFloat($('#first_floor_area').val());
                var first_floor_rate = parseFloat($('#first_floor_rate').val());
                var first_floor_amount = first_floor_area * first_floor_rate;

                if (isNaN(first_floor_amount)){
                    first_floor_amount = 0;
                }
                $('#first_floor_amount').val(first_floor_amount);

                // second floor
                var second_floor_area = parseFloat($('#second_floor_area').val());
                var second_floor_rate = parseFloat($('#second_floor_rate').val());
                var second_floor_amount = second_floor_area * second_floor_rate;

                if (isNaN(second_floor_amount)){
                    second_floor_amount = 0;
                }
                $('#second_floor_amount').val(second_floor_amount);

                // third floor
                var third_floor_area = parseFloat($('#third_floor_area').val());
                var third_floor_rate = parseFloat($('#third_floor_rate').val());
                var third_floor_amount = third_floor_area * third_floor_rate;

                if (isNaN(third_floor_amount)){
                    third_floor_amount = 0;
                }
                $('#third_floor_amount').val(third_floor_amount);

                // fourth floor
                var fourth_floor_area = parseFloat($('#fourth_floor_area').val());
                var fourth_floor_rate = parseFloat($('#fourth_floor_rate').val());
                var fourth_floor_amount = fourth_floor_area * fourth_floor_rate;

                if (isNaN(fourth_floor_amount)){
                    fourth_floor_amount = 0;
                }
                $('#fourth_floor_amount').val(fourth_floor_amount);

                // total
                var total = underground_floor_amount + ground_floor_amount + first_floor_amount + second_floor_amount + third_floor_amount + fourth_floor_amount;
                $('#total').val(total);

                //other
                other_amount = parseFloat($('#other_amount').val());
                if (isNaN(other_amount)){
                    other_amount = 0;
                }

                //grand total
                var grand_total = underground_floor_amount + ground_floor_amount + first_floor_amount + second_floor_amount + third_floor_amount + fourth_floor_amount + other_amount;
                $('#grand_total').val(grand_total);
            });
        });
        // jQuery(document).ready(function() {
        //     // Switchery
        //     var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        //     $('.js-switch').each(function() {
        //         new Switchery($(this)[0], $(this).data());
        //     });
        //     // For select 2
        //     $(".select2").select2();
        //     $('.selectpicker').selectpicker();
        //     //Bootstrap-TouchSpin
        //
        // });
    </script>
@endsection
