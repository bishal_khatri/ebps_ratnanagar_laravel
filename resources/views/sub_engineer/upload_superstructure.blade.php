@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@endsection
@section('page_title')
    Files listing
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>फाईल खोज्नुहोस</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <form action="{{ route('subengineer.superstructure') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="form_id" value="{{$form_id}}">

                            <div class="form-group">
                                <label for="superstructure_certificate_scan" class="control-label">
                                    Superstructure Certificate  फाईल अपलोड गर्नुहोस|
                                </label>

                                <input type="file" name="superstructure_certificate_scan" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                                @if($errors->has('superstructure_certificate_scan'))
                                    <span class="text-danger">{{ $errors->first('superstructure_certificate_scan') }}</span>
                                @endif
                            </div>

                            <div class="row col-md-3 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('permission.model')
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });
    </script>
@endsection
