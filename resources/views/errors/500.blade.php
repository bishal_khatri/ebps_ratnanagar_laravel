@extends('layouts._partials.head')
@section('css')
    <link href="{{ asset(STATIC_DIR.'assets/css/colors/default.css') }}" id="theme" rel="stylesheet">

@endsection

@section('page_title')
    Page Not Found
@endsection

<body>
<!-- Preloader -->


<section id="wrapper" class="error-page">
    <div class="error-box">
        <div class="error-body text-center">
            <h1>500</h1>
            <h3 class="text-uppercase">Internal Server Error.</h3>
            <p class="text-muted m-t-30 m-b-30">Please try after some time</p>
            <a href="{{route('home')}}" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40">Back to home</a> </div>
    </div>
    <footer class="footer text-center " style=""> 2018 &copy; Electronic Building Permit System brought to you by Technology Sales </footer>

</section>

@include('layouts._partials.scripts')

</body>
</html>

@section('scripts')
@endsection

