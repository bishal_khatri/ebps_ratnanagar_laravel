<?php
$count = App\Helper\Tools::count_file();
?>
<div class="navbar-default sidebar hide-on-print" role="navigation">
	<div class="sidebar-nav slimscrollsidebar">
		<div class="sidebar-head">
			<h3>
                <span class="fa-fw open-close">
                    <i class="ti-close ti-menu"></i>
                </span> <span class="hide-menu">Navigation</span></h3>
		</div>
		<ul class="nav" id="side-menu">
			<li class="user-pro">
				<a href="#" class="waves-effect @if(Request::is('profile')) active @endif">

					@if(!empty(Auth::user()->avatar))
						<img src="{{ asset(STATIC_DIR.'storage/'.Auth::user()->avatar) }}" alt="user"  class="img-circle" />
					@else
						<img src="{{ asset(STATIC_DIR.'uploads/defualt.png') }}" alt="user"  class="img-circle" />
					@endif

					{{--<img src="{{ asset(STATIC_DIR.'uploads/defualt.png') }}" alt="user-img" class="img-circle">--}}
					<span class="hide-menu"> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}<span class="fa arrow" style="padding-top:10px;"></span>
                    </span>
				</a>
				<ul class="nav nav-second-level collapse" aria-expanded="true" style="height: @if(Request::is('profile')) 62px @endif;">
					<li>
						<a href="{{ route('profile') }}" class="waves-effect @if(Request::is('profile')) active @endif"  >
							<i class="ti-user"></i>
							<span class="hide-menu">My Profile</span>
						</a>
					</li>

					<!-- <li><a href="javascript:void(0)"><i class="ti-settings"></i> <span class="hide-menu">Account Setting</span></a></li> -->
					<li style="padding-left: 12px;">
						<form id="logout-form" action="{{ route('logout') }}" method="POST" >
							@csrf
							<button type="submit" class="btn btn-link btn-flat text-danger"><i class="fa fa-power-off"></i>&nbsp;Logout</button>
						</form>
					</li>
				</ul>
			</li>
			<li> <a href="{{ route('home') }}" class="waves-effect @if(Request::is('/')) active @endif ">
					<i class="mdi mdi-av-timer fa-fw"></i>
					<span class="hide-menu">Dashboard</span>
				</a>
			</li>


			@can('permission-list')
				<li> <a href="{{ route('permission.index') }}" class="@if(Request::is('permission/*')) active @endif">
						<i class="mdi mdi-lock fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Permissions</span>
					</a>
				</li>
			@endcan

			@can('administration')
				<li>
					<a href="#" class="waves-effect @if(Request::is('user/*')) active @endif">
						<i class="mdi mdi-account-multiple fa-fw"></i>
						<span class="hide-menu">Users<span class="fa arrow open"></span></span>
					</a>
					<ul class="nav nav-second-level @if(Request::is('user/*')) collapse in @endif">
						<li>
							<a class="@if(Request::is('user/register')) active @endif" href="{{ route('user.register') }}">
								<i class="mdi mdi-account-multiple-plus fa-fw"></i> <span class="hide-menu">Add</span>
							</a>
						</li>
						<li>
							<a class="@if(Request::is('user/list')) active @endif" href="{{ route('user.index') }}">
								<i class="mdi mdi-format-list-bulleted fa-fw"></i> <span class="hide-menu">List</span>
							</a>
						</li>
					</ul>
				</li>

				{{--<li class="devider"></li>--}}
				{{--<li class="" style="color: white;">&nbsp;&nbsp;Engineer</li>--}}
				<li class="devider"></li>
				<li>
					<a href="{{ route('administration.get_files') }}" class="@if(Request::is('administration/get_files')) active @endif">
						<i class="mdi mdi-format-list-bulleted fa-fw" data-icon="v"></i>
						<span class="hide-menu"> All Files </span>
					</a>
				</li>
                <li>
                    <a href="{{ route('file.get_files_count') }}" class="@if(Request::is('file/count') || Request::is('file/ward_files/*')) active @endif">
                        <i class="mdi mdi-file-multiple fa-fw" data-icon="v"></i>
                        {{-- <i class="far fa-tally"></i> --}}
                        <span class="hide-menu"> Files by Ward </span>
                    </a>
                </li>
				<li>
					<a href="{{ route('file.get_file_status') }}" class="@if(Request::is('file/status')) active @endif">
						<i class="mdi mdi-checkbox-multiple-marked fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Tracker</span>
					</a>
                </li>

			@endcan
			@can('consultancy')
				<li class="devider"></li>
				<li class="text-center text-uppercase sidebar-label">Consultancy</li>
				<li class="devider"></li>
				<li>
					<a href="{{ route('consultancy.add_form_personalinfo') }}" class="@if(Request::is('consultancy/add_form_personalinfo') OR Request::is('consultancy/add_form_landinfo/*')) active @endif">
						<i class="mdi mdi-file-document fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Create New File</span>
					</a>
				</li>
				<li>
					<a href="{{ route('consultancy.index') }}" class="@if(Request::is('consultancy/list') OR Request::is('consultancy/list/*')) active @endif">
						<i class="mdi mdi-format-list-bulleted fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Listing</span>
					</a>
				</li>

				<li>
					<a href="{{ route('consultancy.draft') }}" class="@if(Request::is('consultancy/draft') OR Request::is('consultancy/draft/*')) active @endif">
						<i class="mdi mdi-file-cloud fa-fw" data-icon="v"></i>
						<span class="hide-menu">Draft Files
							@if($count['draft'] > 0)
								<span class="label label-rouded label-warning pull-right" style="font-weight: bold ">{{$count['draft'] ?? ''}}</span>
							@endif
						</span>
					</a>
				</li>
				<li>
					<a href="{{ route('consultancy.rejected.list') }}" class="@if(Request::is('consultancy/rejected/*') OR Request::is('consultancy/rejected/*')) active @endif">
						<i class="mdi mdi-file-excel fa-fw" data-icon="v"></i>

						<span class="hide-menu"> Rejected Files
							@if($count['rejected'] > 0)
								<span class="label label-rouded label-danger pull-right" style="font-weight: bold ">{{$count['rejected']}}</span>
							@endif
						</span>
					</a>
				</li>
			@endcan

			@can('sub-engineer')
				<li class="devider"></li>
				<li class="text-center text-uppercase sidebar-label">Sub Engineer</li>
				<li class="devider"></li>
				<li>
					<a href="{{ route('subengineer.index') }}" class="@if(Request::is('subengineer/list')) active @endif">
						<i class="mdi mdi-format-list-bulleted fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Listing</span>
					</a>
				</li>
				<li>
					<a href="{{ route('file.get_file_status') }}" class="@if(Request::is('file/status')) active @endif">
						<i class="mdi mdi-checkbox-multiple-marked fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Tracker</span>
					</a>
				</li>
			@endcan

			@can('engineer')
				<li class="devider"></li>
				<li class="text-center text-uppercase sidebar-label">Engineer</li>
				<li class="devider"></li>
				<li>
					<a href="{{ route('engineer.index') }}" class="@if(Request::is('engineer/list')) active @endif">
						<i class="mdi mdi-format-list-bulleted fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Listing</span>
					</a>
				</li>

				<li>
					<a href="{{ route('file.get_file_status') }}" class="@if(Request::is('file/status')) active @endif">
						<i class="mdi mdi-checkbox-multiple-marked fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Tracker</span>
					</a>
				</li>
			@endcan

			@can('main-engineer')
				<li class="devider"></li>
				<li class="text-center text-uppercase sidebar-label">Main Engineer</li>
				<li class="devider"></li>
				<li> <a href="{{ route('mainengineer.index') }}" class="@if(Request::is('mainengineer/list')) active @endif">
						<i class="mdi mdi-format-list-bulleted fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Listing</span>
					</a>
				</li>
				<li>
					<a href="{{ route('file.get_file_status') }}" class="@if(Request::is('file/status')) active @endif">
						<i class="mdi mdi-checkbox-multiple-marked fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Tracker</span>
					</a>
				</li>
			@endcan

			@can('aamin')
				<li class="devider"></li>
				<li class="text-center text-uppercase sidebar-label">Aamin</li>
				<li class="devider"></li>
				<li> <a href="{{ route('aamin.index') }}" class="@if(Request::is('aamin/list')) active @endif">
						<i class="mdi mdi-format-list-bulleted fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Listing</span>
					</a>
				</li>
				<li>
					<a href="{{ route('aamin.list_rejected') }}" class="@if(Request::is('consultancy/list_rejected')) active @endif">
						<i class="mdi mdi-file-excel fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Rejected Files
							@if($count['rejected_aamin'] > 0)
								<sup>
									<span class="badge badge-danger" style="font-size: 9px !important;">
										{{$count['rejected_aamin']}}
									</span>
								</sup>
							@endif
						</span>
					</a>
				</li>
				<li>
					<a href="{{ route('file.get_file_status') }}" class="@if(Request::is('file/status')) active @endif">
						<i class="mdi mdi-checkbox-multiple-marked fa-fw" data-icon="v"></i>
						<span class="hide-menu"> File Tracker</span>
					</a>
				</li>
			@endcan
			<li style="padding-bottom: 100px;"></li>
		</ul>
	</div>
</div>
