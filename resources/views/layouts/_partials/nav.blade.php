<nav class="navbar navbar-default navbar-static-top m-b-0 hide-on-print">
	<div class="navbar-header">
		<!-- Toggle icon for mobile view -->
		<div class="top-left-part">
			<!-- Logo -->
			<a class="logo" href="{{ route('home') }}">
				<!-- Logo icon image, you can use font-icon also --><b>
					<!--This is dark logo icon-->
					<img src="{{ asset(STATIC_DIR.'uploads/logo/logo.png') }}" alt="home" class="dark-logo"  width="50"/><!--This is light logo icon-->
					<img src="{{ asset(STATIC_DIR.'uploads/logo/logo.png') }}" alt="home" class="light-logo" width="50" />
				</b>
				<strong class="text-success">E B P S</strong>
				<!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text-->
					
					<!-- <img src="{{ asset(STATIC_DIR.'plugins/images/admin-text.png') }}" alt="home" class="dark-logo" /> --><!--This is light logo text-->
				<!-- <img src="{{ asset(STATIC_DIR.'plugins/images/admin-text-dark.png') }}" alt="home" class="light-logo" /> -->
                     </span> </a>
		</div>
		<!-- /Logo -->
		<!-- Search input and Toggle icon -->
		<ul class="nav navbar-top-links navbar-left">
			<li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
			{{--<li class="dropdown">--}}
				{{--<a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-gmail"></i>--}}
					{{--<div class="notify"><span class="heartbit"></span><span class="point"></span></div>--}}
				{{--</a>--}}
				{{--<ul class="dropdown-menu mailbox animated bounceInDown">--}}
					{{--<li>--}}
						{{--<div class="drop-title">You have 4 new messages</div>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<div class="message-center">--}}
							{{--<a href="#">--}}
								{{--<div class="user-img"> <img src="{{ asset(STATIC_DIR.'plugins/images/users/pawandeep.jpg') }}" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>--}}
								{{--<div class="mail-contnet">--}}
									{{--<h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>--}}
							{{--</a>--}}
						{{--</div>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>--}}
					{{--</li>--}}
				{{--</ul>--}}
				{{--<!-- /.dropdown-messages -->--}}
			{{--</li>--}}
			<!-- .Task dropdown -->
			{{--<li class="dropdown">--}}
				{{--<a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-check-circle"></i>--}}
					{{--<div class="notify"><span class="heartbit"></span><span class="point"></span></div>--}}
				{{--</a>--}}
				{{--<ul class="dropdown-menu dropdown-tasks animated slideInUp">--}}
					{{--<li>--}}
						{{--<a href="javascript:void(0)">--}}
							{{--<div>--}}
								{{--<p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>--}}
								{{--<div class="progress progress-striped active">--}}
									{{--<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</a>--}}
					{{--</li>--}}
					{{--<li class="divider"></li>--}}
					{{--<li>--}}
						{{--<a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>--}}
					{{--</li>--}}
				{{--</ul>--}}
			{{--</li>--}}
			<!-- .Megamenu -->
			{{--<li class="mega-dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><span class="hidden-xs">Mega</span> <i class="icon-options-vertical"></i></a>--}}
				{{--<ul class="dropdown-menu mega-dropdown-menu animated bounceInDown">--}}
					{{--<li class="col-sm-3">--}}
						{{--<ul>--}}
							{{--<li class="dropdown-header">Header Title</li>--}}
							{{--<li><a href="javascript:void(0)">Link of page</a> </li>--}}
						{{--</ul>--}}
					{{--</li>--}}
					{{--<li class="col-sm-3">--}}
						{{--<ul>--}}
							{{--<li class="dropdown-header">Header Title</li>--}}
							{{--<li><a href="javascript:void(0)">Link of page</a> </li>--}}
						{{--</ul>--}}
					{{--</li>--}}
					{{--<li class="col-sm-3">--}}
						{{--<ul>--}}
							{{--<li class="dropdown-header">Header Title</li>--}}
							{{--<li><a href="javascript:void(0)">Link of page</a> </li>--}}
						{{--</ul>--}}
					{{--</li>--}}
					{{--<li class="col-sm-3">--}}
						{{--<ul>--}}
							{{--<li class="dropdown-header">Header Title</li>--}}
							{{--<li> <a href="javascript:void(0)">Link of page</a> </li>--}}
						{{--</ul>--}}
					{{--</li>--}}
				{{--</ul>--}}
			{{--</li>--}}
			<!-- /.Megamenu -->
		</ul>
		<!-- This is the message dropdown -->
		<ul class="nav navbar-top-links navbar-right pull-right">
			<!-- /.Task dropdown -->
			<!-- /.dropdown -->

			<li class="dropdown">
				<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                    @if(!empty(Auth::user()->avatar))
                        <img src="{{ asset(STATIC_DIR.'storage/'.Auth::user()->avatar) }}" alt="user" width="36" class="img-circle" />
                    @else
                        <img src="{{ asset(STATIC_DIR.'uploads/defualt.png') }}" alt="user" width="36" class="img-circle" />
                    @endif
					<b class="hidden-xs">{{ Auth::user()->first_name ?? '' }}</b><span class="caret"></span>
				</a>
				<ul class="dropdown-menu dropdown-user animated flipInY">
					<li>
						<div class="dw-user-box">
							<div class="u-img">
                                @if(!empty(Auth::user()->avatar))
                                    <img src="{{ asset(STATIC_DIR.'storage/'.Auth::user()->avatar) }}" alt="user"  />
                                @else
                                    <img src="{{ asset(STATIC_DIR.'uploads/defualt.png') }}" alt="user"  />
                                @endif
                            </div>
							<div class="u-text"><h4>{{ Auth::user()->first_name ?? '' }} {{ Auth::user()->last_name ?? '' }}</h4><p class="text-muted">{{ Auth::user()->email ?? '' }}</p>

							</div>
						</div>
					</li>
					<li role="separator" class="divider"></li>
					<li><a href="{{ route('profile')  }}"><i class="ti-user"></i> My Profile</a></li>
					<li role="separator" class="divider"></li>
					{{--<li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>--}}
					<li role="separator" class="divider"></li>
					<li class="pull-right" style="padding-right: 20px;">
						<form id="logout-form" action="{{ route('logout') }}" method="POST" >
							@csrf
							<button type="submit" class="btn btn-link btn-flat text-danger"><i class="fa fa-power-off"></i>&nbsp;Logout</button>
						</form>
					</li>
				</ul>
				<!-- /.dropdown-user -->
			</li>
			
			
			
			<!-- /.dropdown -->
		</ul>
	</div>
	<!-- /.navbar-header -->
	<!-- /.navbar-top-links -->
	<!-- /.navbar-static-side -->
</nav>
