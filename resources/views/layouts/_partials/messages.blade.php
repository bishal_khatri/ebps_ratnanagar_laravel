@if(Session::has('success'))
    <div class="row">
        <div class="col-md-12">
            <div id="message" class="updated notice notice-success is-dismissible alert alert-dismissable" style="width:100%;">
                <p>
                    <strong>
                        {{Session::get('success')}}
                    </strong>
                </p>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-top:-25px; margin-right:8px;padding-right:5px;">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
        </div>
    </div>
    <br>
@endif
