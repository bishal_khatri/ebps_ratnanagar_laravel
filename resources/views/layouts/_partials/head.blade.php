<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset(STATIC_DIR.'uploads/logo/logo.png') }}">
	<title>EBPS | @yield('page_title')</title>
	<!-- Bootstrap Core CSS -->
	<link href="{{ asset(STATIC_DIR.'assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    @yield('css-top')

    <link href="{{ asset(STATIC_DIR.'plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

    <!-- Menu CSS -->
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
	<!-- animation CSS -->
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
	<link href="{{ asset(STATIC_DIR.'assets/css/animate.css') }}" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="{{ asset(STATIC_DIR.'assets/css/style.css') }}" rel="stylesheet">
	<!-- color CSS -->
	<link href="{{ asset(STATIC_DIR.'assets/css/colors/megna-dark.css') }}" id="theme" rel="stylesheet">

	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/custom.css') }}">
	@yield('css')
	<style>
		.action{
			display: none;
		}
		.title:hover .action {
			display: block;
			margin-left: -12px;
		}
		.vl {
		    border-left: 1px solid green;
		    height: 2px;
		}
		.sidebar .label {
			margin-top: -2px;
		}
		.sidebar-label{
			color: white;
			font-weight: bold;
			letter-spacing: 8px;
			padding-bottom: 15px;
			padding-top: 15px;
		}
	</style>
</head>
