<!-- /#wrapper -->
<!-- jQuery -->
<script src="{{ asset(STATIC_DIR.'plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset(STATIC_DIR.'assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script src="{{ asset(STATIC_DIR.'plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<!-- Sidebar menu plugin JavaScript -->
<script src="{{ asset(STATIC_DIR.'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--Slimscroll JavaScript For custom scroll-->
<script src="{{ asset(STATIC_DIR.'assets/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset(STATIC_DIR.'assets/js/waves.js') }}"></script>
<!-- Custom Theme JavaScript -->
@yield('scripts')
<script src="{{ asset(STATIC_DIR.'assets/js/custom.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
