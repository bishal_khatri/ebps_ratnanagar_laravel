<div class="row print-head" style="display: none;margin-top: -50px;">
	<div class="col-md-3">
		<img src="{{ asset(STATIC_DIR.'uploads/logo/logo.png') }}" alt="" width="70">
		
	</div>
	<div class="col-md-6 text-center" style="margin-top: -90px;">
		<h2 style="margin-bottom: -15px;">रत्ननगर  नगरपालिका </h2>
		<h4 style="margin-bottom: -1px;">नगर कार्यपालिकाको कार्यालय</h4>
		<p style="margin-bottom: -15px;">रत्ननगर, चितवन, ३ नं प्रदेश नेपाल </p>
		<h4 style="margin-bottom: -15px;">भुकम्प सुरक्षा तथा घरनक्शा महाशाखा</h4>
	</div>
	<div class="col-md-3 text-right" style="margin-top: -80px; padding-bottom: 50px">
		<p>फोन नं : ५६०५२९,५६२४३६</p>
		<p>FAX : ०५६-५६०५२९</p>
	</div>
</div>
