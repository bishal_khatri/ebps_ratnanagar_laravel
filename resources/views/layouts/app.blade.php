<!DOCTYPE html>
<html lang="en">
@include('layouts._partials.head')
<style>
	body {
		color: #444;
		font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif !important;
		font-size: 13px;
		line-height: 1.4em;
	}
	.bg-title {
		background: #fff;
		overflow: hidden;
		padding: 0px 0px 0px;
		margin-bottom: 5px;
		margin-left: -25.5px;
		margin-right: -25.5px;
		margin-top: 5px;
	}
	 .notice-success, div.updated {
		 border-left-color: #46b450 !important;
	 }
	.notice, div.error, div.updated {
		background: #fff;
		border-left: 4px solid #fff;
		border-left-color: rgb(255, 255, 255);
		box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
		margin: 5px 0px 2px;
		padding: 0px 12px;
		padding-right: 12px;
	}
	.form-table td .notice p, .notice p, .notice-title, div.error p, div.updated p {
		margin: .3em 0;
		padding: 1px;
	}
</style>
<body class="fix-header">

<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div>

<div id="wrapper">

	@include('layouts._partials.nav')

	@include('layouts._partials.sidebar')

	<div id="page-wrapper">

		<div class="container-fluid">
			
			<div class="row bg-title hide-on-print">
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					<h4 class="page-title">@yield('content-title')</h4> </div>
				<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					@yield('right_button')
				</div>
				<!-- /.col-lg-12 -->
			</div>
			
			{{--<div class="row bg-title hide-on-print">--}}
				{{--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
					{{--<h4 class="page-title">@yield('content-title')</h4>--}}
				{{--</div>--}}
				{{--<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 pull-right">--}}
					{{--<div class="col-md-3 pull-right">--}}
                        {{--@yield('right_button')--}}
                    {{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
			@include('layouts._partials.messages')
			@yield('content')

		</div>
		<footer class="footer text-center hide-on-print"> 2018 &copy; Electronic Building Permit System designed by
			<a href="http://www.technosales.com.np" target="_blank">Technology Sales </a>
		</footer>
	</div>

</div>
@include('layouts._partials.scripts')
</body>

</html>
