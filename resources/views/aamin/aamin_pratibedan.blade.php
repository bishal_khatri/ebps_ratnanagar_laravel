@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/css/nepaliDatePicker.css') }}" >

@endsection
@section('page_title')
    सँधियार सूचना
@endsection
@section('right_button')
@stop
@section('content-title')
    <h2>
        अमिन प्रतिवेदन
    </h2>
@endsection

@section('content')
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    
    <form  method="post"  action="{{ route('aamin.store_aaminPratibedan') }}">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="white-box">
                    @csrf
                    <input type="hidden" name="form_id" value="{{ $formData->id }}">
                    <input type="hidden" name="action" value="add">
                    <input type="hidden" name="token" value="aamin">

                    <div class="row">
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field_owner_name" class="control-label">
                                    जग्गा धनी को नाम थर :
                                </label>
                                <input type="text" name="field_owner_name" readonly class="form-control" value="{{ !empty($formData->sambodhan) && !empty($formData->field_owner_name) ? $formData->sambodhan.' ' .$formData->field_owner_name : 'n/a'  }}">
                            
                            
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="building_type" class="control-label">
                                    निर्माण किसिम :
                                </label>
                                <input type="text" name="building_type" readonly class="form-control" value="{{ !empty($formData->building_type) ? $formData->building_type : 'n/a' }}">
                            
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="electricity_line" class="control-label">
                                    बिजुली को तार :
                                </label>
                                <input type="text" name="electricity_line" readonly class="form-control" value="{{ !empty($formData->landInfo->electricity_line) ? $formData->landInfo->electricity_line : 'n/a' }}">
                            
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="river" class="control-label">
                                    नदि किनारा :
                                </label>
                                <input type="text" name="river" readonly class="form-control" value="{{ !empty($formData->landInfo->river ) ? $formData->landInfo->river : 'n/a'}}">
                            
                            
                            </div>
                        </div>
                    
                    </div>
                    <!--/ first row-->
                    
                    <div class="row">
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bato_category" class="control-label">
                                    १.बाटोको वर्ग
                                </label>
                                <input type="text" name="bato_category" class="form-control" required placeholder="बाटोको वर्ग" value="{{ old('bato_category') }}">
                                
                                @if($errors->has('bato_category'))
                                    <span class="text-danger">{{ $errors->first('bato_category') }}</span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bato_length" class="control-label">
                                    २.बाटो को चौडाई
                                </label>
                                <input type="text" name="bato_length" placeholder="बाटो को चौडाई" required='' class="form-control" placeholder="बाटोको वर्ग" value="{{ old('bato_length') }}">
                                
                                @if($errors->has('bato_length'))
                                    <span class="text-danger">{{ $errors->first('bato_length') }}</span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bato_direction" class="control-label">
                                    ३.बाटो को दिशा
                                </label>
                                <input type="text" name="bato_direction" class="form-control" placeholder="बाटो को दिशा"  required="" value="{{ old('bato_direction') }}">
                                
                                @if($errors->has('bato_direction'))
                                    <span class="text-danger">{{ $errors->first('bato_direction') }}</span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bato_area" class="control-label">
                                    ४.जग्गा रहेको क्षेत्र / उपक्षेत्र
                                </label>
                                <input type="text" name="bato_area" class="form-control" required="" placeholder="जग्गा रहेको क्षेत्र / उपक्षेत्र" value="{{ old('bato_area') }}">
                                
                                @if($errors->has('bato_area'))
                                    <span class="text-danger">{{ $errors->first('bato_area') }}</span>
                                @endif
                            </div>
                        </div>
                    
                    </div>
                    <!--/ second row-->
                    
                    <p>५.जग्गा को मोहोडा / पिछाड</p>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mohoda_front" class="control-label">
                                    अगाडी
                                </label>
                                <input type="text" name="mohoda_front" class="form-control" required="" placeholder="अगाडी" value="{{ old('mohoda_front') }}">
                                
                                @if($errors->has('mohoda_front'))
                                    <span class="text-danger">{{ $errors->first('mohoda_front') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mohoda_back" class="control-label">
                                    पछाडी
                                </label>
                                <input type="text" name="mohoda_back" class="form-control" required="" placeholder="पछाडी" value="{{ old('mohoda_back') }}">
                                
                                @if($errors->has('mohoda_back'))
                                    <span class="text-danger">{{ $errors->first('mohoda_back') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mohoda_left" class="control-label">
                                    दायाँ
                                </label>
                                <input type="text" name="mohoda_left" class="form-control" required="" placeholder="दायाँ" value="{{ old('mohoda_left') }}">
                                
                                @if($errors->has('mohoda_left'))
                                    <span class="text-danger">{{ $errors->first('mohoda_left') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="mohoda_right" class="control-label">
                                    बायाँ
                                </label>
                                <input type="text" name="mohoda_right" class="form-control" required="" placeholder="बायाँ" value="{{ old('mohoda_right') }}">
                                
                                @if($errors->has('mohoda_right'))
                                    <span class="text-danger">{{ $errors->first('mohoda_right') }}</span>
                                @endif
                            </div>
                        
                        </div>
                    
                    </div>
                    {{--third row--}}
                    
                    <p>६.बाटोको केन्द्रबिन्दुबाट छाड्नु पर्ने दुरी</p>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_duri_purba" class="control-label">
                                    पूर्व
                                </label>
                                <input type="text" name="chadnu_parne_duri_purba" placeholder="बाटोको केन्द्रबिन्दुबाट छाड्नु पर्ने दुरी" class="form-control" required=""  value="{{ old('chadnu_parne_duri_purba') }}">
                                
                                @if($errors->has('chadnu_parne_duri_purba'))
                                    <span class="text-danger">{{ $errors->first('chadnu_parne_duri_purba') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_duri_paschim" class="control-label">
                                    पश्चिम
                                </label>
                                <input type="text" name="chadnu_parne_duri_paschim" placeholder="पश्चिम"  class="form-control" required=""  value="{{ old('chadnu_parne_duri_paschim') }}">
                                
                                @if($errors->has('chadnu_parne_duri_paschim'))
                                    <span class="text-danger">{{ $errors->first('chadnu_parne_duri_paschim') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_duri_utar" class="control-label">
                                    उत्तर
                                </label>
                                <input type="text" name="chadnu_parne_duri_utar" placeholder="उत्तर" class="form-control" required=""  value="{{ old('chadnu_parne_duri_utar') }}">
                                
                                @if($errors->has('chadnu_parne_duri_utar'))
                                    <span class="text-danger">{{ $errors->first('chadnu_parne_duri_utar') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_duri_dakshin" class="control-label">
                                    दक्षिण
                                </label>
                                <input type="text" name="chadnu_parne_duri_dakshin" placeholder="दक्षिण"  class="form-control" required=""  value="{{ old('chadnu_parne_duri_dakshin') }}">
                                
                                @if($errors->has('chadnu_parne_duri_dakshin'))
                                    <span class="text-danger">{{ $errors->first('chadnu_parne_duri_dakshin') }}</span>
                                @endif
                            </div>
                        
                        </div>
                    
                    
                    </div>
                    {{--fourth row--}}
                    
                    <p>७.छाड्नुपर्ने न्यूनतम् सेट ब्याक</p>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_setback_purba" class="control-label">
                                    पूर्व
                                </label>
                                <input type="text" name="chadnu_parne_setback_purba" placeholder="छाड्नुपर्ने न्यूनतम् सेट ब्याक" class="form-control" required=""  value="{{ old('chadnu_parne_setback_purba') }}">
                                
                                @if($errors->has('chadnu_parne_setback_purba'))
                                    <span class="text-danger">{{ $errors->first('chadnu_parne_setback_purba') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_setback_paschim" class="control-label">
                                    पश्चिम
                                </label>
                                <input type="text" name="chadnu_parne_setback_paschim" placeholder="पश्चिम" class="form-control" required=""  value="{{ old('chadnu_parne_setback_paschim') }}">
                                
                                @if($errors->has('chadnu_parne_setback_paschim'))
                                    <span class="text-danger">{{ $errors->first('chadnu_parne_setback_paschim') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_setback_utar" class="control-label">
                                    उत्तर
                                </label>
                                <input type="text" name="chadnu_parne_setback_utar" placeholder="उत्तर" class="form-control" required=""  value="{{ old('chadnu_parne_setback_utar') }}">
                                
                                @if($errors->has('chadnu_parne_setback_utar'))
                                    <span class="text-danger">{{ $errors->first('chadnu_parne_setback_utar') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chadnu_parne_setback_dakshin" class="control-label">
                                    दक्षिण
                                </label>
                                <input type="text" name="chadnu_parne_setback_dakshin" placeholder="दक्षिण"  class="form-control" required=""  value="{{ old('chadnu_parne_setback_dakshin') }}">
                                
                                @if($errors->has('chadnu_parne_setback_dakshin'))
                                    <span class="text-danger">{{ $errors->first('chadnu_parne_setback_dakshin') }}</span>
                                @endif
                            </div>
                        
                        </div>
                    
                    </div>
                    {{--fifth row--}}
                    
                    <p>८.नापी नक्शा अनुसार जग्गाको</p>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="napi_length" class="control-label">
                                    लम्बाई
                                </label>
                                <input type="text" name="napi_length" placeholder="लम्बाई" class="form-control" required=""  value="{{ old('napi_length') }}">
                                
                                @if($errors->has('napi_length'))
                                    <span class="text-danger">{{ $errors->first('napi_length') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="napi_width" class="control-label">
                                    चौडाई
                                </label>
                                <input type="text" name="napi_width" placeholder="क्षेत्रफ़ल्"  class="form-control" required=""  value="{{ old('napi_width') }}">
                                
                                @if($errors->has('napi_width'))
                                    <span class="text-danger">{{ $errors->first('napi_width') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="napi_field_area" class="control-label">
                                    क्षेत्रफ़ल्
                                </label>
                                <input type="text" name="napi_field_area" placeholder="चौडाई"  class="form-control" required=""  value="{{ old('napi_field_area') }}">
                                
                                @if($errors->has('napi_field_area'))
                                    <span class="text-danger">{{ $errors->first('napi_field_area') }}</span>
                                @endif
                            </div>
                        </div>
                    
                    </div>
                    {{--sixth row--}}

                    {{--this field is not necessary --}}
                    {{--<p>९.साईटको नापी अनुसार जग्गाको</p>--}}
                   {{-- <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="siteko_napi_length" class="control-label">
                                    लम्बाई
                                </label>
                                <input type="text" name="siteko_napi_length" placeholder="लम्बाई" class="form-control" required=""  value="{{ old('siteko_napi_length') }}">
                                
                                @if($errors->has('siteko_napi_length'))
                                    <span class="text-danger">{{ $errors->first('siteko_napi_length') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="siteko_napi_width" class="control-label">
                                    चौडाई
                                </label>
                                <input type="text" name="siteko_napi_width" placeholder="चौडाई"  class="form-control" required=""  value="{{ old('siteko_napi_width') }}">
                                
                                @if($errors->has('siteko_napi_width'))
                                    <span class="text-danger">{{ $errors->first('siteko_napi_width') }}</span>
                                @endif
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="siteko_napi_field_area" class="control-label">
                                    क्षेत्रफ़ल्
                                </label>
                                <input type="text" name="siteko_napi_field_area" placeholder="क्षेत्रफ़ल्"  class="form-control" required=""  value="{{ old('siteko_napi_field_area') }}">
                                
                                @if($errors->has('siteko_napi_field_area'))
                                    <span class="text-danger">{{ $errors->first('siteko_napi_field_area') }}</span>
                                @endif
                            </div>
                        
                        </div>
                    
                    </div>--}}
                    {{--seventh row--}}
                    
                    
                    <div class="row">
                        @if($formData->landInfo->electricity_line == "भएको")
                            <div class="col-md-3">
                                <div class="form-group ">
                                    <label for="hitension_wire_distance" class="control-label">
                                        हाईटेन्सन को तार बाटछाड्नु पर्ने दुरी
                                    </label>
                                    <input type="text" name="hitension_wire_distance"  required='' class="form-control" placeholder="हाईटेन्सन को तार बाटछाड्नु पर्ने दुरी " value="{{ old('hitension_wire_distance') }}">
                                    
                                    @if($errors->has('hitension_wire_distance'))
                                        <span class="text-danger">{{ $errors->first('hitension_wire_distance') }}</span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group ">
                                    <label for="electricity_volt" class="control-label">
                                        भोल्ट
                                    </label>
                                    <input type="text" name="electricity_volt"  required='' class="form-control" placeholder="भोल्ट " value="{{ old('electricity_volt') }}">
                                    
                                    @if($errors->has('electricity_volt'))
                                        <span class="text-danger">{{ $errors->first('electricity_volt') }}</span>
                                    @endif
                                </div>
                            </div>
                        @endif
                        
                        @if($formData->landInfo->river == "भएको")
                            <div class="col-md-3">
                                <div class="form-group ">
                                    <label for="river_side_bata_chodnu_parne_duri" class="control-label">
                                        नदि भएको किनारा बाट छाड्नु पर्ने दुरी
                                    </label>
                                    <input type="text" name="river_side_bata_chodnu_parne_duri"  required='' class="bod-picker form-control" placeholder="नदि भएको किनारा बाट छाड्नु पर्ने दुरी" value="{{ old('river_side_bata_chodnu_parne_duri') }}">
                                    
                                    @if($errors->has('river_side_bata_chodnu_parne_duri'))
                                        <span class="text-danger">{{ $errors->first('river_side_bata_chodnu_parne_duri') }}</span>
                                    @endif
                                </div>
                            </div>
                        @endif
                    
                    </div>
                    {{--eight row--}}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="created_at_nepali" class="control-label">
                                    मिति &nbsp;
                                </label>
                                <input type="text" name="created_at_nepali"  required='' class="bod-picker form-control" placeholder="मिति चयन गर्नुहोस " value="{{ old('created_at_nepali') }}">
                                
                                @if($errors->has('created_at_nepali'))
                                    <span class="text-danger">{{ $errors->first('created_at_nepali') }}</span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            
                            <div class="form-group ">
                                <label for="other" class="control-label">
                                    अन्य &nbsp;
                                </label>
                                <input type="text" name="other"  class=" form-control"  value="{{ old('other') }}">
                                
                                @if($errors->has('other'))
                                    <span class="text-danger">{{ $errors->first('other') }}</span>
                                @endif
                            
                            </div>
                        </div>
                    </div>
                    {{--end of nine row --}}
                    
                    {{--ten row--}}
                    <div class="row">
                        <div class="col-md-3">
                            <button class="btn btn-block btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                {{--nine row--}}
            </div>
        </div>
        </div>
    </form>

@endsection

@section('scripts')
    <!-- this should go after your </body> -->
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'assets/js/nepaliDatePicker.js') }}" ></script>

    <script>
        $(".bod-picker").nepaliDatePicker({
            dateFormat: "%D, %M %d, %y",
            closeOnDateSelect: true
        });


        $(document).ready(function() {
            $('.dropify').dropify();

        });
    
    
    </script>

@endsection
