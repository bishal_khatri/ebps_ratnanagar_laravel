<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">अस्वीकृत गर्नुको कारण</h4> </div>
            <div class="modal-body">
                <p class="text-danger">{{ isset($value->rejected_message) ? $value->rejected_message : 'n/a' }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{--modal for uploading aamin pratibedan signature file.--}}
<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('aamin.upload_aaminPratibedan') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$action ?? '' }}" name="action">
                <input type="hidden" name="form_id" id="form_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >अमिन  प्रतिवेदन  फाईल अपलोड</h4> </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="dpc_scan" class="control-label">
                            अमिन  प्रतिवेदन
                        </label>
                        <input type="file" name="aamin_pratibedan_scan" id="input-file-now" class="dropify"  accept=".jpeg,.jpg,.png"/>
                        @if($errors->has('aamin_pratibedan_scan'))
                            <span class="text-danger">{{ $errors->first('aamin_pratibedan_scan') }}</span>
                        @endif
                    </div>
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>

