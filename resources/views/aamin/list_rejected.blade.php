@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
@endsection
@section('page_title')
    अस्वीकृत फाईल
@endsection
@section('right_button')
@stop
@section('content-title')
    <h4>अमिन प्रतिवेदनको अस्वीकृत फाईल</h4>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="table-responsive">
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                    <tr>
                                        <th width="150" class="text-center">S.N.</th>
                                        <th width="150" class="text-center">दर्ता नं.</th>
                                        <th>जग्गा धनीको नाम थर</th>
                                        <th>निर्माण किसिम :</th>
                                        <th>बिजुली को तार :</th>
                                        <th>नदि किनारा :</th>
                                        <th>दर्ता मिति</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if (isset($formData))
                                        @foreach($formData as $value)
                                            <tr>
                                                <td class="text-center">{{ $loop->iteration }}</td>
                                                <td class="text-center">{{ $value->darta_number }}</td>
                                                <td>{{ $value->sambodhan ?? '' }} {{ $value->field_owner_name ?? '' }}</td>
                                                <td>{{ $value->building_type ?? '' }}</td>
                                                <td>{{ $value->landInfo->electricity_line ?? '' }}</td>
                                                <td>{{ $value->landInfo->river ?? '' }}</td>
                                                <td>{{ $value->created_at_nepali ?? '' }}</td>
                                                <td>
                                                    <a class="btn btn-danger btn-outline m-r-5" alt="default"  data-toggle="modal" data-target="#myModal" >Show Error</a>

                                                    <a href="{{ route('aamin.edit_aaminPratibedan',$value->id) }}" type="button" class="btn btn-info btn-outline m-r-5">Proceed</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('aamin.modal')
    @include('permission.model')
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            //Bootstrap-TouchSpin

        });



    </script>
@endsection
