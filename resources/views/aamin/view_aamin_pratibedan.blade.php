@extends('layouts.app')
@section('css')
    {{--<link href="../plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />--}}
    
    <link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/css/animated-masonry-gallery.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.css') }}" />
@endsection
@section('page_title')
    अमिन  प्रतिवेदन
@endsection
@section('right_button')
@stop
@section('content-title')
    <h2>
        अमिन  प्रतिवेदन
    </h2>
@endsection

@section('content')
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h3 class="box-title text-center text-danger">
                            Proceeded to Sub-Engineer
                        </h3>
                        <div id="gallery" class="text-center">
                            <a href="{{ asset(STATIC_DIR.'storage/'.$formData->aamin_pratibedan_scan) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="अमिन प्रतिवेदन">
                                <img src="{{ asset(STATIC_DIR.'storage/'.$formData->aamin_pratibedan_scan) }}" class="all studio" alt="gallery" width="500" />
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/animated-masonry-gallery.js') }}"></script>
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/jquery.isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.js') }}"></script>
    {{--<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>--}}
    {{--<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>--}}
    <script>
        $(document).ready(function($) {
            // delegate calls to data-toggle="lightbox"
            $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                event.preventDefault();
                return $(this).ekkoLightbox({
                    onShown: function() {
                        if (window.console) {
                            return console.log('Checking our the events huh?');
                        }
                    },
                    onNavigate: function(direction, itemIndex) {
                        if (window.console) {
                            return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                        }
                    }
                });
            });
        });
    </script>
@endsection
