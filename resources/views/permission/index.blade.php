@extends('layouts.app')
@section('css')
    @endsection
@section('page_title')
    	List permissions
    @endsection
@section('right_button')
        <a href="#add" class="btn btnblock btninfo" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Add</a>
    @stop
@section('contenttitle')
    	<h2>
        		List permissions
        	</h2>
    
    @endsection

@section('content')
        <div class="preloader">
                <svg class="circular" viewBox="25 25 50 50">
                        <circle class="path" cx="50" cy="50" r="20" fill="none" strokewidth="2" strokemiterlimit="10" />
                    </svg>
            </div>
        <div class="row">
                <div class="colmd12">
                        <div class="whitebox">
                                <div class="tableresponsive">
                                        <table id="datatable" class="table tableborderless tablestriped" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                        <th>#</th>
                                                        <th>Display Name</th>
                                                        <th>Code Name</th>
                                                        <th style="width: 90px;textalign: center;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($permissions))

                                                        @foreach($permissions as $val)
                                                                <tr>
                                                                        <td>{{ $loop->iteration }}</td>
                                                                        <td>{{ $val->display_name }}</td>
                                                                        <td>{{ $val->code_name }}</td>
                                                                        <td style="textalign: center">
                                                                                <a href=""><i class="fa fapencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <a href="#confirmDelete" dataids="{{ $val->id }}" datatoggle="modal" datarel="delete" datauser="{{ $val->display_name }}">
                                                                                        <i class="fa fatrash textdanger"></i>
                                                                                    </a>
                                                                            </td>
                                                                    </tr>
                                                            @endforeach
                                                    @endif
                                            </table>
                                    </div>
                            </div>
                    </div>
            </div>
    
    @include('permission.model')
    @endsection

@section('scripts')
    	<script>
                $('#datatable').DataTable({
                        dom: 'Bfrtip',
                        columnDefs: [ { "targets": [3], "searchable": false, "orderable": false, "visible": true } ],
                    });
        
                    $('#add').find('.modalfooter #confirm_yes').on('click', function () {
                            $("#confirmDelete").modal("hide");
                            window.location.reload();
                
                            });
        
                    // AJAX PERMISSION USER
                        $('#confirmDelete').on('show.bs.modal', function (e) {
                                //e.preventDefault();
                                    var button = $(e.relatedTarget);
                                // console.log(button.data('ids'));
                                    var ids = button.data('ids');
                                var user = button.data('user');
                                // Pass form reference to modal for submission on yes/ok
                                    var form = $(e.relatedTarget).closest('form');
                                $(this).find('.modalfooter #confirm').data('form', form);
                                $("#hidden_id").val(ids);
                                $(".hidden_title").html(' "' + user + '" ');
                            });
        
                    $('#confirmDelete').find('.modalfooter #confirm_yes').on('click', function () {
                            //$(this).data('form').submit();
                                var id = $("#hidden_id").val();
                            // console.log(id);
                                $.ajax({
                                        type: "POST",
                                        url: "{{ route('permission.delete') }}",
                                        headers: {
                                            'XCSRFToken': $('meta[name="csrftoken"]').attr('content')
                                        },

                                        data: "id=" + id,
                                            success: function (msg) {
                                            // console.log(msg);
                                            $("#confirmDelete").modal("hide");
                                            window.location.reload();
                                    }
                            });
                        });
                // AJAX DELETE PERMISSION END
            	</script>
    @endsection
