@extends('layouts.app')
@section('page_title')
	फाईल स्तिथि
@endsection
@section('content-title')
	फाईल स्तिथि
@endsection
@section('css')
	<style>
		.clear {
			position: absolute;
			border: none;
			display: block;
			top: 2px;
			bottom: 2px;
			right: -50px;
		}
	</style>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<form action="{{ route('file.get_file_status') }}" method="get">
					@csrf
					<div class="input-group m-t-10 col-md-4">
						<input type="text" id="search" value="{{ $data->darta_number ?? '' }}" name="darta_number" class="form-control" placeholder="दर्ता नं. राख्नुहोस">
						<button type="button" class="clear" onclick="">clear</button>
						<span class="input-group-btn">
                            <button type="submit" class="btn waves-effect waves-light btn-info">Search</button>
                        </span>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Personal Information</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						@if ($data===false)
							<p>दर्ता नं. राख्नुहोस</p>
						@elseif(is_null($data))
							<p>Data not found</p>
						@else
							<p><strong>दर्ता नं. :</strong> {{ $data->darta_number ?? '' }}</p>
							<p><strong>जग्गा धनी को नाम थर :</strong> {{ $data->sambodhan ?? '' }} {{ $data->field_owner_name ?? '' }}</p>
							<p><strong>जग्गा धनीको ठेगाना :</strong> {{ $data->field_owner_address ?? '' }}</p>
							<p><strong>बर्ष :</strong> {{ $data->field_owner_age ?? '' }}</p>
							<p>
								<strong>Proceeded To : </strong>
								<span class="badge text-uppercase @if($data->is_rejected==1) badge-danger @else badge-success @endif">{{ $data->proceeded_to ?? '' }}</span>
							</p>
						@endif
					
					</div>
					<div class="panel-footer"> </div>
				</div>
			</div>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Form Status</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						@if (!is_null($data))
							@if (isset($data->form_level))
								<ul class="common-list">
									<div class="row">
										<div class="col-md-6">
											<li>
												<a href="javascript:void(0)">
													1. Create Form [ Consultancy }
													@if($data->form_level >= 1)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											<li>
												<a href="javascript:void(0)">
													2. Check [ Engineer }
													@if($data->form_level >= 2)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
													
													@if ($data->is_rejected==1 AND isset($data->last_reject_log) AND $data->form_level < 2)
														<span class="btn btn-outline btn-warning btn-sm" style="margin-left: 20px;">Rejected</span>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													3. 15 Days Notice
													@if($data->form_level > 2 OR $data->days_left == '0')
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
													
													@if ($data->form_level == 2 AND !empty($data->days_left))
														<span class="btn btn-outline btn-warning btn-sm" style="margin-left: 20px;">{{ $data->days_left }} Days left</span>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													4. Fill Amin Pretibaden [ Amin ]
													@if($data->form_level >= 3)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
													
													@if ($data->is_rejected==1 AND isset($data->last_reject_log) AND $data->form_level < 3)
														<span class="btn btn-outline btn-warning btn-sm" style="margin-left: 20px;">Rejected</span>
													@endif
												</a>
											</li>
											<li>
												<a href="javascript:void(0)">
													5. Check Amin Pretibaden & calculate Rajashow [ Sub Engineer ]
													@if($data->form_level >= 4)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													6. Upload Rajashow [ Sub Engineer ]
													@if($data->form_level >= 5)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													7. Print and upload टिप्पणी आदेश & अस्थायी सूचना [ Engineer ]
													@if($data->form_level >= 6)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											<li>
												<a href="javascript:void(0)">
													8. Fill DPC Form [ Consultancy ]
													@if($data->form_level >= 7)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											<li>
												<a href="javascript:void(0)">
													9. Check [ Sub Engineer ]
													@if($data->form_level >= 8)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
													@if ($data->is_rejected==1 AND isset($data->last_reject_log) AND $data->form_level < 7)
														<span class="btn btn-outline btn-warning btn-sm" style="margin-left: 20px;">Rejected</span>
													@endif
												</a>
											</li>
										</div>
										<div class="col-md-6">
											<li>
												<a href="javascript:void(0)">
													10. Upload टिप्पणी र आदेश [ Engineer ]
													@if($data->form_level >= 9)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													11. Check [ Main Engineer ]
													@if($data->form_level >= 10)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													12. Upload Superstructure [ Sub Engineer ]
													@if($data->form_level >= 11)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													13. Fill & Upload Sampanna Form [ Consultancy ]
													@if($data->form_level >= 12)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													14. Check [ Sub Engineer ]
													@if($data->form_level >= 13)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
													@if ($data->is_rejected==1 AND isset($data->last_reject_log) AND $data->form_level < 12)
														<span class="btn btn-outline btn-warning btn-sm" style="margin-left: 20px;">Rejected</span>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													15. Check [ Engineer ]
													@if($data->form_level >= 14)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													16. Check [ Main Engineer ]
													@if($data->form_level >= 15)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													17. Upload Sampanna [ Sub Engineer ]
													@if($data->form_level >= 16)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
											
											<li>
												<a href="javascript:void(0)">
													18. Complete
													@if($data->form_level == 16)
														<i class="fa fa-check text-success fa-fw"></i>
													@else
														<i class="fa fa-close text-danger fa-fw"></i>
													@endif
												</a>
											</li>
										</div>
									</div>
									
									
									
								</ul>
							@else
								<p>दर्ता नं. राख्नुहोस</p>
							@endif
						@else
							<p>Data Not found</p>
						@endif
					</div>
					<div class="panel-footer"> </div>
				</div>
			</div>
		</div>
		
	</div>
@endsection
@section('scripts')
	<script>
        $(document).ready(function(){
            var search_value = $("#search").val();
            if (search_value.length===0){
                $(".clear").hide();
            }

            $("#search").on('keyup', function(){
                $(".clear").show();
            })
        });
        $(".clear").on("click", function(){
            $("#search").val('')
            window.location.replace("{{ route('file.get_file_status') }}")
        })
	</script>
@endsection
