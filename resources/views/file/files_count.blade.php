@extends('layouts.app')
@section('css')
@endsection
@section('page_title', 'Files count')

@section('right_button')
@stop
@section('content-title')Files Count per Ward. @endsection
@section('content')

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Total Files :
                    <span class="btn btn-default btn-rounded "> {{ $total_files }}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row" >
        @foreach($files as $count)
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">

            <a href="{{route('file.get_files_by_ward', $count['current_ward_number1'])}}">
                <div class="panel panel-default">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body text-center">
                            <h3>Ward Number: {{$count['current_ward_number']}} </h3>
                            <h5>Total Files:</h5>
                            <span class="btn btn-default btn-rounded">{{ $count['total'] }} </span><br>
                        </div>
                    </div>
                </div>
        </a>
            </div>

        @endforeach
    </div>


@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    {{-- <script>
        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            //Bootstrap-TouchSpin

        });
    </script> --}}
@endsection
