@extends('layouts.app')

@section('page_title')
	Register user
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">

@endsection

@section('content-title')
    {{--<h1>--}}
    {{--Register user--}}
    {{--</h1>--}}
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('user.index')}}">List user</a></li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> Register User</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form method="POST" action="{{ route('user.register') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <h3 class="box-title"><p style="color:red;">Personal Info</p></h3>
                                <hr>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name" class="control-label">{{ __('First Name') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{ __('Last Name') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{ __('Contact') }}</label>
                                        <input id="contact" type="text" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" name="contact" value="{{ old('contact') }}" required autofocus>

                                        @if ($errors->has('contact'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{ __('Address') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required autofocus>

                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="email" class="control-label">{{ __('E-Mail Address') }}</label>

                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>

                            <div  class="row">

                                <div class="col-md-6">
                                    <label for="password" class="control-label">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>

                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-md-5">
                                        <label for="avatar" class="control-label">
                                           {{ __('Choose a Profile Picture:') }}
                                        </label>
                                        <input type="file" name="avatar" id="input-file-now" class="dropify"  accept=".jpg,.png,.bmp,.jpeg" />
                                        @if($errors->has('avatar'))
                                            <span class="text-danger">{{ $errors->first('avatar') }}</span>
                                        @endif
                                </div>
                            </div>

                            <hr>
                            <h3 class="box-title"> <p style="color:red;">Assign Permission:</p></h3>
                            <div class="row">
                                {{--<form action="{{ route('user.assign_permission_store') }}" method="post">--}}
                                {{--<input type="hidden" name="user_id" value="{{ $user->id }}">--}}
                                <div class="form-group" >
                                    <select  class="form-control required" id="assign" name="selected_permission"  >
                                        <option disabled="" selected> Select Any One Permission </option>
                                        @foreach($permissions as $prem)
                                            <option value="{{ $prem->id }}" >{{ $prem->display_name }}</option>
                                        @endforeach
                                    </select>

                                    @if($errors->has('selected_permission'))
                                        <span class="text-danger">
                                            <strong>
                                                {{ $errors->first('selected_permission') }}
                                            </strong>
                                        </span>

                                    @endif

                                </div>
                            </div>
                            <hr>




                            <div id="consultancy_info" style="display:none;">
                                <h3 class="box-title"><p style="color:red;"> Consultancy Details:</p></h3>
                                <hr>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name" class="control-label">{{ __('Consultancy Name') }}</label>
                                            <input  type="text" class="form-control{{ $errors->has('consultancy_name') ? ' is-invalid' : '' }}" name="consultancy_name" value="{{ old('consultancy_name') }}"  autofocus>

                                            @if ($errors->has('consultancy_name'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('consultancy_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">{{ __('Consultancy Address') }}</label>
                                            <input  type="text" class="form-control{{ $errors->has('consultancy_address') ? ' is-invalid' : '' }}" name="consultancy_address" value="{{ old('consultancy_address') }}"  autofocus>

                                            @if ($errors->has('consultancy_address'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('consultancy_address') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">{{ __('Consultancy License') }}</label>
                                            <input  type="text" class="form-control{{ $errors->has('consultancy_license') ? ' is-invalid' : '' }}" name="consultancy_license" value="{{ old('consultancy_license') }}"  autofocus>

                                            @if ($errors->has('consultancy_license'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('consultancy_license') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name" class="control-label">{{ __('Consultancy License Number') }}</label>
                                            <input  type="text" class="form-control{{ $errors->has('consultancy_license_number') ? ' is-invalid' : '' }}" name="consultancy_license_number" value="{{ old('consultancy_license_number') }}"  autofocus>

                                            @if ($errors->has('consultancy_license_number'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('consultancy_license_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">{{ __('Staff Id') }}</label>
                                            <input  type="text" class="form-control{{ $errors->has('staff_id') ? ' is-invalid' : '' }}" name="staff_id" value="{{ old('staff_id') }}"  autofocus>

                                            @if ($errors->has('staff_id'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('staff_id') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">{{ __('Extension Number') }}</label>
                                            <input  type="text" class="form-control{{ $errors->has('extension_number') ? ' is-invalid' : '' }}" name="extension_number" value="{{ old('extension_number') }}"  autofocus>

                                            @if ($errors->has('extension_number'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('extension_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>

                        </div>
                        {{--end form body--}}

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });


        $("#assign").on ('change',  function( e ) {
            var data = $(this).val();

            if(data == 1){
               $("#consultancy_info").show();
            } else {
                $("#consultancy_info").hide();
            }
        });
    </script>
@endsection
