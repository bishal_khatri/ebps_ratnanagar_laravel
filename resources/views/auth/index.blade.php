@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	List user
@endsection
@section('right_button')
    <a href="{{ route('user.register') }}" class="btn btn-block btn-info" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Add</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Address</th>
                            <th>Last login</th>
                            <th style="width: 100px;" class="text-center">Permission</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($users))
                            @foreach($users as $val)
                                <tr class="title">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $val->first_name }} {{ $val->last_name }} <br>
                                        <div class="action">
                                            @can('administration')
                                            <a href="{{ route('user.edituser', $val->id) }}" class="btn btn-link btn-sm">Edit</a>
                                            <span class="vl"></span>
                                            @endcan
                                            @can('administration')
                                            <a class="btn btn-link btn-sm" href="#confirmDelete" data-ids="{{ $val->id }}" data-toggle="modal" data-rel="delete" data-user="{{ $val->first_name }}" style="color:red">Delete</a>
                                            @endcan
                                        </div>
                                    </td>
                                    <td>{{ $val->email }}</td>
                                    <td>{{ $val->contact }}</td>
                                    <td>{{ $val->address }}</td>
                                    <td>@if(is_null($val->last_login_at)) n/a @else {{ $val->last_login_at->diffForHumans() }} @endif </td>
                                    <td class="text-center">
                                        <a href="#viewPermission" class="btn btn-link btn-sm" data-ids="{{ $val->id }}" data-toggle="modal">View</a>
                                        <span class="vl"></span>
                                        @can('administration')
                                        <a href="{{ route('user.assign_permission',$val->id) }}" class="btn btn-link btn-sm">Set</a>
                                        @endcan
                                    </td>
                                </tr>
                        @endforeach
                        @endif
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@include('auth.model')
@endsection

@section('scripts')
	<script>
        $('#datatable').DataTable({
            dom: 'Bfrtip',
            columnDefs: [ { "targets": [6], "searchable": false, "orderable": false, "visible": true } ],
        });

        $('#viewPermission').on('show.bs.modal', function(e){
            $('#list').html('');
            var button = $(e.relatedTarget);
            var id = button.data('ids');

            $.ajax({
                type: "POST",
                url: "{{ route('user.permission_view') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    setTimeout( function () {
                    $('#list').html(msg);
                    },500);
                }
            });
               $('#list').html('<img width="90" style="margin-left:240px;margin-top:20px;" src="{{ asset(STATIC_DIR.'assets/icons/loading.gif') }}"><p style="margin-left:265px;">Fetching....</p>');
        });

        // AJAX DELETE USER
        $('#confirmDelete').on('show.bs.modal', function (e) {
            //e.preventDefault();

            var button = $(e.relatedTarget);
            // console.log(button.data('ids'));
            var ids = button.data('ids');
            var user = button.data('user');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });

        $('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
            //$(this).data('form').submit();
            var id = $("#hidden_id").val();
            // console.log(id);
            $.ajax({
                type: "POST",
                url: "{{ route('user.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
        // AJAX DELETE USER END
	</script>
@endsection
