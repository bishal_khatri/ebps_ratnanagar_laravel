<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>EBPS | Password Reset</title>
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset(STATIC_DIR.'uploads/logo/logo.png') }}">
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets-old/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets-old/bower_components/font-awesome/css/font-awesome.min.css') }}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets-old/bower_components/Ionicons/css/ionicons.min.css') }}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets-old/dist/css/AdminLTE.min.css') }}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets-old/plugins/iCheck/square/blue.css') }}">
{{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
		.invalid-feedback{
			color: red;
		}
	</style>
	
	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href=""><b>EBPS</b>LOGIN</a>
	</div>
	<!-- /.login-logo -->
	<div class="login-box-body">
		<p class="login-box-msg">Enter your email address</p>
		<form method="POST" action="{{ route('password.email') }}">
			@csrf
			@if (Session::has('status'))
				<div class="form-group has-feedback">
					<p class="alert alert-success">{{ Session::get('status') }}</p>
				</div>
			@endif
			<div class="form-group has-feedback">
				<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
				@endif
			</div>
			<div class="row">
				<div class="col-xs-5">
					<a class="btn btn-primary btn-block btn-flat" href="{{ route('login') }}"><i class="fa fa-arrow-left"></i>&nbsp;Go Back</a>
				</div>
				<!-- /.col -->
				<div class="col-xs-7 pull-right">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Send Password Reset Link</button>
				</div>
				<!-- /.col -->
			</div>
		</form>
	</div>
	<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{ asset(STATIC_DIR.'assets-old/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset(STATIC_DIR.'assets-old/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset(STATIC_DIR.'assets-old/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>
</body>
</html>
