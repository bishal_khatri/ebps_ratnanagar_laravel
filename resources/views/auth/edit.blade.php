@extends('layouts.app')

@section('page_title')
    Edit user
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('content-title')
    {{--<h1>--}}
        {{--Edit user--}}
    {{--</h1>--}}
    <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('user.index')}}">User List</a></li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> Edit User</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form method="POST" action="{{ route('user.register') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="action" value="edit">
                        <input type="hidden" name="id" value="{{ $formData->id }}">

                    <div class="form-body">
                        <h3 class="box-title"><p style="color:red;">Personal Info</p></h3>
                        <hr>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="control-label">{{ __('First Name') }}</label>
                                    <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') ?? $formData->first_name }}" required autofocus>

                                @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{ __('Last Name') }}</label>
                                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') ?? $formData->last_name }}" required autofocus>

                                @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{ __('Contact') }}</label>
                                    <input id="contact" type="text" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" name="contact" value="{{ old('contact') ?? $formData->contact }}" required autofocus>

                                @if ($errors->has('contact'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{ __('Address') }}</label>
                                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') ?? $formData->address }}" required autofocus>


                                @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="email" class="control-label">{{ __('E-Mail Address') }}</label>--}}
                                    {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') ?? $formData->email }}" required>--}}
                                    {{--@if ($errors->has('email'))--}}
                                        {{--<span class="invalid-feedback" role="alert">--}}
                                            {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                        {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div>

                        {{--<h3 class="box-title"><p style="color:red;">Change Password Info <sup style="color:blue;">(optional)</sup> </p></h3>--}}
                        {{--<hr>--}}
                        {{--<div  class="row">--}}

                            {{--<div class="col-md-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="password" class="control-label">{{ __('Old Password') }}</label>--}}
                                    {{--<input type="password"--}}
                                           {{--class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}"--}}
                                           {{--name="old_password" >--}}

                                    {{--@if ($errors->has('old_password'))--}}
                                        {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('old_password') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="password" class="control-label">{{ __('New Password') }}</label>--}}
                                    {{--<input id="password" type="password"--}}
                                           {{--class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"--}}
                                           {{--name="password" >--}}

                                    {{--@if ($errors->has('password'))--}}
                                        {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}

                            {{--</div>--}}

                            {{--<div class="col-md-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="password-confirm" class="control-label">{{ __('Confirm New Password') }}</label>--}}
                                    {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" >--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        <hr>
                        <div class="row">
                            <div class="col-md-5">
                                <label for="avatar" class="control-label">
                                    {{ __('Choose a Profile Picture:') }}
                                </label>
                                <input type="file" name="avatar" id="input-file-now" class="dropify"  @if(!empty($formData->avatar)) data-default-file="{{  asset(STATIC_DIR.'storage/'.$formData->avatar ) }}" @endif    accept=".jpg,.png,.bmp,.jpeg" />
                                @if($errors->has('avatar'))
                                    <span class="text-danger">{{ $errors->first('avatar') }}</span>
                                @endif
                            </div>
                        </div>

                        <hr>
                        <h3 class="box-title"> <p style="color:red;">Assign Permission:</p></h3>
                        <div class="row">
                            {{--<form action="{{ route('user.assign_permission_store') }}" method="post">--}}
                            {{--<input type="hidden" name="user_id" value="{{ $user->id }}">--}}
                            <div class="form-group">
                                <select  class="form-control" id="assign" name="selected_permission" >
                                    <option disabled="" selected> Select Any One Permission </option>
                                    @foreach($permissions as $prem)

                                        <option value="{{ $prem->id }}"
                                            @if($prem->id == $permission_user->permission_id)
                                                selected
                                             @endif
                                            >{{ $prem->display_name }} </option>

                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <hr>


                        <div id="consultancy_info" style="display:none;">
                            <h3 class="box-title"><p style="color:red;"> Consultancy Details:</p></h3>
                            <hr>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name" class="control-label">{{ __('Consultancy Name') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('consultancy_name') ? ' is-invalid' : '' }}" name="consultancy_name" value="{{ old('consultancy_name') ?? $formData->consultancy_name }}"  autofocus>

                                        @if ($errors->has('consultancy_name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('consultancy_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{ __('Consultancy Address') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('consultancy_address') ? ' is-invalid' : '' }}" name="consultancy_address" value="{{ old('consultancy_address') ?? $formData->consultancy_address }}"  autofocus>

                                        @if ($errors->has('consultancy_address'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('consultancy_address') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{ __('Consultancy License') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('consultancy_license') ? ' is-invalid' : '' }}" name="consultancy_license" value="{{ old('consultancy_license') ?? $formData->consultancy_license }}"  autofocus>

                                        @if ($errors->has('consultancy_license'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('consultancy_license') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name" class="control-label">{{ __('Consultancy License Number') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('consultancy_license_number') ? ' is-invalid' : '' }}" name="consultancy_license_number" value="{{ old('consultancy_license_number') ?? $formData->consultancy_license_number }}"  autofocus>

                                        @if ($errors->has('consultancy_license_number'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('consultancy_license_number') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{ __('Staff Id') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('staff_id') ? ' is-invalid' : '' }}" name="staff_id" value="{{ old('staff_id') ?? $formData->staff_id }}"  autofocus>

                                        @if ($errors->has('staff_id'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('staff_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{ __('Extension Number') }}</label>
                                        <input  type="text" class="form-control{{ $errors->has('extension_number') ? ' is-invalid' : '' }}" name="extension_number" value="{{ old('extension_number') ?? $formData->extension_number }}"  autofocus>

                                        @if ($errors->has('extension_number'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('extension_number') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 col-md-offset-6">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });

        $("#assign").on ('change',  function( e ) {
            var data = $(this).val();

            if(data == 1){
                $("#consultancy_info").show();
            } else {
                $("#consultancy_info").hide();
            }
        });



        $(document).ready(function( e ){
            var data = $("#assign").val();
            if(data == 1){
                $("#consultancy_info").show();
            }
        });
    </script>
@endsection

