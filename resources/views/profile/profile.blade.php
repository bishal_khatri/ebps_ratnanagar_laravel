@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@endsection


@section('page_title')
    PROFILE
@endsection

@section('content-title')
    <h4>Profile Page</h4>




@endsection

@section('content')

<div class="row">

    <div class="col-md-4 col-xs-12">
        <div class="white-box">
            <div class="user-bg"> <img width="100%"  src="{{ asset(STATIC_DIR.'plugins/images/large/img1.jpg') }}">
                <div class="overlay-box">
                    <div class="user-content">
                        <a href="javascript:void(0)">
                            @if(!empty(Auth::user()->avatar))
                                <img src="{{ asset(STATIC_DIR.'storage/'.Auth::user()->avatar) }}" class="thumb-lg img-circle" alt="img" />
                            @else
                                <img src="{{ asset(STATIC_DIR.'uploads/defualt.png') }}" class="thumb-lg img-circle" alt="img" />
                            @endif


                        </a>
                        <h4 class="text-white">{{ $formData->first_name }}&nbsp; {{ $formData->last_name }}</h4>
                        <h5 class="text-white">{{ $formData->email }}</h5> </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-8 col-xs-12">
        <div class="white-box">
            <ul class="nav nav-tabs tabs customtab">
                <li class="{{$profile}} tab">
                    <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Profile</span> </a>
                </li>

                <li class="{{$settings}} tab">
                    <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Settings</span> </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane {{$profile}} " id="profile">
                    <p style="font-size: 25px;">Personal Details:</p>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-xs-6 b-r"> <strong>Full Name</strong>
                            <br>
                            <p class="text-muted">{{ $formData->first_name }}&nbsp;{{ $formData->last_name }}</p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r"> <strong>Mobile</strong>
                            <br>
                            <p class="text-muted">{{ $formData->contact }}</p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r"> <strong>Email</strong>
                            <br>
                            <p class="text-muted">{{ $formData->email }}</p>
                        </div>

                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-md-4 col-xs-6 b-r"> <strong>Address</strong>
                            <br>
                            <p class="text-muted">{{ $formData->address }}</p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r"> <strong>Last login</strong>
                            <br>
                            <p class="text-muted">{{ $formData->last_login_at }}</p>
                        </div>


                    </div>
                    <hr>
                    @can('consultancy')
                    <p style="font-size: 25px;">Consultancy Details:</p>
                    <hr>

                    <div class="row">
                        <div class="col-md-4 col-xs-6 b-r"> <strong>Name</strong>
                            <br>
                            <p class="text-muted">{{ ($formData->consultancy_name) ?? 'n/a' }}</p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r"> <strong>Address</strong>
                            <br>
                            <p class="text-muted">{{ $formData->consultancy_address ?? 'n/a' }}</p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r"> <strong>License</strong>
                            <br>
                            <p class="text-muted">{{ $formData->consultancy_license ?? 'n/a' }}</p>
                        </div>

                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-md-4 col-xs-6 b-r"> <strong>License Number</strong>
                            <br>
                            <p class="text-muted">{{ $formData->consultancy_license_number ?? 'n/a' }}</p>
                        </div>


                    </div>
                     @endcan
                </div>

                <div class="tab-pane {{$settings}} " id="settings">
                    <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Personal Information</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <form method="post" action="{{ route('update_profile') }}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" value="1" name="action">
                                        <div class="form-group">
                                            <label for="first_name">First Name</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                                <input type="text" class="form-control"  name="first_name" placeholder="Username"  value="{{$formData->first_name}}">
                                            </div>

                                            @if($errors->has('first_name'))
                                                <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                            @endif

                                        </div>

                                        <div class="form-group">
                                            <label for="last_name">Last Name</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                                <input type="text" class="form-control"  placeholder="Username" name="last_name" required value="{{$formData->last_name}}"> </div>

                                            @if($errors->has('last_name'))
                                                <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                            @endif

                                        </div>

                                        <div class="form-group">
                                            <label for="contact">Phone</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                <input type="tel" class="form-control"  placeholder="Enter phone" name="contact" required value="{{$formData->contact}}"> </div>

                                            @if($errors->has('contact'))
                                                <span class="text-danger">{{ $errors->first('contact') }}</span>
                                            @endif

                                        </div>

                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-home"></i></div>
                                                <input type="tel" class="form-control"  placeholder="Enter phone" name="address" required value="{{ $formData->address  }}"> </div>

                                            @if($errors->has('address'))
                                                <span class="text-danger">{{ $errors->first('address') }}</span>
                                            @endif

                                        </div>


                                            <div class="form-group ">
                                                <label for="change_avatar">
                                                    Change Avatar
                                                </label>
                                                {{--<input type="file" name="change_avatar" id="input-file-now" class="dropify"  accept=".jpg,.png,.bmp,.jpeg" required/>--}}
                                                <input type="file" name="change_avatar" id="input-file-now-custom-3" class="dropify" accept=".jpg,.png,.bmp,.jpeg"

                                                       @if(!empty(Auth::user()->avatar))
                                                                data-default-file="{{ asset(STATIC_DIR.'storage/'.Auth::user()->avatar) }}"
                                                       @endif
                                                >

                                                @if($errors->has('change_avatar'))
                                                    <span class="text-danger">{{ $errors->first('change_avatar') }}</span>
                                                @endif
                                            </div>



                                        <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Update</button>
                                    </form>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <form method="post" action="{{ route('update_profile') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="old_password">Old Password</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-lock"></i></div>
                                                <input type="password" class="form-control" id="exampleInputEmail1" required placeholder="Enter Old Password" name="old_password"> </div>

                                            @if($errors->has('old_password'))
                                                <span class="text-danger">{{ $errors->first('old_password') }}</span>
                                            @endif

                                        </div>

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-lock"></i></div>
                                                <input type="password" class="form-control" id="exampleInputpwd1" required placeholder="Enter Password" name="password"> </div>
                                            @if($errors->has('password'))
                                                <span class="text-danger">{{ $errors->first('password') }}</span>
                                            @endif

                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-lock"></i></div>
                                                <input type="password" class="form-control" id="exampleInputpwd2" required placeholder="Enter password again" name="password_confirmation"> </div>

                                            @if($errors->has('password_confirmation'))
                                                <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                            @endif

                                        </div>

                                        <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Change Password</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div></div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();

        });
    </script>
@endsection
