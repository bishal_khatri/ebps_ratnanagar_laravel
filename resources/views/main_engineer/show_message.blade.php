@extends('layouts.app')
@section('css')

@endsection
@section('page_title')
    सम्पन्न
@endsection
@section('right_button')
@stop
@section('content-title')
    <h3>सम्पन्न</h3>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title text-center text-danger">
                    @if(isset($action))
                        @if($action == 'sub-engineer')
                            Proceeded to Sub Engineer.
                        @elseif($action == 'consultancy')
                            Proceeded to Consultancy.
                        @endif
                    @endif
                </h3>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
