@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	सम्पन्न फारम
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>सम्पन्न फारम</h4>
@endsection

@section('content')
	<div class="row">
		<!-- tbl of form_landinfo -->
		<div class="col-md-12">
			
			<form  method="post"  action="">
				@csrf
				
				<input type="hidden" name="form_id" value= {{ $formData->id }} >
				
				<div class="col-md-12">
					<div class="panel panel-info">
						
						<div class="panel-wrapper collapse in" aria-expanded="true">
							<div class="panel-body">
								
								<table class="table table-sm table-bordered table-hover">
									<tr class="d-none d-print-table-row text-center font-weight-bold">
										<td colspan="5">
											निर्माण सम्पन्न प्रतिवेदन सम्बन्धमा
										</td>
									</tr>
									<tr>
										<td colspan="5">
											<p>महोदय,</p>
											<p style="text-indent:60px;">
												उपरोक्त सम्बन्धमा {{ $formData->buildingInfo->district ?? '............' }}
												जिल्ला {{ $formData->landInfo->village_name ?? '............' }} गा.वि.स / न.पा
												वडा नं {{ $formData->landInfo->current_ward_number ?? '............' }} बस्ने

												<span class="border_line">{{ $formData->sambodhan ?? '............' }} 

												{{ $formData->field_owner_name ?? '............' }}</span> 
												को
												र.न.पा वडा नं {{ $formData->landInfo->current_ward_number ?? '............' }}
												साबिक 
												कि.नं {{ $formData->landInfo->kitta_number ?? '............' }} मा पास भएको {{ $formData->building_type ?? '............' }}को निर्माण सम्पन्न प्रमाण पत्र पाऊ भनि मिति ........................दर्ता नं 

												{{ $formData->darta_number ?? '............' }} मा दिनु भएको निबेदन तोकादेश भए अनुसार उक्त निर्माण कार्यको स्थल गत निरक्षणमा सम्पूर्ण नाप जाँच गरि यो निर्माण सम्पनको प्रतिवेदन पेश गरको छु |
											</p>
										</td>
									</tr>
									<tr>
										<td class="font-weight-bold" colspan="5">मापदण्ड</td>
									</tr>
									
									<tr>
										<td>१.</td>
										<td>ज.ध.प्र.पु. क्षेत्रफ़ल्</td>
										<td colspan="3">
											{{ $formData->sampanna->field_area ?? 'n/a' }}
										</td>
									</tr>
									<tr>
										<td>२.</td>
										<td>साईट प्लान क्षेत्रफ़ल्</td>
										<td colspan="3">
											{{ $formData->sampanna->site_plan_area ?? 'n/a' }}
										</td>
									</tr>
									<tr class="text-center">
										<td>क्र.सं.</td>
										<td>विवरण</td>
										<td>मापदण्ड अनुसार</td>
										<td>स्विकृत अनुसार</td>
										<td>निर्माण स्थिति </td>
									</tr>
									<tr>
										<td>१.</td>
										<td>
											ग्रउण्ड कभरेज 
											{{ $formData->sampanna->ground_coverage ?? 'n/a' }}
											 %  
										</td>

										<td>
											{{ $formData->sampanna->ground_coverage_mapdanda ?? 'n/a' }} व.फि
											
										</td>
										<td>
											{{ $formData->sampanna->ground_coverage_swikrit_anusar ?? 'n/a' }} व.फि
										</td>

										<td>
											{{ $formData->sampanna->ground_coverage_nirman_esthithi ?? 'n/a' }} व.फि
										</td>
									</tr>
									<tr>
										<td>२.</td>
										<td>तला</td>

										<td>
											{{ $formData->sampanna->talla_mapdanda ?? 'n/a' }} 
										</td>

										<td>
											{{ $formData->sampanna->talla_swikrit_anusar ?? 'n/a' }} 
										</td>

										<td>
											{{ $formData->sampanna->talla_nirman_esthithi ?? 'n/a' }}
										</td>

									</tr>
									<tr>
										<td>३.</td>
										<td>उचाई</td>
										<td>
											{{ $formData->sampanna->height_mapdanda ?? 'n/a' }} फि
										</td>
										
										<td>
											{{ $formData->sampanna->height_swikrit_anusar ?? 'n/a' }} फि
										</td>

										<td>
											{{ $formData->sampanna->height_nirman_esthithi ?? 'n/a' }} फि
										</td>

									</tr>

									<tr>
										<td>४.</td>
										<td>कूल फ्लोर एरिया</td>
										<td>
											{{ $formData->sampanna->floor_area_mapdanda ?? 'n/a' }} व.फि
										</td>
										<td>
											{{ $formData->sampanna->floor_area_swikrit_anusar ?? 'n/a' }} व.फि
										</td>
										<td>
											{{ $formData->sampanna->floor_area_nirman_esthithi ?? 'n/a' }} व.फि
										</td>
									</tr>

									<tr>
										<td>५.</td>
										<td>स्विकृत भन्दा बदी</td>
										<td>
											{{ $formData->sampanna->swikrit_vanda_badi_mapdanda ?? 'n/a' }}	
										</td>
										<td>
											{{ $formData->sampanna->swikrit_vanda_badi_swikrit_anusar ?? 'n/a' }} 
										</td>
										<td>
											{{ $formData->sampanna->swikrit_vanda_badi_nirman_esthithi ?? 'n/a' }} 
										</td>
									</tr>

									<tr>
										<td>६.</td>
										<td>आर. ओ. डब्लु</td>
										<td>
											{{ $formData->sampanna->r_o_w_mapdanda ?? 'n/a' }} मि
										</td>
										<td>
											{{ $formData->sampanna->r_o_w_swikrit_anusar ?? 'n/a' }} मि
										</td>
										<td>
											{{ $formData->sampanna->r_o_w_nirman_esthithi ?? 'n/a' }} मि
										</td>
									</tr>

									<tr>
										<td>७.</td>
										<td>सेट ब्यक</td>
										<td>
											{{ $formData->sampanna->setback_mapdanda ?? 'n/a' }} मि
										</td>
										<td>
											{{ $formData->sampanna->setback_swikrit_anusar ?? 'n/a' }} मि
										</td>
										<td>
											{{ $formData->sampanna->setback_nirman_esthithi ?? 'n/a' }} मि
										</td>
									</tr>

									<tr>
										<td>८.</td>
										<td>
											बिधुत लाईन 
											{{ $formData->sampanna->electricity_line ?? 'n/a' }} के.भि
										</td>
										<td>{{ $formData->sampanna->electricity_line_mapdanda ?? 'n/a' }} फि</td>
										<td>{{ $formData->sampanna->electricity_line_swikrit_anusar ?? 'n/a' }} फि</td>
										<td>{{ $formData->sampanna->electricity_line_nirman_esthithi ?? 'n/a' }} फि</td>
									</tr>

									<tr>
										<td>९.</td>
										<td>नदि किनार</td>
										<td>{{ $formData->sampanna->river_side_mapdanda ?? 'n/a' }} फि</td>
										<td>{{ $formData->sampanna->river_side_swikrit_anusar ?? 'n/a' }} फि</td>
										<td>{{ $formData->sampanna->river_side_nirman_esthithi ?? 'n/a' }} फि</td>
										
									</tr>

									<tr>
										<td colspan="5">
											कैफियत : स्वीकृत नक्शा भन्दा विपरित  भए त्यसको विवरण :
										</td>
									</tr>
									<tr>
										<td colspan="3">
											कन्सल्टेन्टको तर्फबाट : <br>
											नाम : {{ $formData->sampanna->entry_name}} <br>
											पद : {{ $formData->sampanna->consultancy_post}} <br>
											कन्सल्टेन्सी : {{ $formData->sampanna->consultancy_name}} <br>
											<!-- सही : -->
										</td>
										<td colspan="2">
											कार्यालयको प्राविधिक शाखाको तर्फबाट : <br>
											नाम : ...................... <br>
											पद : ...................... <br>
											सही :......................
										</td>
									</tr>
									<!-- <tr class="d-block-table-row d-print-none">
										<td colspan="5">
											<center>
												<button class="btn_one" onclick="window.print()" data-toggle="modal" data-target="#reject">
													<i class="fa fa-print fa-fw"></i>&nbsp;Print
												</button>
												<button type="button" class="btn_two" data-toggle="modal" data-target="#sampana">
													Upload
												</button>
											</center>
										</td>
									</tr> -->
									<tr>
										

											<td colspan="5" style="text-align: center;">
													<a href="{{ route('mainengineer.proceed_sampanna',$formData->id) }}" type="button" class="btn btn-success text-white"> Proceed</a>
											</td>

									</tr>
								</table>

								{{-- end form-body --}}
							</div>
							{{-- panel-body --}}
						</div>
						{{-- end panel-wrapper --}}
					</div>
					{{-- end panel --}}
				
				</div>
				{{-- end col-md-12 --}}
			
			</form>
			<!-- end form -->
		
		</div>
		<!-- end form_landinfo -->
	</div>
@include('sub_engineer.modal')
@endsection

@section('scripts')

 <script>
        $('#rejectsampanna').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');

            $("#sampanna_form_id").val(id);
        });
    </script>

@endsection
