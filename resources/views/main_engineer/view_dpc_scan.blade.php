@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/css/animated-masonry-gallery.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.css') }}" />
@endsection
@section('page_title')
	D.P.C
@endsection
@section('right_button')
@stop
@section('content-title')
	<h4>DPC बारे संबन्धित प्राविधिकको प्रतिवेदन</h4>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div id="gallery text-center">
							<div id="gallery-content">
								<a href="{{ asset(STATIC_DIR.'storage/'.$dpcScan->dpc_scan ?? '') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="सँधियार सूचना">
									<img src="{{ asset(STATIC_DIR.'storage/'.$dpcScan->dpc_scan ?? '') }}" class="all studio" alt="gallery" width="500"/>
								</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<a href="{{ route('mainengineer.proceed',$form_id) }}" class="btn btn-primary">Proceed</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/animated-masonry-gallery.js') }}"></script>
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/gallery/js/jquery.isotope.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset(STATIC_DIR.'plugins/bower_components/fancybox/ekko-lightbox.min.js') }}"></script>
	{{--<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>--}}
	{{--<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>--}}
	<script>
        $(document).ready(function($) {
            // delegate calls to data-toggle="lightbox"
            $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                event.preventDefault();
                return $(this).ekkoLightbox({
                    onShown: function() {
                        if (window.console) {
                            return console.log('Checking our the events huh?');
                        }
                    },
                    onNavigate: function(direction, itemIndex) {
                        if (window.console) {
                            return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                        }
                    }
                });
            });
        });
	</script>
@endsection
