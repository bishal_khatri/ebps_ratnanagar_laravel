-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2019 at 07:12 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebps_ratnanagar`
--

-- --------------------------------------------------------

--
-- Table structure for table `aamin_pratibedan`
--

CREATE TABLE `aamin_pratibedan` (
  `id` int(20) NOT NULL,
  `form_id` int(11) NOT NULL,
  `bato_category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_direction` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bato_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_front` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_back` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_left` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mohoda_right` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_purba` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_paschim` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_utar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_duri_dakshin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_purba` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_paschim` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_utar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chadnu_parne_setback_dakshin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_width` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `napi_field_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_length` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_width` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `siteko_napi_field_area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at_nepali` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `other` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hitension_wire_distance` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `electricity_volt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `river_side_bata_chodnu_parne_duri` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aamin_pratibedan`
--

INSERT INTO `aamin_pratibedan` (`id`, `form_id`, `bato_category`, `bato_length`, `bato_direction`, `bato_area`, `mohoda_front`, `mohoda_back`, `mohoda_left`, `mohoda_right`, `chadnu_parne_duri_purba`, `chadnu_parne_duri_paschim`, `chadnu_parne_duri_utar`, `chadnu_parne_duri_dakshin`, `chadnu_parne_setback_purba`, `chadnu_parne_setback_paschim`, `chadnu_parne_setback_utar`, `chadnu_parne_setback_dakshin`, `napi_length`, `napi_width`, `napi_field_area`, `siteko_napi_length`, `siteko_napi_width`, `siteko_napi_field_area`, `created_at_nepali`, `other`, `hitension_wire_distance`, `electricity_volt`, `river_side_bata_chodnu_parne_duri`, `created_by`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, 3, 'Deserunt minim quia', 'Eum nostrud non inci', 'In laborum culpa re', 'Ipsum reprehenderit', 'Iure soluta voluptas', 'At est et mollit dol', 'Libero molestias iru', 'Voluptate accusantiu', 'Quis ipsum animi no', 'Dolor est ut libero', 'Facilis quos est ip', 'Quo et incididunt ut', 'Quod sint fugit il', 'Quas accusantium qui', 'Voluptates facere be', 'Quasi voluptas occae', 'Voluptates illo sequ', 'Dolore obcaecati tem', 'Animi elit reprehe', '', '', '', 'सोम, जेठ २७, २०७६', 'Nulla quas nesciunt', 'Accusantium cum haru', 'Ut nihil ea quis dis', NULL, 1, '2019-06-10 09:32:00', NULL, NULL),
(2, 1, 'Vel totam architecto', 'Rerum molestiae id t', 'Est distinctio Con', 'Voluptates placeat', 'Omnis magnam qui sae', 'Qui consectetur sunt', 'Deserunt tempore et', 'Commodi vel adipisci', 'Harum et non nihil c', 'Beatae nihil pariatu', 'Deserunt quia dolore', 'Unde consequatur opt', 'Rem id unde facere', 'Est aut ex molestiae', 'Est aut sed natus ex', 'Mollit omnis omnis a', 'Sed neque doloribus', 'Voluptatum nostrud d', 'Omnis velit et sed n', '', '', '', 'सोम, जेठ २७, २०७६', 'Do maiores quasi qui', NULL, NULL, NULL, 1, '2019-06-10 11:14:22', NULL, NULL),
(3, 5, 'Eu dolores aspernatu', 'Soluta sit nostrum', 'Sunt perspiciatis', 'Proident temporibus', 'Cumque quis quo quo', 'Repudiandae omnis no', 'Irure nobis voluptas', 'Quia obcaecati id r', 'Id nobis ad consecte', 'Cum consequat Incid', 'Reiciendis duis magn', 'Aut sunt cupiditate', 'Vero id facere vitae', 'Excepturi ut cillum', 'Ducimus id earum ul', 'Aute commodo quae ut', 'Labore rerum saepe o', 'Mollitia aperiam rem', 'Obcaecati iure non e', '', '', '', 'मंगल, जेठ २८, २०७६', 'Non ullamco quaerat', 'Quos rem aliquam bla', 'Lorem mollit eum ill', NULL, 1, '2019-06-11 05:27:05', '2019-06-11 05:27:05', 1),
(4, 6, '123', '12', '12', '12', '12', '123', '123', '123', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '', '', '', 'मंगल, जेठ २१, २०७६', '12', '12', '12', NULL, 34, '2019-06-11 08:28:36', NULL, NULL),
(5, 11, 'Ut et est enim esse', 'Nisi ipsum facilis n', 'Officia vel ipsa ut', 'Ut consequatur Vero', 'Doloremque molestiae', 'Sequi voluptatibus d', 'Molestiae laborum ac', 'Dolor asperiores qua', 'Optio placeat accu', 'Laborum fugiat omni', 'Temporibus iste temp', 'Asperiores commodo s', 'Facilis omnis duis q', 'Consectetur nulla i', 'Aspernatur aut et vo', 'Rerum molestias adip', 'Eos molestias exerci', 'Perspiciatis fugiat', 'Inventore molestias', '', '', '', 'सोम, असार ९, २०७६', 'Sit in cupiditate mi', NULL, NULL, NULL, 1, '2019-06-24 08:17:26', NULL, NULL),
(6, 4, 'Aspernatur nulla dol', 'Amet cumque quaerat', 'Delectus nisi aperi', 'Quo ea quos quo quo', 'Eiusmod non facilis', 'Esse autem voluptate', 'Commodi voluptatem', 'Sunt ut nihil commo', 'Sint sed nihil aute', 'Eaque est aliquam r', 'Laborum Molestiae e', 'Officiis cumque saep', 'Adipisicing magna al', 'Id voluptatem quas', 'Sed molestias corrup', 'Ipsam laboriosam vo', 'Qui voluptatem culpa', 'Quasi anim deserunt', 'Voluptatem in tempor', '', '', '', 'बिही, असार १२, २०७६', 'Deleniti amet moles', NULL, NULL, NULL, 34, '2019-06-27 06:18:49', NULL, NULL),
(7, 12, 'Tempore fugit prov', 'Reiciendis non ut es', 'Aut aut id ut sit re', 'Dolore delectus err', 'Veritatis ad volupta', 'Ullamco Nam reprehen', 'Numquam inventore ne', 'Quod quos enim archi', 'Et quod quidem id mo', 'Dolor ipsum volupta', 'Praesentium placeat', 'Dolor recusandae Ob', 'Ab reprehenderit con', 'Aut eaque proident', 'Corporis dolor ut do', 'Ullamco mollitia qui', 'In amet consectetur', 'Nostrum odio ipsum', 'Autem quis consectet', '', '', '', 'सोम, असार १६, २०७६', 'Blanditiis reprehend', 'Sed non labore paria', 'Veniam dolor sint u', NULL, 1, '2019-07-01 07:36:51', NULL, NULL),
(8, 13, 'Reiciendis ut pariat', 'Sed qui enim ea enim', 'Enim irure eum et cu', 'Repellendus Enim te', 'Dicta autem ab volup', 'Dolores voluptate it', 'Eaque dignissimos qu', 'Non et qui blanditii', 'Provident numquam c', 'Totam fuga Dicta fu', 'Blanditiis ipsum re', 'Corrupti odio sint', 'Dolore impedit blan', 'Voluptas repudiandae', 'Repellendus Digniss', 'Est sit magna id q', 'Eos modi qui sapient', 'Est temporibus est n', 'Et non magna dolore', '', '', '', 'सोम, असार १६, २०७६', 'Vel et esse aut vita', 'Voluptatem dicta om', 'Aliquip est voluptat', NULL, 1, '2019-07-01 07:45:30', '2019-07-01 07:45:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `aamin_pratibedan_scan`
--

CREATE TABLE `aamin_pratibedan_scan` (
  `aamin_pratibedan_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `aamin_pratibedan_scan` varchar(100) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aamin_pratibedan_scan`
--

INSERT INTO `aamin_pratibedan_scan` (`aamin_pratibedan_id`, `form_id`, `aamin_pratibedan_scan`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'files/3/aaminPratibedan_scan/uIRyTNxmX9keASJ90pw0HjjgcATbPTSZYxfzh5pA.jpeg', '2019-06-10 09:33:45', 1, NULL, NULL),
(2, 1, 'files/1/aaminPratibedan_scan/6shbBli3uWqx5ZjkMwApV8ZQ6Ku3nS2KAqUcnu8t.jpeg', '2019-06-10 11:14:37', 1, NULL, NULL),
(3, 5, 'files/5/aaminPratibedan_scan/dEf8r8W4r27RtLXUa3dPnOkQfunsQvhsam5ddbNX.jpeg', '2019-06-11 05:02:09', 1, NULL, NULL),
(4, 6, 'files/6/aaminPratibedan_scan/dWe9l2N3jiAHJvem8M3bQA7sQECg2314BLpg4eXt.jpeg', '2019-06-11 08:28:45', 34, NULL, NULL),
(5, 11, 'files/11/aaminPratibedan_scan/PBI2O8qDHa9jmY9kcdH7H1VTUVAKlCi4BfhNPxrW.jpeg', '2019-06-24 08:17:33', 1, NULL, NULL),
(6, 4, 'files/4/aaminPratibedan_scan/zOSFD6BIvpfnxO4RxRz7g8xpJlCJz2FW8ZyrRKEt.jpeg', '2019-06-27 06:19:10', 34, NULL, NULL),
(7, 13, 'files/13/aaminPratibedan_scan/4rUBMMpUwDY5hyIK0BJGNtxBM3ZhV7Dics17h9hn.png', '2019-07-01 07:44:37', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `code_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permissions`
--

INSERT INTO `auth_permissions` (`id`, `display_name`, `code_name`) VALUES
(1, 'Consultancy', 'consultancy'),
(2, 'Engineer', 'engineer'),
(3, 'Sub Engineer', 'sub-engineer'),
(4, 'Aamin', 'aamin'),
(5, 'Main Engineer', 'main-engineer'),
(6, 'administration', 'administration');

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission_user`
--

CREATE TABLE `auth_permission_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `permission` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission_user`
--

INSERT INTO `auth_permission_user` (`id`, `user_id`, `permission_id`, `permission`) VALUES
(1, 13, 6, 'administration'),
(4, 14, 2, 'engineer'),
(5, 15, 3, 'sub-engineer'),
(8, 21, 1, 'consultancy'),
(10, 23, 1, 'consultancy'),
(11, 32, 1, 'consultancy'),
(12, 26, 1, 'consultancy'),
(13, 20, 1, 'consultancy'),
(14, 16, 2, 'engineer'),
(15, 17, 3, 'sub-engineer'),
(16, 18, 4, 'aamin'),
(17, 19, 5, 'main-engineer'),
(18, 24, 1, 'consultancy'),
(19, 28, 1, 'consultancy'),
(20, 22, 1, 'consultancy'),
(21, 33, 1, 'consultancy'),
(24, 31, 1, 'consultancy'),
(25, 34, 4, 'aamin'),
(26, 35, 1, 'consultancy'),
(27, 36, 1, 'consultancy'),
(28, 37, 6, 'administration'),
(29, 38, 1, 'consultancy'),
(30, 39, 6, 'administration'),
(31, 40, 1, 'consultancy'),
(32, 41, 2, 'engineer'),
(33, 41, 1, 'consultancy'),
(34, 42, 1, 'consultancy'),
(35, 43, 1, 'consultancy'),
(36, 44, 1, 'consultancy');

-- --------------------------------------------------------

--
-- Table structure for table `dpc`
--

CREATE TABLE `dpc` (
  `dpc_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `building_category` varchar(255) NOT NULL,
  `building_structure` varchar(255) NOT NULL,
  `building_length` varchar(255) NOT NULL,
  `building_width` varchar(255) NOT NULL,
  `foundation_size` varchar(255) NOT NULL,
  `setback_purba` varchar(255) NOT NULL,
  `setback_pachim` varchar(255) NOT NULL,
  `setback_utar` varchar(255) NOT NULL,
  `setback_dakshin` varchar(255) NOT NULL,
  `mohoda_right` varchar(255) NOT NULL,
  `mohoda_left` varchar(255) NOT NULL,
  `strap_beam_swkrit_anusar` varchar(255) NOT NULL,
  `strap_beam` varchar(255) NOT NULL,
  `plinth_height` varchar(255) NOT NULL,
  `pillar_size` varchar(255) NOT NULL,
  `pillar_rod_size` varchar(255) NOT NULL,
  `pillar_rod_number` varchar(255) NOT NULL,
  `pillar_churi_size` varchar(255) NOT NULL,
  `tie_beam_size_swkrit_anusar` varchar(255) NOT NULL,
  `tie_beam_size` varchar(255) NOT NULL,
  `hodding_board_swkrit_anusar` varchar(255) NOT NULL,
  `hodding_board` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dpc`
--

INSERT INTO `dpc` (`dpc_id`, `form_id`, `building_category`, `building_structure`, `building_length`, `building_width`, `foundation_size`, `setback_purba`, `setback_pachim`, `setback_utar`, `setback_dakshin`, `mohoda_right`, `mohoda_left`, `strap_beam_swkrit_anusar`, `strap_beam`, `plinth_height`, `pillar_size`, `pillar_rod_size`, `pillar_rod_number`, `pillar_churi_size`, `tie_beam_size_swkrit_anusar`, `tie_beam_size`, `hodding_board_swkrit_anusar`, `hodding_board`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3, 'Iusto sit facilis h', 'Dolor autem reiciend', 'Qui aliquid omnis ma', 'Iure sed harum quo m', 'Iusto rem ex vel con', 'Porro aliquip non id', 'Sit natus aperiam a', 'Pariatur Minima qui', 'Sed amet commodo ut', 'Ab incidunt rerum s', 'Quia dolorum labore', 'Officia voluptates q', 'Exercitationem adipi', 'Velit ea exercitatio', 'Itaque sit vitae adi', 'Facere suscipit offi', '474', 'Iure at et reprehend', 'Ipsum inventore do d', 'Itaque sint ea enim', 'भएको', 'Adipisicing doloribu', 1, NULL, '2019-06-10 09:40:40', '2019-06-10 09:40:40'),
(2, 5, 'Praesentium doloremq', 'Voluptatem Laudanti', 'Et mollit aut sed se', 'Laudantium voluptat', 'Accusamus optio et', 'Culpa rem eaque sit', 'Aliqua Quisquam vol', 'Rerum iure id volup', 'Eum autem nesciunt', 'Sint animi sed dol', 'Fuga Molestiae in v', 'Provident id nostru', 'Ullam est odio sit v', 'Commodo sit asperior', 'Aut aliqua Aut quib', 'Eos distinctio Sit', '383', 'Dolores eligendi qui', 'Nobis amet veniam', 'Exercitation aliquam', 'नभएको', 'Maxime quasi similiq', 1, NULL, '2019-06-11 05:34:27', '2019-06-11 05:34:27'),
(3, 6, 'Omnis voluptate vel', 'Magni ex tempor vita', 'Nam rerum hic accusa', 'Quis voluptas volupt', 'Quia id in quas atqu', 'Suscipit obcaecati m', 'Maiores exercitation', 'Magnam id ipsam amet', 'Facilis qui incidunt', 'Autem sunt tempore', 'Corporis sint offici', 'Aliquam est qui quos', 'Illum adipisicing s', 'Ad quis molestias au', 'Et totam quos odit a', 'Expedita ad placeat', '983', 'Temporibus ut iusto', 'Molestiae irure ulla', 'In tenetur nihil cum', 'भएको', 'Ut velit sint moles', 20, NULL, '2019-06-11 08:31:44', '2019-06-11 08:31:44');

-- --------------------------------------------------------

--
-- Table structure for table `dpc_scan`
--

CREATE TABLE `dpc_scan` (
  `dpc_scan_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `dpc_scan` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dpc_scan`
--

INSERT INTO `dpc_scan` (`dpc_scan_id`, `form_id`, `slug`, `dpc_scan`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 3, 'upload_dpc_form', 'files/3/dpc_scan/0EvlXmfOwvBwZr7pL5PHRkJzjovHWmYlSU799L5i.png', '2019-06-10 09:40:56', '1', NULL, NULL),
(2, 3, 'dpc-tippani', 'files/3/dpc_scan/6oDBwnQDMcVRRTdJJZ8cKqQa7Luu2Lpn4hERp2Rh.jpeg', '2019-06-10 09:43:28', '1', NULL, NULL),
(3, 3, 'superstructure', 'files/3/dpc_scan/c0GpUgQrfc1b5w7MrFT6z6VhijcwwuWBgmxk1672.jpeg', '2019-06-10 09:47:33', '1', NULL, NULL),
(4, 5, 'upload_dpc_form', 'files/5/dpc_scan/7wHF4rlfEM2MNVQY1PVHCN3TcU9LGknDnFliUjky.jpeg', '2019-06-11 05:34:42', '1', '2019-06-11 06:01:53', '1'),
(5, 5, 'dpc-tippani', 'files/5/dpc_scan/RWn0d8uOFNAs5ohm5zdMR8vLExFjM5c0sNUznJAH.png', '2019-06-11 06:55:28', '1', NULL, NULL),
(6, 5, 'superstructure', 'files/5/dpc_scan/LJX5i46X4sZ3NcJxZUY0SqJEQZ1nyzWlSJMBlUyx.png', '2019-06-11 07:04:07', '1', NULL, NULL),
(7, 6, 'upload_dpc_form', 'files/6/dpc_scan/konKNV1dkCwS2SiZ5pSLgTISA7JDfb4eht6ycmvD.png', '2019-06-11 08:31:54', '20', '2019-06-11 08:33:37', '20'),
(8, 6, 'dpc-tippani', 'files/6/dpc_scan/n1o1KHIDRz6dErECjUobWfgcVcstHxRcEhvc1rmK.jpeg', '2019-06-11 08:34:27', '16', NULL, NULL),
(9, 6, 'superstructure', 'files/6/dpc_scan/EasBkPdsZ3g4IHn79T6W0YfKml2lUDZVO7GYKLCX.png', '2019-06-11 08:35:38', '17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_buildingfiles`
--

CREATE TABLE `form_buildingfiles` (
  `buildingfiles_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `lalpurja` varchar(255) NOT NULL,
  `citizenship` varchar(255) NOT NULL,
  `blueprint` varchar(255) NOT NULL,
  `tero_rashid` varchar(255) NOT NULL,
  `house_map` varchar(255) NOT NULL,
  `naamsari` varchar(255) NOT NULL,
  `ward_sifaris` varchar(255) DEFAULT NULL,
  `sampanna_patra` varchar(255) DEFAULT NULL,
  `other_document_1` varchar(255) DEFAULT NULL,
  `other_document_1_description` text CHARACTER SET utf8,
  `other_document_2` varchar(255) DEFAULT NULL,
  `other_document_2_description` text CHARACTER SET utf8,
  `other_document_3` varchar(255) DEFAULT NULL,
  `other_document_3_description` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_buildingfiles`
--

INSERT INTO `form_buildingfiles` (`buildingfiles_id`, `form_id`, `lalpurja`, `citizenship`, `blueprint`, `tero_rashid`, `house_map`, `naamsari`, `ward_sifaris`, `sampanna_patra`, `other_document_1`, `other_document_1_description`, `other_document_2`, `other_document_2_description`, `other_document_3`, `other_document_3_description`) VALUES
(1, 1, 'files/1/building_file/lalpurja/KW2UaVrQ8nxQoP6UPbxPNGpN5tnkcrQTGhOM51Xy.jpeg', 'files/1/building_file/citizenship/2R4dux7ILDJm8l0nnujBUbXW8YXXgz0I7p8kO2vh.jpeg', 'files/1/building_file/blueprint/SY5xLARjVuYEvy4OtirCazFJn1caS5weqh17JmxF.png', 'files/1/building_file/tero_rashid/cUnz0bwgfM4jI9vu99qve1UaYPiaV7H8dF6ngMxF.jpeg', 'files/1/building_file/house_map/kX7WNcpnPSpQ38UyHnWuLOxRaTL90hxZPn3U8xzP.pdf', 'files/1/building_file/naamsari/KL2pZrpeRtq3YtqBb8txcaxIafEDMKCH0PdiTdrw.jpeg', 'files/1/building_file/ward_sifaris/FSRl1ipQLPrnhTdeERQMdr7PfbeBL19pdRilxfJB.jpeg', NULL, 'files/1/building_file/other_document_1/XEYl5eL9MIMAbMAEpPkf2QdlCFbkoR7oLQSbdnyL.pdf', 'STRUCTURAL REPORT', '', NULL, '', NULL),
(2, 3, 'files/3/building_file/lalpurja/zAwJX36GN8CSqVBu26VYAzm76ZnQLw7s7bPBBdHb.jpeg', 'files/3/building_file/citizenship/ZCco6EHGhGsnqrfh7jGneig87GzDPDUmHII3lYDx.png', 'files/3/building_file/blueprint/DRUpuS87zCqAG800NdFRCn4iJFNmneNcnKz1qrYd.jpeg', 'files/3/building_file/tero_rashid/ZaCGoAXRBbxUclE7JmHEvosRyaijDJZA4hqhL4H4.jpeg', 'files/3/building_file/house_map/xst3JmyuSVJP7yJ0PGbiSaFyxfZBZk3yuA7jMPK4.pdf', 'files/3/building_file/naamsari/cS5RFOJO9coquLVWWrUN06iEGgEN19kg3fvEuJk4.jpeg', '', NULL, '', NULL, '', NULL, '', NULL),
(3, 4, 'files/4/building_file/lalpurja/2e4Z2VgfCzHr3twohQvswhjZ59W0F3fRuMo6kw4Q.jpeg', 'files/4/building_file/citizenship/dtjZOOoxfymEUIEcqgHLbzmwBeY093t7exSYQSKq.png', 'files/4/building_file/blueprint/2tdgJkeb9Z7n54wGCXvjP9Mpns8ihMeVvI0TqA8o.jpeg', 'files/4/building_file/tero_rashid/iut6it31PqKHaNj6bB7Jb0gifr4799M7pRYWHXKP.jpeg', 'files/4/building_file/house_map/RztCOiyORz96mf3lWCn0cjfWWCOf4XiFhotobzXL.pdf', 'files/4/building_file/naamsari/JR7junGNwfhBBtJNLrswvQoZoyAFh9x7z0eFz8aE.jpeg', 'files/4/building_file/ward_sifaris/MzLHy47b3IesZgecyOUqbl365g6fmMf7NRchrdiF.jpeg', NULL, 'files/4/building_file/other_document_1/fALG8Yq7UXLIgorJQIHm4gkg49bZcyREBu7X37kj.pdf', 'Consectetur eaque ut', 'files/4/building_file/other_document_2/2nGkYFNxPoqVDJRLZxmNvhZcVbU46fC39ScPXqPX.jpeg', 'Facere quidem ex del', 'files/4/building_file/other_document_3/HL7mbikr5Q3amsy4Zu7dyhvcEc5d2AZqbD78Bb2V.jpeg', 'Rem nesciunt volupt'),
(4, 5, 'files/5/building_file/lalpurja/pn10mSpPorVRMnsr26kabhpUFcT6bxVRlN4CBwIP.png', 'files/5/building_file/citizenship/2oyCvTwRFfc3NwagptKzOWVWHmqkSExEmBTXlV4n.png', 'files/5/building_file/blueprint/cMN8UlwMaj2RiJM816Udw4jof9TY7cUIZit3HVaT.jpeg', 'files/5/building_file/tero_rashid/RMR1OxOonxb4dQfWwjYIdbQoPOSHcfBG3403yG2m.jpeg', 'files/5/building_file/house_map/3rP2b78ut9M44vRGTmLara1Pdz3whAxL4swRajWi.pdf', '', '', NULL, '', NULL, '', NULL, '', NULL),
(5, 6, 'files/6/building_file/lalpurja/IX3Z0rQDqVtUYVNL9rPbS22zyZAIPC19wY4dkT3Q.jpeg', 'files/6/building_file/citizenship/1smo4iaknswkTZXnrFdPYBzQUPpjJupbvEX0e69q.jpeg', 'files/6/building_file/blueprint/cGO73vsg4awZmntKwjH0XsjG5y79ipb6ryUjizEk.jpeg', 'files/6/building_file/tero_rashid/PfkjBkYjrysNnTS2nvB4gLLNlfTPUpQaKk9qeTIF.jpeg', 'files/6/building_file/house_map/0fKjEmHfsn4RgNUanLyeYjbQdRf1ATNvhVmbcD95.pdf', 'files/6/building_file/naamsari/Z9U6LbDpAuvrckvs7HNfSkyliWyRiECYMhLCJwvV.png', 'files/6/building_file/ward_sifaris/QYop9a65Ht0pxLNvwDgtjE8Xels31rQf990isnuJ.png', NULL, '', NULL, '', NULL, '', NULL),
(6, 7, 'files/7/building_file/lalpurja/OM8Xdf1Wi6QxS98egxZNK0eRhZXJhRrf5wIFkwLj.png', 'files/7/building_file/citizenship/jH66aozf3oQl08c0MVWVSB13EgkEfCJpYeQi0lLI.png', 'files/7/building_file/blueprint/8u4AvySTxK0Z0cwwusLpksorPoj4WYM0fQdZzLZs.jpeg', 'files/7/building_file/tero_rashid/RY5nwMhvBiIjN74JL5tLADbgnmthSX8gPJbTQFMI.jpeg', 'files/7/building_file/house_map/sf2EEzYZlE6Ctt6R0McKI8zJiZLPntjlVcN7A7jc.pdf', '', '', NULL, '', NULL, '', NULL, '', NULL),
(7, 8, 'files/8/building_file/lalpurja/znhbVsuxTUeJ154NO3WMtoiTffHTWlla11kSrnbf.png', 'files/8/building_file/citizenship/GRbrkZirNVcNla8A6UM0SAMwKCFk0U3sCnMcfSN4.png', 'files/8/building_file/blueprint/tfMZ6GqeKGZ6wqqErM36d2PQNdrVv2NTageUcno3.jpeg', 'files/8/building_file/tero_rashid/8V4Tyb2WYuVJp24wY3JoBFevpYMh4qpk3Sw24yVY.jpeg', 'files/8/building_file/house_map/N46LZgmMWl1OF1YfTVkjwNrarWFPX7JN4O4TMCB1.pdf', '', '', NULL, '', NULL, '', NULL, '', NULL),
(8, 9, 'files/9/building_file/lalpurja/4LI2TvknaP7I9CkD8a8aRlp3Eligr9TQhhcw1I2m.png', 'files/9/building_file/citizenship/G3UrGecT47ouEUYnAstQvsovNvBU1tKQNA7SbDqZ.png', 'files/9/building_file/blueprint/Ge3YX9JG27lKrh2aweGRkcwOXVcHWO13RO8yriZH.jpeg', 'files/9/building_file/tero_rashid/WpDvkuTHUDuxV3g5TAcC0DOZtBNXBdUgR3pk8aK5.png', 'files/9/building_file/house_map/JypwiG5g5kDcoPDZ1AFNysBry8DQdJ5EnhuS1VNF.pdf', '', '', NULL, '', NULL, '', NULL, '', NULL),
(9, 10, 'files/10/building_file/lalpurja/S6Q7z75psjZFa6RZFpXGhqtvh29P3Kogqey75RXv.png', 'files/10/building_file/citizenship/GnwZoxwkgiliKQoCbpHvGCvxiCiIehq8NlAiBwsa.png', 'files/10/building_file/blueprint/iu2bQdDKbkygkDJsXYmpLvyaSRW8hWPETz63dRwR.jpeg', 'files/10/building_file/tero_rashid/mWV61NJIYHwpl52NKunopShosztsY8kuwjYNbHgs.jpeg', 'files/10/building_file/house_map/9SRafhy2kF903065SAoY7zDznzAHYGTbDb2cGnyS.pdf', 'files/10/building_file/naamsari/Gx4HygdtEMWeSzv3QqBO7fJH32lKV6mo2KVh1FXL.jpeg', 'files/10/building_file/ward_sifaris/o0WlCcqEzQPECHGBcO4raxK7uBXPbTEQZjO1sSoc.jpeg', NULL, '', NULL, '', NULL, '', NULL),
(10, 11, 'files/11/building_file/lalpurja/PnmLzBDUQ0eB2Gocj1vt5tVq3pawyJQ1oHRiHV3e.jpeg', 'files/11/building_file/citizenship/M4elLqNPIAIwCMQjTUB9EgF5HC7AanLAa0wgkKrs.jpeg', 'files/11/building_file/blueprint/2YTjGpdbwPAUoPSzgN2WAvjzx2XGGdBVjt2pkYZO.jpeg', 'files/11/building_file/tero_rashid/rSDUMTivEwtRdkRqhLFAtcxl4j1IEnHdMMsZSEvE.png', 'files/11/building_file/house_map/GDSsJjigPJz9ggRQ7VbFOPwmcgrrMsCAnUJ43Xs3.pdf', '', '', NULL, '', NULL, '', NULL, '', NULL),
(11, 12, 'files/12/building_file/lalpurja/oNU2kJyWmXg6F8s3Ocksm9EjiNL9pDnLktB76z2M.jpeg', 'files/12/building_file/citizenship/xSy6ZgEW4V64NVgm5PpRXjJuK9woZjHmOr8QCK4L.jpeg', 'files/12/building_file/blueprint/RQavsR7KPOCr0VdeEiyPIlYU131qSEiqRxfYKM0l.jpeg', 'files/12/building_file/tero_rashid/kmCWiwFg73jHMjOAMM9DHFKQnaTHeG3XCXHzZw6v.png', 'files/12/building_file/house_map/kCKh9ZeYZGj7nt8oRfY7msi9ONaImYklvZzt0ZKp.pdf', '', '', NULL, '', NULL, '', NULL, '', NULL),
(12, 13, 'files/13/building_file/lalpurja/iBBcnDgZyFccDXGqwSvrCHbi0WMt2JuymQXr2flS.png', 'files/13/building_file/citizenship/LAVjHdPT4NHBaxfzDpM06ruYWv2qM6A6VnHu22MN.jpeg', 'files/13/building_file/blueprint/eWCfpijTZXxJ24k2iI04eN5WeB3Mj8mbRqQLek8g.jpeg', 'files/13/building_file/tero_rashid/iuf08fz0tHrdPJkX6tZOwr6eSkjWFEJgYs5lnNII.jpeg', 'files/13/building_file/house_map/NmYa4j4a04w03rJstEswLZ0cz1JyzkWWZMN5sgEH.pdf', '', '', NULL, '', NULL, '', NULL, '', NULL),
(13, 14, 'files/14/building_file/lalpurja/T0ZbrWt2YVMmhX6a7tJ0DxFElEzlNANLSDxW0kwU.jpeg', 'files/14/building_file/citizenship/DBwfBYjNbfq3jEasbcMisMoeEC3RZ46VhNtP1B9r.jpeg', 'files/14/building_file/blueprint/SgabDIncpwwHvlaMzcE5XOTWwbSPWyKc6XH5b4Dp.png', 'files/14/building_file/tero_rashid/vpO9SDMhSnxeQ0aOKiSoPgFnm8VEsq77l6UXgLDi.jpeg', 'files/14/building_file/house_map/Y7Yld8WnB6eU6VJ61akEv7CJ63x9xrgeFt71lWHU.pdf', 'files/14/building_file/naamsari/6c7H1foetghCILKTWT7OZc77ZcmXwfZmScJ0yFNg.png', 'files/14/building_file/ward_sifaris/06qDWMZUx51FKgmqcPCMiKATZ1b5MevCA7nTJsrh.png', NULL, '', NULL, '', NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_buildinginfo`
--

CREATE TABLE `form_buildinginfo` (
  `buildinginfo_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `building_category` varchar(50) NOT NULL,
  `building_structure` varchar(50) NOT NULL,
  `plith_height` varchar(50) NOT NULL,
  `foundation_size` varchar(50) NOT NULL,
  `pillar_size` varchar(50) NOT NULL,
  `pillar_rod_size` varchar(50) NOT NULL,
  `pillar_rod_number` varchar(50) NOT NULL,
  `pillar_churi_size` varchar(50) NOT NULL,
  `left_right_chadne_duri` varchar(50) NOT NULL,
  `setback` varchar(50) NOT NULL,
  `east_border` varchar(50) NOT NULL,
  `west_border` varchar(50) NOT NULL,
  `north_border` varchar(50) NOT NULL,
  `south_border` varchar(50) NOT NULL,
  `building_length` varchar(50) NOT NULL,
  `building_breadth` varchar(20) NOT NULL,
  `building_height` varchar(50) NOT NULL,
  `number_of_room` varchar(50) NOT NULL,
  `number_of_door` varchar(50) NOT NULL,
  `number_of_window` varchar(50) NOT NULL,
  `number_of_satar` varchar(50) NOT NULL,
  `number_of_bathroom` varchar(50) NOT NULL,
  `number_of_channel_gate` varchar(50) NOT NULL,
  `septic_tank` varchar(50) NOT NULL,
  `roof` varchar(50) NOT NULL,
  `jodai` varchar(50) NOT NULL,
  `building_purpose` varchar(50) NOT NULL,
  `house_owner_name` varchar(255) NOT NULL,
  `citizenship_number` varchar(50) NOT NULL,
  `district` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `ground_area_coverage` varchar(255) NOT NULL,
  `floor_area_ratio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_buildinginfo`
--

INSERT INTO `form_buildinginfo` (`buildinginfo_id`, `form_id`, `building_category`, `building_structure`, `plith_height`, `foundation_size`, `pillar_size`, `pillar_rod_size`, `pillar_rod_number`, `pillar_churi_size`, `left_right_chadne_duri`, `setback`, `east_border`, `west_border`, `north_border`, `south_border`, `building_length`, `building_breadth`, `building_height`, `number_of_room`, `number_of_door`, `number_of_window`, `number_of_satar`, `number_of_bathroom`, `number_of_channel_gate`, `septic_tank`, `roof`, `jodai`, `building_purpose`, `house_owner_name`, `citizenship_number`, `district`, `phone`, `ground_area_coverage`, `floor_area_ratio`) VALUES
(1, 1, 'क', 'फ्रेम', '२\'-६\"', '५\'-६\"*५\'-६\'\' *५\'-६\'\'', '१२\"*१२\"', '८-१६ यम यम /४-१२ यम यम + ४-१६ यम यम', '८', '८ यम यम', '१०\'', '२मी', 'सन्दिप  खनाल', 'साखा  पिच', 'लक्ष्मी आचार्य', 'अन्छुल  मिया', '४३\'-०\"', '३०\'-०\"', '२२\'-६\"', '१०', '१४', '१०', 'छैन', '२', 'छैन', 'सेफ्टी ट्यांकी', 'आरसिसि', 'सिमेन्ट', 'आवासीय', 'जलिल  मिया', '३४३०२५/११०', 'चितवन', '९८६५१७१२७२', '०.३५', '०.७०'),
(2, 2, 'ग', 'फ्रेम', '३\'-० \"', '५\'६ \"*५\'६ \"', '१२\"*१२\'\'', '४-१२,४ -१६ यम यम', '८', '८ यम यम', '६\'-४\'\'', '६ +२ मीटर', 'सुर्य लम्साल', 'ग्राबेल बाटो', 'कमला थापा', 'कमला  अधिकारी', '२८\'', '२७\'', '२३\'-० \"', '६', '१२', '८', 'छैन', '२', 'छैन', 'सेफ्टी ट्यांकी', 'आरसिसि', 'सिमेन्ट', 'आवासीय', 'सरिता पौडेल', '३५३०२० /२५०', 'चितवनसुर्य लम्साल', '९८४५१४०६८५', '०.२९', '०.५८'),
(3, 3, 'घ', 'वाल', 'Quaerat animi rem v', 'Ab eius velit ad om', 'Nulla est libero sae', 'Eum omnis eaque perf', '549', 'Mollitia minim conse', 'Proident omnis vita', 'Obcaecati eaque anim', 'Et similique veritat', 'Quia quia molestiae', 'At eu nisi qui volup', 'Irure assumenda eu e', 'Ratione sunt volupta', 'Iste corrupti debit', 'Amet adipisicing re', '909', '186', '133', 'छैन', '364', 'छैन', 'सेफ्टी ट्यांकी', 'अन्य', 'सिमेन्ट', 'शैक्षिक', 'Ivana Humphrey', '290', 'Architecto quis dese', '+1 (959) 148-1726', 'Odio minima vel assu', 'Consectetur id rerum'),
(4, 4, 'घ', 'वाल', 'Officiis quis evenie', 'Et ut hic repudianda', 'Ea consequatur Rem', 'Illum sint id aut a', '592', 'Culpa corporis moles', 'Ab tempore accusant', 'Qui sit ea molestia', 'Ea et voluptas odio', 'Consectetur dolor il', 'Voluptas nostrud quo', 'Vel consectetur dict', 'Sed sapiente qui dol', 'Nihil non quam qui n', 'Dolor amet consequa', '339', '799', '125', 'छैन', '705', '०३', 'सेफ्टी ट्यांकी', 'ट्रस', 'अन्य', 'शैक्षिक', 'Emi Garza', '845', 'Laboriosam dolores', '+1 (926) 225-4842', 'Aute praesentium opt', 'Laboris dolor libero'),
(5, 5, 'क', 'फ्रेम', 'Eaque ea ipsum dict', 'Adipisci rem ullamco', 'Culpa velit dolor', 'Iste qui obcaecati u', '237', 'Alias dolore sunt l', 'Mollit illo voluptat', 'Itaque unde labore v', 'Quis autem reprehend', 'Tempore reprehender', 'Sit rerum vero maior', 'Mollit irure praesen', 'Illum molestiae lau', 'Consequuntur est fa', 'Explicabo Exercitat', '492', '240', '283', '०५', '849', '०३', 'सेफ्टी ट्यांकी', 'आरसिसि', 'अन्य', 'औध्योगिक', 'Sean Hendrix', '561', 'Odit labore reprehen', '+1 (616) 155-3001', 'Tempor laboris asper', 'Velit eu excepturi'),
(6, 6, 'क', 'फ्रेम', 'Illo nemo repellendu', 'Nihil nisi quos moll', 'Repellendus Eveniet', 'In suscipit non reru', '278', 'Molestiae eveniet i', 'Aute error labore ea', 'Aliquip distinctio', 'Hic aliquid eum cons', 'Adipisci optio minu', 'Illum sed sed accus', 'Molestiae dolor eos', 'Vero qui sint aut v', 'Incididunt Nam odio', 'Proident lorem aut', '488', '28', '50', 'छैन', '59', '०३', 'सेफ्टी ट्यांकी', 'अन्य', 'सिमेन्ट', 'अन्य', 'Maggie Singleton', '654', 'Asperiores qui adipi', '+1 (774) 348-9422', 'Eos doloribus archit', 'Consequatur id molli'),
(7, 7, 'क', 'वाल', 'Cum quibusdam labori', 'In sit molestias dol', 'In ex incidunt pers', 'Cillum et aperiam et', '190', 'Corrupti voluptatum', 'Qui sed do laborum e', 'Excepturi nisi volup', 'Est nulla dolore mol', 'Sed enim adipisicing', 'Magna atque et in re', 'Nihil elit dolor qu', 'Consectetur perferen', 'Sequi cupiditate ass', 'Nam perspiciatis li', '490', '671', '60', '०४', '613', '०३', 'सेफ्टी ट्यांकी', 'ट्रस', 'माटो', 'अन्य', 'Penelope Nelson', '832', 'Dolorem assumenda lo', '+1 (233) 292-1323', 'Amet perferendis ei', 'Ipsum praesentium c'),
(8, 8, 'क', 'वाल', 'Id placeat aliquid', 'Sunt ut illo quod es', 'Laboris dolor sunt', 'Veritatis commodo iu', '508', 'Consequatur Mollit', 'Aut labore dolor Nam', 'Mollit aliquip nesci', 'Consectetur Nam dol', 'Omnis ea ipsam quisq', 'Modi et quod asperio', 'Dolor consequatur E', 'Deleniti ratione nec', 'Id nostrum distincti', 'Aliquid voluptas tot', '711', '551', '264', '०२', '180', '०५', 'सेफ्टी ट्यांकी', 'ट्रस', 'माटो', 'सार्वजनिक', 'Gemma Clarke', '995', 'Commodo dolores hic', '+1 (618) 301-7487', 'Ut nisi unde similiq', 'Duis hic voluptas ar'),
(9, 9, 'ग', 'वाल', 'Similique qui dicta', 'Quos ullamco qui vol', 'Dolor beatae elit n', 'Suscipit totam aliqu', '754', 'Nulla sint distincti', 'Tempora quia reprehe', 'Eos ducimus quisqu', 'Vel fugit labore ne', 'Id eum anim sint te', 'Asperiores hic quam', 'Natus autem autem au', 'Labore magna est ull', 'Nisi aut ipsam perfe', 'Minima tempora itaqu', '833', '565', '998', '०१', '369', '०२', 'सेफ्टी ट्यांकी', 'आरसिसि', 'माटो', 'औध्योगिक', 'April Adams', '953', 'Duis cupidatat sit v', '+1 (341) 711-8765', 'Excepturi sit quia u', 'Nulla nisi nihil off'),
(10, 10, 'ग', 'फ्रेम', 'Ex quod repudiandae', 'Omnis magna maiores', 'Eum sunt voluptate a', 'Voluptates earum sit', '562', 'Voluptate sunt duis', 'Autem lorem deserunt', 'Incididunt elit imp', 'Numquam pariatur Au', 'Porro deserunt eos', 'Consequuntur nihil e', 'Mollit qui optio qu', 'Tempora soluta sunt', 'Qui esse voluptas s', 'Modi odit deserunt i', '722', '547', '356', '०४', '383', 'छैन', 'सेफ्टी ट्यांकी', 'जस्ता', 'सिमेन्ट', 'शैक्षिक', 'Kai Mcclure', '701', 'Quis Nam et incididu', '+1 (114) 359-5316', 'Repudiandae esse et', 'Inventore recusandae'),
(11, 11, 'ग', 'फ्रेम', 'Placeat accusamus m', 'Consequat Molestiae', 'Ipsa cumque repudia', 'Repellendus Sunt al', '992', 'Consectetur eveniet', 'Cum eiusmod ab ab ne', 'Doloribus pariatur', 'Doloremque aute non', 'Eos aut nobis except', 'Mollitia vero iure f', 'Nesciunt nulla volu', 'Saepe dicta odit del', 'Dignissimos omnis te', 'Cillum aliqua Est', '848', '448', '297', '०४', '633', '०४', 'सेफ्टी ट्यांकी', 'ट्रस', 'सिमेन्ट', 'शैक्षिक', 'Kay Stanley', '59', 'Est velit laboris e', '+1 (141) 256-3872', 'Nisi eu corporis est', 'Nam molestiae et ut'),
(12, 12, 'ग', 'वाल', 'Illo esse labore au', 'Temporibus sed labor', 'Necessitatibus ut pa', 'Dolor sed amet elit', '630', 'Culpa consequatur i', 'Qui rerum modi volup', 'Dolorem sed pariatur', 'Quis elit sed quide', 'Vel rem voluptatem h', 'Alias beatae exceptu', 'Sed sed quis invento', 'Sit non nostrud in f', 'Dolore dolor eum qua', 'Sed animi nihil pra', '656', '807', '718', '०१', '158', '०५', 'सेफ्टी ट्यांकी', 'आरसिसि', 'सिमेन्ट', 'आवासीय', 'Lillian Hansen', '819', 'Commodo duis similiq', '+1 (976) 764-8678', 'Quae dolorem quis al', 'Rerum cillum volupta'),
(13, 13, 'ख', 'वाल', 'Qui molestiae est qu', 'Quo aliquip consequu', 'Odio quis laborum B', 'Voluptatem blanditii', '907', 'Qui sint est duis ve', 'Veritatis ut non qui', 'Rem duis qui rem pro', 'Natus harum nisi ex', 'Temporibus voluptate', 'Consequatur dolore d', 'Consequuntur in volu', 'Perspiciatis quo eo', 'Qui Nam anim consect', 'Labore adipisicing p', '824', '970', '405', '०४', '631', 'छैन', 'सेफ्टी ट्यांकी', 'अन्य', 'माटो', 'शैक्षिक', 'Merrill Green', '632', 'Perspiciatis saepe', '+1 (817) 306-1722', 'Ducimus ut nihil la', 'Blanditiis eos id e'),
(14, 14, 'ग', 'वाल', 'Modi esse sed quo c', 'Reprehenderit dolor', 'Eum sit recusandae', 'Nisi atque ipsam pro', '453', 'In sapiente impedit', 'Nisi ab velit tempor', 'Quidem ut veniam pa', 'Fugiat in esse enim', 'Dolor excepteur eum', 'In sit officia conse', 'Dignissimos earum qu', 'Nulla velit vel quia', 'Ducimus doloremque', 'Sunt magni eveniet', '649', '374', '743', '०१', '518', '०१', 'सेफ्टी ट्यांकी', 'जस्ता', 'सिमेन्ट', 'औध्योगिक', 'Dylan Stanley', '137', 'Irure vitae voluptat', '+1 (336) 278-8022', 'Do qui illo dolor te', 'Ut nihil et suscipit');

-- --------------------------------------------------------

--
-- Table structure for table `form_floorinfo`
--

CREATE TABLE `form_floorinfo` (
  `no_of_floor` int(11) NOT NULL,
  `floorinfo_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `underground_floor_area` float NOT NULL,
  `ground_floor_area` float NOT NULL,
  `first_floor_area` float DEFAULT NULL,
  `second_floor_area` float DEFAULT NULL,
  `third_floor_area` float DEFAULT NULL,
  `fourth_floor_area` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_floorinfo`
--

INSERT INTO `form_floorinfo` (`no_of_floor`, `floorinfo_id`, `form_id`, `underground_floor_area`, `ground_floor_area`, `first_floor_area`, `second_floor_area`, `third_floor_area`, `fourth_floor_area`) VALUES
(2, 1, 1, 0, 1290, 1290, NULL, NULL, NULL),
(2, 2, 2, 0, 756, 756, NULL, NULL, NULL),
(5, 3, 3, 12, 23, 34, 45, 576, 45),
(3, 4, 4, 12, 32, 34, 12, NULL, NULL),
(2, 5, 5, 33, 32, 34, NULL, NULL, NULL),
(5, 6, 6, 42, 23, 43, 234, 434, 23),
(2, 7, 7, 12, 123, 123, NULL, NULL, NULL),
(2, 8, 8, 12, 23, 23, NULL, NULL, NULL),
(1, 9, 9, 12, 21, NULL, NULL, NULL, NULL),
(2, 10, 10, 123, 3212, 123, NULL, NULL, NULL),
(1, 11, 11, 1223, 123, NULL, NULL, NULL, NULL),
(1, 12, 12, 12, 21, NULL, NULL, NULL, NULL),
(2, 13, 13, 32, 123, 21, NULL, NULL, NULL),
(2, 14, 14, 123, 3213, 123, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_landinfo`
--

CREATE TABLE `form_landinfo` (
  `landinfo_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `ward_number` varchar(255) NOT NULL,
  `sabik_ward_number` varchar(255) NOT NULL,
  `current_ward_number` varchar(255) NOT NULL,
  `kitta_number` varchar(255) NOT NULL,
  `sabik_kitta_number` varchar(255) NOT NULL,
  `field_area` varchar(255) NOT NULL,
  `village_name` varchar(255) NOT NULL,
  `sabik_kittama_ghar` varchar(255) NOT NULL,
  `haak_pugeko` varchar(255) NOT NULL,
  `jagga_mohada` varchar(255) NOT NULL,
  `electricity_line` varchar(255) NOT NULL,
  `river` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_landinfo`
--

INSERT INTO `form_landinfo` (`landinfo_id`, `form_id`, `ward_number`, `sabik_ward_number`, `current_ward_number`, `kitta_number`, `sabik_kitta_number`, `field_area`, `village_name`, `sabik_kittama_ghar`, `haak_pugeko`, `jagga_mohada`, `electricity_line`, `river`) VALUES
(1, 1, '१३', '९  ख', '१', '१७४८,१७४७', 'पन्चकन्या -९ ख', '३३८.६ वर्ग.मि', 'साखा पिच बाटो', 'नभएको', 'पुस्तौनी', 'पक्षिम्', 'नभएको', 'नभएको'),
(2, 2, 'र न पा -१०', 'पन्चकन्य /६क', '१०', '२२९५', '२२९५', '०-०-१४', 'सिबालय  टोल', 'नभएको', 'राजिनामा', 'पच्छिम', 'नभएको', 'नभएको'),
(3, 3, '982', '499', '१६', '362', '822', 'Qui repudiandae expl', 'Hillary Prince', 'भएको', 'गुठी', 'Molestiae vel quia o', 'भएको', 'कुलो'),
(4, 4, '32', '103', '१', '713', '438', 'Deserunt et fugit q', 'MacKensie Mcpherson', 'भएको', 'अंशबण्डा', 'Nisi expedita volupt', 'नभएको', 'नहर'),
(5, 5, '496', '789', '७', '572', '991', 'Minus voluptatem lab', 'Emerald Dawson', 'भएको', 'पुस्तौनी', 'Enim aut tempor reru', 'भएको', 'नभएको'),
(6, 6, '611', '849', '१४', '737', '903', 'Ipsa corporis ex am', 'Harlan Glover', 'नभएको', 'राजिनामा', 'Doloremque est amet', 'भएको', 'नदी'),
(7, 7, '881', '392', '९', '550', '680', 'Rerum minus explicab', 'Germane Hamilton', 'नभएको', 'अंशबण्डा', 'Saepe aut saepe sit', 'नभएको', 'नदी'),
(8, 8, '862', '245', '३', '189', '935', 'Error dolore consequ', 'Stephanie Farrell', 'भएको', 'पुस्तौनी', 'Sequi non ut ullam i', 'भएको', 'नभएको'),
(9, 9, '208', '210', '१४', '507', '936', 'Mollit ipsam omnis c', 'Jena Grimes', 'भएको', 'बकसपत्र', 'Nam aliqua Ad quos', 'नभएको', 'नहर'),
(10, 10, '362', '453', '१४', '505', '623', 'Harum et et nihil ha', 'Farrah Noble', 'नभएको', 'अंशबण्डा', 'Odio voluptates qui', 'नभएको', 'नभएको'),
(11, 11, '528', '38', '१५', '405', '100', 'Eius rem exercitatio', 'Urielle Allison', 'नभएको', 'राजिनामा', 'Omnis temporibus exc', 'नभएको', 'नभएको'),
(12, 12, '179', '433', '५', '473', '133', 'Itaque voluptas null', 'Rahim Shaffer', 'भएको', 'हाल साविक', 'Quia laudantium nul', 'भएको', 'नहर'),
(13, 13, '629', '786', '११', '793', '410', 'Doloribus sed aut mi', 'Dustin Schroeder', 'नभएको', 'गुठी', 'Laboriosam est aut', 'भएको', 'कुलो'),
(14, 14, '13', '552', '१२', '999', '504', 'Tempora ullamco qui', 'Maxwell Cleveland', 'भएको', 'बकसपत्र', 'Illo tempore ipsum', 'नभएको', 'नहर');

-- --------------------------------------------------------

--
-- Table structure for table `form_personalinfo`
--

CREATE TABLE `form_personalinfo` (
  `id` int(11) NOT NULL,
  `sambandha_ma` varchar(50) CHARACTER SET utf8 NOT NULL,
  `option_1` varchar(50) CHARACTER SET utf8 NOT NULL,
  `option_2` varchar(50) CHARACTER SET utf8 NOT NULL,
  `building_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `building_type_slug` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sambodhan` varchar(50) CHARACTER SET utf8 NOT NULL,
  `field_owner_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `field_owner_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `field_owner_age` varchar(50) CHARACTER SET utf8 NOT NULL,
  `field_owner_job` varchar(255) CHARACTER SET utf8 NOT NULL,
  `father_husband_option` varchar(50) CHARACTER SET utf8 NOT NULL,
  `father_husband_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `grandfather_option` varchar(50) CHARACTER SET utf8 NOT NULL,
  `grandfather_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `form_level` int(11) NOT NULL,
  `proceeded_to` varchar(20) NOT NULL,
  `is_rejected` int(11) NOT NULL,
  `rejected_message` text CHARACTER SET utf8,
  `rejected_by` varchar(11) DEFAULT NULL,
  `rejected_at` timestamp NULL DEFAULT NULL,
  `is_draft` int(11) NOT NULL,
  `is_completed` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at_nepali` varchar(50) CHARACTER SET utf8 NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `darta_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_personalinfo`
--

INSERT INTO `form_personalinfo` (`id`, `sambandha_ma`, `option_1`, `option_2`, `building_type`, `building_type_slug`, `sambodhan`, `field_owner_name`, `field_owner_address`, `field_owner_age`, `field_owner_job`, `father_husband_option`, `father_husband_name`, `grandfather_option`, `grandfather_name`, `form_level`, `proceeded_to`, `is_rejected`, `rejected_message`, `rejected_by`, `rejected_at`, `is_draft`, `is_completed`, `created_by`, `created_at`, `created_at_nepali`, `updated_at`, `updated_by`, `darta_number`) VALUES
(1, 'मैले', 'छु', 'गर्दछु', 'नयाँ घर निर्माण', 'new_building', 'श्री', 'जलिल  मिया', 'र न पा १३', '५७', 'कृषि', 'बाबु', 'अशिन मिया', 'बाजे', 'वालु मिया', 6, 'consultancy', 0, NULL, NULL, NULL, 0, 0, 31, '2019-06-03 07:05:33', 'सोम, जेठ २०, २०७६', '2019-06-10 12:02:36', 1, '1930601195'),
(2, 'मैले', 'छु', 'गर्दछु', 'नयाँ घर निर्माण', 'new_building', 'सुश्री', 'सरिता पौडेल', 'र न पा-१०', '३५', 'जागिर', 'बाबु', 'रामचन्द्र पौडेल', 'बाजे', 'खगेशोर', 0, 'consultancy', 0, NULL, NULL, NULL, 1, 0, 22, '2019-06-04 09:09:31', 'मंगल, जेठ २१, २०७६', '2019-06-04 09:27:09', NULL, NULL),
(3, 'मैले', 'छु', 'गर्दछु', 'नयाँ घर निर्माण', 'new_building', 'श्री', 'राम प्रदास रिमाल', 'नेपाल', '४६', 'घर', 'बाबु', 'नाम द्ज्स्फझ', 'बाजे', 'नेपाल', 16, 'consultancy', 0, '', NULL, NULL, 0, 1, 1, '2019-06-04 11:14:42', 'बुध, जेठ २२, २०७६', '2019-06-10 09:59:10', 1, '191006023'),
(4, 'हामीले', 'छौं', 'गर्दछौं', 'नयाँ घर निर्माण', 'new_building', 'श्री', 'Aphrodite Fowler', 'Ea amet excepturi d', 'Labore eu ad est ni', 'Eiusmod aut totam ne', 'पति', 'Sybill Wyatt', 'बाजे', 'Thor Farmer', 3, 'sub-engineer', 0, NULL, NULL, NULL, 0, 0, 1, '2019-06-10 12:09:09', 'सोम, जेठ २७, २०७६', '2019-06-27 06:19:10', 34, '191006054'),
(5, 'मैले', 'छु', 'गर्दछु', 'नयाँ घर निर्माण', 'new_building', 'सुश्री', 'Flavia Rich', 'Quod perferendis sit', 'Veniam expedita sit', 'Excepturi consequatu', 'पति', 'Irene Barton', 'ससुरा', 'Quentin Delgado', 16, 'consultancy', 0, '', NULL, NULL, 0, 1, 1, '2019-06-11 04:02:18', 'मंगल, जेठ २८, २०७६', '2019-06-11 07:33:32', 1, '191106095'),
(6, 'मैले', 'छु', 'गर्दछु', 'तल्ला थप', 'add_floor', 'श्री', 'Ferris Cain', 'Voluptate rerum ea e', 'At quis ipsum ea qu', 'Ut illum delectus', 'बाबु', 'Jaden Stokes', 'बाजे', 'Brady Peck', 16, 'consultancy', 0, '', NULL, NULL, 0, 1, 20, '2019-06-11 08:22:23', 'मंगल, जेठ २८, २०७६', '2019-06-11 08:40:27', 17, '191106026'),
(7, 'हामीले', 'छौं', 'गर्दछौं', 'तल्ला थप', 'add_floor', 'सुश्री', 'Marvin Sandoval', 'Inventore est quia', 'Molestias perspiciat', 'Culpa molestias quod', 'पति', 'Darius Delacruz', 'बाजे', 'Tanya England', 1, 'engineer', 0, NULL, NULL, NULL, 0, 0, 20, '2019-06-12 05:42:48', 'बुध, जेठ २९, २०७६', '2019-06-12 05:50:00', NULL, '191206117'),
(8, 'हामीले', 'छौं', 'गर्दछौं', 'तल्ला थप', 'add_floor', 'श्री', 'Fitzgerald Richmond', 'Deserunt sed iusto h', 'Nihil magni voluptat', 'Ullam omnis sed sunt', 'बाबु', 'Iris Hardy', 'बाजे', 'Robert Good', 2, 'aamin', 0, NULL, '1', NULL, 0, 0, 1, '2019-06-12 06:50:28', 'बुध, जेठ २९, २०७६', '2019-06-12 07:32:15', 1, '191206128'),
(9, 'मैले', 'छु', 'गर्दछु', 'तल्ला थप', 'add_floor', 'श्री', 'Hiroko Barlow', 'Laboris ea aute nemo', 'Hic qui delectus se', 'Distinctio Iusto od', 'बाबु', 'Britanni Holcomb', 'बाजे', 'Linus Anthony', 1, 'engineer', 0, NULL, NULL, NULL, 0, 0, 1, '2019-06-12 06:56:05', 'बुध, जेठ २९, २०७६', '2019-06-12 09:02:56', NULL, '191206029'),
(10, 'हामीले', 'छौं', 'गर्दछौं', 'नयाँ घर निर्माण', 'new_building', 'सुश्री', 'Tanek Ashley', 'Reprehenderit aut f', 'Porro aut itaque quo', 'Perferendis aliquid', 'बाबु', 'Leslie Wood', 'ससुरा', 'Sara Quinn', 1, 'consultancy', 1, 'sadfsaf', '1', '2019-06-20 05:16:59', 0, 0, 20, '2019-06-20 05:15:39', 'बिही, असार ५, २०७६', '2019-06-20 05:16:59', NULL, '1920061110'),
(11, 'मैले', 'छु', 'गर्दछु', 'तल्ला थप', 'add_floor', 'श्री', 'Jennifer Dyer', 'Reprehenderit venia', 'Omnis consequatur I', 'Fugit quisquam cupi', 'पति', 'Zorita Hensley', 'ससुरा', 'Clio Pacheco', 3, 'sub-engineer', 0, NULL, NULL, NULL, 0, 0, 1, '2019-06-24 08:14:20', 'सोम, असार ९, २०७६', '2019-06-24 08:17:33', 1, '1924060111'),
(12, 'हामीले', 'छौं', 'गर्दछौं', 'नयाँ घर निर्माण', 'new_building', 'सुश्री', 'Isabella Jensen', 'Quaerat alias id om', 'Nostrum est ad rerum', 'Aperiam maxime repre', 'पति', 'Bianca Santos', 'ससुरा', 'Yoshio Horn', 2, 'aamin', 0, NULL, NULL, NULL, 0, 0, 1, '2019-07-01 07:34:04', 'सोम, असार १६, २०७६', '2019-07-01 07:35:30', 1, '191070112'),
(13, 'हामीले', 'छौं', 'गर्दछौं', 'नयाँ घर निर्माण', 'new_building', 'सुश्री', 'Nolan Waters', 'Ipsa aliquam aut eo', 'Culpa accusantium e', 'Ut sed dolor est vol', 'बाबु', 'Marvin Franks', 'बाजे', 'Ferdinand Shepard', 3, 'sub-engineer', 0, NULL, NULL, NULL, 0, 0, 1, '2019-07-01 07:38:03', 'सोम, असार १६, २०७६', '2019-07-01 07:46:22', 1, '191070113'),
(14, 'हामीले', 'छौं', 'गर्दछौं', 'तल्ला थप', 'add_floor', 'श्री', 'Scott Hudson', 'Enim reprehenderit e', 'Vitae eveniet cillu', 'Accusamus molestiae', 'पति', 'Ainsley Morrow', 'ससुरा', 'April Sears', 1, 'engineer', 0, NULL, NULL, NULL, 0, 0, 1, '2019-07-01 10:42:03', 'सोम, असार १६, २०७६', '2019-07-01 11:48:07', NULL, '191070514');

-- --------------------------------------------------------

--
-- Table structure for table `form_sampanna`
--

CREATE TABLE `form_sampanna` (
  `sampanna_id` int(20) NOT NULL,
  `form_id` int(11) NOT NULL,
  `field_area` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `site_plan_area` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ground_coverage` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ground_coverage_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ground_coverage_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ground_coverage_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `talla_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `talla_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `talla_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `height_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `height_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `height_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `floor_area_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `floor_area_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `floor_area_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `swikrit_vanda_badi_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `swikrit_vanda_badi_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `swikrit_vanda_badi_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `r_o_w_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `r_o_w_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `r_o_w_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `setback_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `setback_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `setback_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `electricity_line` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `electricity_line_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `electricity_line_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `electricity_line_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `river_side_mapdanda` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `river_side_swikrit_anusar` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `river_side_nirman_esthithi` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `entry_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `consultancy_post` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `consultancy_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_sampanna`
--

INSERT INTO `form_sampanna` (`sampanna_id`, `form_id`, `field_area`, `site_plan_area`, `ground_coverage`, `ground_coverage_mapdanda`, `ground_coverage_swikrit_anusar`, `ground_coverage_nirman_esthithi`, `talla_mapdanda`, `talla_swikrit_anusar`, `talla_nirman_esthithi`, `height_mapdanda`, `height_swikrit_anusar`, `height_nirman_esthithi`, `floor_area_mapdanda`, `floor_area_swikrit_anusar`, `floor_area_nirman_esthithi`, `swikrit_vanda_badi_mapdanda`, `swikrit_vanda_badi_swikrit_anusar`, `swikrit_vanda_badi_nirman_esthithi`, `r_o_w_mapdanda`, `r_o_w_swikrit_anusar`, `r_o_w_nirman_esthithi`, `setback_mapdanda`, `setback_swikrit_anusar`, `setback_nirman_esthithi`, `electricity_line`, `electricity_line_mapdanda`, `electricity_line_swikrit_anusar`, `electricity_line_nirman_esthithi`, `river_side_mapdanda`, `river_side_swikrit_anusar`, `river_side_nirman_esthithi`, `entry_name`, `consultancy_post`, `consultancy_name`, `created_by`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, 3, 'Dolor eum sed tempor', 'Mollit aut officia e', 'Adipisicing adipisci', 'Et ducimus quae eaq', 'Ex autem ipsam aut e', 'Nisi aperiam vitae e', 'In minima deserunt e', 'Nisi sint ut exercit', 'Eum dolore duis dolo', 'In enim omnis et mag', 'Commodo Nam adipisci', 'Sed facilis sunt tem', 'Nulla nihil proident', 'Veniam molestiae ha', 'Minima do et nemo ut', 'Eos porro dolore al', 'Ad et quia vero nece', 'Delectus lorem sequ', 'Debitis dolor magni', 'Vero sed blanditiis', 'Ratione duis consect', 'Alias impedit ullam', 'Id minim aut autem', 'Cupiditate placeat', 'Voluptatem a ad Nam', 'Ad quaerat eos nesc', 'Harum labore sit ip', 'Qui sit vel commodo', 'Iste excepturi verit', 'Rerum nobis sit mole', 'Nesciunt cumque pos', 'Elaine Cote', 'Et in pariatur Temp', 'Grant Roth', 1, '2019-06-10 09:49:08', NULL, NULL),
(2, 5, 'Ut voluptatem sed pe', 'Reiciendis mollitia', 'Unde dolor dolorem e', 'Quasi illo iure even', 'Voluptatibus accusam', 'Sunt tempora ut quia', 'Sed praesentium aut', 'Dolor nisi labore si', 'Aliquip nostrud cons', 'Do molestiae nostrud', 'Eu qui adipisci offi', 'Voluptas inventore e', 'Aliqua Maxime est', 'Omnis cumque eligend', 'Recusandae Eu ullam', 'Dolor ut voluptates', 'Et ut consectetur do', 'Non velit qui eius u', 'Reprehenderit vitae', 'Dolore sunt possimus', 'Quis cillum voluptas', 'Numquam voluptas aut', 'Culpa occaecat perfe', 'Ut expedita qui iste', 'Earum corrupti volu', 'Quis labore repellen', 'Mollitia blanditiis', 'Dignissimos est qui', 'Voluptatem iusto lab', 'Vitae velit distinct', 'Nulla facere dolore', 'Erasmus Hensley', 'Beatae ad consequunt', 'Kirestin Drake', 1, '2019-06-11 07:04:48', NULL, NULL),
(3, 6, 'Rerum tempore maxim', 'Architecto sint et m', 'Velit minima in labo', 'Ullamco ea dolor ali', 'Incididunt excepturi', 'Veniam amet consec', 'Consequatur volupta', 'Labore sed laborum a', 'Voluptatem Consequu', 'Ad facilis dolore ma', 'Quis ad incididunt l', 'Ea architecto nemo c', 'Dolor aliqua Nihil', 'Qui voluptatem volu', 'Esse officiis sint', 'Distinctio Cupidata', 'Dolore tempora totam', 'Est quis deleniti c', 'Corporis elit nesci', 'Non velit aperiam vo', 'Magna ut molestiae o', 'Distinctio Quaerat', 'Aliquid et in nemo u', 'Ut est labore commo', 'Mollitia eos tempora', 'Labore quia velit mi', 'Est do temporibus la', 'Non ad veniam autem', 'Nostrud est numquam', 'Officiis eu dolores', 'Consequatur magni qu', 'Mikayla English', 'Aute necessitatibus', 'Athena Nichols', 20, '2019-06-11 08:35:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ravi.tiwari2064@gmail.com', '$2y$10$FBhzmfyWUPokigpgU1lv1ePr6hHpYNzeMs/3Y7ibKpRWtWjh3BSrK', '2018-12-04 10:54:58'),
('amin@gmail.com', '$2y$10$nxxJy01s8zPZbn9VuyHyI.A25nCiYyXE4SWL7hLJ0UOvxBMMX7O3i', '2019-01-01 05:49:59'),
('competentengineering@gmail.com', '$2y$10$fMcDKonUIiuKNRW5Xtu/xOo3KsPoY1fAqFQid/ci4L.Fxe8.QeNtO', '2019-01-30 06:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `rajashow`
--

CREATE TABLE `rajashow` (
  `rajashow_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `underground_floor_rate` float DEFAULT NULL,
  `underground_floor_amount` float DEFAULT NULL,
  `ground_floor_rate` float DEFAULT NULL,
  `ground_floor_amount` float DEFAULT NULL,
  `first_floor_rate` float DEFAULT NULL,
  `first_floor_amount` float DEFAULT NULL,
  `second_floor_rate` float DEFAULT NULL,
  `second_floor_amount` float DEFAULT NULL,
  `third_floor_rate` float DEFAULT NULL,
  `third_floor_amount` float DEFAULT NULL,
  `fourth_floor_rate` float DEFAULT NULL,
  `fourth_floor_amount` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `other_amount` float DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at_nepali` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rajashow`
--

INSERT INTO `rajashow` (`rajashow_id`, `form_id`, `underground_floor_rate`, `underground_floor_amount`, `ground_floor_rate`, `ground_floor_amount`, `first_floor_rate`, `first_floor_amount`, `second_floor_rate`, `second_floor_amount`, `third_floor_rate`, `third_floor_amount`, `fourth_floor_rate`, `fourth_floor_amount`, `total`, `other_amount`, `grand_total`, `created_by`, `updated_by`, `created_at`, `updated_at`, `created_at_nepali`) VALUES
(1, 3, 12, 144, 3, 69, 1, 34, 234, 10530, 232, 133632, 3, 135, 144544, 34, 144578, 1, NULL, '2019-06-10 09:36:15', '2019-06-10 09:36:15', 'सोम, जेठ २७, २०७६'),
(2, 1, 0, 0, 1, 1290, 1, 1290, 0, 0, 0, 0, 0, 0, 2580, 1, 2581, 1, NULL, '2019-06-10 11:46:54', '2019-06-10 11:46:54', 'सोम, जेठ २७, २०७६'),
(3, 5, 122, 4026, 211, 6752, 233, 7922, 0, 0, 0, 0, 0, 0, 18700, 32, 18732, 1, NULL, '2019-06-11 05:32:44', '2019-06-11 05:32:44', 'मंगल, जेठ २८, २०७६'),
(4, 6, 45, 1890, 4754, 109342, 245, 10535, 455, 106470, 545, 236530, 544, 12512, 477279, NULL, 477279, 17, NULL, '2019-06-11 08:29:59', '2019-06-11 08:29:59', 'मंगल, जेठ २८, २०७६');

-- --------------------------------------------------------

--
-- Table structure for table `rajashow_scan`
--

CREATE TABLE `rajashow_scan` (
  `rajashow_scan_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `rajashow` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rajashow_scan`
--

INSERT INTO `rajashow_scan` (`rajashow_scan_id`, `form_id`, `rajashow`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 3, 'files/3/rajashow_scan/LrpsH0t6FrbZDbkMnm5Dss1eACAmMkARq6hBCwJR.png', 1, '2019-06-10 09:36:27', '2019-06-10 09:36:27'),
(2, 1, 'files/1/rajashow_scan/sK6gUXbHLxNYtRbmEtEsiX5FNZv4gZijPej0stna.jpeg', 1, '2019-06-10 11:48:25', '2019-06-10 11:48:25'),
(3, 5, 'files/5/rajashow_scan/mAsiBDXdrEN3lkljSKMVbSWSKm7DpAFYckcQ5F46.jpeg', 1, '2019-06-11 05:33:00', '2019-06-11 05:33:00'),
(4, 6, 'files/6/rajashow_scan/FyxDGz2yVm4JFG84fjGTaNp0FOusCWIwAxKRO46U.jpeg', 17, '2019-06-11 08:30:50', '2019-06-11 08:30:50');

-- --------------------------------------------------------

--
-- Table structure for table `reject_log`
--

CREATE TABLE `reject_log` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `rejected_by` int(11) NOT NULL,
  `reason` text CHARACTER SET utf8,
  `from_form_level` int(11) NOT NULL,
  `to_form_level` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reject_log`
--

INSERT INTO `reject_log` (`id`, `form_id`, `rejected_by`, `reason`, `from_form_level`, `to_form_level`, `created_at`) VALUES
(1, 5, 1, NULL, 1, 1, '2019-06-11 04:24:41'),
(2, 5, 1, 'As you can see, we executed exactly the same query as previously when using eager loading. However this time Eloquent stored only 20 Post objects in memory instead of 2000, because the relation is hasOne', 1, 1, '2019-06-11 04:36:01'),
(3, 5, 1, 'As you can see, we executed exactly the same query as previously when using eager loading. However this time Eloquent stored only 20 Post objects in memory instead of 2000, because the relation is hasOne.', 1, 1, '2019-06-11 04:41:57'),
(4, 5, 1, 'हालको वडा नं', 1, 1, '2019-06-11 04:46:08'),
(5, 5, 1, 'As you can see, we executed exactly the same query as previously when using eager loading. However this time Eloquent stored only 20 Post objects in memory instead of 2000, because the relation is hasOne.', 3, 2, '2019-06-11 05:17:15'),
(6, 5, 1, '@if ($data->is_rejected==1 AND isset($data->last_reject_log) AND $data->form_level < 2)\r\n														<span class=\"btn btn-outline btn-warning btn-sm\" style=\"margin-left: 20px;\">Rejected</span>\r\n													@endif', 3, 2, '2019-06-11 05:26:39'),
(7, 5, 1, 'Yatra.com ventured into Homestays in India in 2015, and is providing great homestay experiences to its customers as well as benefits to its hosts. If you are looking for a holiday with a difference, want to closely savour the local flavours of a place, and prefer the comforts of a homely environment, Yatra Homestays is the answer.', 7, 6, '2019-06-11 05:55:32'),
(8, 5, 1, 'Yatra.com ventured into Homestays in India in 2015, and is providing great homestay experiences to its customers as well as benefits to its hosts. If you are looking for a holiday with a difference, want to closely savour the local flavours of a place, and prefer the comforts of a homely environment, Yatra Homestays is the answer.', 7, 6, '2019-06-11 05:57:17'),
(9, 6, 17, 'Officiis animi reru', 7, 6, '2019-06-11 08:33:04'),
(10, 8, 1, 'sdfsdsdf', 1, 1, '2019-06-12 06:53:06'),
(11, 8, 1, 'fghfgh', 1, 1, '2019-06-12 06:54:32'),
(12, 10, 1, 'sadfsaf', 1, 1, '2019-06-20 05:16:59'),
(13, 13, 1, 'ASFSD', 3, 2, '2019-07-01 07:45:07');

-- --------------------------------------------------------

--
-- Table structure for table `sampanna_scan`
--

CREATE TABLE `sampanna_scan` (
  `sampanna_scan_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `form` varchar(255) NOT NULL,
  `tippani` varchar(255) NOT NULL,
  `certificate` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sampanna_scan`
--

INSERT INTO `sampanna_scan` (`sampanna_scan_id`, `form_id`, `form`, `tippani`, `certificate`, `created_by`, `created_at`) VALUES
(1, 3, 'files/3/sampanna_scan/form/kARp8mpNcVkmIpShR6CcCiqZhmYAJOHDt942QeXe.jpeg', 'files/3/sampanna_scan/tippani/nDOkrIA2wEJWT1uj2liz3rIZKezW07uZ4IX8T32P.png', 'files/3/sampanna_scan/certificate/Ddt1v7WEhjyop7M3vWBy9njABAax0861pnXsQVMW.jpeg', 1, '2019-06-10 09:59:10'),
(2, 5, 'files/5/sampanna_scan/form/rcRWXPdANJsZo6LGjVFt2JjCbRbnE776cKK7Eej1.png', 'files/5/sampanna_scan/tippani/FOzXXfxzhJgRj50vPW1W8UrISLUl0kgJbtKCSiJe.jpeg', 'files/5/sampanna_scan/certificate/zKuoTh7haxEhLxkdSMAN3qIBh2Ta6pZSJNPbdmST.jpeg', 1, '2019-06-11 07:33:31'),
(3, 6, 'files/6/sampanna_scan/form/YCO7x13K2gBi81IvB7WYj7H26JfcHpTN4zYVN13V.png', 'files/6/sampanna_scan/tippani/laCLm9L49CbvsvbW0mX9B7BUZCuaM2IzBPcFbi0c.jpeg', 'files/6/sampanna_scan/certificate/wkm33bBsMFQnDEQo6mqhRrGwOYYcxvY36laT9YUf.jpeg', 17, '2019-06-11 08:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `sandhirsuchana`
--

CREATE TABLE `sandhirsuchana` (
  `id` int(11) NOT NULL,
  `sandhir_suchana` varchar(255) NOT NULL,
  `form_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sandhirsuchana`
--

INSERT INTO `sandhirsuchana` (`id`, `sandhir_suchana`, `form_id`, `created_at`, `updated_at`) VALUES
(1, 'files/3/sandhir_suchana/xFWzo3WnXGXBeIz5AQMJAFFVYrNeQPV73vZ5rVWn.jpeg', 3, '2019-05-24 09:07:34', '2019-06-10 09:07:34'),
(2, 'files/1/sandhir_suchana/m7u57L3JYyf9e0453FT0MYNlj673NnvpizPaUKgp.jpeg', 1, '2019-05-24 11:06:11', '2019-06-10 11:06:11'),
(3, 'files/4/sandhir_suchana/Hus9tTMreIJc7a83vkCj5K9wcn2dG1074UJG6uVj.jpeg', 4, '2019-06-10 12:11:50', '2019-06-10 12:11:50'),
(4, 'files/5/sandhir_suchana/eakDLNUQy6tlXZBqT2tnHfbQNAWWrladYx2VzLG2.jpeg', 5, '2019-05-27 04:53:02', '2019-06-11 04:53:02'),
(5, 'files/6/sandhir_suchana/Z6fFP627f1sTiiydo19uIZlZBBAmr0eXRC4jYEHU.jpeg', 6, '2019-05-27 08:25:46', '2019-06-11 08:25:46'),
(6, 'files/8/sandhir_suchana/BE91P5OCkpv20OzPIgNFrD3Xj1Cy01t6RCFptlxu.png', 8, '2019-06-12 07:32:15', '2019-06-12 07:32:15'),
(7, 'files/11/sandhir_suchana/nifQ3UadQvjbUSsOcDSSnD83ZaztVI5XK5m05ODX.png', 11, '2019-05-09 08:15:33', '2019-06-24 08:15:33'),
(8, 'files/12/sandhir_suchana/qz7Yj4aUt4eAwsMRSsc7XnSdNPk1TLBHaCjc2oE1.jpeg', 12, '2019-04-10 07:35:30', '2019-07-01 07:35:30'),
(9, 'files/13/sandhir_suchana/5kcyZ4Xw140Qj3mwvyNRgBdzMQBmFqI67MyQMeFY.jpeg', 13, '2019-06-01 07:39:31', '2019-07-01 07:39:31');

-- --------------------------------------------------------

--
-- Table structure for table `tippani_scan`
--

CREATE TABLE `tippani_scan` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `asthai_scan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tippani_scan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tippani_scan`
--

INSERT INTO `tippani_scan` (`id`, `form_id`, `asthai_scan`, `tippani_scan`, `created_by`, `created_at`, `updated_by`) VALUES
(1, 3, 'files/3/asthai_scan/0Avm6sZChTlFzM7ybHpagXO6ahc8KPC51Jkdk3qQ.png', 'files/3/tippani_scan/bPEwrJTALKiIbef6HDqAzRTSYLts6sjCqPF3TsMT.jpeg', 1, '2019-06-10 09:38:49', NULL),
(2, 1, 'files/1/asthai_scan/KhC4vuLesBUToaMwYv4BY248e4vixeiWXq848RXe.png', 'files/1/tippani_scan/9e3SMgadSjEm3ZPbV5Rcwc9wZOY30Ye7yeagQhAy.jpeg', 1, '2019-06-10 12:02:36', NULL),
(3, 5, 'files/5/asthai_scan/4Itz2NfltUxW8bQNeM1LTktUG7XbCj9W5iMpElot.jpeg', 'files/5/tippani_scan/X0kY4Y5pLmlT3sdEe86hBfWSWOdVqr4FOzZQEeNZ.jpeg', 1, '2019-06-11 05:33:42', NULL),
(4, 6, 'files/6/asthai_scan/CsXVS0fujPBbvNnLIXfAjWV9sHXxgR4BcEKvwTzz.jpeg', 'files/6/tippani_scan/j9qulHEYHxnsBpJCztiBPsVp34YjXwwiZem5DWev.jpeg', 16, '2019-06-11 08:31:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_super` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultancy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultancy_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultancy_license` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultancy_license_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `contact`, `address`, `email`, `email_verified_at`, `password`, `last_login_at`, `created_by`, `is_super`, `remember_token`, `consultancy_name`, `consultancy_address`, `consultancy_license`, `consultancy_license_number`, `staff_id`, `extension_number`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Bishal', 'Khatri', '9842721343', 'Kathmandu', 'superadmin@gmail.com', NULL, '$2y$10$q9/qJSRy5i3nA6b7Iix1auIlJjIZ5Bw213RXkjZKUau2x3NDqeJHu', '2019-07-03 06:56:29', NULL, 1, '1T5YYC8ySHSigt3O9fqgX0mi81WHiy6fpy2SMHUCXo4Q55wkKBR8Qp2YyE31', '', '', '', '', '', '', 'upload_image/1/oB30tX3S6uySJQJAXoVMadjmASO8s0qZTfOXF7LB.jpeg', '2018-09-10 23:29:31', '2019-07-03 06:56:29'),
(13, 'Ravi', 'Tiwari', '0', 'Ratnanagar', 'rabitiwari@gmail.com', NULL, '$2y$10$Ub5Yex1HAi7PHnkkimId2etSDD6KP2UbH1S4Q9PU0WTzxPnSZV7CK', '2019-06-04 09:56:03', NULL, 0, 'HDIfg7zqnJnVRu3JtGsgOidljZtu16AaHJjjEoqyNbJKEXOXU9PkrPMyNWu0', NULL, NULL, NULL, NULL, NULL, NULL, 'upload_image/13/IavtTRbnJVvEaSayV7g0tSCeOZBEYvSZzVhifMr4.png', '2018-12-04 04:50:15', '2019-06-04 09:56:03'),
(16, 'Nameshs', 'Gurung', '9845633495', 'Bharatpur', 'engineer@gmail.com', NULL, '$2y$10$Ub5Yex1HAi7PHnkkimId2etSDD6KP2UbH1S4Q9PU0WTzxPnSZV7CK', '2019-06-17 04:50:00', NULL, 0, 'gryIqJ8kD3k3gryFouSsVVPuf4yqlNayzGC5YglMB5wXIChP9V3TzwLHdX7n', NULL, NULL, NULL, NULL, NULL, NULL, 'upload_image/16/rAa9WLAzv8HusA6gCYK7pUox5AvnMKi33Bx7ZTuk.jpeg', '2018-12-05 06:28:59', '2019-06-17 04:50:00'),
(17, 'Mohan', 'Timalsina', '9855060514', 'Ratnanagar', 'subengineer@gmail.com', NULL, '$2y$10$Ub5Yex1HAi7PHnkkimId2etSDD6KP2UbH1S4Q9PU0WTzxPnSZV7CK', '2019-06-11 08:24:29', NULL, 0, '6TUncsGYe740GbvMxzqAOXV8oSdfASCF2CdMpDZVYVPvKkz24WApsertHvfH', NULL, NULL, NULL, NULL, NULL, NULL, 'upload_image/17/7OKuJ7yIvc1qfy8tdrwdexNBBf9NZNa5winQl4jd.png', '2018-12-05 06:29:44', '2019-06-10 20:44:55'),
(19, 'Bijaya', 'Ranamagar', '9801101400', 'Kathmandu', 'mainengineer@gmail.com', NULL, '$2y$10$Ub5Yex1HAi7PHnkkimId2etSDD6KP2UbH1S4Q9PU0WTzxPnSZV7CK', '2019-06-11 08:34:48', NULL, 0, 'f9D7DFELguqjT2cIA2aPlEqrHT4GZO0joPQRwWeluqiJP0kGZdiWbKqPrpcD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-05 06:32:49', '2019-06-11 08:34:48'),
(20, 'Trisha', 'Engineering Consultancy', '9855072699', 'Bharatpur', 'consultancy@gmail.com', NULL, '$2y$10$Ub5Yex1HAi7PHnkkimId2etSDD6KP2UbH1S4Q9PU0WTzxPnSZV7CK', '2019-06-20 05:13:57', NULL, 0, 'rntaXkKWshvB5gVJTJBS7RMmfKfcufcdWAK1nXpFzJ67vtW2lRhNmQLlQ3es', NULL, NULL, NULL, NULL, NULL, NULL, 'upload_image/20/fs1QZXlXY31ji0oizBoYyNpEiXmYHSuKyyVwhszo.png', '2018-12-05 09:43:32', '2019-06-20 05:13:57'),
(21, 'Binay', 'Achary', '9845711238', 'Ratnanagar', 'unitedengineering@gmail.com', NULL, '$2y$10$F.WzJ7mU25jdjSSx0jsvmu1vy.kgrTQijDcUigcTpGKgQ8M4LCfLS', '2019-04-04 05:35:09', NULL, 0, 'xodKo5R4poVPa9xSidlgSoCDRulAWjN798qIfusffnB3CegGtreeAXd5N4Sl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:01:52', '2019-04-04 05:35:09'),
(22, 'sesmic', 'Engineering consultancy', '9841811995', 'Ratnanagar', 'sesmicengineering@gmail.com', NULL, '$2y$10$7ovKaDFKDZU5sf2RazL4nuI0WeIvi3t6q7Uz6TmiKwvhHeYCVqzNq', '2019-06-04 09:08:10', NULL, 0, '38FPlEPKd1Fxw5hazyVptTC0fBC3H1qIk1fcdmo6lgIZZeUEnxjMQsWqGBh0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:03:52', '2019-06-04 09:08:10'),
(23, 'A.s', 'Engineering Cosultant', '9845046361', 'Ratnanagar-2', 'asengineering@gmail.com', NULL, '$2y$10$7.C8OfUn1XWGLAePyAy4mOL7pPQyLRU8.RkJ0oacUwIv7FI31shdC', '2019-02-20 08:22:08', NULL, 0, '93iEfXt45X8lhYqwWY9o1neRqHuxnm1iKoHrHpRszWgOIRu72he5ZchDiAfA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:05:27', '2019-02-20 08:22:08'),
(24, 'Image', 'Engineering Consultancy', '9855062992', 'Ratnanagar-1', 'imageengineering@gmail.com', NULL, '$2y$10$QmBRmDdQG1rPInwB9BMe3eb8KajsQZ7cvGV863ti/RKpdgUd5Etvy', '2019-04-04 09:04:09', NULL, 0, 'R5lMOc2IGqYx89XBcjnQl41uHM6rqIM2sDplzBjwWrMgPM2oK1rxujuTksMR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:06:37', '2019-04-04 09:04:09'),
(26, 'Samrachana', 'Engineering Consultancy', '9855080657', 'Ratnanagar-16', 'samrachanaengineering@gmail.com', NULL, '$2y$10$1dnTM4Og74IcDFQJ9hXHi.UaPXcWCdozQHVILViZSwId9ALYAZqm.', '2019-03-22 07:36:06', NULL, 0, 'hFgCvOPO2eafVxaMkoxFNKYOtKppj4gfHh45Y3lDCTCV7krBFiuuz3JTCE5m', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:11:04', '2019-03-22 07:36:06'),
(27, 'Comat', 'Engineering Consultancy', '9855080005', 'Khairani-8', 'comatengineering@gmail.com', NULL, '$2y$10$tHstMaaq.SYV03GNBaW8becPGYnmIH6sgfgVeo18vB.7f5CIvjcIW', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:12:14', '2018-12-06 07:12:14'),
(28, 'Devine', 'Engineering Consultancy', '9855080382', 'Ratnanagar-1', 'devineengineering@gmail.com', NULL, '$2y$10$JYcPdMCvjqWqQObVgEO6uOZcj1jSUUMe.ispSdexwtKJDFpXLlyN6', '2019-04-01 05:06:25', NULL, 0, 'uECdwP5yl4cDtzdMTX1o4jPbAcIDvgYsmAudwLa1gVvdksEydGekuMtDNRwQ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:13:22', '2019-04-01 05:06:25'),
(29, 'lofty', 'developers', '9851170614', 'Kathmandu', 'loftyengineering@gmail.com', NULL, '$2y$10$8dJoSuPX.27ICWjdLRduPOaqsmM1I6qMd5sy3q5/F7FQPATBygg6C', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:14:19', '2018-12-06 07:14:19'),
(30, 'Barsha', 'Engineering Consultancy', '056527597', 'Bharatpur-12', 'barshaengineering@gmail.com', NULL, '$2y$10$bjsA/BtNRHm2pBLF5zU/w.i0KlFXcXXXYtXVHuSGxeYQNKwBz5SJ.', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:15:58', '2018-12-06 07:15:58'),
(31, 'Mb', 'Engineering Consultancy', '9845067592', 'Ratnanagar-14', 'mbengineering@gmail.com', NULL, '$2y$10$ccKtX6meqlguzBv5r89OEOmtwTWp/xNbskmgCIYKGIbPcvRdnh54.', '2019-06-03 06:31:39', NULL, 0, 'njbP2Kvy6iEPkGVSKHvjVqtsNx0SchMnE4EbiFvpK3Lfz8ql5ahEAu7X3iZQ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:17:07', '2019-06-03 06:31:39'),
(32, 'Salin', 'Engineering Consultancy', '123456', 'Ratnanagar-12', 'salinengineering@gmail.com', NULL, '$2y$10$UVGqaA.aI76n2RCUit5WwOCFqKBr04AwdZbgRfp.Edz/KYH8208qu', '2019-03-11 05:43:30', NULL, 0, '9V9vWDGkr2MvRou8Qog99bU1rzXv31mzk0q7Z8U8Qf10JRXFx07R0SWFbZvK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-06 07:18:41', '2019-03-11 05:43:30'),
(34, 'Krishna', 'Magar', '9855053277', 'Kalika', 'aamin@gmail.com', NULL, '$2y$10$Ub5Yex1HAi7PHnkkimId2etSDD6KP2UbH1S4Q9PU0WTzxPnSZV7CK', '2019-07-01 10:41:25', NULL, 0, 'UJ9n5s0iiMgcZkbNn23ZSxVC3tA1ScxhrwTogP9nHO0xnsEChrLVF6rEJhHJ', NULL, NULL, NULL, NULL, NULL, NULL, 'files/avatar_file/c3Kx53w8d2qz3PTIo1jAR14EYKTgon14SSHLRubu.jpeg', '2019-01-01 05:57:04', '2019-07-01 10:41:25'),
(35, 'Ashish', 'Baral', '9851172674', 'Ratnanagar-10,Bhanuchowk', 'ratnanagarbuilders10@gmail.com', NULL, '$2y$10$8VX2fdcTdUOf9Mst/RXW8uLXwnZtCxnxCWRuUPKb1vsC/Uimwuthu', '2019-01-18 06:55:19', NULL, 0, NULL, 'Ratnanagar Builders and Engineering', 'Ratnanagar-1', '16628', '35', NULL, NULL, 'files/avatar_file/QkKGP36zv9rlHGsWETQS8zsiJB17EHrfIKNxL2WK.jpeg', '2019-01-11 06:50:06', '2019-01-18 06:55:19'),
(36, 'Binod', 'Badu', '9857039404', 'bharatpur-7', 'bb.thirdpolengineering@gmail.com', NULL, '$2y$10$ljbtckiar3cNYaP5JcwGt.jXOGtDV0i4Bg4E2TH2NaeY2FDXJdnHu', '2019-03-27 08:45:53', NULL, 0, 'Uq72A45zX3IdCaLMSLqwHPIHNhxtvQy2PYURjyaFZmh4j0OJt7tYVnN1oOBY', 'Third pole engineering consultancy', 'bharatpur-7', '14000', '029', NULL, NULL, 'files/avatar_file/lWWQikWDI5Xpr67eKF73O2GRqVNT6WTnBtGDpgvj.jpeg', '2019-01-18 07:02:42', '2019-03-27 08:45:53'),
(39, 'developer', 'account', '9843166988', 'thecho, kathmandu', 'lamaprabin05@gmail.com', NULL, '$2y$10$y9RQub.7WWD7w5nt9os2yOYjfkeQA9dMGED39Dx00471qeOCpJVRq', '2019-01-22 05:14:08', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-22 05:13:43', '2019-01-22 05:15:21'),
(40, 'Ramkrishna', 'Dhital', '9856042752', 'Ratnanagar-2', 'maxpvt2014@gmail.com', NULL, '$2y$10$GhLTFv6DxrOSZVoLfVEPge7CoYO496WUMOnuaZ9uglpGAMTFv8MiO', '2019-04-20 07:23:17', NULL, 0, '6RVMTBfAYx0MHWufvAGamkrtzYWcQgHWQHfmNlMGlT5d6fgJXBq2n0ywpZXa', 'Max Engineering', 'Ratnanagar-2', '7215', '1400', NULL, NULL, 'files/avatar_file/GEmngKZ8EIxF5GrUOivAorPF4jg1DQiEkXXq8So8.jpeg', '2019-01-22 05:20:15', '2019-04-20 07:23:17'),
(41, 'Kapil', 'Paneru', '9851190018', 'Bharatpur-9, sharadpur', 'cecnepal18@gmail.com', NULL, '$2y$10$4OZt2keJEJjoeB5fkCrThO41jPQiyXVyp.RDdVAOUCb0.2M2iaeOq', '2019-02-20 08:49:58', NULL, 0, '4plWrOCPAhk9NjF5hP8GA7Ex7PwdXiWqHI5fCF2Bds171f7bAE9cBBk9IGWL', 'competent engineering consultancy', 'Bharatpur-9', '184943', '184943', NULL, NULL, 'files/avatar_file/UBAExSakIqqUImT7KQqIollX6X3gQ5YPdLpx4rI1.jpeg', '2019-02-02 16:46:59', '2019-02-20 08:49:58'),
(42, 'Niranjan', 'Neupane', '9845417627', 'Ratnanagar-11', 'neupaneniranjan51@gmail.com', NULL, '$2y$10$LWqKzAaBBdLLZWqQwy8JtOcMIRxBI8iceP/AmN2rYR.9XwvFJgXWW', '2019-03-22 13:28:07', NULL, 0, NULL, 'Unique Creation Engineering Consultancy', 'Ratnanagar-11', '32', '15788 \'A\'', NULL, NULL, NULL, '2019-03-01 06:21:28', '2019-03-22 13:28:07'),
(43, 'Ashok', 'pokhrel', '9867534576', 'Ratnanagar-11', 'kumargeeree@gmail.com', NULL, '$2y$10$I/lSs9S375eC4pNpJBBZq.rSRPx0N7ih9Jdp3uTUwDJP97dFRfQoq', '2019-03-25 09:46:06', NULL, 0, NULL, 'Gudban Engineering research and multi disiplinary consultant', 'Ratnanagar-11', '2195', '119', NULL, NULL, NULL, '2019-03-24 07:16:09', '2019-03-25 09:46:06'),
(44, 'Bharat Kumar', 'Acharya', '9855060856', 'Bharatpur-10,Chitwan', 'acme.engineering2015@gmail.com', NULL, '$2y$10$vtKcqK7Rcgxs8Tzz36tFBOLGUKN0SeY3jAMCyNow/psMocPLl.JKe', '2019-04-05 09:55:33', NULL, 0, 'ewCzn489xtBsLm0OYTLb3m0PMe0Hwbz8P1CPsZhoeZtOM4BEo1AFkVh5V4Dv', 'Acme Engineering Consultancy', 'Bharatpur-10,Chiwtwan', '12', '1727  Civil A', NULL, NULL, NULL, '2019-04-02 08:28:07', '2019-04-05 09:55:33');

-- --------------------------------------------------------

--
-- Table structure for table `ward`
--

CREATE TABLE `ward` (
  `id` int(11) NOT NULL,
  `ward_number` varchar(5) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ward`
--

INSERT INTO `ward` (`id`, `ward_number`) VALUES
(1, '१'),
(2, '२'),
(3, '३'),
(4, '४'),
(5, '५'),
(6, '६'),
(7, '७'),
(8, '८'),
(9, '९'),
(10, '१०'),
(11, '११'),
(12, '१२'),
(13, '१३'),
(14, '१४'),
(15, '१५'),
(16, '१६');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aamin_pratibedan`
--
ALTER TABLE `aamin_pratibedan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aamin_pratibedan_scan`
--
ALTER TABLE `aamin_pratibedan_scan`
  ADD PRIMARY KEY (`aamin_pratibedan_id`);

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_permission_user`
--
ALTER TABLE `auth_permission_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dpc`
--
ALTER TABLE `dpc`
  ADD PRIMARY KEY (`dpc_id`);

--
-- Indexes for table `dpc_scan`
--
ALTER TABLE `dpc_scan`
  ADD PRIMARY KEY (`dpc_scan_id`);

--
-- Indexes for table `form_buildingfiles`
--
ALTER TABLE `form_buildingfiles`
  ADD PRIMARY KEY (`buildingfiles_id`);

--
-- Indexes for table `form_buildinginfo`
--
ALTER TABLE `form_buildinginfo`
  ADD PRIMARY KEY (`buildinginfo_id`);

--
-- Indexes for table `form_floorinfo`
--
ALTER TABLE `form_floorinfo`
  ADD PRIMARY KEY (`floorinfo_id`);

--
-- Indexes for table `form_landinfo`
--
ALTER TABLE `form_landinfo`
  ADD PRIMARY KEY (`landinfo_id`);

--
-- Indexes for table `form_personalinfo`
--
ALTER TABLE `form_personalinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_sampanna`
--
ALTER TABLE `form_sampanna`
  ADD PRIMARY KEY (`sampanna_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `rajashow`
--
ALTER TABLE `rajashow`
  ADD PRIMARY KEY (`rajashow_id`);

--
-- Indexes for table `rajashow_scan`
--
ALTER TABLE `rajashow_scan`
  ADD PRIMARY KEY (`rajashow_scan_id`);

--
-- Indexes for table `reject_log`
--
ALTER TABLE `reject_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sampanna_scan`
--
ALTER TABLE `sampanna_scan`
  ADD PRIMARY KEY (`sampanna_scan_id`);

--
-- Indexes for table `sandhirsuchana`
--
ALTER TABLE `sandhirsuchana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tippani_scan`
--
ALTER TABLE `tippani_scan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `ward`
--
ALTER TABLE `ward`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aamin_pratibedan`
--
ALTER TABLE `aamin_pratibedan`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `aamin_pratibedan_scan`
--
ALTER TABLE `aamin_pratibedan_scan`
  MODIFY `aamin_pratibedan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `auth_permission_user`
--
ALTER TABLE `auth_permission_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `dpc`
--
ALTER TABLE `dpc`
  MODIFY `dpc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dpc_scan`
--
ALTER TABLE `dpc_scan`
  MODIFY `dpc_scan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `form_buildingfiles`
--
ALTER TABLE `form_buildingfiles`
  MODIFY `buildingfiles_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `form_buildinginfo`
--
ALTER TABLE `form_buildinginfo`
  MODIFY `buildinginfo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `form_floorinfo`
--
ALTER TABLE `form_floorinfo`
  MODIFY `floorinfo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `form_landinfo`
--
ALTER TABLE `form_landinfo`
  MODIFY `landinfo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `form_personalinfo`
--
ALTER TABLE `form_personalinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `form_sampanna`
--
ALTER TABLE `form_sampanna`
  MODIFY `sampanna_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rajashow`
--
ALTER TABLE `rajashow`
  MODIFY `rajashow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rajashow_scan`
--
ALTER TABLE `rajashow_scan`
  MODIFY `rajashow_scan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reject_log`
--
ALTER TABLE `reject_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sampanna_scan`
--
ALTER TABLE `sampanna_scan`
  MODIFY `sampanna_scan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sandhirsuchana`
--
ALTER TABLE `sandhirsuchana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tippani_scan`
--
ALTER TABLE `tippani_scan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `ward`
--
ALTER TABLE `ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
