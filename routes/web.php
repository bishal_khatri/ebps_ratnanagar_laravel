<?php

//defines
//use Illuminate\Routing\Route;

if(!defined('STATIC_DIR')) define('STATIC_DIR','public/');
if(!defined('ROOT_DIR')) define('ROOT_DIR','');
if(!defined('ROOT_DIR_NAME')) define('ROOT_DIR_NAME','');
if(!defined('DEFAULT_USER')) define('DEFAULT_USER','');
if(!defined('LOGO_DARK')) define('LOGO_DARK','assets/icons/delta-dark.png');
if(!defined('LOGO_WHITE')) define('LOGO_WHITE','assets/icons/delta-white.png');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// BASE ROUTES
Route::get('/', 'HomeController@index')->name('home');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::get('/profile/{status?}', ['uses'=>'ProfileController@get_profile','as'=>'profile']);
Route::post('/update/profile', ['uses'=>'ProfileController@post_profile','as'=>'update_profile']);

Route::group(['prefix' =>ROOT_DIR.'user', 'middleware' => ['auth','can:administration'], 'as' => 'user.'], function () {
    Route::get('/register', ['uses'=>'Auth\RegisterController@showRegistrationForm','as'=>'register']);

    //user edit route here
    Route::get('/edit/{action}', ['uses'=>'Auth\RegisterController@showEditForm','as'=>'edituser']);
    Route::post('/register', 'Auth\RegisterController@register');

    Route::get('/list', ['uses'=>'Auth\RegisterController@index', 'as'=>'index']);
    Route::post('/permission_view', ['uses'=>'PermissionController@permission_view', 'as'=>'permission_view']);
    Route::get('/assign_permission/{id}', ['uses'=>'PermissionController@assign_permission', 'as'=>'assign_permission']);
    Route::post('/assign_permission/store', ['uses'=>'PermissionController@assign_permission_store', 'as'=>'assign_permission_store']);
    Route::post('/delete', ['uses'=>'Auth\RegisterController@delete', 'as'=>'delete']);
});

Route::group(['prefix' =>ROOT_DIR.'permission', 'middleware' => 'auth', 'as' => 'permission.'], function () {
    Route::get('/list', ['uses'=>'PermissionController@index','as'=>'index'])->middleware('can:user-add');
    Route::post('/add', ['uses'=>'PermissionController@create','as'=>'add'])->middleware('can:user-add');
    Route::post('/delete', ['uses'=>'PermissionController@delete','as'=>'delete'])->middleware('can:user-add');
});

Route::group(['prefix' =>ROOT_DIR.'file', 'middleware' => 'auth', 'as' => 'file.'], function () {
    Route::get('/status', ['uses'=>'FileController@get_file_status','as'=>'get_file_status']);
    // files count @MR
    Route::get('/count', ['uses'=>'FileController@getCount','as'=>'get_files_count']);
    Route::get('/ward_files/{id}', ['uses'=>'FileController@showFilesByWard','as'=>'get_files_by_ward']);
});

// CONSULTANCY ROUTE START
Route::group(['prefix' =>ROOT_DIR.'consultancy', 'middleware' => ['auth','can:consultancy'], 'as' => 'consultancy.'], function () {
    Route::get('/list', ['uses'=>'ConsultancyController@index','as'=>'index'])->middleware('can:consultancy');
    Route::get('/draft', ['uses'=>'ConsultancyController@get_draft','as'=>'draft'])->middleware('can:consultancy');
    Route::get('/add_form_personalinfo/{form_id?}', ['uses'=>'ConsultancyController@get_form_personalinfo','as'=>'add_form_personalinfo'])->middleware('can:consultancy');
    Route::post('/add_form_personalinfo', ['uses'=>'ConsultancyController@post_form_personalinfo','as'=>'store_form_personalinfo'])->middleware('can:consultancy');
    Route::get('/view_form/{form_id}', ['uses'=>'ConsultancyController@get_full_form','as'=>'view_full_form'])->middleware('can:consultancy');

    //form_landinfo routes
    Route::get('/add_form_landinfo/{form_id}', ['uses'=>'ConsultancyController@get_form_landinfo','as'=>'add_form_landinfo'])->middleware('can:consultancy');
    Route::get('/add_form_landinfo/{form_id}', ['uses'=>'ConsultancyController@get_form_landinfo','as'=>'add_form_landinfo'])->middleware('can:consultancy');
    Route::post('/add_form_landinfo', ['uses'=>'ConsultancyController@post_form_landinfo','as'=>'store_form_landinfo'])->middleware('can:consultancy');

    //page = 3 form_buildinginfo
    Route::get('/add_form_buildinginfo/{form_id}', ['uses'=>'ConsultancyController@get_form_buildinginfo','as'=>'add_form_buildinginfo'])->middleware('can:consultancy');
    Route::post('/add_form_buildinginfo', ['uses' => 'ConsultancyController@post_form_buildinginfo' , 'as' => 'store_form_buildinginfo'])->middleware('can:consultancy');

    //page = 4 form_floorinfo
    Route::get('/add_form_floorinfo/{form_id}', ['uses'=>'ConsultancyController@get_form_floorinfo','as'=>'add_form_floorinfo'])->middleware('can:consultancy');
    Route::post('/store_form_floorinfo', ['uses' => 'ConsultancyController@post_form_floorinfo' , 'as' => 'store_form_floorinfo'])->middleware('can:consultancy');

    //page = 5 form_buildingfiles
    Route::get('/add_form_buildingfiles/{form_id}', ['uses'=>'ConsultancyController@get_form_buildingfiles','as'=>'add_form_buildingfiles'])->middleware('can:consultancy');
    Route::post('/add_form_buildingfiles', ['uses' => 'ConsultancyController@post_form_buildingfiles' , 'as' => 'store_form_buildingfiles'])->middleware('can:consultancy');

    // rejected files
    Route::group(['prefix' =>'rejected', 'as'=>'rejected.'], function () {
        Route::get('/list', ['uses' => 'ConsultancyController@get_rejected_file' , 'as' => 'list'])->middleware('can:consultancy');

        //edit form
        Route::get('/edit_form/{form_id}', ['uses'=>'ConsultancyController@get_edit_form','as'=>'edit_form'])->middleware('can:consultancy');
        Route::post('/edit_form', ['uses'=>'ConsultancyController@post_edit_form','as'=>'post_edit_form'])->middleware('can:consultancy');

        //route to edit dpc
        Route::get('/edit_dpcform/{form_id}', ['uses'=>'ConsultancyController@get_edit_dpcform','as'=>'edit_dpcform'])->middleware('can:consultancy');

        //route to edit sampanna
        Route::get('/edit_sampanna/{form_id}', ['uses'=>'ConsultancyController@get_edit_sampanna','as'=>'edit_sampanna'])->middleware('can:consultancy');
    });

    // DPC
    Route::get('/list/dpc/{form_id}', ['uses'=>'ConsultancyController@get_dpc_form','as'=>'get_dpc_form'])->middleware('can:consultancy');
    Route::post('/dpc', ['uses'=>'ConsultancyController@post_dpc_form','as'=>'post_dpc_form'])->middleware('can:consultancy');
    Route::get('/dpc_view/{form_id}', ['uses'=>'ConsultancyController@get_dpc_view','as'=>'get_dpc_view'])->middleware('can:consultancy');
    Route::get('/view_dpc/{form_id}', ['uses'=>'ConsultancyController@get_upload_dpc','as'=>'view_dpc'])->middleware('can:consultancy');

    //dpc after print upload
    Route::post('/upload_dpc', ['uses'=>'ConsultancyController@post_upload_dpc','as'=>'uploaddpc'])->middleware('can:consultancy');

    // SAMPANNA
    Route::get('/sampanna/{form_id}', ['uses'=>'ConsultancyController@get_sampanna_form','as'=>'get_sampanna_form'])->middleware('can:consultancy');
    Route::post('/store_form_sampanna', ['uses'=>'ConsultancyController@post_form_sampanna','as'=>'store_form_sampanna'])->middleware('can:consultancy');
    Route::get('/upload_sampanna/{form_id}', ['uses'=>'ConsultancyController@get_upload_sampanna_form','as'=>'get_upload_sampanna_form'])->middleware('can:consultancy');

    //sampanna after print upload
    Route::post('/upload_sampanna', ['uses'=>'ConsultancyController@post_upload_sampanna','as'=>'uploadsampanna'])->middleware('can:consultancy');
    Route::get('/view_sampanna/{form_id}', ['uses'=>'ConsultancyController@get_view_sampanna','as'=>'view_sampanna'])->middleware('can:consultancy');

    //change house_map
    Route::get('/view_housemap/{form_id}', ['uses'=>'ConsultancyController@get_housemap','as'=>'view_housemap'])->middleware('can:consultancy');
    Route::post('/change_housemap', ['uses'=>'ConsultancyController@post_change_housemap','as'=>'change_housemap'])->middleware('can:consultancy');
});

// AAMIN ROUTE
Route::group(['prefix' =>ROOT_DIR.'aamin', 'middleware' => 'auth', 'as' => 'aamin.'], function () {
    Route::get('/list', ['uses'=>'AaminController@index','as'=>'index'])->middleware('can:aamin');
    Route::get('/view_sandhir/{form_id}', ['uses'=>'AaminController@get_sandhir','as'=>'view_sandhir'])->middleware('can:aamin');
    Route::post('/view_sandhir', ['uses' => 'AaminController@store_aaminPratibedan' , 'as' => 'store_aaminPratibedan'])->middleware('can:aamin');
    Route::get('/view_aamin_pratibedan/{form_id}', ['uses' => 'AaminController@get_aaminPratibedan' , 'as' =>'view_aamin'])->middleware('can:aamin');

    //to display the rejected files.
    Route::get('/list_rejected', ['uses' => 'AaminController@get_rejected_file' , 'as' =>'list_rejected'])->middleware('can:aamin');

    //to edit the aamin_pratibedan form.
    Route::get('/edit_form/{form_id}', ['uses' => 'AaminController@editAaminPratibedan' , 'as' =>'edit_aaminPratibedan'])->middleware('can:aamin');

    //to store uploaded aamin_pratibedan form
    Route::post('/aamin_pratibedan_upload', ['uses' => 'AaminController@store_aaminPratibedanUpload' , 'as' => 'upload_aaminPratibedan'])->middleware('can:aamin');

    //to view both file after upload amin_pratibedan form.
    Route::get('/view_uploaded_aaminpratibedan/{form_id}', ['uses' => 'AaminController@getUploadedaaminpratibedan' , 'as' =>'view_uploaded_aaminpratibedan'])->middleware('can:aamin');
});

//ENGINEER ROUTE
Route::group(['prefix' =>ROOT_DIR.'engineer', 'middleware' => 'auth', 'as' => 'engineer.'], function () {
    Route::get('/list', ['uses'=>'EngineerController@index','as'=>'index'])->middleware('can:engineer');

    Route::get('/sandhir_suchana/{form_id}', ['uses'=>'EngineerController@sandhir_suchana','as'=>'sandhir_suchana'])->middleware('can:engineer');

    //route to view rajashow form
    Route::get('/view_rajashow/{form_id}', ['uses'=>'EngineerController@view_rajashow_form','as'=>'view_rajashow'])->middleware('can:engineer');
    Route::post('/reject_form', ['uses'=>'EngineerController@reject_form','as'=>'rejectForm'])->middleware('can:engineer');
    Route::post('/sandhir_upload', ['uses'=>'EngineerController@sandhir_upload','as'=>'sandhir_upload'])->middleware('can:engineer');
    Route::get('/view_sandhir/{form_id}', ['uses'=>'EngineerController@view_sandhir','as'=>'view_sandhir'])->middleware('can:engineer');

    //to find which route to choose consultancy form or aamin_pratibedan form.
    Route::get('/check_form/{form_id}', ['uses'=>'EngineerController@check_form_level','as'=>'check_form'])->middleware('can:engineer');
    Route::get('/tippani_adesh/{form_id}', ['uses'=>'EngineerController@tippani_adesh','as'=>'tippani_adesh'])->middleware('can:engineer');

    //route to store tippani and esthae scan
    Route::post('/tippani', ['uses' => 'EngineerController@store_tippani', 'as'=>'tippani_upload'])->middleware('can:engineer');
    Route::get('/upload/tippani_adesh/{form_id}', ['uses'=>'EngineerController@get_uploaded_tippani_adesh','as'=>'uploaded_tippani_adesh'])->middleware('can:engineer');
    //dpc
    Route::get('/get_print_dpc', ['uses'=>'EngineerController@get_print_dpc','as'=>'get_print_dpc'])->middleware('can:engineer');
    Route::post('/dpc_upload', ['uses' => 'EngineerController@store_dpc', 'as'=>'dpc_upload'])->middleware('can:engineer');
    Route::get('/dpc/uploaded_dpc/{form_id}', ['uses'=>'EngineerController@get_uploaded_dpc','as'=>'uploaded_dpc'])->middleware('can:engineer');

    //sampanna
    Route::get('/get_print_sampanna/{form_id}', ['uses'=>'EngineerController@get_print_sampanna','as'=>'get_print_sampanna'])->middleware('can:engineer');
    Route::post('/proceed_sampanna', ['uses' => 'EngineerController@proceed_sampanna', 'as'=>'proceed_sampanna'])->middleware('can:engineer');
    Route::get('/sampanna/display_file/{form_id}', ['uses'=>'EngineerController@get_uploaded_sampanna','as'=>'view_sampanna'])->middleware('can:engineer');
    Route::get('/skipdate/{form_id}', ['uses'=>'EngineerController@skipdate','as'=>'skipdate'])->middleware('can:engineer');
});

// MAIN ENGINEER ROUTE
Route::group(['prefix' =>ROOT_DIR.'mainengineer', 'middleware' => 'auth', 'as' => 'mainengineer.'], function () {
    Route::get('/list', ['uses'=>'MainengineerController@index','as'=>'index'])->middleware('can:main-engineer');
    Route::get('/dpc/{form_id}', ['uses'=>'MainengineerController@get_dpc','as'=>'get_dpc'])->middleware('can:main-engineer');
    Route::get('/dpc_scan/{form_id}', ['uses'=>'MainengineerController@get_dpc_scan','as'=>'get_dpc_scan'])->middleware('can:main-engineer');
    Route::get('/proceed/{form_id}', ['uses'=>'MainengineerController@proceed','as'=>'proceed'])->middleware('can:main-engineer');
    Route::get('/proceeddpc/{form_id}', ['uses'=>'MainengineerController@get_proceeddpc','as'=>'proceeddpc'])->middleware('can:main-engineer');
    //to view sampanna
    Route::get('/view_sampanna/{form_id}', ['uses'=>'MainengineerController@get_sampanna','as'=>'view_sampanna'])->middleware('can:main-engineer');
    Route::get('/proceed_sampanna/{form_id}', ['uses'=>'MainengineerController@proceed_sampanna','as'=>'proceed_sampanna'])->middleware('can:main-engineer');
});

// SUB ENGINEER ROUTE
Route::group(['prefix' =>ROOT_DIR.'subengineer', 'middleware' => 'auth', 'as' => 'subengineer.'], function () {
    Route::get('/list', ['uses'=>'SubengineerController@index','as'=>'index'])->middleware('can:sub-engineer');
    Route::get('/view_aamin_pratibaden/{form_id}', ['uses'=>'SubengineerController@get_aamin_pratibaden','as'=>'get_aamin_pratibaden'])->middleware('can:sub-engineer');
    Route::get('/rajashow/{form_id}', ['uses'=>'SubengineerController@get_rajashow','as'=>'rajashow'])->middleware('can:sub-engineer');
    Route::post('/rajashow/save', ['uses'=>'SubengineerController@post_rajashow','as'=>'rajashow.store'])->middleware('can:sub-engineer');
    Route::get('/rajashow/view/{form_id}', ['uses'=>'SubengineerController@display_rajashow','as'=>'rajashow.view'])->middleware('can:sub-engineer');
    Route::post('/rejectForm', ['uses'=>'SubengineerController@reject_form','as'=>'rejectForm'])->middleware('can:sub-engineer');
    //after rajashow print.
    Route::post('/rejashow/upload', ['uses'=>'SubengineerController@upload_rajashow_print','as'=>'rajashow.upload'])->middleware('can:sub-engineer');
    Route::get('/rajashow/displayrajashow/{form_id}',['uses' => 'SubengineerController@get_uploaded_rajashow', 'as' => 'displayrajashow'])->middleware('can:sub-engineer');

    //dpc route
    Route::get('/view_dpc/{form_id}', ['uses'=>'SubengineerController@get_dpc','as'=>'get_dpc'])->middleware('can:sub-engineer');
    Route::post('/post_proceeddpc', ['uses'=>'SubengineerController@post_proceeddpc','as'=>'post_proceeddpc'])->middleware('can:sub-engineer');
    Route::get('/view_proceeddpc/{form_id}',['uses' => 'Subengineercontroller@get_proceeddpc', 'as' => 'view_proceeddpc'])->middleware('can:sub-engineer');

    //route for reject dpc modal
    Route::post('/rejectdpc', ['uses'=>'SubengineerController@post_rejectdpc','as'=>'rejectdpc'])->middleware('can:sub-engineer');

    //route for upload for superstructure certificate
    Route::get('/view_upload_superstructure/{form_id}', ['uses'=>'SubengineerController@get_upload_superstructure','as'=>'view_upload_superstructure'])->middleware('can:sub-engineer');
    Route::post('/superstructure', ['uses'=>'SubengineerController@post_superstructure','as'=>'superstructure'])->middleware('can:sub-engineer');
    Route::get('/superstructure/view_uploaded/{form_id}', ['uses'=>'SubengineerController@get_uploaded_superstructure','as'=>'view_uploaded'])->middleware('can:sub-engineer');

    //route for viewing and rejecting  for sampanna_form
    Route::get('/view_sampanna/{form_id}', ['uses'=>'SubengineerController@get_sampanna','as'=>'view_sampanna'])->middleware('can:sub-engineer');
    Route::get('/proceed_sampanna/{form_id}', ['uses'=>'SubengineerController@get_proceedSampanna','as'=>'proceed_sampanna'])->middleware('can:sub-engineer');
    Route::post('/rejectsampanna', ['uses'=>'SubengineerController@post_rejectsampanna','as'=>'rejectsampanna'])->middleware('can:sub-engineer');
    Route::get('/view_rejectsampanna/{form_id}', ['uses'=>'SubengineerController@get_rejectsampanna','as'=>'view_rejectsampanna'])->middleware('can:sub-engineer');
    Route::get('/upload_sampanna/{form_id}', ['uses'=>'SubengineerController@get_upload_sampanna','as'=>'get_upload_sampanna'])->middleware('can:sub-engineer');
    Route::post('/upload_sampanna_file', ['uses'=>'SubengineerController@post_upload_sampanna','as'=>'post_upload_sampanna'])->middleware('can:sub-engineer');
    Route::get('/view_sampanna_file/{form_id}', ['uses'=>'SubengineerController@view_sampanna_file','as'=>'view_sampanna_file']);
});

// ADMINISTRATOR ROUTE
Route::group(['prefix' =>ROOT_DIR.'administration', 'middleware' => ['auth','can:administration'], 'as' => 'administration.'], function () {
    Route::get('/get_files', ['uses'=>'AdministrationController@get_files','as'=>'get_files']);
    Route::get('/get_edit/{form_id}', ['uses'=>'AdministrationController@get_edit','as'=>'get_edit']);

    //to edit form
    Route::get('/edit_form/{form_id}', ['uses'=>'ConsultancyController@get_edit_form','as'=>'edit_form_administration']);
    Route::post('/edit_form', ['uses'=>'ConsultancyController@post_edit_form','as'=>'post_edit_form']);

    //edit rajashow
    Route::get('/rajashow/{form_id}', ['uses'=>'SubengineerController@get_rajashow','as'=>'rajashow'])->middleware('can:administration');
    Route::post('/rajashow/save', ['uses'=>'SubengineerController@post_rajashow','as'=>'rajashow.store'])->middleware('can:administration');

    //edit dpc
    Route::get('/edit_dpcform/{form_id}', ['uses'=>'ConsultancyController@get_edit_dpcform','as'=>'edit_dpcform'])->middleware('can:administration');
    Route::post('/dpc', ['uses'=>'ConsultancyController@post_dpc_form','as'=>'post_dpc_form'])->middleware('can:administration');

    //edit aamin pratibedan
    Route::get('/edit_aamin_form/{form_id}', ['uses' => 'AaminController@editAaminPratibedan' , 'as' =>'edit_aaminPratibedan'])->middleware('can:administration');
    Route::post('/view_sandhir', ['uses' => 'AaminController@store_aaminPratibedan' , 'as' => 'store_aaminPratibedan'])->middleware('can:administration');
});
